<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Archived Forum Discussion</title>
<link rel="stylesheet" href="css/transfans.css" type="text/css">
<link rel="stylesheet" href="relics/styles.css" type="text/css">
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr>
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt">

<h1>War with Iraq has begun.</h1>
<h2>Thread originally posted by LeAtHeRnEcK</h2>


<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-19-2003 at 09:57 PM:</div>
<div class="relics_body"> <a href="http://www.washingtonpost.com/wp-dyn/articles/A56580-2003Mar19.html" target="_blank">http://www.washingtonpost.com/wp-dyn/articles/A56580-2003Mar19.html</a> <br /> <a href="http://www.washingtonpost.com/wp-dyn/articles/A56580-2003Mar19.html" target="_blank">http://www.washingtonpost.com/wp-dyn/articles/A56580-2003Mar19.html</a> <br /><br />in short, air raid sirens heard in baghdad, along with massive explosions and sounds of aircrafts. ATM it seems like we've hit them with heavy bombers and cruise missiles. Bush is on at 1015 EST.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-19-2003 at 10:01 PM:</div>
<div class="relics_body">It begins...<br /><br />*I'm allowing this topic to be open simply because these are extraordinary times. Keep debate about the morality of the war etc to the one and only Iraq thread. This thread exists for war updates and news, not commentary. Understood? Posts deemed as commentary WILL be deleted as judged by the mod/admins*</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Pretender Bumblebee</span> on 03-19-2003 at 10:04 PM:</div>
<div class="relics_body">Agreed. Keep this to updates on the war and attacks and what you hear . Keep personal feelings on morality and pro/anti war sentiment to the official thread for it. <br /><br />Posts will be deleted if they go over the line as we made a thread just for the purpose of discusing feelings on things. <br /><br />Anyway Im watching Fox News right now and they are saying that it was a pre surgical strike where they know that the leadership will feel its effects. <br /><br />Bush will be making a formal speach at 10:15 Eastern time. <br /><br />I am quite curious to see what he has to say here . I didn't think he would start it on the first day in all honesty. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">IronHide</span> on 03-19-2003 at 10:15 PM:</div>
<div class="relics_body">Apparently the strike caught a lot of people of guard. Apparently a 'Target of Opportunity' appeared and thus the strike was ordered....<br /><br />Hmmmm.....wonder what it was...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-19-2003 at 10:24 PM:</div>
<div class="relics_body">  <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by IronHide:<br /><strong><br />Hmmmm.....wonder what it was...</strong><hr /></blockquote>Saddam's beer fridge...<br /><br />You know the time from 8:00 till when the strike hit must have been the longest hour and a half in my recent memory...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">IronHide</span> on 03-19-2003 at 10:30 PM:</div>
<div class="relics_body">40 Tomahawk Missles were sent at those 'Targets'.<br /><br />Wish we knew what they were....<br /><br />However, the speculations that reporters are making are keeping me entertained.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-19-2003 at 10:32 PM:</div>
<div class="relics_body">10:38pm<br /><br />Did I miss the speach?  Not on the radio.  CNN doesn't have a transcript.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Pretender Bumblebee</span> on 03-19-2003 at 10:33 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by shaxper:<br /><strong>10:38pm<br /><br />Did I miss the speach?  Not on the radio.  CNN doesn't have a transcript.<br /><br /></strong><hr /></blockquote>yes it was a very short speech. It should be on the web sites soon. It was your typical "The war has begun. We will accept nothing but the goals we set out to do for victory" type of speech. Very short and to the point . Im sure it will be up somewhere shortly. Fox and CNN have little phrases from what he said showing up on the ticker at the bottom if you are by a tv. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Redstreak</span> on 03-19-2003 at 10:34 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong>  Saddam's beer fridge...</strong><hr /></blockquote>You know it...<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />You know the time from 8:00 till when the strike hit must have been the longest hour and a half in my recent memory...<br /><hr /></blockquote>I can think of longer...10 am-11:30 am on 9/11...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-19-2003 at 10:37 PM:</div>
<div class="relics_body">According to radio news, the official war hasn't been declared yet.  This "target of opportunity" was an early strike, but not the begining of the larger offensive.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-19-2003 at 10:40 PM:</div>
<div class="relics_body">I've been keeping track of things on cbc.ca and as far as I know things are being kept fairly vague. They listed some out lying towns and the oil feilds as the inital objectives. Refugee camps are set up and ready and the pamphlet campaign has been reported as well. I hope the soldiers in Iraq take the advice and give up their weapons and leave their vehicles. It also looks like they're going to stay the heck out of Bahgdad and settle for a seige to lessen civilian casualties. Of course that's in the future. Here's to low casualties and surreneder.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-19-2003 at 10:46 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Redstreak:<br /><strong> I can think of longer...10 am-11:30 am on 9/11...<br /><br /></strong><hr /></blockquote>Indeed, good point...I still remember quite vividly running back to my dorm room after my prof cancelled class (With an extremely vivid memory of his telling us what had just happened), and bursting in to my room to see my roommate watching CNN and an image of the towers...<br /><br />But this hour and a half definitely is up there in terms of endless anxiety...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-19-2003 at 10:49 PM:</div>
<div class="relics_body">We're living in a fascinating generation.  Screw "where were you when Kennedy was shot?".  Our grandkids are gonna be so sick of our stories...<br /><br />(assuming, of course, we all live that long)</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-19-2003 at 11:02 PM:</div>
<div class="relics_body">Compy I know what you mean about 9/11. I still remember the exact words my video production teacher said to me. At first I thought he was joking/something serious didn't occur... like some single seat Cessna hit one of the towers. Because all he said to me was "A plane has hit the WTC". I guess it was before anybody knew what had happened, because it was only a few minutes after it happened. I skipped most of my classes that day and stayed in the video room watching footage... man. I watched as both towers collapsed. I still can't watch it again; like if there's some special about it on tv i can't handle watching it, i get teary eyed and stuff. And a day hasn't gone by where I haven't thought about it.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-19-2003 at 11:09 PM:</div>
<div class="relics_body">I was there for the whole thing.  I heard at work that a plane had hit the WTC.  I thought it was odd, hoped it wasn't too bad, and left it at that, thinking "what were the chances?".  Then someone came up and said "A plane hit the second tower (or whichever tower it was).  I was like "yeah, I heard earlier" and then she said "no.  Another one" and then I knew things were never going to be the same.  I'll never forget working the library desk, having the radio on, reporting on 5 planes still in the air (a lot of false reports that day) and people coming up to me bitching about authors, totally ignoring what was happening.  I've never worked that hard to smile in my life  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-19-2003 at 11:13 PM:</div>
<div class="relics_body">I was in Calculus class when my prof mentioned it. He didn't say anything about terrorists just a huge disaster and planes hitting the WTC. I initially thought it was some kind of sick joke maybe or some kind of flight accident, until I went back to residence during my break and walked into the common room to see the frist and the second planes hit on the TV as I entered and thw word "terrorists". Man I thought it was all over then.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-19-2003 at 11:20 PM:</div>
<div class="relics_body">well its being said that two f-117a stealth fighters, (the lil ones) struck at a a target or oportunity.<br /><br />So its belived this is not the start, and somthing of 'political' interest was seen and destroyed.<br /><br />Cruise missles were fired from US ships aswell.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-19-2003 at 11:23 PM:</div>
<div class="relics_body">Latest from the CBC<br /><br />Iraq spits defiance at U.S. <br />Last Updated Wed, 19 Mar 2003 23:18:32 <br />BAGHDAD - A radio station owned by Saddam Hussein's son Uday has called on Iraqis to "rise up like lions" and defend their country. <br /><br />"Saddam is our beloved leader," said an announcer, "and God is our great Allah. Rise up, rise up and attack the foreigners. Victory with God's help is near. Defeat the infidel enemy." <br /><br /> <br />The defiant statement came as the Iraqi capital, turned into a ghost town by the threat of a U.S. attack, was wakening to the sound of anti-aircraft fire. <br /><br />"Our brother martyrs will die defending our great Iraq. Today is our day. Rise up our brothers for the jihad. Hit the enemies and prove that we are sons of freedom and honour." <br /><br />Dawn in Baghdad was accompanied by loud bursts of automatic fire, apparently aimed at coalition jets on a mission, possibly targeting the Iraqi leadership. <br /><br />Some reports said the initial strikes were with cruise missiles and precision-guided bombs against a site near Baghdad, where Iraqi leaders were thought to be. There is no indication whether the attack was successful. <br /><br />But another move appears to have been successful. The U.S. military may have taken over the main frequency of Iraqi state radio. <br /><br />The radio said Saddam's administration was under attack, and that "this is the day we have been waiting for." <br /><br />A British military spokesman says orders to launch a ground attack have not yet been issued. And reporters in the Kuwait-Iraq border area, where thousands of troops are massed, say everything was quiet on Thursday morning. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-19-2003 at 11:24 PM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/870749.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/870749.asp?0cv=CA01</a> <br /><br />Apparently Saddam himself was the target.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-19-2003 at 11:29 PM:</div>
<div class="relics_body">The news just reported that we just sent ground troops into Afghanistan in search of Al Quiada operatives.  Do you think maybe this "target of opportunity" was just a diversion?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-19-2003 at 11:31 PM:</div>
<div class="relics_body">It seems it was a quick attack, no one seemed to know much about it down at the HQ.<br /><br />Fighters are taking off now.<br />------edit.<br />Just seen pictures of a f-117a!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">IronHide</span> on 03-19-2003 at 11:38 PM:</div>
<div class="relics_body">Good ole Lockheed-Martin.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-20-2003 at 12:05 AM:</div>
<div class="relics_body">More tomahawks were fired about 10 minutes ago.  The target was, indeed, Hussain, but he's supposed to be making a televised announcement right now, so it's either prerecorded or they failed.<br /><br />In addition, it's over 10,000 troops in Afghanistan right now.  Largest invasion in over a year.<br /><br />Finally, just now, unidentified forces are fighting their way through the border from Kuwait to Iraq.  <br /><br />A whole LOT is happening right now.  Not the minor offensive we thought.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-20-2003 at 12:08 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by shaxper:<br /><strong>More tomahawks were fired about 10 minutes ago.  The target was, indeed, Hussain, but he's supposed to be making a televised announcement right now, so it's either prerecorded or they failed.<br /><br />In addition, it's over 10,000 troops in Afghanistan right now.  Largest invasion in over a year.<br /><br />Finally, just now, unidentified forces are fighting their way through the border from Kuwait to Iraq.  <br /><br />A whole LOT is happening right now.  Not the minor offensive we thought.<br /><br /></strong><hr /></blockquote>Links?<br /><br />Just what the heck is happening in the world? Good heavens...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">IronHide</span> on 03-20-2003 at 12:13 AM:</div>
<div class="relics_body">Crazy weird shite. <br /><br />It's a mad mad world.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Pretender Bumblebee</span> on 03-20-2003 at 12:15 AM:</div>
<div class="relics_body">I just heard on Fox News that at least 1000 troops in east and west afghanistan were sent in righ tnow to get bin Laden.  They just showed pictures and it was confirmed. A surprise attack to get Al Quieda as the air strikes in Iraq go on. <br /><br />This is crazy stuff!!!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 12:19 AM:</div>
<div class="relics_body">im watching BBC news 24, and they havent metioned anything about afghanistan. still if they have found bin laden then go flush the rat out!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 12:33 AM:</div>
<div class="relics_body">i think Saddams speach is crappy. i would have thought if this wasnt a recording he would have said that us attack on him had failed?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">shaxper</span> on 03-20-2003 at 12:50 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong> Links?<br /></strong><hr /></blockquote>ABC news radio.  CNN and MSNBC's sites are too slow on the breaking news.<br /><br />Incidentally, I've heard nothing to confirm they found or are even looking for Bin Laden in Afghanistan.  The operation is official, but all that's been officially said is they're looking for Al Quaida operatives.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Pretender Bumblebee</span> on 03-20-2003 at 12:59 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by shaxper:<br /><strong> <br />ABC news radio.  CNN and MSNBC's sites are too slow on the breaking news.<br /><br />Incidentally, I've heard nothing to confirm they found or are even looking for Bin Laden in Afghanistan.  The operation is official, but all that's been officially said is they're looking for Al Quaida operatives.<br /><br /></strong><hr /></blockquote>Nope they haven't found Bin Laden but have some key villages that have Taliban and Al Quaida links that they coudln't pass up on so they moved in. So far they took in 2 that had weapons and one that was saying that "If we attack Iraq, they will attack our soldiers " amazing how brave they get once they are caught and come out of hiding  <img src="relics/rolleyes.gif"> </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 11:11 AM:</div>
<div class="relics_body">Airstrikes have begun.<br /><br />reports are comming in that some oil wells have been set alight.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-20-2003 at 11:17 AM:</div>
<div class="relics_body">Yup, CBC's reporting that too. Looks like they're getting ready for the big push.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 11:31 AM:</div>
<div class="relics_body">its all going off now, troops, planes, cruise missles.<br /><br />I hear that the B2 bombers based in the UK at fairford airstrip are preparing to leave now.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-20-2003 at 11:38 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>its all going off now, troops, planes, cruise missles.<br /><br />I hear that the B2 bombers based in the UK at fairford airstrip are preparing to leave now.</strong><hr /></blockquote>I thought all B-2's launched from Missouri, only...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 11:47 AM:</div>
<div class="relics_body">TYPO!. B-52  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 12:04 PM:</div>
<div class="relics_body">US troops from the 3rd battalion have engaged Iraq forces on the ground.<br /><br /><I>God speed</I></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">god</span> on 03-20-2003 at 12:34 PM:</div>
<div class="relics_body">I exspect B2's to leave whiteman soon</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-20-2003 at 12:44 PM:</div>
<div class="relics_body">Latest from CBC<br /><br />U.S. artillery fires into Iraq <br />Last Updated Thu, 20 Mar 2003 12:42:14 <br />KUWAIT CITY - U.S. forces in Kuwait began a heavy artillery barrage across the Iraqi border on Thursday evening, after oil wells began to burn in southern Iraq. <br /><br />In what could be the first stage of a ground assault on Iraq, the U.S. 3rd Infantry Division's artillery fired on Iraqi troops with Paladin self-propelled howitzers and multiple launch rocket systems. <br />Maj.-Gen. Bufourd Blount, the division commander, had said an artillery barrage along the entire Kuwait-Iraq border would signal the first phase of the ground war against Iraq. <br /><br />Earlier in the evening, witnesses reported seeing orange flames on the horizon in the direction of Iraq's oil centre near Basra. A U.S. marine commander said three wells were burning. <br /><br />"The Iraqi regime may have set fire to as many as three or four oil wells to the south," said U.S. Defense Secretary Donald Rumsfeld in Washington. <br /><br />Rumsfeld called setting fire to the wells "a crime." <br /><br />Witnesses inside Kuwait said they spotted the flames after a series of explosions shook the area. <br /><br />U.S. military officials have said they suspected Iraq would set fire to oil wells in an attempt to slow down the advance of invading forces. <br /><br />At the same time, reports from the Iraq-Kuwait border area suggested U.S. and British forces were getting ready for an attack, having been told to put on their combat gear. <br /><br />There are tens of thousands of troops near the border. <br /><br />When the full attack comes, it will be like no war ever, said Rumsfeld. <br /><br />"It will be of a force, scope and scale beyond what has been seen before," he said. <br /><br />Rumsfeld said the strike against Baghdad earlier in the day was aimed at a "senior Iraqi leadership compound." <br /><br />At dawn, U.S. forces fired Tomahawk missiles and dropped bombs on Baghdad hoping to kill Saddam Hussein and other Iraqi leaders. <br /><br />Military officials are assessing the damage, Rumsfeld said. <br /><br />The Red Cross said one person was killed and 14 injured in the attack. <br /><br />Air raid sirens sounded repeatedly in Kuwait City during the day as Iraq responded with missiles aimed across the border. <br /><br />Impy where are you getting your news from? Seems a bit quicker than mine?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Ultrasabre</span> on 03-20-2003 at 12:55 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by shaxper:<br /><strong>According to radio news, the official war hasn't been declared yet.  This "target of opportunity" was an early strike, but not the begining of the larger offensive.<br /><br /></strong><hr /></blockquote>Taking out power, radar and other technology that may post a threat to the main attack force. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-20-2003 at 01:11 PM:</div>
<div class="relics_body">Shock and Awe has just begun. Baghdad using anti-air with low flying planes dropping bombs overhead. I saw it live along with the explosions resulting from the weapons.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 02:07 PM:</div>
<div class="relics_body">Im getting my news from BBC news 24. its very good.<br /><br />U should watch channel 4. its so biest against this war, all very negative in response.<br /><br />im sorry, i dont want an opinion pushed down my throat, i just want to see the news.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">HoistKeeper 2.0</span> on 03-20-2003 at 02:56 PM:</div>
<div class="relics_body">*EDIT: narration not fact, er oops.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-20-2003 at 03:38 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong><br /><br />U should watch channel 4. its so biest against this war, all very negative in response.<br /><br />im sorry, i dont want an opinion pushed down my throat, i just want to see the news.</strong><hr /></blockquote>keep it in the other topic.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-20-2003 at 05:31 PM:</div>
<div class="relics_body">The homes of most of the top Iraqi's have been blown up, hope their was no families or servents in them.<br /><br />The Royal Marine Commando Brigade and the Queens Royal Dragoon Guards(Yorkshire lads) has captured an Iraqi port in an amphibious operation and is starting to secure the Al Faw peninsula they are on.<br /><br />Harriers have launched air strikes and according to the Pentagon the Cruise missiles were launched from HMS Splendid and HMS Turbulent.<br /><br />US 3rd Infantry Divison has crossed the border and has started advancing.<br /><br />Question time on in 10 minuites all UK folk should watch it</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 07:06 PM:</div>
<div class="relics_body">Military press offices in Kuwait belive a chemical or biological attack is imminent. 00:12 GMT.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-20-2003 at 07:13 PM:</div>
<div class="relics_body">That would be... bad. Because as far as I know, our policy is to strike back as we were struck; i.e. if we are hit with WOMD, we attack back with WOMD. And we only have one kind of those :-/<br /><br />I'm not sure if that would apply in this situation though.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-20-2003 at 07:50 PM:</div>
<div class="relics_body">As I understand it Bush has already given authority for the use of tactical nuclear weapons under special circumstances and the US forces have already planned to use several banned chemical anti-personal gas weapons in Baghdad(usually not fatal) if they use the gases in question then they are on their own as British troops cannot legally by our own military laws support them and must withdraw.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 08:01 PM:</div>
<div class="relics_body">I dont see any need for the use of them to be honest.<br /><br />The most i think u will see in gas form would be CS gas used by all the worlds special forces.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 09:20 PM:</div>
<div class="relics_body">It is now confirmed that saddams leading men, and republican gaurd are now in talks with the US and CIA over surrender. <br />02:25 GMT</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 09:39 PM:</div>
<div class="relics_body">A US Marine helicopter (ch-46) has crashed on the kuwait side of the border. 12 US personal, 4 UK comandos belived dead on board.<br /><br />Reports point to mech failure.<br /><br />Both the US and the UK MoD confirm.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">IronHide</span> on 03-20-2003 at 09:43 PM:</div>
<div class="relics_body">It's been confirmed that a mixed unit of 4 British Royal Guardsmen and 12 US Marines have died.<br /><br />Their helicopter crashed somewhere over Kuwait, there were no survivors. No reports as to how the bird went down, but it's been ruled as an accident.<br /><br />Damn that's F***ING BS. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Aaron Hong</span> on 03-20-2003 at 10:52 PM:</div>
<div class="relics_body">This just in from Channel News Asia: <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Saddam and his two sons may have been in the palace compound when it was hit.<br /><br />An expert in identifying Saddam and his doubles says that the Saddam in the video message may have been a double.<hr /></blockquote>Anyone else heard this yet?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-20-2003 at 10:56 PM:</div>
<div class="relics_body">Yep. I don't remember Saddam being fat and with glasses...<br /><br />Unless all we've been seeing for the last 10 yrs has been doubles, which I suppose is possible.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-20-2003 at 11:02 PM:</div>
<div class="relics_body">Its noe been confirmed that the chopper went down 9 miles south of the kuwait border.<br /><br />Its now known that its was 8 uk marines, and 4 US marines.<br /><br />My thoughts go out to thier familys  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-21-2003 at 11:20 AM:</div>
<div class="relics_body">All indications so far point to quick victory in southern Iraq. Basra is or soon will be under control, Iraq's port is under control, and the 3rd infantry division is pushing straight towards Baghdad. So far casualties have been extremely low, only 1 combat death which for war, is amazing. We'll see how things go once they reach Baghdad. If the Republican guard surrenders, which I think they will, Iraq will be in total control, virtually undamaged in a few days. If they decide to fight urban style it could get a bit messy.<br /><br />The war part isn't what bothers me tho, it's the aftermath.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-21-2003 at 01:20 PM:</div>
<div class="relics_body">Presently Baghdad is under massive bombardment.<br />the pictures on TV are quite unreal.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">god</span> on 03-21-2003 at 01:24 PM:</div>
<div class="relics_body">well they gave it 3 days to do it the smart way, now it will be the hard way.......to bad, this will mean more casualties</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 03-22-2003 at 06:46 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong>If the Republican guard surrenders, which I think they will,</strong><hr /></blockquote>Very unlikely, the Rupublican guard are hated by the Iraqi's and won't surrender, if the are taken alive they will be killed in the aftermath.  They owe their lives to sadam because if anyone goes against them they are killed so without Sadam members of the RG will be killed mob style.<br /><br />Also I think things are going to get a damm site more difficult now, that port will be held for a while (perhaps till the end of the day).  They arn't bothering with Basra because it is a death trap for a non-important city.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">god</span> on 03-22-2003 at 07:20 AM:</div>
<div class="relics_body">this invasion is going to slow, they should have basra by now and moving over the tygris</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 03-22-2003 at 10:24 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Getaway:<br /><strong> the Queens Royal Dragoon Guards(Yorkshire lads) </strong><hr /></blockquote>The QDG's (Queens Dragoon Guards) are Welsh - Just being picky I know  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-22-2003 at 06:03 PM:</div>
<div class="relics_body">But are based and recruited largely like most of the army from Yorkshire, around 25% of all our armed forces comes this area.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 03-22-2003 at 07:47 PM:</div>
<div class="relics_body">They are based in Cardiff in Maindy Barracks.  Whilst they may get posted up there for some reason (that was news to me) the home of the 1st the Queens Dragoon Guards is Cardiff.<br /><br />They are even called the Welsh Cavaliers - I only know cause its what all my family have/ do serve in  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-23-2003 at 02:41 PM:</div>
<div class="relics_body">Not a good day from the coalition point of view;<br /><br />One UK Tornadao shot down by a US patriot.<br /><br />Several US soldiers killed and several others captured.<br /> <a href="http://news.bbc.co.uk/2/hi/middle_east/2878495.stm" target="_blank">http://news.bbc.co.uk/2/hi/middle_east/2878495.stm</a> </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-23-2003 at 02:48 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Best First:<br /><strong>Not a good day from the coalition point of view;</strong><br /><hr /></blockquote>True, but from a pure military perspective things are still going exceedindly well. <br /><br />Tho that still doesn't minimize the sheer shock of losing troops due to being captured, shot or friendly fire.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-23-2003 at 03:33 PM:</div>
<div class="relics_body">Yes.<br />If u were to look at this all purely tacticly we are doing very well.<br /><br />This is war, things do go wrong. the accidents can and should have been avoided but systems like these can go wrong.<br /><br />in terms of tactics tho, we are within 100 miles of bagdad. resistence, well they say heavy but im sorry i dont belive that for an instance. when u can call on B-52 bombers with MOAB thet talk crap. i think the point being is that they are actually trying to take POW where possible, not just blow everything to hell.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 03-23-2003 at 03:37 PM:</div>
<div class="relics_body">I'm sure you've all heard by now but this is very sad news:<br /><br />LONDON, England (CNN) -- British television reporter Terry Lloyd is dead and his body believed to be in a Basra hospital, under Iraqi control, broadcaster ITN said Sunday. <br /><br />Two members of his team, Belgian cameraman Fred Nerac and Hussein Osman, a translator from Lebanon, remain missing, ITN said in a statement. A fourth crew member, Daniel Demoustier, was injured in Saturday's incident at Iman Anas, near Basrabut, but was able to get to safety. <br /><br />London-based ITN said Lloyd, 51, and his team apparently were fired on by forces from the U.S.-led coalition while driving toward coalition lines, accompanied by vehicles driven by Iraqis, including a truck filled with soldiers. ITN said the Iraqis might have been intending to surrender. <br /><br />The statement said: "ITN has received sufficient evidence to believe that ITV news correspondent Terry Lloyd was killed in an incident on the southern Iraq war front yesterday." <br /><br />It added: "The ITN team came under fire, apparently from coalition forces, outside Basra. Iraqi ambulances took a number of dead and injured from the area into Basra and locally-based journalists have given ITN information which leaves no doubt that Terry Lloyd's body was among the dead." <br /><br />ITN, which provides the news for Britain's ITV network, said it believed the other missing members of the crew had also been taken to Basra and efforts were continuing to locate them. <br /><br />Lloyd had been with ITN for 20 years and was ITV's longest serving reporter. He is the first journalist killed on assignment in ITN's 48-year history, the UK Press Association said. <br /><br />He was the first reporter inside the Iraqi town of Halabjah in 1988 after Iraqi President Saddam Hussein attacked Kurds there with chemical weapons. He had also reported from Kosovo, Bosnia and Yugoslavia, <br /><br />ITN Chief Executive Stewart Purvis said: "Terry was brave, he was determined and he was safety conscious. He was a lovely guy." </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-23-2003 at 04:19 PM:</div>
<div class="relics_body">I'm sorry but this drives me mad, so far, we've had 8 Royal Marines killed by either US pilot or mechanic error, 1 Tornado shot down by US missiles, 1 Reporter shot dead by from what the latest report is US Marines, we havn't lost anyone to the Iraqi's yet, but we've already reached double figures from the Yanks, I had very little confidence that we would manage to avoid being hit by them this war, most people on various news groups before the confict even begun were giving figures of around half a dozen freindly fire incidents, we've had three so far.<br /><br />The americans are brave, well equipped and well meaning, but they are more dangerous to us than the Iraqi's, they fired a missile at Iraq that managed to miss the whole country, how can you miss a country?  There own troops have already turned upon each other, apparantly one US lad threw 3 grenades into a generals tent with around 13 casulties caused.  They need training in self control.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Tenoh</span> on 03-23-2003 at 06:33 PM:</div>
<div class="relics_body">I hope americans loose as many ppl as posible!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-23-2003 at 06:39 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Tenoh:<br /><strong>I hope americans loose as many ppl as posible!</strong><hr /></blockquote>Guess what! One more remark like that and you're banned!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-23-2003 at 06:48 PM:</div>
<div class="relics_body">Tenoh is.....odd</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 07:38 PM:</div>
<div class="relics_body">  <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Getaway:<br /><strong>Tenoh is.....odd</strong><hr /></blockquote>Tenoh is a c***, and I'd like to reiterate what I said in repsonse to similar trolling at the Archive, and wish him/her a slow, lingering death from cancer.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-23-2003 at 07:42 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Getaway:<br /><strong>Tenoh is.....odd</strong><hr /></blockquote>You can add banned to that list. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 07:53 PM:</div>
<div class="relics_body">Um, if the reporter was in front of coalition (notice it says coalition, not American) weapons, he wouldn't have got shot... he was in the wrong place.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-23-2003 at 07:54 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by LeAtHeRnEcK:<br /><strong>Maybe if the brits weren't in the way of American bullets...</strong><hr /></blockquote>What kinda crap is that? How exactly is a RAF tornado supposed to avoid a Patriot?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 07:58 PM:</div>
<div class="relics_body">That is one of <strong>the</strong> most offensive things I've read, you bloody ignorant fool. You're even cowardly enough to go back and edit it out. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:01 PM:</div>
<div class="relics_body">yes compy i kinda realized that... note the change.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-23-2003 at 08:01 PM:</div>
<div class="relics_body">ugh! u know how angry that makes me.<br /><br />i just sit here and except that it was an accident, these things happen.<br /><br />But, but....nah i cant actually be bothered.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:03 PM:</div>
<div class="relics_body">cj, you dont seem to realize that a FF incident isn't totally the fault of a the firer. If the person on the open end of the barrel fails to communicate, then they're going to get shot... I'm sorry if i have to put it bluntly for you to realize.<br /><br />To see what I mean, go play some FPS game with FF. The only fault in that is in these games usually the character models for the teams is very different. Camo looks the same no matter who is wearing it.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:04 PM:</div>
<div class="relics_body">Imagine us colonist scum having the nerve to get in the way of AMERICAN BULLETS OF JUSTICE AND FREEDOM...<br /><br />My biggest problem with the terry Lloyd thing is that the coaition forces could see the following trucks well enough to work out they had Iraqi troops onboard, and yet missed the whacking great big "TV" daubing on all faces of the journos' SUVs.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-23-2003 at 08:04 PM:</div>
<div class="relics_body">latest breaking news.<br />reports are coming in.<br />A huge cache of chemical weapons has been discovered!<br />A huge 100 acre complex! not a small plant, a huge plant!<br /><br />this could have huge ramifications on the 'told u so' theroy.<br /><br />Not confirmed yet. still waiting.<br /><br />on a sadder note, it seems that US soilders have been executed and the corpses mutalated.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"><br />sick fecks.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Pretender Bumblebee</span> on 03-23-2003 at 08:05 PM:</div>
<div class="relics_body">Im sorry but this stuff here is really making me sick to my stomach. We are talking about lives that were lost and we have some  saying you wish death on Americans, British and Iraqis and i don't find it funny or cool to be saying such things. <br /><br />This is real life not a video game. People are really dying there on both sides and I don't think the familes of the British soldiers who lost their lives are finding the humor int his at all. War is horrible and you can have your view pro or against it but you can't make life like it didn't matter, at least not on this board as we have people all over the world on here and everyones feelings should matter. If people continue with the wishing of death of people and praising it there will be bannings. take it to a political board if you can't handle the rules of this board in an adult manner.  </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:07 PM:</div>
<div class="relics_body">Just to calrify, PB, where exactly was that aimed?<br /><br />And IR, that's terrible  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:07 PM:</div>
<div class="relics_body">PB, not to undermine what you're saying or anything...<br /><br />But I find it funny that you're trying to put forward an adult additude (sp) in a board focused on children's toys  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:08 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Cliffjumper:<br /><strong>Imagine us colonist scum having the nerve to get in the way of AMERICAN BULLETS OF JUSTICE AND FREEDOM...<br /><br />My biggest problem with the terry Lloyd thing is that the coaition forces could see the following trucks well enough to work out they had Iraqi troops onboard, and yet missed the whacking great big "TV" daubing on all faces of the journos' SUVs.</strong><hr /></blockquote>Iraq doesn't have trucks? And media?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:09 PM:</div>
<div class="relics_body">I seriously doubt they have them leading army columns.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:11 PM:</div>
<div class="relics_body">They put military bases between schools and hospitals. And you think having a TV truck in a combat area is something they wouldn't do?<br /><br />Once again, go ahead and blame us trigger happy Americans, we're happy to kill anything, no matter what or who it is. God forbid someone takes a little responsibilty for something that happens to them...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Pretender Bumblebee</span> on 03-23-2003 at 08:12 PM:</div>
<div class="relics_body">  <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by LeAtHeRnEcK:<br /><strong>PB, not to undermine what you're saying or anything...<br /><br />But I find it funny that you're trying to put forward an adult additude (sp) in a board focused on children's toys    <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></strong><hr /></blockquote>yes ironic I know but in GD its anything but TFs here. <br /><br />But that was adressed to Tenoh and  Leatherneck. <br /><br />A comment like they shoudln't have gotten in the way of our missiles is just silly and heartless. They are real people that died. have some dignity and respect man.  They are helping our country, Horrible mistakes happen. Don't make it like it was one sides fault over the other. it happend. <br /><br />But back to what I was saying as Im not in a debating mood,  being that this is a board for all ages .. just remember there are rules here and wishing death to groups is certainly a violation of them. Always has been always will be no matter who it is.  You can say your views just do it in the right way and there won't be probs. Thanks. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:13 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Pretender Bumblebee:<br /><strong> <br />Don't make it like it was one sides fault over the other. </strong><hr /></blockquote>i guess cj didnt get that message...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:15 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by LeAtHeRnEcK:<br /><strong>Once again, go ahead and blame us trigger happy Americans, we're happy to kill anything, no matter what or who it is. God forbid someone takes a little responsibilty for something that happens to them...<br /></strong><hr /></blockquote>Sorry, jackass, but when exactly did I say I blame the Americans? I didn't. I objected to you saying that people who get shot shouldn't get in the way of bullets, a callous comment when you consider there are friends and families grieving out there. You were the one who started splitting "Coalition" down into "Brits" and "Americans" with that crack about the jet shot down.<br /><br />And PB, cheers  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"> FYI, Tenoh is a poster @ Seibertron whor egularly trolls at the Archive with that sort of juvenile comment, examples being "I hope Saddam wins" and "come on you lot start fighting". I doub anyone who's read the screen-name before takes what he says seriously.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:17 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Imagine us colonist scum having the nerve to get in the way of AMERICAN BULLETS OF JUSTICE AND FREEDOM...<hr /></blockquote>I guess that isn't blame?<br /><br />And I don't need another person here mad at me. Truce? Can I apologize for an out-of-line comment?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-23-2003 at 08:19 PM:</div>
<div class="relics_body">B-52's are taking off from fairford again.<br /><br />U can actually see them loading JDAMS, looks like 2000 pounders</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:19 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by LeAtHeRnEcK:<br /><strong> I guess that isn't blame?<br /><br />And I don't need another person here mad at me. Truce? Can I apologize for an out-of-line comment?</strong><hr /></blockquote>No, that's a straight parry of<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><strong>Originally posted by LeAtHeRnEcK:</strong><br />Maybe if the brits weren't in the way of American bullets...<hr /></blockquote>also known as taking the piss out of you.<br /><br />Truce? You should have thought of that before posting without engaging brain, frankly.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:22 PM:</div>
<div class="relics_body">I never placed blame. You did. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:23 PM:</div>
<div class="relics_body">Where, pray tell?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:25 PM:</div>
<div class="relics_body">I'd rather not copy and paste the same thing again. Despite what you may or may not have meant, that placed the blame on an American weapon.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-23-2003 at 08:27 PM:</div>
<div class="relics_body">What are you, an idiot? Wait, don't answer it...<br /><br />It's called satire, you damn fool.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-23-2003 at 08:29 PM:</div>
<div class="relics_body">Wow. The moment I flame someone the mods are all over it. And yet he is allowed to continue this. GG for treating people the same.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-23-2003 at 08:36 PM:</div>
<div class="relics_body">Ok.<br /><br />Leatherneck. What you posted was to be blunt, short-sighted, ignorant, insensitive and just plain rude. Is it any surprise that such a virulent response came about? Can you honestly blame someone for responding strongly after the comment you made? By all accounts you can't.<br /><br />Now to be perfectly fair we did warn you in the other Iraq topic about making such inflammatory comments. Since you just did it again, I'm instituting a temp ban for you, 3 Days; however, I'm saying this right now, that upon further review this number may be revised upwards or downwards...<br /><br />And to be fair, calm down a bit Cliffy, we've taken care of it. <br /><br />Questions/comments, AIM/Email/Send a letter...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 03-23-2003 at 08:37 PM:</div>
<div class="relics_body">It's called satire you damn fool</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-23-2003 at 08:45 PM:</div>
<div class="relics_body">more b-52 are leaving fairford now. the total count comes to 4. and more are bing fired up. this would indicate the highest level of activity since the war began.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-23-2003 at 08:48 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>more b-52 are leaving fairford now. the total count comes to 4. and more are bing fired up. this would indicate the highest level of activity since the war began.</strong><hr /></blockquote>A few Iraqi divisions are going to be feeling the effects of JDAMs and JSOMs soon...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-24-2003 at 10:20 AM:</div>
<div class="relics_body">***Just a quick comment as regards what has happened in this topic.***<br /><br />Please bear in mind that just because someone acts like a complete ass does not mean that the rules are suddenly suspended and it is ok to flame the crap out of them. This kind of behaviour, whilst understandable in light of some of the comments made here, is equally against the rules and doesnt help the mods when it comes to dealing with the genuine troublemakers as it gives them cause for appeal and generally encourages them.<br /><br />Please can people bear this in mind in future<br /><br />Thanks<br /><br />BF</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-24-2003 at 03:37 PM:</div>
<div class="relics_body"> <a href="http://dear_raed.blogspot.com/" target="_blank">http://dear_raed.blogspot.com/</a> <br /><br />A person living in Baghdad is writing blogs for the world to see. It's very interesting reading. <br /><br />"If Um Qasar is so difficult to control what will happen when they get to Baghdad? It will turn uglier and this is very worrying. People (and I bet �allied forces�) were expecting things to be mush easier. There are no waving masses of people welcoming the Americans nor are they surrendering by the thousands. People are oing what all of us are, sitting in their homes hoping that a bomb doesn�t fall on them and keeping their doors shut."<br /><br />"The images we saw on TV last night (not Iraqi, jazeera-BBC-Arabiya) were terrible. The whole city looked as if it were on fire. The only thing I could think of was �why does this have to happen to Baghdad�. As one of the buildings I really love went up in a huge explosion I was close to tears. <br />today my father and brother went out to see what happening in the city, they say that it does look that the hits were very precise but when the missiles and bombs explode they wreck havoc in the neighborhood where they fall. Houses near al-salam palace(where the minister Sahaf took journalist) have had all their windows broke, doors blown in and in one case a roof has caved in. I guess that is what is called �collateral damage� and that makes it OK?"</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 03-24-2003 at 07:46 PM:</div>
<div class="relics_body">Well latest I've heard is that the British have now finally lost a troop to the Iraqi's - heard nothing about the execution of the hostages.<br /><br />To be honest I reacon they are still alive and being treated very well, they are very strong Political leeway.  If this war goes on long Sadam knows that by appearing to obey the Geneva convention he will gain more support - Killing them would effectivly rule out any aid he may get from a prolonged campaign<br /><br />Also just heard that the Iraqis have also reclaimed Basra, the coalition have been forced to retreat to regroup</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-24-2003 at 08:09 PM:</div>
<div class="relics_body">its war.<br />not to sound cold, but even if 100 died on our sides, i wouldnt flinch much. because its war. thats what happens in war.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-24-2003 at 08:26 PM:</div>
<div class="relics_body">One thing someone mentioned about the Geneva convention thing at the Archive - can be really get s****y @ Saddam for ignoring the thing when America & Britain are staging what's basically an illegal war?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-24-2003 at 08:30 PM:</div>
<div class="relics_body">well there are so many small legal matters involved here it depends how u look at it.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Ultrasabre</span> on 03-24-2003 at 08:38 PM:</div>
<div class="relics_body">WTF??????????????????????????????????????????????????????????????????????????<br /><br />Why were my posts deleted?? :ULTRAANNOYED: <br /><br />WTF Sup with that mods.??? :ULTRAANNOYED: </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-24-2003 at 10:10 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Ultrasabre:<br /><strong>WTF??????????????????????????????????????????????????????????????????????????<br /><br />Why were my posts deleted?? :ULTRAANNOYED: <br /><br />WTF Sup with that mods.??? :ULTRAANNOYED: <br /><br /></strong><hr /></blockquote>Read the second post in this thread.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-25-2003 at 08:18 AM:</div>
<div class="relics_body">Reports are comming through that Elite republican gaurds are approaching British troops using civilians as human shields.<br /><br />Ugh!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/icons/icon8.gif"> </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-25-2003 at 08:50 AM:</div>
<div class="relics_body">Whats your source for that, i cant find anything about it?<br /><br />edit - found it; <a href="http://www.sky.com/skynews/article/0,,30000-12275466,00.html" target="_blank">http://www.sky.com/skynews/article/0,,30000-12275466,00.html</a> <br /><br />nb - my suggestion for most up to date news would be;<br /> <a href="http://www.bbc.co.uk" target="_blank">http://www.bbc.co.uk</a> </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-25-2003 at 10:10 AM:</div>
<div class="relics_body">BBC news is providing a good serivce i feel.<br /><br />Marching ppl ahead of troops is soooo wrong.<br />no wonder they are not happy to soilders.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-25-2003 at 10:47 AM:</div>
<div class="relics_body">BBC News 24's on in the background most of the way, though I tend to watch C$ news when it's on...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Ultrasabre</span> on 03-25-2003 at 12:52 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong> Read the second post in this thread.</strong><hr /></blockquote>Why does your story remain posted then.  <img src="relics/rolleyes.gif"> <br /><br />Just cuzz you can. <br /><br />Not cool man. :|</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-25-2003 at 01:00 PM:</div>
<div class="relics_body">its not a story. it was a news Quote.<br />What ppl are not doing, or trying not to is run into the politics.<br /><br />Were just trying to keep a running current new events list here so ppl can see, and maybe in time, look back to see how things changed.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-25-2003 at 01:15 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Ultrasabre:<br /><strong> Why does your story remain posted then.   <img src="relics/rolleyes.gif"> <br /></strong><hr /></blockquote>Because i'm posting a link to an Iraqi citizen in Baghdad who is giving a first hand account of the war. If you bother to pay attention you'll notice i'm not giving commentary.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 03-25-2003 at 01:21 PM:</div>
<div class="relics_body">Reports of a civilian uprising in Basra.This is just what we were waiting for...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-25-2003 at 01:26 PM:</div>
<div class="relics_body">QUOTE]Originally posted by Ultrasabre:<br /><strong> Why does your story remain posted then.  <br /><br /></strong></blockquote>Comps posted:<br /><br /><strong>*I'm allowing this topic to be open simply because these are extraordinary times. Keep debate about the morality of the war etc to the one and only Iraq thread. This thread exists for war updates and news, not commentary. Understood? Posts deemed as commentary WILL be deleted as judged by the mod/admins*</strong><br /><br />he then posted a personal account from the conflict � this falls nicely under the category of news. Your posts didn�t. Hence his stays and yours does not? See?<br /><br />Trying to make the mods look bad because you ignored the guidelines at the start of the topic and got busted for it;<br /><br />Not cool man. :|</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-25-2003 at 01:57 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by spiderfrommars:<br /><strong>Reports of a civilian uprising in Basra.This is just what we were waiting for...<br /><br /></strong><hr /></blockquote>This is very interesting news.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-25-2003 at 05:02 PM:</div>
<div class="relics_body">They'll all be dead by the time we can reach them though and that won't look good to anyone else who might have thought of resisting.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-25-2003 at 05:09 PM:</div>
<div class="relics_body">thats opinion stuff - other topic please.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-25-2003 at 05:29 PM:</div>
<div class="relics_body">reports say Iraq troops, are firing on the iraq uprisers  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-25-2003 at 06:23 PM:</div>
<div class="relics_body">2 more British friendly fire casulties one of our Challengers chewed up another one in a night battle</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-26-2003 at 03:59 PM:</div>
<div class="relics_body">1000 elite iraq tanks etc are moving from baghdad to engage US troops.<br /><br />75-120 iraq tanks are moving from basra to al faw.<br /><br />1 commander put this action as suicidal. Saddam might have played his cards wrong here.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 03-26-2003 at 04:31 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>1 commander put this action as suicidal. Saddam might have played his cards wrong here.</strong><hr /></blockquote>It could well be but as this war has shown it is that you should never underestimate Sadam & the Iraqi's<br /><br />the launching of missiles at Kuwait and the forces after the first patriot attack which in turn forced the coalition into war a few days sooner than they had wanted, the increased resistance and in particular the fact he is using gorrilla tactics to use small teams to cut off the supply lines - This could be yet another trick (100 tanks are hard to kill in one go, especially with the bad weather limiting airial support.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-26-2003 at 04:39 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Nosecone:<br /><strong> (100 tanks are hard to kill in one go, especially with the bad weather limiting airial support.</strong><hr /></blockquote>Not when facing the 3rd Heavy Armor Division...especially when it's Abrams vs ancient T-65's or T-72's...<br /><br />In addition now the Longbows are going to rip the <I>piss</I> outta those advancing tanks. Before they had a web of AAA and buildings to shield them. Now they are in the open and on the move. Can you say "fish in a barrel"?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-26-2003 at 04:42 PM:</div>
<div class="relics_body">It could well be but as this war has shown it is that you should never underestimate Sadam & the Iraqi's<br /><strong>i dont think the wisest miltary minds in the wolrd havent under-estimated anything</strong><br /><br />the launching of missiles at Kuwait and the forces after the first patriot attack which in turn forced the coalition into war a few days sooner than they had wanted<br /><strong>The coalition had been ready to go for weeks. when they left didnt matter.</strong><br /><br />, the increased resistance and in particular the fact he is using gorrilla tactics to use small teams to cut off the supply lines<br /><strong> the only reason why these gorilla tactics might work is because the coalition forces do not want to use uneccary force. so far these pockets of resitance are being stamped out. no problem.</strong><br /><br /> - This could be yet another trick (100 tanks are hard to kill in one go, especially with the bad weather limiting airial support.<br /><strong>well the 75-120 armour unit has now been broken, using air force mainly, and hardley any armour. im not sure losing your own armour is a good trick</strong><br /><br />To be honest they are well ahead of shedual and doing fine. we have no idea whats going on in the north and the west.<br /><br />Saddams army is being depleted heavily unlike ours.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-26-2003 at 05:03 PM:</div>
<div class="relics_body">The Americans will slaughter any Iraqi armour in the open, that has to be a massive mistake by Saddam.  Remember the road of death<br /><br />As many as 15 Iraqi Civillians look to have been killed by a US missle hitting a busy marketplace today.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">MaximusFan</span> on 03-26-2003 at 06:04 PM:</div>
<div class="relics_body">Well, about the armour, I think the T-62s and T-72 could easily be domestically upgraded to be more effective. Both tanks have big smoothbore guns, which means it's easier to make new kinds of ammo for the weapon. I have no proof of this actually happening, but you'd think the Iraqis learned a lesson or two from last war. I'm curious what will happen this time when T-72 meets M1. T-72 is inferior, but IMO that can be remedied somewhat by skilled crews and improved ammo. The weakness of the T-72 is not the gun, but rather the crappy ammo the Iraqis have and the thin armour of the T-72. Hardly surprising seeing the fact that the T-72 weighs 42 tons and the Abrams 65 tons.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 03-26-2003 at 06:12 PM:</div>
<div class="relics_body">its not just down to weapons its the actually tank armour employed.<br /><br />But more importantly the computer controlled independent targeting systems, and move and fire ability of the modern tank which gives it the advantage in battle.<br /><br />In GUlf war 1 (what a phrase) 10 us Abrhams took out 30 iraq T tanks in 5 mins due to increase hit ratios and speed.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-26-2003 at 06:38 PM:</div>
<div class="relics_body">I was also thinking of air power, by the time the tanks get in range of each other the Iraqi's will have been well thinned out if not destroyed.  The US could easily use one plane per tank on the Basra convoy and that would quite probably be that.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-27-2003 at 01:49 AM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/891353.asp?0cv=CB10" target="_blank">http://www.msnbc.com/news/891353.asp?0cv=CB10</a>  ~ A short but very cool story. Can you imagine the Helicopters luck? They land right next to a MechInf Platoon. If that ain't a miracle I don't know what is. <br /><br />On other news of a much sadder note:<br /><br />Central Command said Thursday morning that it had no information on reports that 37 U.S. Marines were injured when coalition forces fired on one another in a �friendly fire� clash around the southern Iraqi city of Nasiriyah.<br />       A correspondent for Agence France-Presse who is traveling with the Marines reported that the Marines� command post headquarters was hit with shell and mortar fire. Quoting officers with the unit, AFP reported that 37 Marines were wounded, including three who were in critical condition and two others who were in serious condition. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-27-2003 at 01:15 PM:</div>
<div class="relics_body">Al-Jazeera is reporting another Apache has been shot down by the Euphrates river south of Baghdad somewhere.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-28-2003 at 03:44 PM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/888057.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/888057.asp?0cv=CA01</a> <br /><br />World War 3</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-29-2003 at 12:28 AM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/892259.asp?0cv=CB10" target="_blank">http://www.msnbc.com/news/892259.asp?0cv=CB10</a> <br /><br />Halliburton has decided to drop it's Iraq contract after coming under fire for conflict on interest.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-29-2003 at 11:44 AM:</div>
<div class="relics_body">  <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong> <a href="http://www.msnbc.com/news/888057.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/888057.asp?0cv=CA01</a> <br /><br />World War 3</strong><hr /></blockquote>Yeah, CBC has that too. Last week one of our experts up here brought up the question of when those Iraqi exiles in Iran would get involved. Temptation to make commentary.... Squashed.<br /><br />On another joyus note, we've seen the start of  suicide attacks on US forces:  <a href="http://www.cbc.ca/stories/2003/03/29/suicide030329" target="_blank">http://www.cbc.ca/stories/2003/03/29/suicide030329</a> <br /><br />Vietnam here we come again. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-29-2003 at 02:22 PM:</div>
<div class="relics_body">We won't let it become Vietnam, I believe we're smarter than that now. We saw what happened there, and how little was accomplished. The premise of the Vietnam conflict is far different from what is here; in Vietnam, we went in to try and protect South Vietnam from North Vietnam (Read: USSR) rolling in and taking over. Here, in Iraq, it's an attempt to get rid of a brutal dictatorship and rid it of weapons it is not allowed to have.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 03-29-2003 at 03:44 PM:</div>
<div class="relics_body">Actually Vietnam was supporting a corrupt authoritarian regine who  cancelled elections becuase teh communists would have won.  S. Vietnam wanted communism The divide was an artifical one. THe only people in Vietnam who were afraid of the Communists was the S. Vietnamese government and high officials in teh military. Vietnam was an internal conflict the US ahd no business being in. <br /><br />That aside, the paralel I'm drawing has to do with a blurring of the line between enemy and civilian. With paramilitaries and suicide bombers in civilian clothes holding up white flags  things are goingto start getting messy especailly with this war now lloking like it's going to last a couple months now at least. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-31-2003 at 10:51 AM:</div>
<div class="relics_body"> <a href="http://www.timesonline.co.uk/article/0,,5944-629644,00.html" target="_blank">http://www.timesonline.co.uk/article/0,,5944-629644,00.html</a> <br /><br />More British Friendly Fire Casualties. From an A-10 so you know it wasn't good...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 03-31-2003 at 11:20 AM:</div>
<div class="relics_body">I'm sorry? IFF?<br /><br />EDIT: They aren't best pleased either, are they? Good reason, but hooo-boy are some of the Americans gonna paste us Britsih for their comments...<br /><br />[This message has been edited by Cliffjumper (edited 03-31-2003).]<br /><br /><strong>Cliffy - please refrain from using terms like the ones i have edited out, i dont want people bashing Americans (or using terms that could be construed as bashing Americans - im not attributing intent)any more than the French or any other nationality. The fact you used what could be considered  a derogatory term for your own nationality as well is irrelevent of this. Its provocative and i dont want to see it again.<br /><br />everyone else be aware of this as well<br /><br />Thankyou<br /><br />BF</strong></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 03-31-2003 at 11:37 AM:</div>
<div class="relics_body">i found this pretty damning;<br /> <a href="http://www.guardian.co.uk/Iraq/Story/0,2763,926204,00.html" target="_blank">http://www.guardian.co.uk/Iraq/Story/0,2763,926204,00.html</a> </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-31-2003 at 12:01 PM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/893141.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/893141.asp?0cv=CA01</a> <br /><br />Divisions are beginning to form in the administration...from Republicans oddly enough.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 03-31-2003 at 12:42 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong> <a href="http://www.timesonline.co.uk/article/0,,5944-629644,00.html" target="_blank">http://www.timesonline.co.uk/article/0,,5944-629644,00.html</a> <br /><br />More British Friendly Fire Casualties. From an A-10 so you know it wasn't good...</strong><hr /></blockquote>That story is horrifying, and so vivid.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">bobaprime85</span> on 03-31-2003 at 05:46 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong> <a href="http://www.msnbc.com/news/893141.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/893141.asp?0cv=CA01</a> <br /><br />Divisions are beginning to form in the administration...from Republicans oddly enough.</strong><hr /></blockquote>I've been expecting something like this to pop up sooner or later.I'm not surprised that some infighting has started.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-31-2003 at 06:55 PM:</div>
<div class="relics_body">Stupid stupid people.... <a href="http://www.washingtonpost.com/wp-dyn/articles/A61039-2003Mar31.html" target="_blank">http://www.washingtonpost.com/wp-dyn/articles/A61039-2003Mar31.html</a> <br /><br />Watch some morons make this into our fault.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 03-31-2003 at 07:17 PM:</div>
<div class="relics_body">Just wondering is there a single American news source that has info on the friendly fire incident?<br /><br />Just asking as I couldn't find it on CNN, or the washington post site and am wondering if the American Press is likely to hide how pissed off British opinion is on this incident?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-31-2003 at 07:23 PM:</div>
<div class="relics_body">I haven't seen a single peep over at MSNBC or CNN on the A-10 incident. But on the other hand they usually have been quick to report on the civilian casualties of this war.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-31-2003 at 07:48 PM:</div>
<div class="relics_body">compy they mentioned it on Fox News... now thats a real news channel.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 03-31-2003 at 08:06 PM:</div>
<div class="relics_body">Faux News makes me want to puke in horror...<br /><br />At the very minimum, at least pretend to be objective and impartial while reporting the news...Faux News doesn't even try that...<br /><br />BTW to give this post more credibility rather than commentary, Rivera was kicked outta his embedded unit after showing live, on air, the battle plans for the 101st, including current location and destination.<br /><br />Dumbass.<br /><br />And Peter Arnett was fired from NBC/National Geographic for giving an interview to Iraqi TV in which he apparently said something quite inflammatory. Dunno what tho.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 03-31-2003 at 08:27 PM:</div>
<div class="relics_body">The exact same incident happened in the last war where some A-10's destroyed several of our AFV's full of troops, the pilots then, were nor prosecuted or apparantly even reprimanded and they were not required ton apologise to the families, I don't understand how it could happen again, what is up with American pilots are they all still on speed, thats apparantly the excuse given the incident with the canadian troops in Afghanistan and the Italian civillians that were killed around Kosovo time.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 03-31-2003 at 09:00 PM:</div>
<div class="relics_body">thats why I like fox  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"><br /><br />Geraldo should be shot in the head anyway. And Arnett should be deported.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 04-01-2003 at 02:15 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Getaway:<br /><strong>The exact same incident happened in the last war where some A-10's destroyed several of our AFV's full of troops, the pilots then, were nor prosecuted or apparantly even reprimanded and they were not required ton apologise to the families, I don't understand how it could happen again, what is up with American pilots are they all still on speed, thats apparantly the excuse given the incident with the canadian troops in Afghanistan and the Italian civillians that were killed around Kosovo time.</strong><hr /></blockquote>Individial incidents, as appalling as they are cannot be taken as a reflection of the entire force GW.<br /><br />the guy was sacked from the news channel for giving an interview on iraqi tv and saying something like 'things are not going to plan for the allies' or something along those lines BTW.<br /><br />The civilian deaths are a tradgedy, but considering recent events with suicide bombers and the fact they did not stop can not be considred a surprise. However bear in mind this news isnt just seen in the west...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 04-01-2003 at 06:37 AM:</div>
<div class="relics_body">post war plan latest;<br /> <a href="http://www.guardian.co.uk/Iraq/Story/0,2763,927055,00.html" target="_blank">http://www.guardian.co.uk/Iraq/Story/0,2763,927055,00.html</a> </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-01-2003 at 06:54 AM:</div>
<div class="relics_body">Never to sure what to make or paper reports like that. how come news channels never seem to report this kind of thing?<br /><br />Seems today at least. good progress is being made across Iraq tho.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 04-01-2003 at 07:48 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by LeAtHeRnEcK:<br /><strong>Stupid stupid people.... <a href="http://www.washingtonpost.com/wp-dyn/articles/A61039-2003Mar31.html" target="_blank">http://www.washingtonpost.com/wp-dyn/articles/A61039-2003Mar31.html</a> <br /><br />Watch some morons make this into our fault.</strong><hr /></blockquote>I guess this makes US 3rd Infantry Division Captain Ronny Johnson a moron then...<br /> <a href="http://www.guardian.co.uk/Iraq/dailybriefing/story/0,12965,927233,00.html" target="_blank">http://www.guardian.co.uk/Iraq/dailybriefing/story/0,12965,927233,00.html</a> <br /><br />Also appears that the bagdad market explosion that killed 60 civilians was from a cruise missile.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-01-2003 at 07:52 AM:</div>
<div class="relics_body">Saddam sacked his air defence minister last week after many of thier defence missles kept landing back on Baghdad... intersting thought that.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 04-01-2003 at 08:42 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>Saddam sacked his air defence minister last week after many of thier defence missles kept landing back on Baghdad... intersting thought that.</strong><hr /></blockquote>It's going to be the first war won by bombing your own side into submission...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 04-01-2003 at 09:16 AM:</div>
<div class="relics_body">BF, just making sure you got my email...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 04-01-2003 at 09:20 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Papa Snarl:<br /><strong> <br />I guess this makes US 3rd Infantry Division Captain Ronny Johnson a moron then...<br /> <a href="http://www.guardian.co.uk/Iraq/dailybriefing/story/0,12965,927233,00.html" target="_blank">http://www.guardian.co.uk/Iraq/dailybriefing/story/0,12965,927233,00.html</a> <br /><br /></strong><hr /></blockquote>Yep.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-01-2003 at 09:27 AM:</div>
<div class="relics_body">The coilition is saying that they are not responsible for last weeks market bomb.<br /><br />Jack straw on TV moments ago.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 04-01-2003 at 10:14 AM:</div>
<div class="relics_body">Impy, LN, I have  response to your posts about the civilian deaths in the Iraq thread. I figure it belongs there since it's opinion. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-01-2003 at 10:17 AM:</div>
<div class="relics_body">I just said that the coalition is saying somthing about cvilian deaths.<br /><br />Im just reporting whats on the news.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 04-01-2003 at 10:56 AM:</div>
<div class="relics_body">Sorry Impy, my cynicism over what passes for truth got the better of me.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-01-2003 at 11:01 AM:</div>
<div class="relics_body">sorry BS, im being grumpy of late.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-01-2003 at 09:38 PM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/889604.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/889604.asp?0cv=CA01</a> <br /><br />US Forces rescued a POW! Awesome news!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 04-01-2003 at 09:47 PM:</div>
<div class="relics_body">Didn't nine Marines die in that rescue though??</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-01-2003 at 09:51 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by LeAtHeRnEcK:<br /><strong>Didn't nine Marines die in that rescue though??</strong><hr /></blockquote>That was something else from a few days ago.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 04-01-2003 at 10:13 PM:</div>
<div class="relics_body">Ahh, i see. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-02-2003 at 07:18 AM:</div>
<div class="relics_body">The US are within 20 miles of Baghdad!<br /><br />A republican gaurd unit has been destroyed, and the US have taken a Damm or somthing.<br /><br />Stunning news.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">MaximusFan</span> on 04-02-2003 at 08:12 AM:</div>
<div class="relics_body">Hmm, the coalition is brutalizing the Iraqi army. Very unsurprising, considering the huge overkill in technology... I have a nasty feeling the Arabs will see the defeat of Saddam Hussein and his army as a humiliation and the "victory" will probably cause a surge in nationalism and anti-westism (sp?).</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-02-2003 at 08:15 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by MaximusFan:<br /><strong>Hmm, the coalition is brutalizing the Iraqi army. Very unsurprising, considering the huge overkill in technology</strong><hr /></blockquote>This is news but anyhows.<br /><br />What do u expect them to do, use harsh language? <br />Its war, u dont make things fair, u kill maim brutalize so that you win.<br />If anything, this is the most humane war. ever.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">MaximusFan</span> on 04-02-2003 at 08:49 AM:</div>
<div class="relics_body">Most humane? I doubt it. Have you been there? Have you seen first hand the results of the battles? Neither have I. As Shaxper pointed out above, we only see some tanks moving up and down highways. What makes this war monstrous in my eyes is the fact that I'm not convinced by the "nobel" intentions of Bush Inc. This is most likely blood for oil or control over oil. All the hundreds of civilian deaths, all thousands of Iraqi military deaths and the few US and UK deaths. This angers me in the most profound sense. That and the fact how my possible children might have to live in a new world torn by anger, hatred and vengeful destruction. All made possible by Monkey Interactive War Productions Inc.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-02-2003 at 09:17 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by MaximusFan:<br /><strong>Most humane? I doubt it. Have you been there? Have you seen first hand the results of the battles? Neither have I. As Shaxper pointed out above, we only see some tanks moving up and down highways.</strong><hr /></blockquote>nearly all news reports, even Iraq reports, indicate that they have been very selective over bombing runs.<br />In previous wars u just bomb everything and anything.<br />As for the battlefeild itself. i dont see colition troops dressed in civillian clothes walking up and pulling a gun out.<br />They are being as humanely possible as war can be. <br />thus the most humane war.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by MaximusFan:<br /><strong> What makes this war monstrous in my eyes is the fact that I'm not convinced by the "nobel" intentions of Bush Inc. This is most likely blood for oil or control over oil. All the hundreds of civilian deaths, all thousands of Iraqi military deaths and the few US and UK deaths. This angers me in the most profound sense. That and the fact how my possible children might have to live in a new world torn by anger, hatred and vengeful destruction. All made possible by Monkey Interactive War Productions Inc.<br /><br /></strong><hr /></blockquote>u see. this is where we get our lines crossed.<br />Im talking about military fighting, and how they are fighting this war.<br />Not the outcome, the reasons nor the future but the actually bang,bang your dead stuff of this war.<br /><br />And currently. in reflection on past wars, this is more humane.<br />u only have to go back to gulf war 1 to see the infamous 'road of death' incident.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-02-2003 at 03:10 PM:</div>
<div class="relics_body"> <a href="http://news.bbc.co.uk/1/hi/uk/2905817.stm" target="_blank">http://news.bbc.co.uk/1/hi/uk/2905817.stm</a> <br /><br />"Apaches are not heavily armoured and it takes just one rocket-propelled grenade (RPG) to bring one down. Compare that with one British Challenger near Basra which survived being hit by 70 RPGs." <br /><br />0_0<br /><br />And people want to take away tanks as part of an arsenal? Apaches are nice but...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 04-02-2003 at 04:21 PM:</div>
<div class="relics_body">Defintiely agree with that article. Right now there's no substitute for ground forces. On a lighter note any one happen to see the glaring typo in teh article Compy quotes?<br /><br /><strong>He said there were obvious comparisons with the <I>1991</I> defence review, which suggested scrapping several amphibious landing ships such as HMS Fearless and HMS Intrepid. <br /><br />"<I>A year later, when Argentina invaded the Falklands </I>we suddenly realised we needed those assets, which they had started to retire," said Mr Tusa. <br /></strong><br /><br />Whoops!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 04-02-2003 at 06:12 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Blacksword:<br /><strong>Defintiely agree with that article. Right now there's no substitute for ground forces. On a lighter note any one happen to see the glaring typo in teh article Compy quotes?<br /><br /><strong>He said there were obvious comparisons with the <I>1991</I> defence review, which suggested scrapping several amphibious landing ships such as HMS Fearless and HMS Intrepid. <br /><br />"<I>A year later, when Argentina invaded the Falklands </I>we suddenly realised we needed those assets, which they had started to retire," said Mr Tusa. <br /></strong><br /><br />Whoops!<br /><br /></strong><hr /></blockquote>Heh... also, if Argentina had held off for something like a month, we wouldn't have been able to do the Vulcan missions either. Not that it would have made much difference, mind...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-03-2003 at 05:25 AM:</div>
<div class="relics_body">what made the biggest difference was.<br /><br />A: our harriers smaking the crap out of thier mirages which just over shot everything.<br /><br />and<br /><br />B: The fact our Marines walked across the whole sodding island over the mountain and took the argys by surprise, as it was almost impposible. but we were hard.<br /><br />And as for timing. well thats called stratergy for u.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 04-03-2003 at 08:23 AM:</div>
<div class="relics_body">I especially love the accounts of Agentine Mirages ditching their bombs and running at the sight of Harriers.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-03-2003 at 08:29 AM:</div>
<div class="relics_body">still, another good thing to come from the Falklands was an end to the exocet. once the scourge of all ships. months later anti-ship missle guns were invented, and the French company lost its trade...ah well.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 04-03-2003 at 10:34 AM:</div>
<div class="relics_body">Well ti wasn't exactly all that great a weapon anyways. Just look at its record in the Falklands. From all accounts I heard teh Britush Sea Skuka (or something like that). I mean they totally missed the big carriers and only got a destroyer I think and one of the merchant carriers, and didn't they both no even explode? I know for sure one didn't expliode. Damn good thing otherwise a lot mroe British sailors would have lost their lives. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-03-2003 at 11:06 AM:</div>
<div class="relics_body">well at the time the exocet with its ground/sea hugging capability. ship pentrating and delayed fuse war head was the best thing since somthing else that went bang.<br /><br />but after that anti missle guns came about. i thiks its called 'sea whizz' or somthing, and anti ship missles are almost useless now. unless they make a 'stealth' version that is...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-03-2003 at 11:19 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>but after that anti missle guns came about. i thiks its called 'sea whizz' or somthing, and anti ship missles are almost useless now. unless they make a 'stealth' version that is...</strong><hr /></blockquote>CIWS<br /><br />Close in weapon system.<br /><br />It looks like a white R2D2 dome with a big vulcan cannon.<br /><br />However it doesn't make one invulnerable, not at all. It'll protect you from 1 or 2 missiles, but a salvo of 5-10, which is what would be fired anyways at a carrier would get through.<br /><br />It's AEGIS's job to shoot down missiles with the Arleigh Burke and Ticonderoga Standard 2 missiles. You resort to CIWS when everything else has failed.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-03-2003 at 11:27 AM:</div>
<div class="relics_body">SEA WHIZZ!<br /><br />Deff sounds better donn it?<br /><br />From today, instead of just drudging through my quite useless brain of info, i will argument it by bothering to type this into a search engine first and learning a lil more!<br /><br />CIWS as seen in crap films such as Under Siege... well there some crap info for u.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 04-03-2003 at 12:45 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>what made the biggest difference was.<br /><br />A: our harriers smaking the crap out of thier mirages which just over shot everything.<br /><br />and<br /><br />B: The fact our Marines walked across the whole sodding island over the mountain and took the argys by surprise, as it was almost impposible. but we were hard.<br /><br />And as for timing. well thats called stratergy for u.</strong><hr /></blockquote>Aye, I totally agree - the Falklands was in execution, if nothing else, a well strategised conflict. The only bad point really was the Sir Galahad thing, where the air cover should have been better... Like Blackie says, the Navy got off lightly with the Exocets really - neither exploded. Oh, and when we sunk the General Belgrano because we didn't like the look of it - that was, to be honest, a little harsh.<br /><br />But have you seen the aerial photographs of the damage the Vulcan raids did to the Port Stanley airfield? If I can do it without knackering the spine of Martin Middlebrook's "Task Force" [an absolute must read], and the complex flightplan diagram involving 13 aircraft [remember, just one of those is the aircraft attacking] and see if it wasn't a colossal waste of time...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-04-2003 at 10:02 AM:</div>
<div class="relics_body"><I>Iraq Warns of 'Non-Conventional' Act on U.S. Forces <br />Fri April 4, 2003 09:41 AM ET <br />BAGHDAD (Reuters) - Iraq said that it might take "non-conventional" action on Friday night against U.S.-led forces who have seized Baghdad airport."We will commit a non-conventional act on them, not necessarily military," Iraqi Information Minister Mohammed Saeed al-Sahaf told a news conference, adding the act might be tonight. "We will do something that will be a great example for these mercenaries."<br />It was not clear what he meant by "non-conventional." U.S.-led forces have been on alert for possible Iraqi use of biological or chemical weapons, which Baghdad denies it possesses.<br />Sahaf also said that U.S. forces were on an "isolated island" at Baghdad airport. "It is difficult for the U.S. forces that are surrounded in Saddam airport to come out alive, he said.</I><br /><br />Please dont tell me its an A-bomb or a dirty one.<br /><br />Mohammed Saeed al-Sahaf smugness and distinction he might win now worrys me.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-05-2003 at 02:38 PM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/870749.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/870749.asp?0cv=CA01</a> <br /><br /> <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif"> @ Nosecone</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Getaway</span> on 04-06-2003 at 04:40 PM:</div>
<div class="relics_body">Someone shot up a Russian Diplomatic convoy today, apprantly this is most likely to be the Iraqis, but could be the US and the US has bombed a convoy of Kurdish fighters and US Special Forces killing 18 Kurds, wounding 50, no word yet on any American injuries.  A large number of reporters in the convoy were also wounded, none that I have heard yet has died.<br /><br />Also the Desert Rats and Royal Marines are making heir final push on Basra, which might fall within the next 2 or 3 days.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 04-06-2003 at 08:23 PM:</div>
<div class="relics_body">and still an eary silence on the fact American troops shot at a British Helecopter.  I mean for crying out loud - Iraq doesn't have any  military helecopters that look like the british ones.<br /><br />Dumbasses!!!!<br /><br />and then they shot at the Russians (crossfire according to Ceefax)<br /><br />and then they bombed their own special forces - mistaking them for a tank<br /><br />Dumbasses!!!!!<br /><br />wonder if Walky will do a new dumbass of the month -&gt; The American Military - They have been taking lessons from the original Megatron one thinks =&gt; how to beat up your own troops.  No wonder the Iraqi military hasn't been seen, they are busy hiding till the American military wipes itself out with friendly fire</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-06-2003 at 08:27 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Nosecone:<br /><strong>No wonder the Iraqi military hasn't been seen, they are busy hiding till the American military wipes itself out with friendly fire</strong><hr /></blockquote>Err...they've all been destroyed. You can't see what no longer exists.<br /><br />Besides since we're suffering so few casualties from the enemy, we tend to focus on the friendly fire incidents. The fact still remains, US Deaths in the region are around 80 and UK deaths are around 30 IIRC. Considering we invaded and are conquering an entire country, that is unprecedented. 50 years ago "only" 80 deaths would have generals jumping for joy. The fact still remains that this is one of the most successful invasions in military history. <br /><br />However friendly fire cases are still regrettable and must be investigated in the hopes of preventing further cases. But make no mistake, in terms of friendly casualties, this has been one of the least bloody wars in...ever.<br /><br />(Not taking into account civilian or enemy deaths of course. There is still the sickening matter of civilian casualties...)</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 04-06-2003 at 08:51 PM:</div>
<div class="relics_body">But when American forces regulaly wipe out your own troops how can that bring confidence to you.  My dad has told me that when he was in the armed forces the general concensious was that if possible they would try to avoid interacting with the American military as this sort of thing happened.<br /><br />Add the gulf War (he didn't experiance the aftermath feelings after the fact most of our deaths came from Americans), Afghanistan (group of Canadian casualties from an American pilot) and the multiple deaths from American forces in this gulf war and you are starting to get to the point where troops simply would rather go it alone than be involved with American forces -&gt; something that is very dangerous -&gt; America (i.e the SWM at the top) needs to learn that in order to be able to do the role they have set themselves they need to become as profesional as the British and other armies<br /><br />It all seems a bit to cowboyish for me at the moment - i.e see enemy, send as many tanks in as possible and fire.  If you lose a few tanks so what.<br /><br />Question - How can they get respect of whoever they are freeing when they don't have the respect of their allies?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-06-2003 at 10:01 PM:</div>
<div class="relics_body">Again, the American military is just as professional and is one of the best trained in the world. And again, friendly fire scenarios seem to be prevalent simply because of the lack of deaths related to enemy fire. I guarantee that if the Iraqis were inflicting more harm on our forces no one would be commenting on the seeming preponderance of friendly fire. Case in point, WW2. There were tons of friendly fire instances; however, they were completely overshadowed by the Axis-related casualties. Since there are so few Iraqi-related casualties on our forces, we focus on friendly fire.<br /><br />Another analogy.<br /><br />Last year there was a media free for all on shark attacks. By the media's interpretation of it, shark attacks were occuring every five seconds. Upon looking at the actual data however, shark attacks were actually lower that year.<br /><br />Same here. It seems to be a friendly fire fiasco, but that's only because of a lack of enemy-related casualties. In addition US forces are flying an incredible amount of sorties. The fact that "only" 4-5 incidents have happened out of 8000 or more sorties is rather amazing, especially considering how many of those sorties are close air support related attacks.<br /><br />Furthermore the weapons technology is extremely deadly. One bomb can do a lot of damage which tends to increase casualties. <br /><br />Blue on blue related deaths are something that must be avoided. But when one looks at the data, one can see that overall, such incidents are very rare.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-07-2003 at 11:16 AM:</div>
<div class="relics_body">I agree that the friendly fire incidents seem to be in the media eye more because of the low number of actually battle casulty deaths on hand. yet i am still confused over how these are occuring.<br /><br />Shooting your own choppers, firing on a clear day at a british tank, flag and all. and other situation etc. tend to confuse me.<br /><br />now this is no dig at the US, but UK forces just dont seem to be doing it. we are still fyling all 100 Tornados around the clock, and we have alot of armour in the field. but it isnt happening as much ?<br /><br />This isnt a dig, and deff not one aimed at putting down US forces. its a generall observation.<br />Like compy says, it is almost unspoken of in war that in under 3 weeks we are now in a countrys capital city. and by all accounts we are begining to gain dominating controll.<br /><br />Was laughing my ass off today at the Iraq general minister who claimed the US forces were being killed and destroyed. when at the same time they were sitting inside Saddams own palace having a drink on his lawn and pee'ing in the toilet. class.<br />is this man just mad, or stupid?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-07-2003 at 11:26 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>is this man just mad, or stupid?<br /><br /></strong><hr /></blockquote>Probably not trying to get executed for treason. He has to say what Saddam wants or he'll have a new wooden suit.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Sheba</span> on 04-07-2003 at 07:16 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Computron:<br /><strong> Probably not trying to get executed for treason. He has to say what Saddam wants or he'll have a new wooden suit.</strong><hr /></blockquote>Or cement shoes...<br /><br />What's really hysterical is the discovery of WMD has the media cracking up.  After all, the libs have set themselves up to benefit if the war goes in Iraq's favor, or if no WMD are found.  After the first report came in to CNN about the Serin, Tabun, and other stuff, Wolf Blitzed had a look on his face like someone had told him his puppy had died.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Nosecone</span> on 04-07-2003 at 07:37 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Sheba:<br /><strong>After the first report came in to CNN about the Serin, Tabun, and other stuff, Wolf Blitzed had a look on his face like someone had told him his puppy had died.</strong><hr /></blockquote>And wasn't it later found out that no chemicals have been found (the two sites are under investigation so no comment on them) and were in fact mearly agricultural insect repellant<br /><br />hey Sheba, any idea when we're gonna go invade Somerset & Cornwall, I hear they have pesticide all over the place?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-07-2003 at 07:42 PM:</div>
<div class="relics_body">They have only looked at around 10% of Iraq so far. and havent even been to the 'main' sites they suspect.<br /><br />id actually ill bet my house that they will find them. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 04-07-2003 at 07:49 PM:</div>
<div class="relics_body">Yeh, like the way Don Beech on The Bll always found drugs on suspected dealers...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-07-2003 at 10:44 PM:</div>
<div class="relics_body"> <a href="http://www.msnbc.com/news/870749.asp?0cv=CA01" target="_blank">http://www.msnbc.com/news/870749.asp?0cv=CA01</a> <br /><br />Saddam may be have been killed by a B1 Lancer dropping 4 2000lb ground penetrating bombs on their location.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-08-2003 at 07:44 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Cliffjumper:<br /><strong>Yeh, like the way Don Beech on The Bll always found drugs on suspected dealers...</strong><hr /></blockquote>yup. either way we will find them.<br />Still the ppl of iraq would be happier with saddam in power so we are bad.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-09-2003 at 08:40 AM:</div>
<div class="relics_body">Well as im sure most of us know by now. we have pretty much got rid of saddam. except for a few pockets of resistance etc.<br /><br />Seems the Iraq ppl are over joyed, which is good to see. shame we couldnt have done a snatch squad and grabbed Saddam himself, that would have been funny.<br /><br />Come home soon lads.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 04-09-2003 at 09:27 AM:</div>
<div class="relics_body"> <a href="http://www.ananova.com/news/story/sm_768569.html?menu=news.wariniraq" target="_blank">http://www.ananova.com/news/story/sm_768569.html?menu=news.wariniraq</a> <br /><br />BBC had it comin to em...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 04-09-2003 at 09:31 AM:</div>
<div class="relics_body">And you would know that how? Because you watch BBC news?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-09-2003 at 09:42 AM:</div>
<div class="relics_body">ppl should watch BBC now, its amazing!<br />This is just amazing, ground breaking history in the making.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">HoistKeeper 2.0</span> on 04-09-2003 at 09:47 AM:</div>
<div class="relics_body">i am! but i have to go to work in a minute...<br /><br />The iraqs are trying to bash the fook outta Saddam's statue in the square and pull down the statue in Baghdad</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">god</span> on 04-09-2003 at 09:48 AM:</div>
<div class="relics_body">somebody lost a capital  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-09-2003 at 09:48 AM:</div>
<div class="relics_body">Go Iraq!<br />This is mental, i so have to watch this statue come down!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-09-2003 at 09:49 AM:</div>
<div class="relics_body">Well looks like we're in the final stage of the war in the classic sense, tho there may still be paramilitaries running around for a while taking potshots at troops a la Afghanistan.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-09-2003 at 10:08 AM:</div>
<div class="relics_body">these pictures are just simply amazing. the yanks are now helping the Iraq ppl. loads of Iraqs on the US troop carrier helping to pull down a very prominent statue of Saddam.<br />ppl are going mad, cheering, jumping for joy. handing the troops flowers. this is unreal to watch.<br /><br />Together, they will topple saddam. im amazed!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">LeAtHeRnEcK</span> on 04-09-2003 at 10:17 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Papa Snarl:<br /><strong>And you would know that how? Because you watch BBC news?</strong><hr /></blockquote>I watch it from time to time here (NJ public tv carries it sometimes) and I listen to it with my mom; she's got a thing for it. So yes, I do. Next?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 04-09-2003 at 10:56 AM:</div>
<div class="relics_body">That was amazing.<br />That must have done more for arab relations then anything in history</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 04-09-2003 at 10:57 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>That was amazing.<br />That must have done more for arab relations then anything in history<br /><br /></strong><hr /></blockquote>Probably. Tho the next several weeks will make or break relations in general. Why do we have to live in such interesting times?  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Master_Fwiffo</span> on 04-09-2003 at 11:33 AM:</div>
<div class="relics_body">w00t!<br /><br />And Lo, there was dancing in the streets of Baghdad, as an opressed people became free...<br /><br />And for anyone who missedit: <a href="http://www.cnn.com/2003/WORLD/meast/04/09/sprj.irq.war.main/index.html" target="_blank">http://www.cnn.com/2003/WORLD/meast/04/09/sprj.irq.war.main/index.html</a> </div>


 </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
</table>
</body>
</html>
