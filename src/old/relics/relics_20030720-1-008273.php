<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Archived Forum Discussion</title>
<link rel="stylesheet" href="css/transfans.css" type="text/css">
<link rel="stylesheet" href="relics/styles.css" type="text/css">
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr>
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt">

<h1>THE BIG FIGHT!! Quarter Finals</h1>
<h2>Thread originally posted by spiderfrommars</h2>


<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 06-29-2003 at 12:08 PM:</div>
<div class="relics_body">Results of Round 2:<br /><br />Ultra Magnus beat Scorponok 12-7<br />Galvatron beat Abominus 18-1<br />Megatron beat Soundwave 18-1<br />Fortress Maximus beat Superion 12-7<br />Shockwave beat Bruticus 17-2<br />Optimus Prime beat Straxus 17-2<br />Trypticon beat Computron 13-6<br />Metroplex beat Omega Supreme 17-1<br /><br />Which means we have some real clashes amongst titans for the Quarter Finals, as it should be! I want front row tickets for each of the following bouts:<br /><br />ULTRA MAGNUS Vs GALVATRON<br /><br />MEGATRON Vs FORTRESS MAXIMUS<br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br /><br />TRYPTICON Vs METROPLEX<br /><br />Voting open for 7 days. Let us know yer reasons. Enjoy.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 06-29-2003 at 12:15 PM:</div>
<div class="relics_body">Huzzar for the big fight!<br /><br />ULTRA MAGNUS Vs GALVATRON<br /><strong>+++Galvatron+++<br />Galvatron does his usuall job of smashing UM for the last and finnal time. the odd knock out here and there doesnt count because this time galvatron is bored of UM...</strong><br /><br />MEGATRON Vs FORTRESS MAXIMUS<br /><strong>+++Megatron+++<br />Tough call. but the ancient and experienced megatron turns on the rage and scares poor lil spike into submission.</strong><br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br /><strong>+++Prime+++<br />SHocwave fights a logical battle against old OP. but suddenly realises why OP carries the disco ball after OP blinds old shockers with its groovey light system...</strong><br /><br />TRYPTICON Vs METROPLEX<br /><strong>+++Metroplex+++<br />Metroplex raises his large size 3074203 squllion foot and mashes try into a fine palp</strong></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Turning Japanese</span> on 06-29-2003 at 12:50 PM:</div>
<div class="relics_body"><strong>ULTRA MAGNUS Vs GALVATRON</strong><br />My always-favorite encounter! I just imagine that this match is so evenly matched with such bad blood between them that it is tuff to judge which one would win. Psychology aside, Magnus is a near phyiscal equal to Galvatron. Maybe Galvatron has slightly more raw power, but Ultra Magnus has enormous endurance (not to mention natural fighting ability). Going with the robot who I think is "cleverer" with his fists, then I back Magny to win this (just). Ultra Magnus WINS!<br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br />Given his greater strength and leaving aside any personality issues with Spike/Cerebros, Fortress M should by right have the advantage over Megatron. Max has the size advantage too. But I can't help thinking that Megatron is simply made of tougher stuff. Megatron is a better fist fighter, has a powerful cannon, and has the dodgy antimatter powers. With both competitors on top form, I would have to give it to Meggy. Megatron WINS!<br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br />Suit you sir! Jeeez, talk about a battle of the giants. I reckon a long-range fight favors Shocky, and a close-in fist fight favors Oppy. And added to that, I figure Shockwave is smart enough to stay wide of Prime and wear him down with cannon blast fly-bys and then move in close to pummel Prime. Prime on the other hand, would be looking to corner Shockwave asap, and use his two-handed advantage to good measure. Almost a flip of a coin, but I'm gonna have to decide. So I favor Shockwave. Shockwave WINS!<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br />Trypticon wisely runs away! Metroplex WINS!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Rebis</span> on 06-29-2003 at 01:15 PM:</div>
<div class="relics_body">ULTRA MAGNUS Vs GALVATRON<br />Galvatron. Now you're talking! Magnus gets all riled up over his classic enemy, even doing him some damage, yet makes the mistake of putting distance between them, allowing Galvy the room he needs to transform to Particle Cannon.<br /><br />MEGATRON Vs FORTRESS MAXIMUS<br />Megatron. Difficult one this, and while Maxy has all those autonomic limb-based weapons, Megs has survived supersonic impacts, and can easily take a pounding without flinching. (Also, if we add a little Straxus into the mix, then Maxy loses his Headmaster advantage.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif">)<br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br />Shockwave. The slippery one-eyed devil manages to evade Prime long enough to transform into his Flying Gun and give Prime a few new "Matrix Holders" in his chest.<br /><br />TRYPTICON Vs METROPLEX<br />Metroplex. Size is the deciding factor, again. Tho Trypticon does do a fair bit of damage while the slower Metroplex "wakes up".</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Astrotrain99</span> on 06-29-2003 at 05:18 PM:</div>
<div class="relics_body">GALVATRON<br />MEGATRON<br />OPTIMUS PRIME<br />METROPLEX</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Dylan</span> on 06-30-2003 at 01:40 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Astrotrain99:<br /><strong>GALVATRON<br />MEGATRON<br />OPTIMUS PRIME<br />METROPLEX<br /></strong><hr /></blockquote>* high-fives Astro *</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Legion</span> on 06-30-2003 at 06:26 AM:</div>
<div class="relics_body">ULTRA MAGNUS Vs GALVATRON<br /> It all comes down to whether Magnus is in hippy mode or not. Against a fighting mad Galvatron he has little chance - but he has proved that he <I>can</I> beat him... and if there was a good enough reson to do it, he could do it again... just. Galvy's noodle of doom(tm) could tares Magnus to shreds tho... close call. <strong>Galvatron</strong> wins... just.<br /><br />MEGATRON Vs FORTRESS MAXIMUS<br />Maxie has the size advantage but Megs has the brute strength and firepower advantage. If he holds his marbles together long enough he rips into Maxie with his Fusion Cannon. If he looses his marbles, he tares Maximus apart with his bare hands. Either way... <strong>Megatron</strong> wins!<br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br />Ohh... good match up! Shockers has the firepower and manouverability advantage over old Hippymus Prime without a doubt. Prime has the warrior's edge though. If Shockers is able to make use of his flying space cannon (of doom(tm)) mode though, Prime is toasted. If it goes hand to hand, well Shockers is at a disadvantage. I reckon <strong>Shockwave</strong> wins!<br /><br />TRYPTICON Vs METROPLEX<br />Trypy bombards Plexy with artillary from long range, but Plexy takes too steps, covers the distance and then realises he stepped on Trypy too. <strong>Metroplex</strong> wins!!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 06-30-2003 at 07:48 AM:</div>
<div class="relics_body"><br /><strong><br />ULTRA MAGNUS Vs GALVATRON</strong><br /><br />Magnus. **** You Galvatron.<br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br /><br />Megs - Max has the ability to beat him, like Mags has to beat Galvy, but i just don't see Fort pulling it out of the bag...<br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br /><br />Prime. Cos he is Prime.<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br /><br />Metroplex. Sigh.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Metal Vendetta</span> on 06-30-2003 at 08:12 AM:</div>
<div class="relics_body">Hurrah! More fighting!<br /><br /><strong>ULTRA MAGNUS Vs GALVATRON</strong><br />Despite some tough talk from the Autobot City Commander, his quips, puns and witticisms are no match for the unstoppable Galvy. Magnus holds him off for as long as possible but in the end Galvatron WINS!<br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br /><br />Max has size on his side, but Megs has strength and the "nutter" factor. Ever seen Knockaround Guys where Vin Diesel takes that guy down with a perfect Glasgow kiss? Megatron WINS!<br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br />Very tough call. Depends on match fitness and whether Prime is really pissed off with Shockwave...under regular conditions I think Shockwave could pick Prime apart. Shockwave WINS!<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br />Can't we go by cartoon scale? Please? No? Then Metroplex WINS!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 06-30-2003 at 09:21 AM:</div>
<div class="relics_body"><strong>ULTRA MAGNUS Vs GALVATRON</strong><br /><br />Depends if Magnus feels he has anything to fight about.  If (somehow :rolleyes <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"> in the event of Galvy winning, say Cybertron were to be destroyed as punishment then Magnus would find some way of tricking or outsmarting Galvatron.  However in just a normal fight, Magnus would probably not have the edge of desperation needed to pull an unlikely victory from it.  Byebye Magnussy  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"><br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br /><br />If it were the cartoon we might have something that could standup to Metroplex in Maxy- but due to the cartoon being made of toilet tissue Megsy would win: hes too tough for Max to crack before he got in close to the Autobot and started tearing chunks out of him.<br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br /><br />Shockwave's good- but simply not that good.  He's second to Megatron, and Prime's beaten Megatron before.<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br /><br />...is it just me, or does Metroplex seem to be getting bigger in the eyes of the community at large...?<br /><br />I'm going to say Trypticon just to be a spanner in the works :P  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif">  Fluke shot from one of Trypticons missile /laser monkey wotsits happens to hit Metroplex in the face while he's in the middle of transforming to robot mode (planning to make an end of the decepticon).  <br /><br />Despite his huge size and shielding, with no brain to hold it up Metroplex falls over- complete with dubbed sound of a tree being cut down.<br /><br />heh heh.<br /><br />die you unfeasible product of lazy script writing!<br /><br /> <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 06-30-2003 at 09:28 AM:</div>
<div class="relics_body">I'm giving it the Autobot 4*!<br /><br />MAGS!<br />MAX!<br />PRIME!<br />PLEX!<br /><br />As for my reasons, I can't be arsed!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 06-30-2003 at 10:20 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Best First:<br /><strong><br /><strong><br />ULTRA MAGNUS Vs GALVATRON</strong><br /><br />Magnus. **** You Galvatron.<br /><br /></strong><hr /></blockquote>*******  magnus fanboys! :rollseyes:</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Steelhaven 1976</span> on 06-30-2003 at 10:29 AM:</div>
<div class="relics_body"><strong>Ultra Magnus</strong><br />Hes a smarter battler than Galvatron<br /><br /><strong>Megatron</strong><br />Makes Forty Max look like a lamer<br /><br /><strong>Optimus Prime</strong><br />Close call but hes tougher than Shocks<br /><br /><strong>Tryptiocn</strong><br />Coz ppl underestimate him!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 06-30-2003 at 12:29 PM:</div>
<div class="relics_body">MAGNUS v GALVY<br /><br />I'm going to go with Magnus, because I like him.<br /><br />OP v SHOCKERS<br /><br />Op. A fighting mad Op wouldn't give Shockwave a chance. Shockwave's all for taking him out from behind when Op thinks he's dead, and then later he gets his head back, and Shockwave just cries.<br /><br />MEGS v FORT MAX<br /><br />Fort Max, like the non-league side that's staggered past Macclesfield, Rotherham and Torquay is in too much of a ddaze of surprise at getting this far despite being as lame as a legless sheep. Spike realises what a useless gimp Max is and bails, and Megatron can't even be bothered to kill him, so just cooks a victory barbeque on him.<br /><br />METROPLEX vs TRYPTICON<br /><br />Metroplex. Yawn.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Brendocon</span> on 06-30-2003 at 01:36 PM:</div>
<div class="relics_body"><strong>ULTRA MAGNUS Vs GALVATRON</strong><br /><br />Now, that depends on if it's UK Galvy, or Rhythms of Darkness Galvy... because one of them knows that Magnus cacks his pants at the thought of it all, whilst the other's never met him.<br />Uhm, Galvy either way.  Just.<br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br /><br />Megatron.  Ugh.<br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br /><br />Woohoo.  Shockers.<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br /><br />Metroplex.   I'm beginning to think Shockwave's the only guy in this who could take the Plex.  Just.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 06-30-2003 at 02:32 PM:</div>
<div class="relics_body">Do I get the feeling that including ol' Plexy ain't a popular decision?  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 06-30-2003 at 02:59 PM:</div>
<div class="relics_body">Down with Metroplex!  Booooo!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 06-30-2003 at 03:10 PM:</div>
<div class="relics_body">And Galvy Knob Jockey's. Burn em all!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 06-30-2003 at 03:22 PM:</div>
<div class="relics_body">and the firecons, dont forget those!<br /><br />down with them all!!! </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 06-30-2003 at 05:03 PM:</div>
<div class="relics_body">LOL!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 06-30-2003 at 06:43 PM:</div>
<div class="relics_body">And the Shockwave fanciers - Shockwave beats Prime?<br /><br />Nah!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">cosmotron</span> on 06-30-2003 at 07:34 PM:</div>
<div class="relics_body"><br />GALVATRON<br />megatron<br />optimus prime<br />metroplex</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 06-30-2003 at 11:03 PM:</div>
<div class="relics_body">and dont forget that little monkey-testicle Seaspray![/topic derailing]</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Metal Vendetta</span> on 07-01-2003 at 04:31 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Best First:<br /><strong>And Galvy Knob Jockey's. Burn em all!<br /><br /></strong><hr /></blockquote>Isn't that a homophobic insult? Besty, I'm ashamed. :P</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-01-2003 at 04:44 AM:</div>
<div class="relics_body">Haha. im waiting for galvy v's [rime because galvy can win!!!<br /><br />WHy? because its G1 Prime, and u need PMOP to beat galvatron. thats why they invented him, or somthing... *sigh*<br /><br />Basicly Galvatron wins if metroplex wasnt init.<br /><br />And stop voting Magnus u fanboys, hes full of crap and Galvy can kick the ****  out of 2 Magnus. if HE feels the need to do so.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-01-2003 at 06:32 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Galvy can kick the **** out of 2 Magnus. if HE feels the need to do so.<hr /></blockquote>yep yep.<br /><br />and metroplex is over-rated... i stand by Galvy being able to kick Metroplex's vaaaast ass from the inside out  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Legion</span> on 07-01-2003 at 07:00 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Karl Lynch:<br /><strong>i stand by Galvy being able to kick Metroplex's vaaaast ass from the inside out</strong><hr /></blockquote>you sir, are quite insane.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-01-2003 at 07:26 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Metal Vendetta:<br /><strong> Isn't that a homophobic insult? Besty, I'm ashamed. :P<br /><br /></strong><hr /></blockquote>Gah.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-01-2003 at 07:31 AM:</div>
<div class="relics_body">poor besty- if its any consolation it didnt bother me... oral sex references can apply to straight relationships as well as gay ones.<br /><br />...and as for you legion have you listened to none of my skelator parody songs?   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">yay</span> on 07-01-2003 at 08:07 AM:</div>
<div class="relics_body">ULTRA MAGNUS Vs GALVATRON<br /><strong>Ultra Magnus</strong><br /><br />MEGATRON Vs FORTRESS MAXIMUS<br /><strong>Fortress Maximus</strong><br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br /><strong>Optimus Prime</strong><br /><br />TRYPTICON Vs METROPLEX<br /><strong>Trypticon</strong></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Metal Vendetta</span> on 07-01-2003 at 12:18 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Karl Lynch:<br /><strong>oral sex references can apply to straight relationships as well as gay ones.</strong><hr /></blockquote>Not that I want to labour the point (I was only light-heartedly ribbing Besty in the first place) but I don't think knob jockey is a reference to oral sex...after all I've never seen a jockey ride a horse with his mouth  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif"> <br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Karl Lynch:<br /><strong>and metroplex is over-rated... i stand by Galvy being able to kick Metroplex's vaaaast ass from the inside out  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></strong><hr /></blockquote>Damn right. As Jimmy Cliff sang this weekend, <I>the bigger they come, the harder they fall</I>. Scratch one Autobot city.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-01-2003 at 12:32 PM:</div>
<div class="relics_body">Well autobot city is crap. Galvy wins because he kills everyone before they can transform it  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Blacksword</span> on 07-01-2003 at 01:21 PM:</div>
<div class="relics_body"><strong>GALVATRON</strong> by the slimmest of margins. This is a fight that hinges on the mindsets of the two combatants but lined up Galvy's just stronger, absolutely more vicious and has the firepower advantage. Magnus would take a good few chunks out of Galvy but the purple and silver monster takes it.<br /><br /><strong>MEGATRON</strong> even as G1 Max is finished, but it's a fairly close match given Maxies huge firepower and his size advantage. But even that isn't enough for Megatron's raw power and fighting ability. Another match that would vary depending on mental state, but Megatron (despite what the cartoon and some por comic writign did to him) is still that damn good. If we took G2 form into account there's little bits of Max everywhere.<br /><br /><strong>PPIME</strong> as I seriously can't see Shockers taking this one. Maybe from far away he's got the firepower advantage but even then Prime always struck me as a pretty agile and quick fighter so he dodges those big blasts no prob. Plus as the best shot among the Autobots and the big hole he's blasted in stuff with his laser rifle before Shocker's in flying gun mode has to watch it. at medium and close range big purple is toast but still doing decent damage but he's simply not as tough or skilled as Prime. Prime in G1 form whooped him before and will do it again, especially if he's mad. Any of Prime's other forms and Shockwave looks about as bad as he did when Death's Head got him.<br /><br /><strong>Metroplex</strong> makes Trypie go squish. IN comic form he's just too small despite his huge fire power. The Dinobots gave him huge problems and I doubt they could take ol' Metro. Not counting cheap shots as this is  a tournament match this one's over before it starts.<br /><br />Cheers to Lynch<br /> <br /><br />As to my bringing up alternate forms like those of Priem and Megatron I think we should throw them in for the next match. I know the assumption was G1 forms but since no stipulation was made the other forms could in theory have been tossed in, especially since, in terms of screen time Prime spent most of his comic life in PM form, in which he stands a good shot of taking down Galvy and would whip G1 Megatron. Just a thought for the next one.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 07-01-2003 at 01:34 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Karl Lynch:<br /><strong> i stand by Galvy being able to kick Metroplex's vaaaast ass from the inside out   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"><br /><br /></strong><hr /></blockquote>Yep, I too will be voting for Sir Galvy when the time comes...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">bobaprime85</span> on 07-01-2003 at 03:55 PM:</div>
<div class="relics_body"><br />ULTRA MAGNUS Vs GALVATRON<br />A very, very brutal match that ends with loss of limbs. Magnus just barely crawls out alive.<br /><br />MEGATRON Vs FORTRESS MAXIMUS<br />Megs gets the jump on Max while the Headmaster is arguing with Spike.<br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br />As I have said before: Prime is THE best.<br /><br />TRYPTICON Vs METROPLEX<br />Metroplex unloads everything he has on the Godzilla rip-off and emerges the victor.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">MaximusFan</span> on 07-01-2003 at 04:31 PM:</div>
<div class="relics_body">Ultra Magnus<br />Fortress Maximus<br />Shockwave<br />Trypticon</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-02-2003 at 04:35 AM:</div>
<div class="relics_body">well, im gonna vote for Oppy to outsmart the big lug anyway. My bet is an Op/Galvy final...  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-02-2003 at 04:38 AM:</div>
<div class="relics_body">herehere!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Legion</span> on 07-02-2003 at 05:14 AM:</div>
<div class="relics_body">noway! Shockers vs Plexy final!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-02-2003 at 06:02 AM:</div>
<div class="relics_body">bah- no charisma versus no use. one eye and one braincell should get on like a house on fire.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-02-2003 at 07:45 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by spiderfrommars:<br /><strong> Yep, I too will be voting for Sir Galvy when the time comes...<br /><br /></strong><hr /></blockquote>Bravvo!  but i think there are a few to many Magnus fanboyz in da house...  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-02-2003 at 07:46 AM:</div>
<div class="relics_body">  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"><br /><br />  <img src="relics/BFMAGNUS.jpg">    </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-02-2003 at 08:02 AM:</div>
<div class="relics_body">i love the last frame of that comic- Galvatron <strong>literally</strong> getting his ass kicked heh heh</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-02-2003 at 08:27 AM:</div>
<div class="relics_body">Oh yeah!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Rebis</span> on 07-02-2003 at 08:56 AM:</div>
<div class="relics_body">There's nothing more dangerous than a man with his own webspace, and the inclination to make images.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-02-2003 at 09:09 AM:</div>
<div class="relics_body">looks like the Galvy Vs. Magnus camp could get ugly...<br /><br />{breaks out riot shield and popcorn}</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Legion</span> on 07-02-2003 at 09:20 AM:</div>
<div class="relics_body">The image Imp's used for the first frame of that sig has always struck me as being a bit... well, not right... maybe it's just the way my mind works...  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Rebis</span> on 07-02-2003 at 09:23 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Legion:<br /><strong>The image Imp's used for the first frame of that sig has always struck me as being a bit... well, not right... maybe it's just the way my mind works...   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></strong><hr /></blockquote>You're probably just remembering the Polymorph episode of Red Dwarf. From Rimmer's perspective.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-02-2003 at 09:33 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Rebis:<br /><strong>There's nothing more dangerous than a man with his own webspace, and the inclination to make images.   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></strong><hr /></blockquote>Its the speed of replication + dedication that helps me too...<br /><br />And Galvy does look odd. but i just cant work out why...*thinks* oh my!!!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-02-2003 at 11:49 AM:</div>
<div class="relics_body">Once again Impy proves that;<br /><br />a) he is very good at making pretty pictures. And has way to mych free time (ooh! Heavy Metal War...)<br /><br />b) he knows nothing about TFs. Of the images you have selected;<br /><br />the first; Is the cover to the first comic where Magnus proves he has the **** to beat Galvy into the ground<br /><br />The second, is about two pages before Magnus proves he has the **** to beat Galvy into the ground<br /><br />The third, is aboyt one pages Magnus proves he has the **** to beat Galvy into the ground.<br /><br />And the final is nicely illustarting that Galvy is suh an ineffectual buffoon he can't even get throwing people into a volcano right. Dur!<br /><br />The images i have chosen however... preceed a huge anti-climax courtesy of a certain Mr. Furman. Sigh.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">yay</span> on 07-02-2003 at 12:58 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Best First:<br /><strong><br />The images i have chosen however... preceed a huge anti-climax courtesy of a certain Mr. Furman. Sigh.   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"><br /></strong><hr /></blockquote>This anti-climax being the fact that the feud between the city commanders wasn't cleanly resolved. In fact the Galvy VS. Mags saga was pretty much dropped after the fudged so-called 'last battle' in Time Wars. We was CHEATED I tells ya! <br /><br />I fancied Furman + Senior doing a one-off 100 page Graphic Novel based solely on Galvatron fighting Ultra Magnus. 100 pages of *CRASH!* *BANG!* *HIT!* *ZOINK!* *WUNT!* *LAMP!* a la "THE LONG BIG PUNCH UP 5" sketch from The Fast Show!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"><br /><br />Nice. T'would be coloured by Gina Hart too.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 07-02-2003 at 01:52 PM:</div>
<div class="relics_body">We all have our dreams.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"><br /><br />Oh, and nice one Impy! </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-02-2003 at 02:36 PM:</div>
<div class="relics_body">THanks mate.<br /><br />Galvatron can beat any TF except Unicron.<br /><br />The only reason he ever loses is because hes a 'bad' guy.<br /><br />And salvage is utter ****  in my opinion.<br />To me its about as usefull as Rythems of Darkness.<br /><br />U know i was just thinking. when we had the Galvatron V's Metroplex debate. i was still amused at ppl backing galvatron. yes the fight was one sided but it wasnt cut and dry. no matter what u say.<br /><br />Now i thought about Magnus in the same postion. and that was cut and dry.<br /><br />Galvatron is the strongest fastest most durable of TF. he has the skill and the wit to out fight and prehaps out smart any TF. Only Prime is his equal and in all honesty Primes own physical atributes are no match for the TF made by a god esque machine.<br /><br />U might not like it, but deep down you know the only reason Galvy is ever beaten, is because hes the 'bad' guy...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-02-2003 at 03:14 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />the only reason Galvy is ever beaten, is because hes the 'bad' guy...<hr /></blockquote>that and he tends to underestimate people- like magnus, magnus beat him essentially by giving up and then suddenly coming back so strong it took galvy by surprise.  thats also how rodimus prime beat him in unicron.<br /><br />his only real equal in terms of combatant skills and stamina i agree is Optimus Prime (PowerMaster Prime) I would give oppy a fighting chance of outwitting Galvatron.<br /><br />ironically Galvatron is at his most dangerous when he completely loses his mind- because at that point the weakness of underestimating his opponent is blown out of the water.<br /><br />I want to see either an Oppy/Galvy or an Oppy/Metroplex showdown!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Rebis</span> on 07-02-2003 at 03:29 PM:</div>
<div class="relics_body">  <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Karl Lynch:<br /><strong>I want to see either an Oppy/Galvy or an Oppy/Metroplex showdown!    <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></strong><hr /></blockquote>Which would explain why you voted for Trypticon.   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif"><br /><br />---<br /><br />Ok, it's a little old, but I thought I'd brin it up, considering the all the Galvy/Magnus play around here!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"><br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Papa Snarl:<br /><strong>And the Shockwave fanciers - Shockwave beats Prime?<br /><br />Nah!</strong><hr /></blockquote>Excuse me? When did Prime ever close a time rift? Could he even close a time rift? Without resorting to powerful things that weren't him? And I know that he might be able to block it up by driving his lardass trailer into the hole, but that's hardly gonna work in the arena!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Metal Vendetta</span> on 07-02-2003 at 03:43 PM:</div>
<div class="relics_body">Sure, Prime just calls his trailer out of nowhere and drops it on old one eye...seriously, Shockwave's a giant gun. A giant frickin' flying gun. Prime's gotta resort to something special to take him down.<br /><br />As for Magnus, he's just a soldier. He's not worthy.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Powermaster Optimus Prime</span> on 07-03-2003 at 01:27 PM:</div>
<div class="relics_body">MAGNUS!!!!!<br />MEGATRON<br />OPTIMUS<br />METROPLEX</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 07-03-2003 at 02:01 PM:</div>
<div class="relics_body">Galvatron - strongest TF, no way is he the most skilled fighter.<br /><br />UM is not on his level in terms of power, he beat him with a bit of grit and determination. If you check their fights, UM is always on the backfoot, but consistently doing damage.<br /><br />And Shockwave is a giant flying gun - care not.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-03-2003 at 02:41 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br /><br /><strong>Galvatron can beat any TF except Unicron.</strong><br /><br />Hence in his time loosing to;<br /><br />1)Magnus<br />2)Hook Line and Sinker<br />3)The Classic Autobots<br />4)Fort Max<br />5)Rodimus Prime (! - come on! he's crap!)<br />6)The weather<br /><br />'can' and 'will' are so far apart they are not even related. <br /><br /><strong>The only reason he ever loses is because hes a 'bad' guy.</strong><br /><br />Or cos he is constantly underestimating more skilled/resolved/morally inclined/sane opponents. Who beat him.<br /><br /><strong>And salvage is utter ****  in my opinion.<br />To me its about as usefull as Rythems of Darkness.</strong><br /><br />Oooh! 2 of the stories where Galvy gets ****ed up are rubbish. Why exactly? How is Galvy getting beaten in Salvage in any way incongruous with the way Magnus beats him in the 'Wanted' saga? Or the way Magnus outskills him in T2006 (only being beaten by Furman deciding that he is going to stand around day dreaming rather than press the advantage)? Its not. Its Magnus on an upcurve realising he has what it takes to beat Galvatron. <br /><br /><strong>U know i was just thinking. when we had the Galvatron V's Metroplex debate. i was still amused at ppl backing galvatron. yes the fight was one sided but it wasnt cut and dry. no matter what u say.</strong><br /><br />You mean... no matter what almost everyone said...<br /><br /><strong>Now i thought about Magnus in the same postion. and that was cut and dry.</strong><br /><br />Theres no difference - they both get squashed. Galvatron doesn't even have the power to eat through plexes armour which is clearly classified as resistant to a nuclear warhead.<br /><br /><strong>Galvatron is the strongest fastest most durable of TF.</strong><br /><br />Except Metroplex is clearly stronger, Blur is clearly faster, Prime and Magnus arguably as fast. Metroplex probably has him covered on the last one as well. In his size class he is probably the strongest and most durable - but not so much so that people with more brains and skill (i.e Prime and Magnus) can't beat him.<br /><br /><strong>he has the skill and the wit to out fight and prehaps out smart any TF.</strong><br /><br />Outsmart? Outsmart? He's insane! And skill - where does he exhibit it? - he relies on power in every story he appears in;<br /><br />T2006; lets everyone shoot him cos he doesnt care, plus only beats Mags due to his firepower, <I>not</I> by outskilling him.<br /><br />Wanted: Dead or Alive; Out fought by Mags - only beats him due to a) Magnus being distracted and b) firepower <I>not</I> by outskilling him.<br /><br />Timewars; Allows himself to be mobbed and then relies on his power to beat those mobbing him - not skill. Plus once again it is firepower that proves his key asset.<br /><br /><strong>Only Prime is his equal and in all honesty Primes own physical atributes are no match for the TF made by a god esque machine.</strong><br /><br />What like, say, Primus? Who made all TF's? Who Prime is clearly the most powerfull of? Primus beat Unicron to the punch by several million years. But he made his sane...<br /><br />And its not just about physical attributes is it? Lennex Lewis vs Bruce Lee - who is stronger? Who wins?<br /><br /><strong>U might not like it, but deep down you know the only reason Galvy is ever beaten, is because hes the 'bad' guy...</strong><br /><br />Hmm, or the fact that he is not powerfull enough that he cannot be outskilled and beasten by someone who is only a few notches below him in power, assuming they are a more skilled warrior and can avoid the one shot kill on his arm that he clearly overly relies on.<br /><br />Deep down i know that if thay had ever met again properly, Magnus would have ****ed him up so bad i would have felt sorry for him. Interstingly i also know this superficially.<br /><br /><hr /></blockquote></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-03-2003 at 02:49 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Karl Lynch:<br />    I want to see either an Oppy/Galvy or an Oppy/Metroplex showdown! <br /><strong>Rebis:</strong><br />Which would explain why you voted for Trypticon. <hr /></blockquote>cos i hate metroplex!  but as its blatant hes going to be in the final whether i like it or not i want him to face oppy if anybody so  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif">  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif">  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/tongue.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Brendocon</span> on 07-03-2003 at 03:04 PM:</div>
<div class="relics_body">I dunno about the Shockwave fanciers, the Galvyholics or the Magnus Fanboyz.  What worries me are the amount of otherwise sensible people who don't see Optimus as the waste of ink he was...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">yay</span> on 07-03-2003 at 03:15 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />And its not just about physical attributes is it? Lennex Lewis vs Bruce Lee - who is stronger? Who wins?<br /><hr /></blockquote>Good analogy. Galvatron has the ultimate physical characteristics but lacks the fighting skill of Ultra Magnus (who has near-ultimate physical characteristics).<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br /><strong>You might not like it, but deep down you know the only reason Galvy is ever beaten, is because he's the 'bad' guy...</strong><br /><br />Hmm, or the fact that he is not powerful enough that he cannot be outskilled and beaten by someone who is only a few notches below him in power, assuming they are a more skilled warrior and can avoid the one shot kill on his arm that he clearly overly relies on.<br /><hr /></blockquote>Again I agree. But credit to Galvatron - since he has this powerful cannon on his arm,  it only makes sense to make good use of it! Take away the cannon however, and Galvatron's chances against the likes of Ultra Magnus, Optimus Prime, or Jhiaxus drops dangerously.<br /><br />Magnus was noted as the "Autobot's greatest warrior" which doesn't individually imply 'the strongest', 'the smartest' or 'the quickest', but it does imply that on the whole, he was the ultimate fighting package.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Deep down i know that if thay had ever met again properly, Magnus would have ****ed him up so bad i would have felt sorry for him. Interstingly i also know this superficially.<br /><hr /></blockquote>Yay!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-03-2003 at 03:24 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /> by BF etc....<br />Hence in his time loosing to;<br />1)Magnus<br /><strong> Hes kicked magnus in on a number of occasions. </strong><br />2)Hook Line and Sinker<br /><strong>Who he kills in the end?</strong><br />3)The Classic Autobots<br /><strong>all the dinobots plus blaster? hmmm dude id like to see magnus do that...</strong><br />4)Fort Max<br /><strong>Fair enough, but he was fecked. and Megatron beats max, as we all agree so galvatron can beat him</strong><br />5)Rodimus Prime (! - come on! he's crap!)<br /><strong> i recall a certain head stomp?</strong><br />6)The weather<br /><strong> Fair enough</strong><br /><hr /></blockquote> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Or cos he is constantly underestimating more skilled/resolved/morally inclined/sane opponents. Who beat him.<hr /></blockquote>but hes the nutty bad guy. he deff has the power but does the stero-typical bad guy nutter and gloats or somthing.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Oooh! 2 of the stories where Galvy gets ****ed up are rubbish. Why exactly? How is Galvy getting beaten in Salvage in any way incongruous with the way Magnus beats him in the 'Wanted' saga? Or the way Magnus outskills him in T2006 (only being beaten by Furman deciding that he is going to stand around day dreaming rather than press the advantage)? Its not. Its Magnus on an upcurve realising he has what it takes to beat Galvatron. <hr /></blockquote>yeah but who wins? he still has the power to beat magnus. magnus knows it too. magnus ok also knows he can probably beat galvatron but i still say its just down to writting him as the sterotypical bad guy.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Now i thought about Magnus in the same postion. and that was cut and dry.<br />Theres no difference - they both get squashed. Galvatron doesn't even have the power to eat through plexes armour which is clearly classified as resistant to a nuclear warhead.<hr /></blockquote>particle cannon. brute strength. I belive he can.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Except Metroplex is clearly stronger, Blur is clearly faster, Prime and Magnus arguably as fast. Metroplex probably has him covered on the last one as well. In his size class he is probably the strongest and most durable - but not so much so that people with more brains and skill (i.e Prime and Magnus) can't beat him.<hr /></blockquote>Apart from the odd exceptions. and ppl like blur are useless anyhows. hes still the fastes, strongest and most durable. hell the uk comic writters seemed to agree.<br />Magnus and PMOP are probably the only match for him.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />he has the skill and the wit to out fight and prehaps out smart any TF.<br />Outsmart? Outsmart? He's insane! And skill - where does he exhibit it? - he relies on power in every story he appears in;<br /><hr /></blockquote>Removing the highly dangerous Bounty hunters arm? and his fight with megatron?<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />T2006; lets everyone shoot him cos he doesnt care, plus only beats Mags due to his firepower, not by outskilling him.<br />Wanted: Dead or Alive; Out fought by Mags - only beats him due to a) Magnus being distracted and b) firepower not by outskilling him.<br />Timewars; Allows himself to be mobbed and then relies on his power to beat those mobbing him - not skill. Plus once again it is firepower that proves his key asset.<hr /></blockquote>in the situation he was in the only logical thing to do would be use brute force.<br />And if u have the fire power, why not use it? he is a skilled fighter. many times he has been able to take on many TF&gt; now even with brawn and firepower. u still require skill to use.<br />Having power is one thing, knowing how to use it is another.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />What like, say, Primus? Who made all TF's? Who Prime is clearly the most powerfull of? Primus beat Unicron to the punch by several million years. But he made his sane...<hr /></blockquote>Primus didnt desing on  selfish basis. he didnt desing one Uber TF in the mould of Galvatron.<br />Hehe that makes me think actually. the finnal autobot? he would be more powerfull...<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />And its not just about physical attributes is it? Lennex Lewis vs Bruce Lee - who is stronger? Who wins?<hr /></blockquote>Impposible to say. but galvatron has all the attributes.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Hmm, or the fact that he is not powerfull enough that he cannot be outskilled and beasten by someone who is only a few notches below him in power, assuming they are a more skilled warrior and can avoid the one shot kill on his arm that he clearly overly relies on.<hr /></blockquote>If u have the tools, u use them. galvatron uses what ever he feels gets the job done. sometimes with his bare hands, sometimes using weapons. and generally because he is out numbered. its not like prime never uses his gun is it?<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr /><br />Deep down i know that if thay had ever met again properly, Magnus would have ****ed him up so bad i would have felt sorry for him. Interstingly i also know this superficially.<hr /></blockquote>Well i awlays felt that galvatron was playing with magnus, it always seemed that way, with all TF he seemed that way. yes hes insane for that reason, but utterrly deadly. <br />I feel he can kill Mgnus when he so chooses. I still feel he has the skill the power, the durablility, and i think hes got the wits to beat nearly any TF. magnus and Prime included. if he feels like it.<br />I think most TF to Galvy are just bothersome side thoughts. a nusence in his way. he doesnt finnish them because he has more pressing matters.<br /><br />hehe all the dinobots plus blaster... thats pretty hard core if u ask me. think Mags can do that?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">yay</span> on 07-03-2003 at 03:50 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>hehe all the dinobots plus blaster... thats pretty hard core if u ask me.<br /></strong><hr /></blockquote>Very hardcore. Especially after wasting energy on Cosmos and company!<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>think Mags can do that?<br /></strong><hr /></blockquote>Definitely.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 07-03-2003 at 05:46 PM:</div>
<div class="relics_body">I'm enjoying the debate.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"><br /><br />I'll just quickly pitch my votes in:<br /><br />GALVATRON<br /><br />MEGATRON<br /><br />OPTIMUS PRIME<br /><br />METROPLEX</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-03-2003 at 06:09 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong> <br />hehe all the dinobots plus blaster... thats pretty hard core if u ask me. think Mags can do that?<br /><br /></strong><hr /></blockquote>Erm... do you remember Magnu's role in the original Op-vol plan?<br /><br />Psyche.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 07-03-2003 at 06:13 PM:</div>
<div class="relics_body">Okay, he layed the smack on Blaster and the Dinobots...<br /><br />Go read that story, galvatron himself points out where the DInobots went wrong, pointing out exactly what not to do when fighting him...<br /> <a href="http://webresources.transfans.com/images/comics/ukcomics/102/pg3.htm" target="_blank">http://webresources.transfans.com/images/comics/ukcomics/102/pg3.htm</a> <br /><br />"No subtlety, no finesse! Just mindless brute force..., "Brute force which I can match..."...And better!"<br /><br />I think UM could take the protagonists had he been involved in Galvy's shoes on this occasion - would have done it in a different way, would have been more difficult, but could have pulled it off.<br /><br />Grimlock matured as the comic went on, I doubt he would have lead an attack this brash against Galvatron had he been clued up.<br /><br />I mean, look. Galvatron got ******  up in that fight anyway! The Dinobots did a job on him and simply didn't think he would get up from it. Who else would have? Very few.<br /><br />So credit to Galvatron for having superb stamina. But this trick can only be used once. <br /><br />He's been grounded on several occasions, just not put totally out of comission. A skilled TF prepared for his second coming will have a far greaeter chance of beating him than a random hardened tf.<br /><br />Magnus knows Galvatron's game. I really think UM could do a job on him. It would be very messy, but I'd fancy him. </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-03-2003 at 06:30 PM:</div>
<div class="relics_body">fair comment. the dinobots did evolve.<br /><br />and knowing galvatrons game would be half the battle. u get the chance, u knock himd down u best finnish him off or u will regret it.<br /><br />I always have the same feeling with galvatron as i do with cartoon apocalypse from the x-men. hes clearly a megalomaniac. and TF or x-men are nucience.<br /><br />And in the same way i agree, he probably does undersetimate UM like Apoc does the x-men. but i do feel if his goal was to say kill UM, because for some reason it furthers his own goals and plans. i belive then hes at his most ruthless, and most deadly. otherwise his mind is normally pre-concerned with other evantualitys. its very much like Magnus shooting galvatron power siphon to gain his attention, or the scaring image that is roddy. these things can divert his mind from his given task, but where as for most TF they are the given task, for galvy they are a mere nucience.<br /><br />obviously this is a factor, his mental state. hormally id say that Magnus does have the ability to kill Galvatron. but galvatron to me is still the stronger, durable and faster of the two. id say skill wise they must be evenly matched. Megatron is a skilled fighter, galvatron must carry the same skills across. but now he can be more flippent with its useage because he can generally win just using brute force.<br /><br />But if Galvys plan of action, his goal, like building a large cannon ala T2006, id say he would most deff win as his mind would be focused.<br /><br />Oo im being sensible.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Metal Vendetta</span> on 07-03-2003 at 06:40 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Brendocon:<br /><strong>I dunno about the Shockwave fanciers, the Galvyholics or the Magnus Fanboyz.  What worries me are the amount of otherwise sensible people who don't see Optimus as the waste of ink he was...</strong><hr /></blockquote>Can we veto now (if it comes to it) Prime sacrificing himself in the final? I know it would run against tradition, but it's such an anti-climax. <br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Pops:<br /><strong>And Shockwave is a giant flying gun - care not.</strong><hr /></blockquote>And Prime is a truck. Last I heard, trucks made great targets for giant flying space guns. Even in robot mode, Prime's still just a target. And Shockwave's nuclear-powered  so he can keep firing all day, if you'll pardon the expression.<br /><br /> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Pops:<br /><strong>Magnus knows Galvatron's game. I really think UM could do a job on him. It would be very messy, but I'd fancy him.</strong><hr /></blockquote>Galvatron has known (Magnus'? Magnus's?) Ultra's game from the beginning, and as Impy said he's been toying with him all along. He had a certain fascination with Ultra Magnus (see pic above) and I got the idea that he rather enjoyed their little fights until Magnus hit back and really hurt him. I got the impression that next time Galvatron would lose his patience, stop playing with Magnus and just put him down...<br /><br />Actually this is off topic a bit, but did we ever find out what Galvatron and Megatron were planning to do together at the start of Time Wars? I doubt very much their overall plan was to just declare "Megatron and Galvatron stick!" and then fight all comers in a massive brawl until a passing timestorm wiped them out...was there actually a scheme behind it all or was it just totally random? </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">-Predaking-</span> on 07-03-2003 at 09:28 PM:</div>
<div class="relics_body"><strong>ULTRA MAGNUS Vs GALVATRON</strong><br />No contest. Even though Galvy is crazy but he's much more powerful than Megatron, which means Magnus is gonna bite the dust mighty quick if he so much as to annoy Galvy. <br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br />This is a tricky one.  Megatron is the ruler of Decepticons and one of the greatest fighters in Cybertron but can he match up against a freaking space ship?  I think I may give this to Megatron because while he may be fraction of Fort Max's size he's anti-matter blast can take out Fort Max if he hits the right spot.  <br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br />I love Shockwave and I think he can take on either Megatron or Optimus in 1-on-1 match but problem is.. he's power lies on his logistic system and in a straight out no holds barred fight he can't pull out the win so the match goes to Optimus.<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br />Another tough call.  My heart says Trypticon but my brain says Metroplex.  Trypticon it is, simply because you can't beat a Godzilla looklike.  </div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Successful Roller</span> on 07-03-2003 at 10:16 PM:</div>
<div class="relics_body"><br />Shockwave is underestimated aginst Optimus, and it was tough to choose winners, but here i go .........<br /><br /><strong>ULTRA MAGNUS Vs GALVATRON</strong><br />Ultra Magnus<br /><br /><strong>MEGATRON Vs FORTRESS MAXIMUS</strong><br />Megatron<br /><br /><strong>SHOCKWAVE Vs OPTIMUS PRIME</strong><br />Shockwave<br /><br /><strong>TRYPTICON Vs METROPLEX</strong><br />Metroplex</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">grymloq</span> on 07-04-2003 at 01:30 AM:</div>
<div class="relics_body">If its power master optimus there is no question in fact if its optimus at all shockwave is in trouble.  im afraid when it comes down to it old shockers just isnt that hot he is just a scared computer. hes been taken down by prime megatron cylclonus and scourge and when the ark crashed at teh end of G1 hes the only bloke whodidnt mange to come out of it at some stage, admittedly megatron and starscream had help but im ranting here so that dosent count.<br /><br />im also gonna stick my neck out and weigh into the galvatron debate.  there is a lot of talk of him being crazy.  but this was mainly a side effect of time travel.  come on this bloke beas all the autobots of the future ( in the rodimus prime and reality ).  dosent seem to crazy there and when hook line and sinker bring him back in time he is deffinetly not mad. in fact he is one of the most sane characters in that story arc.  as long as he dosent see megatron and get all confused this bloke has his wits about him. and i cant think of a single occasion where that magnus put him down without some sort of outside influence. sorry mate but galvatron in an arena is ging to the semis and ulra magnus well hes stuffed.<br /><br />if this is a straight fight competition in an arena well megatron grew up on this stuff, even before he got his cannon he was kikin the sh*t out of the best fighters on cybertron in the games, this is  his life he has to win. oh and i know this is G1 but lets face it in G2 weve seen him and max go it poor max.  in fact if were talking G2 megatron is it.... he's sane, more powerfull than ever, incredibly experienced if some alternate futre galvatron came back now hed be gettin the bludgeon treatment. which has got to hurt.<br /><br />and the battle of the exceptionaly big bits of metal ill say tryp to be different and lets face it whoever wins is gonna get masacred next round, any one of prime, megatron, galvatron and shockwave could all outsmart and outfight trypticon and if it is metroplex, well they would probably be walking around inside him pulling bits out before he even woke up, its not a question on wether he would be beaten but rather how much the winner wuld get for selling all the stuff that they got out of him.<br /><br />so the winners are prime, galvatron, megatron and well trypticon if i have to say one.<br /><br />oh and if we do talk G2; optimus prime forged by the swarm can quite clearly take on all comers no worries.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-04-2003 at 04:08 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by grymloq:<br /><strong>  but this was mainly a side effect of time travel.  </strong><br /><br />based on what? That's entirely speculation on your part, and considering the soure material for Galvy it doesn't seem the most obvious explanation for his insanity to me.<br /><br /><strong>dosent seem to crazy there and when hook line and sinker bring him back in time he is deffinetly not mad. in fact he is one of the most sane characters in that story arc.</strong><br /><br />Actually - thats kind of interesting - cos in ROD he definitely is mad, as iluustrated by his killing his own troops and banging on about himself for the first three pages in the third person, but once he comes back in time he seems to regain focus (so, er, effects of time travel then?)... altho he does go barking again once he bumps into Megs, and then again when he bumps into earth. Madness seems an inevitable destinantion for all the incsrnations of Galvy we have bumped into so far. IMO<br /><br /><hr /></blockquote></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Legion</span> on 07-04-2003 at 05:27 AM:</div>
<div class="relics_body">[QUOTE]Originally posted by grymloq:<br /><strong>If its power master optimus there is no question in fact if its optimus at all shockwave is in trouble.</strong><br /> only if he is foolish enough to get close to him. if shockers is flying around in cannon mode then it makes little difference!<br /><br /><strong>hes been taken down by prime megatron cylclonus and scourge and when the ark crashed at teh end of G1 hes the only bloke whodidnt mange to come out of it at some stage</strong><br />i don't recall Shockers actually going up against Cyc and Scourge... IRC he engineered the situation so that he wouldn't have to. Remember, one blast from Shockers fried Scourge!<br /><br /><strong>so the winners are prime, galvatron, megatron and well trypticon if i have to say one.</strong><br />Typticon?!<br /><br /><strong>oh and if we do talk G2; optimus prime forged by the swarm can quite clearly take on all comers no worries.</strong><br />possibly, but then again, we have no evidence of his abilities... but i admit, it would make sense that his new body would be s**t hot.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-04-2003 at 07:51 AM:</div>
<div class="relics_body">I guess the shockwave issue comes down to just how agile he is in that 40 feet gun mode? if hes as swift as a jet, <br />then prime might have problems avoiding him, or shooting him.<br /><br />But i feel that cannon mode is probably pretty slugish and static in comparison to a con jet. <br />so i dont feel its gonna be the ultimate advantage.<br /><br />Prime deals with shockers in gun mode in some of the first comics? forget the name and number as usuall...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Papa Snarl</span> on 07-04-2003 at 08:37 AM:</div>
<div class="relics_body">Shockwave has been battered by many, Prime has not.<br /><br />End of.<br /><br />No contest.<br /><br />Big flying gun = big flying target.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 07-04-2003 at 08:42 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong><br /><br />Prime deals with shockers in gun mode in some of the first comics? forget the name and number as usuall...<br /><br /></strong><hr /></blockquote>Prime Time Issue 40.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"><br /><br />Prime grabs Shockwave in gun mode and sends him flying. <br /><br />Hence my vote for Prime when against Shockers.<br /><br />That said, I too fancy Shockwave against Metroplex. I recall Aspects of Evil when he was blowing big holes in Autobot City.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-04-2003 at 08:42 AM:</div>
<div class="relics_body">i dont think shockwave is a match for oppy either- shockwave is second in power to megatron, this is stated in his specs, and Megsy has been defeated many times by Prime.<br /><br />allow me to compare:<br /> <img src="relics/Shockwave_ts001.jpg"> <br /> <img src="relics/OptimusPrime_ts001.jpg"> <br /><br />Oppy beats shockers on strength, endurance, courage and skill, matches him in intelligence and comes only one point lower in firepower.  I think its safe to say shockwave is outclassed.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Brendocon</span> on 07-04-2003 at 10:15 AM:</div>
<div class="relics_body">Optimus Prime = The p00pz0r5<br /><br />He got taken by the Constructicons in City of Steel, he got taken for a fool in Megatron's Masterplan, he got taken by Megatron (Megatron for crying out loud) in TF:TM (yes he did - it's only because Starscream's an idiot that nobody capitalised on it) and he got taken by a computer game in Afterdeath.<br /><br />Shockwave is capable of taking Prime.  Whether or not he would is a job for the writers to decide (ie, "is part 1 of a two-part story?" "is his toy being discontinued?").  Even glancing at the tech spec, Shockers just needs to ice Roller to gain the upperhand.<br /><br />Prime was a waste of space.  He only ever won stuff because they'd decided to make him leader.  They could have made Ironhide Autobot leader and given him high specs.  It still wouldn't have changed the fact that he's not worth the time and effort I've spent posting this.<br /><br />Incidentally, for those who've voted Trypticon on any basis whatsoever - he got taken by Wheelie.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-04-2003 at 10:45 AM:</div>
<div class="relics_body">I could take you all, from behind!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-04-2003 at 10:57 AM:</div>
<div class="relics_body">oooh is that an offer?</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-04-2003 at 04:26 PM:</div>
<div class="relics_body">Get in line baby.<br /><br />Er, but get in line in front of me.<br /><br />Hmm, i find Brendocon's arguement's unsually weak.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Brendocon</span> on 07-04-2003 at 04:36 PM:</div>
<div class="relics_body">I was at work, didn't have time to look anything up and really hate Optimus Prime too much to put any real effort into it.<br /><br />Sorry.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-04-2003 at 04:36 PM:</div>
<div class="relics_body"><strong>unsually</strong>? <br /><br />help me out, is that 'unusually' spelt wrong or 'usually' spelt wrong?  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">impactor returns</span> on 07-04-2003 at 04:37 PM:</div>
<div class="relics_body">If u take a sheep to cliff edge, then it has no where to go but back into you. get in line baby!  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Karl Lynch</span> on 07-04-2003 at 04:38 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by impactor returns:<br /><strong>If u take a sheep to cliff edge, then it has no where to go but back into you. get in line baby!   <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/redface.gif"><br /><br /></strong><hr /></blockquote>...gah?<br /><br /> <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/confused.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Optimus Prime</span> on 07-04-2003 at 10:29 PM:</div>
<div class="relics_body">ULTRA MAGNUS Vs GALVATRON<br />**Ultra Magnus** Magnus roughs up the crazy galvy with is close range fighting skills.<br /><br />MEGATRON Vs FORTRESS MAXIMUS<br />**Megatron** Megatron will find a way to topple the giant.<br /><br />SHOCKWAVE Vs OPTIMUS PRIME<br />**Optimus Prime** Lets just say it would be a major SHOCK if Prime lost this one.<br /><br />TRYPTICON Vs METROPLEX<br />**Metroplex** Metroplex turns him into scrap.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Metal Vendetta</span> on 07-05-2003 at 07:55 AM:</div>
<div class="relics_body">Hey, what are you all queuing for?<br /><br />*Joins back of line*</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 07-05-2003 at 11:59 AM:</div>
<div class="relics_body">D'oh!<br /><br />Hmm, mayhave to archive this topic when itd done...  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 07-06-2003 at 03:24 AM:</div>
<div class="relics_body">Voting closed!<br /><br />(Mainly because I'm out all day today and might not have chance to post later).</div>

 </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
</table>
</body>
</html>
