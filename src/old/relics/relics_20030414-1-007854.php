<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Archived Forum Discussion</title>
<link rel="stylesheet" href="css/transfans.css" type="text/css">
<link rel="stylesheet" href="relics/styles.css" type="text/css">
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr>
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt">

<h1>The Dreamwave Disaster</h1>
<h2>Thread originally posted by Cliffjumper</h2>


<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 12-14-2002 at 02:07 AM:</div>
<div class="relics_body">Essay I've written for the next update on my site, I thought I'd give it a dry run here... 'This page' refers to issue 3, p15.<br /><br />'I'm sure I wasn't the only one who jumped for joy when they heard that the original Transformers were returning to the comic book medium towards the end of 2001. I'm sure I'm not the only one who drooled over those glorious posters of Optimus Prime and Megatron. And I'm also sure I'm not the only one with half-a-dozen of these comics who is rather disappointed. Last month, Dreamwave's six-part G1 mini-series finally jolted to an unsatisfying halt, with the promise of a further six-part series in 2003 rousing only a fraction of the interest the first had.<br /><br />Put simply, the series was a poorly-paced mess, with some very mixed art, poor dialogue and third-hand plots. There are bright moments in the series - the first battle in San Francisco, Prime's do-or-die charge on Devastator, a terrific choice of basic character models and good showings by a couple of under-used characters, such as Trailbreaker.<br /><br />But this is far outweighed by many, many failings. First of all, the story-pacing itself. I'm ignoring the Preview, as it's a waste of everyone's time, and doesn't cover anything not dealt with in the first issue. G1 #1 itself doesn't feature a single line of Transformer dialogue, and scant Transformer appearances to boot. Instead, the star of the show is the achingly terrible Lazarus. I can't say an awful lot about Lazarus that hasn't already been said by Walky [see <a href="http://www.itswalky.com/transformers/dumbass/" target="_blank">http://www.itswalky.com/transformers/dumbass/</a>],  aside from saying he's that old horror/sci-fi clich�- the villain who thinks he can control the sleeping monsters. Lazarus' fate would seem so obvious that it would seem to be a double-bluff - maybe he's going to pull it off? But no, Dreamwave aren't that clever. Unsurprisingly, by the start of issue 3 Megatron has Lazarus literally on a leash, and by the conclusion of that book he's dead. That's not all. After precisely 24 pages of not an awful lot happening, we get a two-page newspaper facsimile telling us all the things that could have been carefully expounded over the first issue, and the preview. The latter could have been a gloriously teasing confusion of flashbacks, showing some of the scenes from Operation: Liberation, the launch and crash of the Ark II, the disbelieving reactions. Issue 1 could have moved a lot faster. But no, we get around 30 pages of a tedious clich� expounding on how great the Transformers are. I know terrorism is very early 21st Century, but that doesn't make it interesting. This is a six-issue limited series - the emphasis for such a nostalgia-driven comic should have been uncomplicated fun, showcasing everyone's favourites.<br /><br />Another character cliched beyond belief is that of General Halo. Reading the story as it unfolds, Halo seems like a very interesting chap. But no, he's another mad general operating above the president with an itchy nuke-trigger finger. Oh, joy. The characterisation throughout is terrible, as is the lack of originality. Spike's spite towards Prime makes him look like a hypocrite, an accusation redoubled after he decides the Autobots are alright after all in the final issue. Dreamwave seem to have taken Optimus Prime and Megatron as simply being Professor X and Magneto, with peace/war binaries. This is reinforced by Megatron's constant attempts to persuade Prime that the humans really aren't worth saving. Incidentally, the moment Prime find his side of this tedious debate to be true seems like a horrible piece of post 9/11 populism, with a squad of firemen driving their trucks into Megatron. Also in the final issue Superion dies, saving San Francisco from a nuke. I for one found little in the series to show that he had any character, and thus felt myself caring on the same sort of level as I would if a nuke hit the moon - 'oh my. Well, at least no-one was hurt'. <br /><br />The plot is wildly inconsistent. At least 17 Autobots escaped a spaceship exploding in the atmosphere without any noticeable damage. Around half of these, including someone the size of Superion, go unfound by either the US government or Lazarus, and appear to be scattered rather nonchalantly around the North pole. At end end of issue 3, Halo drops a nuke on the warring factions. It knocks the Autobots into a wall, and damages the Decepticons to the extent that they recover before the Autobots and are able to get to San Francisco. The problem with a devastating cliff-hanger is that you have to find a plausible way to get out of it the next issue. Again, Dreamwave fail miserably. Then you've got a guy like Larry, who knows all Halo's past misdeeds. Does Halo have him quietly killed? No. Does he post him to some faraway outpost in Alaska? No. He gets him to work as a janitor in his HQ. Genius. Then you've got Grimlock. The Dreamwave team firstly lift something done in a first season episode ["War of the Dinobots"] then does nothing interesting whatsoever with it. It wouldn't be uncharitable to suggest that the latest of the release of Issues 5 & 6 were because the Dreamwave hacks were reworking the last two issues to bleed a second series from the line, leaving the Grimlock plot thread dangling. More inconsistencies ring out - Superion isn't knocked down into the individual Aerialbots by the explosion of the Ark II, or Operation Cleanup, but a few missiles from Starscream & Co. do the job. Now, I've hesitated to name Chris Saccarini as the offender for the turgid story as I'm really questioning whether he was so much a writer as a scripter. His work on Armada [at least the two issues that have reached me so far] has been impressive, and shown a good grasp of narrative structure. I honestly think Pat Lee had total control over just about every aspect of this series, and would even suggest that to some extent his art came first, with Saccarini left to fit his dialogue around it.<br /><br />Pat Lee's art itself is often less than stellar. He frequently uses near-identical frames to express something simple. For an example, check out this page from issue 3. I really cannot imagine a writer handing in a page of plot that reads: -<br /><br />PAGE 15<br /><br />Five equal landscape panels<br /><br />OPEN ON: Arctic landscape<br /><br />DISPLAY LETT; Nearby...<br />FRAME 1: SUNSTREAKER, SIDESWIPE, JAZZ, OPTIMUS PRIME, MIRAGE, TRAILBREAKER and WHEELJACK driving towards in car mode.<br /><br />FRAME 2: Getting closer<br /><br />FRAME 3: Getting closer<br /><br />FRAME 4: Getting closer, transforming<br /><br />FRAME 5: Getting closer, robot modes<br />OPTIMUS PRIME: No...<br /><br />Can you really see any self-respecting writer sitting down and scripting that? No. I think it's much more likely that Saccarini submitted a brief synopsis to Lee, who then went away and drew the issue, and Saccarini then had to script around the pencils. Some of Lee's art is also deformed and ugly, especially considering the age of computers, where it's possible for Lee to draw on any sized canvas, scan the image and resize it to the frame size required. The fight scenes are often indistinct and undynamic, most notably issue 3's conclusion, where both sides appear to shuffle up to each other and pose. At other times it's difficult to tell what's actually going on. A prime example of this would be the conclusion of issue 4, where Devastator defeats Superion [with a little help] and is standing unopposed over the city. The final splash was so badly drawn that most online forums saw considerable debate as to what exactly Devastator was staring up at, with suggestions such as Omega Supreme, Jetfire and the Dinobots. But no, he'd been roaring in triumph. Once someone's pointed that out, and we've seen the start of Issue 5, this seems quite blatant, but the fact there was such an animated debate suggests that Lee needs to take a course in visual storytelling.<br /><br />There were a number of other things which irked me about the Dreamwave series I'll run through briefly. The commercialism of the project was deeply irritating. Okay, so Dreamwave need to make money. Does this really justify 6 different covers for issue 1, three for issues 2 and five and two for the other three issues? No. Much of this, especially the new reprint covers, reeked of bleeding money from completists. Now, the argument often given is that Marvel were often just as bad. No, no they weren't. Marvel tended to make 2 covers of first issues or other event issues, and they were often of a limited edition variety - to give the example of X-Men Alpha, a number of limited edition holofoil covers were released. When these had been sold and demand was still there, a version featuring the same art work printed on the normal paper was issued. When this too sold out, a second printing was done. This differed from the non-holofoil release by having "2nd printing" written in the white information box on page 1. Few X-Men fans feel the need to buy the 2nd & 3rd copies. The argument that Dreamwave are a small company that needs money seems a little weak when you consider that 3 generous printings had sold out on Issue 1, and two of Issue 2, before these new variants were made available. On top of this, the alternate covers tended to feature characters that didn't appear in the whole series, let alone in the issues they adorned - even the recent trend for irrelevant covers shown by New X-Men is nowhere near as misleading. The posters and lithographs seemed to have priority over the actual series, leading to the last two parts arriving unacceptably late. The final insult which came to light recently was that the trade of the series will include an exclusive Preview of the second Generation 1 mini, which may force some fans to buy some of the material within for the umpteenth time.<br /><br />That just about seems to sum the Dreamwave mini-series up. Thankfully, the DW team chose to make their own continuity for their little extortion piece, which means Transformers fans won't have to acknowledge it. Expect this one to rot with the Ladybird books and Choose-Your-Own Adventure series. It deserves it.'<br /><br />It needs some minor revision [e.g. Hallo is mispelt throughout], but just thought I'd gather some other opinions before I put it online...</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Computron</span> on 12-14-2002 at 02:50 AM:</div>
<div class="relics_body">Nice one QuickJumper. Sums up the whole DW comic issue perfectly.<br /><br />But I'm still going to buy volume two...I'm such a mindless drone... <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/biggrin.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">spiderfrommars</span> on 12-14-2002 at 02:53 PM:</div>
<div class="relics_body">You've pretty much summed up everyone's major misgivings about the series.<br /><br />You were right to particularly expose Issue 3 for the mess that it was. That was definitely the nadir of the whole affair.<br /><br />However, personally I make no concessions for Saccarini as a storyteller. As far as I'm concerned he is below par, and his Armada work IMO is even worse. <br /><br />The only thing hopeful about the next series is that Saccarini's script is having input from Simon Furman. Even after all these years, there's still only one guy who can breathe life into these metal machines.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Weirdwolf and Monzo</span> on 12-14-2002 at 08:04 PM:</div>
<div class="relics_body">While I didn't dislike the series quite as much as that, I do agree with some of your senitments.  The series didn't have much depth--I mean, I could sit and read an issue in less than 5 minutes.  In fact, if I remember correctly, someone said they once read an issue while sitting waiting for a traffic light to change on the way home.  Not much conent, I'm afraid, though I was pleased with the artwork for the most part.  Dialogue seemed indirect at times and seemed to slow down any action that was building, and sometimes left me confused as to what in the world was going on.  A couple of times I had to go back and reread the previous issue just to get a better handle on what was going on in the present issue.  <br /><br />But, I'm not too disappointed, and I'll be buying the next volume as well.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 12-14-2002 at 09:55 PM:</div>
<div class="relics_body">One - lovely to see you here mate.<br /><br />Two - bang on - alomst as good asmy issue 6 rant floating on page 3 or so of this forum.  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif">  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/smile.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Prof_Smooth</span> on 12-15-2002 at 02:17 AM:</div>
<div class="relics_body">See this proves that the Dreamwave books were a success.  True fans hate EVERYTHING.  This is right up their alley!</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 12-15-2002 at 12:32 PM:</div>
<div class="relics_body">As a test last nite, I read the whole series through without lingering on the artwork... It took 23 minutes. And that includes the time it took to get them out of the plastic bags and back in again...<br /><br />Oh, and who says I'm a true fan  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/wink.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">maximized</span> on 12-15-2002 at 10:49 PM:</div>
<div class="relics_body">wow...I have quite enjoyed the new comics  <img src="http://www.TransFans.co.uk/phpBB2/images/smiles/frown.gif"></div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Gekigengar</span> on 12-15-2002 at 11:45 PM:</div>
<div class="relics_body">You pretty much summed up on how I also felt about DreamWave's TF series, or for any DW's comic. Unfortunately, they are trying to do that Manga or Jim Lee feel in which they do so miserably.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Orion Pax</span> on 12-15-2002 at 11:52 PM:</div>
<div class="relics_body">*yawns*<br /><br />Keep complaining and maybe they'll cancel all the TF series.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Death's Head</span> on 12-16-2002 at 06:05 AM:</div>
<div class="relics_body">You have to wonder how hard it is, though? I mean, he gets to write about things he's thought were cool since a kid....and he chooses a bloody nuclear weapon plot?<br /><br />Truly, a child of the Reagan/Bush era :P</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Best First</span> on 12-16-2002 at 07:57 AM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Orion Pax:<br /><strong>*yawns*<br /><br />Keep complaining and maybe they'll cancel all the TF series.</strong><hr /></blockquote>Ot maybe they'll make a good one - WW is only happening cos loads of us shouted at dreamwave to get furman on board in the first place.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">Cliffjumper</span> on 12-16-2002 at 01:30 PM:</div>
<div class="relics_body"> <blockquote><font size="1" face="Verdana, Arial">quote:</font><hr />Originally posted by Orion Pax:<br /><strong>*yawns*<br /><br />Keep complaining and maybe they'll cancel all the TF series.</strong><hr /></blockquote>Yeh, actually I'd best keep my voice down and make Dreamwave think taking about �15-20 of my money for no return is actually okay by me so they don't raise their game.<br /><br />Yeh, Lee sitting on his pile of cash is really going to cancel the series because I don't like it... You guys better all top being nasty about Armada, or Hasbro will stop making Transformers  <img src="relics/rolleyes.gif"><br /><br />If you're prepared to wallow in mediocrity, that's fine by me. But don't get in my way, or expect your comments to carry any kind of interest.</div>
&nbsp;

<div class="relics_head">Posted by <span class="relics_name">bobaprime85</span> on 12-16-2002 at 06:32 PM:</div>
<div class="relics_body">I think it is safe to say it sucked. </div>


 </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
</table>
</body>
</html>
