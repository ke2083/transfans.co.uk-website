<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Steve White</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Steve White<br>
              </span><span class="blackstrong">interviewed in:</span> May 2007</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/white.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
		Things are coming full circle... the UK once more is getting comics with material original for its market, courtesy of <a href="http://www.titanmagazines.co.uk/" target="_blank">Titan Magazines</a>. Heading up efforts is Steve White, who brings a Marvel UK connection and has been instrumental in getting other creators adult fans will recognise on-board!
                </td>
              </tr>
            </table> 
       
	     <p><br><strong>Thanks again for agreeing to do this interview. </strong></p>

    
            <p><strong><span class="head2">1.</span></strong><strong> First of all if we could look back on your career, all the way back to Christmas 1986, you made your debut as colourist on the Marvel UK Transformers comic. You worked on the majority of the UK strips for the next year or so. What was that time like?  </strong></p>
	    
            <p>It was thrilling for me. Christmas 1986, I'd only been with the company for about eight months and was employed as a colour separator &ndash; a now-lost art form practised when we used to use the mechanical method, where all the colours were split between three percentages of three colours &ndash; yellow, magenta and cyan. Mixing and matching these percentages made the printed colour and basically it was my job to use special pens to fill in all the elements of each percentage on nine sheets of acetate overlays. It was a laborious and time-consuming job but it kind of got me on the radar.</p>

<p>Saying I was colourist is actually a slight misnomer. What the colourist used to do was colour up a photocopy of the black and white artwork with markers that acted as guides to the separators, which then had to have all the colours marked up on which was pretty tedious. Hopefully this all makes sense &ndash; as I said, it's kind of a lost art!</p>

<p>Anyway, I started doing colour roughs freelance on the likes of such junior titles as Care Bears (the first comic I ever had a credit in...). I seemed to have developed a reputation as a halfway decent colourist. At the time, the Transformers artwork was all fully-painted and at some point the decision was taken to shift to cheaper mechanical colouring. It was weird at the time as the Boys Action Dept. never spoke to the Junior Dept where I worked. So one day I was suddenly summoned to their office and it felt like being called before the Jedi Council. The editor, Ian Rimmer, asked if I was interested in doing colour roughs for Transformers and I was a) completely thrilled, and b) surprised they even knew who the hell I was. It took a bit of nipping and tucking to settle on the final method, but it ended up being great fun &ndash; although the early colouring still reflected my Junior influences, as there's so much bloody pink on it!</p>

<p>I started Action Force not long after, and kind of went from strength to strength. We then started the likes of Thundercats and Ghostbusters, so it become something of a halcyon period for me and all my Marvel UK peers. Still seen by us as something of golden age. It was intensely busy, because I was assistant editor then editor on Action Force, then took on Thundercats, both of which were weeklies, so the deadlines are insane and we used to burn out pretty quickly. Then, at 5.30, you'd down tools and pick up your markers to start colouring. We often stayed in the office until 9-10 at night, then go for pizza or drinks. I was newly married at the time and, as Tom DeFalco once told me, comics and marriage don't mix. He was right, of course. Still, I loved it. We had a big social scene, lots of gorgeous girls, and a heady mix of hormones, all being in our early 20s. Lots of inter-office romances! Wouldn't have missed it for the world! </p>
 
            <p><strong><span class="head2">2.</span> Did you have any artists who you particularly liked to work with? </strong></p>
	    
            <p>My favourite artist was always Geoff Senior on Transformers &ndash; not just because he was the best to colour but just because I loved his style. It was a great moment for me when he drew the first strip I ever wrote (for Action Force) and then gave me the artwork! As a colourist, I loved working on his pages because they were so clean and crisp. Some artists didn't finish their line work and you'd be colouring away and suddenly have nowhere to go. Geoff remained my favourite right up until I got to colour Alan Davis on Captain Britain. That was a real career high point. </p>


            <p><strong><span class="head2">3.</span>  Action Force lasted 50 issues before turning into a monthly, and shared many of the same creators as Transformers yet could never quite match their success in the UK. Why do you think that was? 
 </strong></p>
	      
            <p>Action Force was one of those strange properties that had everything going for it but, for whatever reason, just didn't hit the right mark with the readers. Maybe it was the influence of Action Man in the UK, or, being a militaristic title in the mid-80s was unfashionable. Just one of those quirks of fate I guess. </p>

            <p><strong><span class="head2">4.</span> What have you been involved in since Transformers fans may have last heard from you? 
 </strong></p>
	    
         <p>I was 'retired' from Marvel at the end of 1991, and by chance ended up almost immediately as Senior Editor at Tundra UK, which had just been set up by Teenage Mutant Ninja Turtle creator Kevin Eastman. In one of those strange twists, I was sharing a flat with the girl who became managing director because her boyfriend was friends with Kevin, and the three of them decided to set up the UK branch of his American publishing company. That was a wild time &ndash; we were a classic case of the light that burns twice as bright burns half as long. Huge amounts of money were thrown around but there wasn't even a publishing schedule, and things turned grim very quickly. I quit in mid-'93 just a couple of weeks before Kevin shut it down. It's of course with no small sense of irony that I find myself senior editor on the new Turtles title...</p>

<p>Anyway, my real undying passion is dinosaurs. In case you haven't spotted the connection, 1993 was Jurassic Park Year. Every publisher in Christendom was looking for anyone who could draw dinosaurs and I had delusions of grandeur that I actually could. I managed to blag an art job with a part-work company, Orbis, doing black and white line art spreads. There was only meant to be 14, but such was the success and resurgence in interest in dinosaurs that I ended doing over 250. It was good money as well, and I could turn them around pretty quickly.</p>

<p>I kind of settled into freelancing, colouring on Sonic The Comic and the junior Judge Dredd title when that came out. I also managed to do a fair bit of writing, including Rogue Trooper for 2000 AD, another dream come true, even if the fans hated it. I had several very successful years and blew all the money learning to scuba dive then travelling the world swimming with sharks!</p>

<p>Then bloody Walking With Dinosaurs came out and more or less killed my career. See, editors are by and large an unimaginative bunch and pigeonhole creators. I was the dinosaur guy. But I never really got into computers and suddenly all editors wanted were CGI dinosaurs &ndash; no matter how crap they were &ndash; and they were all pretty crap.</p>

<p>The freelance pretty much dried up and I was forced to take a job in a book shop just to pay the rent. However, I did (and still do) keep drawing and writing (results can be seen at: <a href="http://thunderlizard.gn.apc.org/" target="_blank">thunderlizard.gn.apc.org</a>). Don't let anyone tell you working in a book shop is fun. It's not. It just makes you realise people are actually idiots.</p>

<p>I was finally rescued by Simon Furman, then working at Titan, who managed to get me into a job in the book department as graphic novels editor. I started in March 2003 and moved into the magazine department, mainly to edit RAF Magazine (I love fighter planes...) but the originated comic stick was dangled before me as well. I launched the official SpongeBob SquarePants comics at the same time, but I'd always secretly hoped to develop a true comics department and was finally able to a couple of years ago, taking on all of Titan's various comics (Simpsons, Star Wars) whilst launching Titan's first originated title, Wallace & Gromit. Shortly after, we managed to pick up the Transformers license from Panini, and sat on it until the movie was announced. So, more or less 20 years on, things have come back full circle and I'm back doing Transformers and originating comics again.</p>
	      
            <p><strong><span class="head2">5.</span> Titan have also been instrumental in reprinting many of the classic Transformers stories in trade paperback releases... 
 </strong></p>
	    
            <p>When I started at Titan, the collections were already underway, and were a surprise hit at the time. Simon had been instrumental in persuading Titan to do the collections and was totally vindicated by people who thought that Transformers was kind of old hat. I did pick up the torch when Si left to start writing again, which was fun as I got to commission some of the hardback exclusive covers using some great artists.  </p>

            <p><strong><span class="head2">6.</span> So onto the brand new comic itself - what UK exclusive comic material can we expect in the new publication? </strong></p>
	    
            <p>So far we have six self-contained stories written by Simon that although are stand-alones, will tie straight into the IDW prequels. After that, we're pondering our options; we have contracts for all the various Transformers properties so who knows where we'll end up? </p>

            <p><strong><span class="head2">7.</span> What homegrown talent have you got lined up to work on the project? 
 </strong></p>
	    
            <p>Aside from Simon (obviously) and Geoff Senior, Andy Wildman and Nick Roche will be doing art for the following two issues. I'd like to see if I can get some of the other 'classic' artists interested but I don't want this to be a complete nostalgiafest. We have to bear in mind we'll be picking up a whole new generation of fans who only know it from the film, so we can't afford to alienate them, especially as there'll probably be a lot more of them!  </p>
	      
            <p><strong><span class="head2">8.</span> So is it aimed at the younger fan, like 2003's Panini Transformers Armada comic, or pitched at an older demographic this time?
  </strong></p>
	    
            <p>Well, in the words of Han Solo, that's the real trick isn't it. We don't want to make, for want of a better word, the mistake that Panini did. Even at the time, I thought they'd aimed it way too low. Okay so it was 15 or more years ago and times have changed, but the Marvel UK one never made any attempt to talk down or play the age group game. We just did what we felt was right and it seemed to work. It's the route I intend to follow, although there are certain constraints now, such as the need for free gifts on every issue. That in itself has been a nightmare for our marketing department, who have no real experience on a more male-orientated older title and have to come up with ideas that don't just rely on some piece of cheap plastic &ndash; Transformers fans won't really be interested in that. However, they're constrained as much by money &ndash; yes, we'd love to have a Minicon on the first issue, but we have to get real. Still, it has led to some imaginative solutions, so all power to them. </p>

	    
            <p><strong><span class="head2">9.</span> We'd wager that you never expected to still be involved in Transformers comics over 20 years on. How has the comics market in the UK changed and what will it take to make this new publication a success? 
 </strong></p>
	    
            <p>I expect to be buried in a transforming coffin... The market's changed a hell of lot since the ol' days. Boys action titles are an endangered species, which I think is a reflection on the rise and rise of computer games &ndash; that was on the cards when I was at Marvel. I hear a lot of veteran comic creators complaining British comics are dead. I disagree &ndash; it's just changed. The kind of comics they worked on just don't exist anymore. It's much more junior now, and driven as much by the free gift on the cover as by the content. I'm hoping Transformers will be the exception that proves the rule. </p>

            <p><strong><span class="head2">10.</span> When is the new comic out, and how can fans contribute? What features, other than stories will the comic contain? 
 </strong></p>
	    
            <p>The first issue launches 19 July &ndash; a week before the movie release. As well as the strip, we're running character profiles and a fun Celebrity Death Match-type feature along the lines of who would in fight between..?, both written by Simon. There'll be three strips: the IDW prequels, the originated prequel material, and Beast Wars reprints, just for a change of pace.   </p>
	    
            <p><strong><span class="head2">11.</span> Have you seen the movie scripts? What do you think we can expect? 
  </strong></p>
	    
            <p>I have read the script. I also got to see some animatics which looked awesome. Whatever we may think of the script won't matter once the robots arrive.  </p>
	    
            <p><strong><span class="head2">12.</span> What else is on the horizon for you? 
 </strong></p>
	    
            <p>Thundering into view right now is the Dreamworks Animation title I'm editing, which is being released in June in time for Shrek The Third &ndash; a fully originated anthology title that runs the gamut of the Dreamworks properties &ndash; Shrek, Madagascar, Shark Tales and the various new ones in the pipeline. The comics dept. is generally expanding so I really have my hands full right now &ndash; Transformers is just part of the plan, even if it happens to be the one dearest to my heart.  </p>
	    
            
            <p><strong><span class="head2">13.</span> And finally, some word association: </strong></p>


	    <p><strong>The Real Ghostbusters    </strong></p>

            <p>Puns!</p>
	    
            <p><strong>Bumblebee   </strong></p>

	    <p>100% yellow </p>

	    <p><strong>Judge Dredd  </strong></p>

	    <p>The Kleggs  </p>
	    
	    <p><strong>Dan Abnett  </strong></p>

	    <p>Prolific  </p>
	    
	    <p><strong>Death's Head   </strong></p>

	    <p>Yes?</p>
	    	    
	    <p><strong>Cats   </strong></p>

	    <p>Kill Snarf</p>
	    
	    <p><strong>Mr White, thank you very much for your time!</strong></p>
	    
	    
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/white1.jpg"><img src="images/white1sm.jpg" style="border:2px white solid;"></a><br>New TFUK #1 cover</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/white2.jpg"><img src="images/white2sm.jpg" style="border:2px white solid;"></a><br>Geoff Senior artwork</td>
	       <td class="subcolor" style="padding:6px;padding-left:0;" align="center" valign="top"><a href="images/senior1600x1200crop.gif"><img src="images/senior1600x1200cropsm.jpg" style="border:2px white solid;"></a><br>Steve was kind enough to send a shiny high-resolution version, so here's a crop to use as a wallpaper base! (1600x1200)</td>
	      </tr>
	      </table>
	      
	      <p>As a reminder, Steve's portfolio site can be found at: <a href="http://thunderlizard.gn.apc.org/" target="_blank">http://thunderlizard.gn.apc.org/</a></p>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
