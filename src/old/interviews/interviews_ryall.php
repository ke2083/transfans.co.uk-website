<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Chris Ryall</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Chris Ryall<br>
              </span><span class="blackstrong">interviewed in:</span> November 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/ryall.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">Many Transformers fans were surprised earlier this year when <a href="http://www.idwpublishing.com/" target="_blank">IDW Publishing</a> seemed to pop up out of nowhere to land the license ahead of its competitors. In fact the company has long experience of working with licensed properties, including CSI, 24, Angel, Shaun of the Dead and many more. Chris Ryall is Editor-In-Chief of the company and is playing a large role in bringing the Transformers up to date in the 21st century. He's also Editor-In-Chief of Kevin Smith's website, transforms into a nifty sports car and has red on him.</td>
              </tr>
            </table> 

<p><br><strong>Hi Chris, many thanks again for agreeing to do this interview with us.<br><br><span class="head2">1.</span>  How did you get into the comic business?</strong></p>

<p>In a sort of roundabout fashion. I'd been sort of on the outskirts of the fringes, doing reviews and such. I'd worked with Stan Lee in the past, while he was running Stan Lee Media and I was working for Dick Clark, and I'd been invited to write for Marvel's aborted Epic line. All of which added up to not much more than a check with "Kill Fee" printed on it from Marvel. I got to know Steve Niles a bit, and a year and a half ago, I was working two gigs, one as an advertising copywriter and also running a Web site for Kevin Smith. I was/am the editor-in-chief of Kevin's site, Movie Poop Shoot.com. In June of '04, Niles called me and asked if I'd be interested in doing the same for IDW Publishing. I came down to San Diego and met with the partners and outgoing editor-in-chief Jeff Mariotte, and it all seemed like a nice fit on both ends, so I talked the wife into moving down to San Diego from LA and here I am, 16 months later. I since added "Publisher" to my "Editor-in-Chief" title here, too, and still find time to run Kevin's site in the off-hours, too.</p>

<p><strong><span class="head2">2.</span>  Transformers: Infiltration Issue 0 is now out. What has the ride been like so far?</strong></p>

<p>It's been like no other book I've worked on here. The response has been phenomenal, and the fan opinions have been stronger and more varied than anything I might've expected. People don't just seem to like the Transformers&#8212;they are passionate about them, and they all have very definite ideas of what should or should not occur in the comics.</p>

<p>I've always been a big fan of letters columns in comic books, and wanted to bring them back to IDW's books. I did that� and the mail was hard to come by on many of our books, for whatever reason. Until Transformers, that is. The Transformers is a lettercol dream franchise. I love all the response, good and bad, and the chance to get in and mix it up with fans, both there and on our message boards.</p>

<p><strong><span class="head2">3.</span>  Why did you choose Simon Furman and EJ Su for your first Transformers release?</strong></p>

<p>I wanted Simon because I knew he understood what worked and what didn't work better than anyone else. And I knew that if he was set free to relaunch the franchise, away from any restrictions or dictates, that he could turn in an exciting story. And he's doing just that&#8212;I really love the new direction for the title.</p>

<p>With EJ, I wanted an artist that had a very different style than the Dreamwave books&#8212;this needed to be IDW's take on Transformers, not just a retread of what others have done, you know?</p>

<p>EJ's Transformers are maybe better than anyone's I've seen before&#8212;well, I love what Don Figueroa, James Raiz and Guido Guidi have done, too. We were a bit unfair to EJ at first, giving him 16 pages where largely no Transformers show up. And I knew that fans would be scrutinizing his work closely, so not allowing him to fully show what he can do meant that he had to take some slack from people who just want Transformers. But at the same time, it was important for us to set up a larger universe, and tell a real story, rather than take the easy cheat of just showing robots fighting for no reason. I can say that when the people who were critical of the #0 issue finally see EJ's Transformers, they're gonna eat their words. His stuff looks so good.</p>

<p><strong><span class="head2">4.</span> After 'Infiltration' (a six-parter) can we expect any shorter story arcs as part of the main ongoing title?</strong></p>

<p>You can expect� something special� between the first storyline and the second one. More on that� eventually.</p>

<p><strong><span class="head2">5.</span>  Describe the process of editing a title like Infiltration.</strong></p>

<p>It all started in many conversations with Simon before we ever got rolling, talking over what we wanted to do, how we wanted to do it and what direction we wanted the title to take. We talked about what characters we planned to use, how to best use them, where we'd go once the initial story was done� so many things. Dan (Taylor, IDW's Editor) and I also spend a fair amount of time developing good covers for the books and plotting future stories. It's all been a very collaborative process, which is always the best thing about working on these books, the sharing of ideas.</p>

<p><strong><span class="head2">6.</span>  So what do you feel is the essence of Transformers? What should a Transformers comic fundamentally deliver?</strong></p>

<p>I don't have lofty goals for the comic&#8212;I want from it what I want from any comic I read, a compelling story that keeps people wanting more and interesting art. I also want the big battles and cosmic stories to mean something and feel earned, not just be a fallback every month. The essence of Transformers is the essence of Spider-Man, of Superman&#8212;it's a concept that has universal appeal to people from age 5 to 50 and beyond. The comics should offer something that doesn't condescend to kids or talk down to adults. I want this book to prove that the property is much more than the "'80s relic" that some nay-sayers try to classify it. The appeal of the property is there, and now the onus is on us to capitalize on that appeal.</p>

<p><strong><span class="head2">7.</span>  Apart from keeping dealings honest and above-board, what do you think you can learn from the way previous companies have handled the license? What do you think Dreamwave and Marvel got right?</strong></p>

<p>I think both companies did things well, and I think both did things that I wouldn't have done. Rather than catalog the things I appreciated about what they did, I've spent more time seeing what the fans thought of what's come before. After all, it's not about what I think as much as it is what the people who buy these books think&#8212;they're the arbiters of what works and what doesn't. So I spent all kinds of time on various TF message boards (including this one) and got a good sense of what worked and what didn't. We still had our direction set, but some of the comments definitely helped keep us on what we think is the right track.</p>

<p><strong><span class="head2">8.</span> How does the Transformers fanbase compare to those of your other licensed properties?</strong></p>

<p>Like I said above, the level of passion that TF fans have for the property seems unequaled in regards to anything else I've worked on (Angel and Spike is a close second). It's always great getting on the boards and talking directly to the people who read the comics; it's one of those things that I'd love to do more, but it's just hard to find the time and also keep the company running. The main thing it taught me is that we damned well better have our stuff together on these books. I love that.</p>

<p><strong><span class="head2">9.</span>  You've read some of the old Transformers comics. Can you tell us what you particularly enjoyed / didn't enjoy?</strong></p>

<p>I'll admit I like the comics where the panels and the characters have a bit of room to breathe&#8212;some of the DW comics just felt so overcrowded and overcolored that it was hard to make sense of what's what. I also always like the "robots in disguise" aspect of the Transformers more than just mindless robot fighting (hence our new direction), even though that can be fun, too.</p>

<p><strong><span class="head2">10.</span>  Is there any chance that stories not published as part of Titan's trade paperback range will be considered for the IDW classic reprint issues, or are you limited to the material already collected (by agreement or the availability of masters)?</strong></p>

<p>We're still working out the specifics of what all we can and can't do. But essentially, everything Hasbro owns, we have access to. There are some Marvel issues that feature Marvel characters, so we can't touch those.</p>

<p><strong><span class="head2">11.</span>  Who is your favourite Transformer? And which one would you like to set to with a hammer?</strong></p>

<p>In reverse order, I honestly wouldn't want to beat any of the characters into scrap as much as I'd like to remake them into someone interesting. The great thing about this "reboot" approach is that it allows us to maybe improve upon some bad characters and make them interesting again. Even bad characters can be good if written properly. In fact, I just might have plans to do something along those lines�</p>

<p>As for favourite, it's hard not to like Ratchet. He's someone you could be friends with, you know? As much as you could be friends with any huge, alien transforming robot. Prime is like Captain America&#8212;he's good and all, but he's a bit too good, not really someone who'd be easy to hang out with.</p>

<p><strong><span class="head2">12.</span>  Which other previous creative/artistic talents can we expect you to bring on board? Can you confirm or deny recent rumours of a Budiansky comeback?</strong></p>

<p>I can confirm I've been trying like crazy to track down Bob, to no avail thus far. So I'll throw this out publicly&#8212;Bob, drop me a line to <a href="mailto:chris@idwpublishing.com">chris@idwpublishing.com</a>. Love to talk to ya. Likewise, if anyone knows how I can reach Bob, I'd love to hear from you.</p>

<p>As for other creators, you'll see some in the form of variant covers (Don, Guido, James, Andrew Wildman, Nick Roche) and more to come in the form of� details to come.</p>

<p><strong><span class="head2">13.</span>  Can you give us any further information about what Transformers fans can look forward to next year from IDW?</strong></p>

<p>Well, a four-part Beast Wars miniseries by Simon and Don, just in time for the 10th anniversary of Beast Wars, begins in February. Don's art is so amazing� he did this gatefold wraparound cover, one quadrant of which is on the new issue of Diamond's Previews, that's just breath-taking.</p>

<p>Beyond that, the reprints of the old material start in March in the form of $1.99 comics, and I'd suggest that all Transformers fans pay special attention as we get closer to next July� I don't want to say more yet, since issue #1 is still a little over a month away, but we have big plans for the Transformers over the next few years.</p>

<p>In goofier news, "ChrisCharger" (first appearance in The Transformers #0, just to make it easy for the Overstreet Price Guide folks)  will be answering your mail in TF #1 and beyond, so if you have anything to say after you read the books, drop me a line.</p>

<p><strong><span class="head2">14.</span>  And finally some word association...</strong></p>

<p><strong>Simon Pegg</strong></p>

<p>Slice of fried gold.</p>

<p><strong>Quintessons</strong></p>

<p>Loki.</p>

<p><strong>Serenity</strong></p>

<p>Bad timing.</p>

<p><strong>Don Figueroa</strong></p>

<p>Awesome. Stunning. Fast and amazing. I could go on and on.</p>

<p><strong>Tony Blair</strong></p>

<p>George W. Bush.</p>

<p><strong>Christmas</strong></p>

<p>Paternity leave! (my first kid's due a week later)</p>

<p><strong>Thanks very much for your time, and all the best with next year's comics and the new addition to your family!</strong></p>

            <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
                <td class="subcolor" style="padding:6px;" align="center"><img src="images/ryallextra1.jpg" style="border:2px white solid;"><br>ChrisCharger (art by Nick Roche)</td><td class="subcolor" style="padding:6px;padding-left:0;" align="center"><img src="images/ryallextra2.jpg" style="border:2px white solid;"><br>EJ Su covers for The Transformers #1 and #2 from IDW</td></tr></table>

&nbsp;

            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
