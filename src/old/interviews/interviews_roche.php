<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Nick Roche</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Nick Roche<br>
              </span><span class="blackstrong">interviewed in:</span> August 2006</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/roche.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
		Nick Roche grew up watching the Transformers cartoon on television and
reading the comics written by Simon Furman. Over 20 years on and Nick is
living the dream, providing artwork for Furman on the new Transformers
Spotlight series. In fact, he has been onboard IDW's Transformers
bandwagon from the very start, illustrating ChrisCharger in their first
issue.
                </td>
              </tr>
            </table> 
       
	    
            <p><br><strong><span class="head2">1.</span></strong><strong> What is your first memory of The Transformers? </strong></p>
	    
            <p>The TV ads, weirdly enough. The sheer urgency of those commercials just compelled you to pay attention! It was like, time and space itself was going to DIE unless you checked out these little robot fellas. It was cars turning into robots, and it blew me away. I just couldn't get the image of this white car that became a robot (who I now know to be Jazz) out of my head. It's weird, I don't recall pestering my folks for the toys. Obviously I gave it my best shot at Christmas time and birthdays, but I was content to go ahead and make my own Transformers with my baby sister's Lego. In fact, it wasn't even Lego, it was some knock-off junk. So not only did I not own any actual Transformers, I didn't even have actual Lego to make them out of. That's the sort of abject poverty some people in the world faced in the early eighties. Then, jammily enough, I happened across the cartoon completely by accident. Never an early riser, by some freakish twist of fate, I woke early for school one morning to watch Roland Rat's morning show on TV-AM. When I wasn't even paying attention, Roland introduced a new cartoon and the screen cut from the superstar's sewer based studio to a scene of a metal planet hanging in space, soundtracked by all these weird, haunting cosmic toots and beeps and, lo and behold, I had accidentally stumbled across the very first episode of Transformers; Bumblebee and Wheeljack, Energy Conductors, Tetrajets...that was it. Hooked at four years old. Poverty and addiction. Took me a few watches to understand that Megatron wasn't where the Bad Guys lived (if the Good Guys lived on Cyber tron, then this Megatron I keep hearing about is where the baddies hang out) and to deal with my irrational fear of Soundwave (Must've been that voice). Though I seem to remember being less frightened of him once he acquired his tape recorder mode.</p>

	    <p>So it was around this time the drawing started. I had been hooked by things like Knight Rider, Dukes of Hazzard and The A-Team, TV shows where the vehicles had real character (literally, in the form of Kitt) even if they weren't characters themselves. I used to draw things like the General Lee, Herbie and KITT especially, over and over again. Now here was a show where the cars could get up and walk around, have conversations, get into fights and impart morals themselves instead of relying on the antics of some half-baked rednecks/military criminals/The Hoff to pad out the show. </p>

            <p><strong><span class="head2">2.</span> So what if you had to choose between those original G1 cartoons and the UK comics?  </strong></p>
	    
            <p>Well, I don't know. What sort of situation would force me into such an ultimatum? What would the conseqeuences be for my friends and family? I'll give you an answer, but I'm uneasy with these conditions...</p>

	    <p>The comics would win hands down. The cartoon was my first introduction to the stories, and yeah, part of me still maintains that More Then Meets The Eye is the definitive origin story, but the breadth and depth of the storytelling in the (UK) comics makes the cartoon cry. In this part of the world, the TV show wasn't consistently scheduled anyway, so you got fed cartoon episodes in dribs and drabs. The pilot episodes and The Ultimate Doom are the only ones I saw as a child, apart from the odd VHS release. And maybe it helps that I started reading the comics just when they were taking this brilliant new direction in Target:2006. Love the Movie so much still; I just find there's something very cathartic about watching it. It's a combination of animation, robots and Hair Metal, so it ticks most of my boxes.</p>

	    <p>Even before seeing The Movie, I felt ready to accept Ultra Magnus and Galvatron as 'The New Leaders' and I'll always think of the Movie cast as 'My' Transformers. Apart from the stories themselves, the comics showed me a way to tell my own Transformers stories. As a six year old, I found the facilities weren't often available to me to create a fully animated episode of shape-shifting mentalism, but with a black biro and an exercise book, you could make your very own comic (In fact, between the age of 6 and 12, I wrote and drew 107 of my own Transformers comics in empty school copy books. I hope to release them as trade paperbacks someday). All the different art styles were a big draw for me too. Never had a whole lot of time for the US artists, but I actually liked the difference between Geoff Senior's interpretation and that of, say, Dan Reed's. Even as a kid, I always felt that the US stories were filler while we waited to get back to the 'main' characters in 2006, or 2007, or 2008, or whatever. The Furman 'epics' were just the way Transformers stories were supposed to be told, to my young mind. Just spoilt from starting with Target: 2006, I guess. At the time, I was like, 'Who cares about the Mechanic, or The Space Hikers, when there's a robotic bounty hunter mixing it up in the future, playing the Autobot leader like a cheap red and orange kazoo, all for a wad of cash?' I mean, now, I can see them as two seperate entities, the UK and US stuff, but the quality of art from Marvel UK swung it for me every time.</p>

	    <p>I'm quite rigid about maintaining that the colour schemes of the cartoon (Purple Galvatron, Blue-torso Swoop, Yellow Ark, etc.) are the true path, but the G1 cartoon has More than Meets The Eye, and The Movie going for it, and that's about it for me. Two exceptionally strong points, I'll agree, but that scene with Galvatron atop Ultra Magnus, careening down the highway in UK #86 makes up my mind everytime.  </p>
	      
            <p><strong><span class="head2">3.</span> Any other particular favourite stories from either media? </strong></p>
	      
            <p>Beast Wars in general is so good, that I have to pinch myself sometimes and think, "Did we Transfans really deserve to be treated that well?" The Agenda was a glorious moment in TF lore. And I loved the aesthetic of Beast Machines, what a brilliant, brave direction to take the show. More risks like that would be great.</p>

	    <p>Comics, it's all obvious stuff really. Target 2006 will always be tops, because it was my introduction to the comics, but also beacuse it was the one epic that was untainted by Unicron or The Matrix. Yeah, yeah, I know in some ways, they were the impetus behind events in that story, but it was before we knew Unicron was a giant Transformer, and that the Matrix will always magically defeat him. A missing Optimus Prime, a new villain who claims he's Megatron, and lots of Geoff Senior goodness. Hard to top, really. So I loved all the future sagas, especially Fallen Angel/Wanted. Time Wars was so brilliantly OTT in its climax, Space Pirates really opened up the TF universe with the Quintesson threat (And inspired James Roberts' Eugenesis. The Greatest TF story ever told...?) and Legacy of Unicron came at a time when I had access to real Lego, and so could construct my own Unicron head and shoulders to freak out my Wreck Gar toy. Liked Deadly Games, Kup's Story and loved Ark Duty. You'd probably be right in saying I was up for pretty much all the non-US stuff, basically. The Furman/Wildman stuff for the US title was the comic moving to a different level, I thought. I was about ten or eleven when that stuff was coming out and I felt like the comic was growing and maturing with me, and that there was an integrity and quality to the storytelling that hadn't been there before. Felt the same about Generation 2. Reading something that's violent, grim and gritty is important to a chap in his early teens, and I just loved the feeling of a war without end that that series got across.  </p>

            <p><strong><span class="head2">4.</span> Why did you set about redrawing some Transformers stories? Why then reimagine the works of some of the more popular artists (eg. Andy Wildman)? </strong></p>
	    
            <p>You're referring to some samples of mine that have cropped up over the years, depicting scenes from USG1 #2, Target: 2006 , and Time Wars, right?</p>

	    <p>They were all submissions to send to comic companies (Dreamwave, mainly, because they were the ones producing TF stuff at the time). I tried to choose sequences that would help me display some storytelling skills, but would also hit all the targets I figured a TF editor would be looking for: vehicle and robot modes, Earth-based and Cybertronian-tech backgrounds, action and quiet scenes, and if I could, a few humans for good measure, to show that I can do people too. So for my own interest, the sequences I chose to redraw had to be interesting to me, so no Pretenders or Micromasters and such. I wanted to have a shot at drawing the most recognisable figures in the mythos and really strut my stuff, as it were. And the reason I chose to redraw as oppose to originate my own stuff is so editors would have a comparison with the original to see if I could convey the same story clearly and succinctly. So keeping me interested and hitting marks that editors need to see informed my choices. It had nothing to do with trying to improve or better the existing stuff. Those stories I redrew were done right first time round.  </p>
	      
            <p><strong><span class="head2">5.</span> Tell us more about the Transformers artists from either the past or the present that have influenced you.  </strong></p>
	    
            <p>I find myself influenced by current TF artists in the sense that their stuff is so good, it makes you want to improve as much and as fast as you can! I love the tech-savvy skills of EJ and Don, and Guido is blowing me away right now. He's a great comic artist first and foremost, and then a great TF artist, in my opinion. I just love that boy's inks. He's really come into his own under IDW, I reckon.</p>

	    <p>But the new artists don't influence me in as much as I (consciously, anyway) take elements of theirs and incorporate it into my own stuff. My TF art over the years has (pretty obvious, this) been influenced by Geoff Senior and Derek Yaniger stylistically, and from a storytelling point of view, Andrew Wildman. Those three were (are, in Andrew's case) so expressive, they really pushed the envelopes with their respective TF art. If you've been unfortunate enough to see my fan art from ten years ago or so, you'll see I'm having a tough time deciding if I want to be Geoff Senior or Derek Yaniger. Geoff's stuff basically taught me how to draw TFs, his depictions were the way these characters were supposed to look, full stop. He could convey action and mass with simple geometric shapes. He's like the Jack Kirby of Transformers. Andrew's storytelling in the final issues of Transformers are as good as anything in any comic I've ever read, and I loved the fact that his 'bots have such expressive features. I definitely think back to his stuff when I'm plotting out sequential pages, I just think he's ace. In fact, the Time Wars piece of his I redrew and another piece from his US run I had a stab at, but never ended up submitting, were nigh-on impossible to redo, just 'cos the best shot choices had already been taken. Automatically, what you drew was gonna look lame, 'cos he'd aced it first time out. And Derek Yaniger just took the whole thing in a new direction. His stuff said to me that it's okay to draw in the style that you feel comfortable in and that what has gone before shouldn't be sacred. Those three just had balls to do things the way they did. I'd like to draw with Senior's energy, Wildman's expressiveness and Yaniger's chins!  </p>

            <p><strong><span class="head2">6.</span> What else inspires your style? </strong></p>
	    
            <p>I kinda find that, apart from classic TF artists mentioned above, my work isn't that informed by many robo-centric sources; I'm not as influenced by the Japanese Mech Manga and Anime that a lot of the current artists seem to be, mainly just due to a lack of exposure to that stuff. So I kinda absorb stuff from mainstream comic artists; people like Joe Madureira, Humberto Ramos, Chris Bachalo, Jim Mahfood, Ben Caldwell, guys with a slight cartoonish tinge. I find myself influenced by Western animation, so I really dig Bruce Timm, Genndy Tartakovsky, and Stephen Silver, character designer on Kim Possible, amongst others. Just love that simplistic, thick-outline style. And Disney cartoons, they melt my butter. I can't say that he influences me, per se, but I'm mesmerised by Bryan Hitch's stuff. And I'd be very happy if I could tell a story as well as Mark Bagley or Steve Dillon in my lifetime.  </p>
	    
            <p><strong><span class="head2">7.</span> How did you get involved in working for IDW?  </strong></p>
	    
            <p>Chris Ryall's a sucker for Irish accents.</p>

	    <p>I was in the middle of developing a creator-owned story called The Nixer, about the Irish criminal underworld, with my dear chum, Dave Hendrick, a being that haemorrhages so many ideas, it's best you carry a kitchen towel with you at all times when you're in his presence. I'd done all the character designs, and was ready to crack into that when IDW started publicising the relaunch. Dave's pretty knowledgeable about Transformers himself and knew all about my obsession with them. Anyway, he had a contact connection to IDW in the shape of the professionally grizzled Beau Smith, who had written Wynonna Earp for IDW, and who also brings manliness to the internet week after week with his Busted Knuckles column at silverbulletcomicbooks.com. So Dave basically stuck his neck out for me, sent a sampler to Beau, who sent it onto Chris, and within the space of a few hours, Chris wanted to talk to me and I got my first piece of professional comic work commissioned in the shape of Chrischarger, which went on to appear in the #0 issue of Infiltration. Which means I've been there from the very beginning!</p>

	    <p>So that's the chain of events that brought me to IDW, and means that I owe my first and second born to Dave and Beau respectively (Ha! Joke's on them! I'm seedless!). And it came at what felt like the right time. I'd been plugging away, trying to get into Dreamwave, and got nowhere. Which, looking back, is probably a good thing. I had made up my mind to stop focusing my attention on Transformers stuff. I had taken that gig with Dave and I'd also won a place at animation college. I figured that going off to hone one's skills wouldn't do my chances any harm, so college, The Nixer, and writing and developing my own comic became my priority. I wasn't giving up trying, but I was definitely on the cusp at changing tack, so it couldn't have happened at a more fortuitous time. It felt so right...a new company to take away some of the sour taste that the demise of Dreamwave had left, and me realising my childhood dream... in the year 2005! Couldn't be happier!  </p>
	      
            <p><strong><span class="head2">8.</span> You followed Simon Furman's stories as a child, and now you draw them! Is that at all surreal? </strong></p>
	    
            <p>Yes. To say the least! I've only just begun working with him, though I'd met and been in contact with him a little over the last couple of years, but the mental baggage of childhood adulation is pretty heavy! Like, literally, the first comic I ever read was by him! I was young enough when I started getting the comics that my mum would read them to me as a bedtime story... and she had to give up because she found them too confusing! So I devoured them myself, and essentially widened my vocabulary as a result (However, being six and talking about 'temporal displacement' doesn't endear you with the cool kids). So he's a big part of my childhood development. I don't know what age the current batch of artists, like Don or Guido, were when they were first exposed to his work and whether or not they have the same sentimental attachment to seeing Simon Furman's name in the little box-out on a credits page as I do. It's trouser-shattering to think that my name will sit next to his the same way Geoff Senior's, Barry Kitson's and Will Simpson's did. And to see his name in my e-mail inbox is twisted, and something I get a buzz out of, even when he's writing to say, "Grimlock is scripted as glowering menacingly in panel 3, not constipated." Though that sometimes takes the edge off it. </p>
	    
            <p><strong><span class="head2">9.</span> You're working on the Transformers Spotlight series. Are there any individuals you'd especially like the comic to concentrate on, either because you like drawing them or simply because you want to know more about the character? </strong></p>
	    
            <p>Actually, there isn't!</p>

	    <p>The Spotlight series is handling and featuring a who's who of my favourite characters, if not as the protagonist, then at least in a supporting role. Can't say too much, but there's a character in the Hot Rod issue that never appeared in a comic before, but is a character I've loved since I got the toy many, many years ago, so I can't believe I'm drawing him. Hot Rod's my favourite character and was the first Transformer I ever owned, so I feel that I'm not allowed to ask for anything else in this lifetime.</p>

	    <p>To be honest, IDW really seem to be pushing the right buttons with who they're featuring, like Nightbeat, Thunderwing, or the odd ex-Decepticon leader here and there. All my favourite characters from the Marvel run. From a personal point of view, I'd love to see The Movie crew of Hot Rod, Kup, Springer etc, teamed up and see what sort of team dynamic they could form in this continuity. If you're talking about stuff I'd like to see happen purely from a 'things I like to draw' viewpoint, then a Star Saber one-shot, a Robots in Disguise mini, and a 'Prowl & Grimlock: Together in Electric Dreams' Maxi series would suit me.  </p>

            <p><strong><span class="head2">10.</span> What do you like and dislike most about Transformers? </strong></p>
	    
            <p>I like the combinations of highs and lows; the fact that a story about an eternal conflict that has raged for millions of years that neither side can ever win and that will only be decided by mutual destruction can be such rollicking good fun. Plus, I like robots with back-ends of cars hanging off 'em.</p>

	    <p>I dislike the fact that Transformers is currently so popular that I am unable to buy all the merchandise.</p>

	    <p>From a drawing point of view, I find that I'm really loving the chance to redesign characters, and figure out their transformations. It's such a fun challenge. And my dislike as far as drawing goes is the sheer wealth of reference needed to draw a Transformers comic, it just sort of slows you down. But you know as a fan, that you wouldn't put up with inconsistencies in the artwork, so you have to stick with it. I'm with Guido on this, I'd like there to be a place for the old-school Cartoon/Marvel type character in today's TF lore! </p>
	    
            <p><strong><span class="head2">11.</span> What are your feelings on the upcoming feature film?  </strong></p>
	    
            <p>Proud to be a Transformers fan and to be rewarded by the mainstream media with a feature-film with a huge profile. That's the first thing I feel. After that, I've managed to obliterate any expectations. I know this isn't gonna be the Marvel comics on the big screen, I know this isn't gonna be my Transformers, but after that, I know nothing! I hope it's something I can fall in love with, or better still, that the uninitiated can fall in love with, because hopefully that will lead them towards the comics, the old G1 cartoon, Beast Wars etc. And I'm not one of these militants that demand a slavish adherence to what has gone before. If these alien robots actually look like alien robots, so be it. It's a new direction. It hasn't wiped whatever your favourite iteration of the Transformers universe is from existence, so go back to that and enjoy it. In my opinion, this is hard to screw up. It's a film about giant robots that transform, fighting other giant robots that do likewise. We're gonna get some nice visuals at the very least. </p>
	    
            <p><strong><span class="head2">12. </span> And to finish, some word association:<br><br>

	    Caffeine </strong></p>

            <p>Useless unless intravenously consumed. </p>
	    
            <p><strong>David Cameron </strong></p>

	    <p>Really liked Terminator 2, but Titanic was a misstep...</p>

	    <p>Oh, David Cameron... Um, UK politician, Jonathan Ross' straightman and Thatcher 'fan'. </p>
	    
	    <p><strong>Thundercracker </strong></p>

	    <p>Jack Lawrence. It's his favouritest Transformer ever. </p>
	    
	    <p><strong>Cylons </strong></p>

	    <p>Opening credits of The A-Team.</p>
	    
	    <p><strong>Gorilla </strong></p>

	    <p>Sigourney Weaver.</p>
	    
	    <p><strong>Thanks very much for your time Nick... </strong></p>

	    <p>Cheers!</p>
	    
	    
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/roche1.jpg"><img src="images/roche1sm.jpg" style="border:2px white solid;"></a><br>Giga... played... guitar...</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/roche2.jpg"><img src="images/roche2sm.jpg" style="border:2px white solid;"></a><br>Powermaster Prime sketch</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/roche3.jpg"><img src="images/roche3sm.jpg" style="border:2px white solid;"></a><br>Hot Rod warm-up (click for more)</td>
	      </tr>
	      <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/roche4.jpg"><img src="images/roche4sm.jpg" style="border:2px white solid;"></a><br>Generations #5 inks</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/roche5.jpg"><img src="images/roche5sm.jpg" style="border:2px white solid;"></a><br>Shockwave Spotlight inks</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/roche6.jpg"><img src="images/roche6sm.jpg" style="border:2px white solid;"></a><br>Shockwave Spotlight inks</td>
	      </tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
