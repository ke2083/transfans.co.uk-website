<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Simon furman - Part 2 'The Present'</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Simon Furman - Part 2 'The Present'<br>
              </span><span class="blackstrong">interviewed in:</span> August 2004</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/simon.jpg" width="90" height="125" class="blueborder"></td>
                <td width="86%" valign="top">Simon Furman began his association 
                  with the Transformers in 1985 when he wrote his first issue 
                  for Marvel Comics. You can read more about Simon's time with 
                  Marvel <a href="http://www.TransFans.co.uk/interviews_furman.php" target="_blank">here</a>. 
                  More recently Simon has worked with Dreamwave comics to produce 
                  the acclaimed <a href="http://www.TransFans.co.uk/comics_guide.php?series=15" target="_blank">War 
                  Within</a>, the third volume of which debuts this month, and 
                  the ongoing <a href="http://www.TransFans.co.uk/comics_guide.php?series=16" target="_blank">Energon</a> 
                  series (previously Armada) for<a href="http://www.dreamwaveprod.ca" target="_blank"> 
                  Dreamwave</a>. This year Simon has also penned the epic <a href="http://uk.dk.com/nf/Book/BookDisplay/0,,11_1405304618,00.html" target="_blank">Transformers: 
                  The Ultimate Guide</a> for <a href="http://www.dk.com" target="_blank">Dorling 
                  Kindersley.</a> Last but by no means least (phew!) Simon also 
                  runs <a href="http://www.wildfur.net" target="_blank">Wildfur 
                  Productions</a> with artist <a href="http://www.TransFans.co.uk/interviews_wildman.php" target="_blank">Andrew 
                  Wildman</a> &ndash; you can read Simon's full biography on their site 
                  <a href="http://www.wildfur.net/furman.html" target="_blank">here</a>. 
                </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.wildfur.net/" target="_blank"><img src="images/wildfur2.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p><strong>Hello again Simon</strong></p>
            <p><strong>Thanks once more for taking the time to do this. The first 
              part of our interview dealt with your time at Marvel, so if it's 
              ok with you we would now like to move onto your recent work and 
              your current projects.</strong></p>
            <p><strong><span class="head2">1.</span> First up, now that the Ultimate Guide is out, what are 
              your feelings for that project? Do you think there is any scope 
              for a second volume?</strong></p>
            <p>The problem with the Ultimate Guide was not what to include, but 
              rather what to leave out. I could have filled three volumes easily. 
              So is there scope for a second volume &#8212; absolutely. Would 
              DK want to do one&#8230; I don&#8217;t know. I think it&#8217;s 
              sold pretty well, so maybe. My best, most realistic, guess would 
              be some kind of updated/revised/expanded edition of what we&#8217;ve 
              got already. With the movie coming out in 2006, that&#8217;d be 
              the ideal time, and by that point we&#8217;d be able to include 
              a fuller Energon section and a Cybertron section. My main problem 
              with the edition as it stands is the lack of a proper Robots in 
              Disguise section. We had problems when we came to do the planned 
              round-up of the animated series, because the rights to the imagery 
              lay (strangely) in other hands. I&#8217;d really like to rip the 
              guts out of that section and do it again, in much more detail. But 
              my overall feelings on the book? Well&#8230; DK do wonderful looking 
              books, and in that department I think it&#8217;s a stunner. On the 
              writing side, I&#8217;m happy with the way it&#8217;s turned out, 
              but I guess it&#8217;s not really for me to say. The feedback I&#8217;ve 
              had so far has been good, though.</p>
            <p><strong><span class="head2">2.</span> Moving onto your comics work, 
              can you tell us which volume of War Within you feel has been the 
              most effective?</strong></p>
            <p> They all have merits and demerits. Vol 1 holds up pretty well, 
              I think, even though we were clearly feeling our way a bit with 
              the whole War Within concept. Vol 2 is a little unfocused at times 
              (and maybe a little too decompressed in terms of storytelling&#8230; 
              I&#8217;m not sure I got the pacing right over the six issues). 
              But in terms of digging into the mythos, it&#8217;s the one with 
              the most pre-apocalyptic oomph (and I think the brooding atmosphere 
              of fear and distrust came through well). As for Vol 3, I have hopes 
              that this will be the best and most cohesive of the trio. I&#8217;m 
              trying to move the story on in significant leaps and bounds in this 
              one, and lay down the broader operating parameters of the series 
              as a whole (as if it were an ongoing entity of its own rather than 
              just a precursor to G1). I&#8217;m also trying to shake up the knee-jerk 
              expectations of readers with Vol 3. We&#8217;re dealing with a large 
              gulf of time and I want to make it clear whole lifetimes are lived, 
              and characters come and go, in sometimes dramatically altered forms. 
              Basically with Vol 3, expect the unexpected. One thing I have no 
              complaints on whatsoever is the art&#8230; on all three volumes 
              to date. I was so blessed with Don on Vol 1. I suspect now that 
              no other artist would have done as much as he did with as much enthusiasm. 
              He really established the look and feel of the whole series. With 
              Vol 2, Andrew took amazing strides as an artist, adapting his already 
              expression and motion-filled panels to incorporate the Dreamwave/Don 
              style. Somehow he managed to do it and still be recognisably Wildman 
              at the same time. And on Vol 3, Joe Ng has clearly reached a maturity 
              and cohesiveness in terms of his artwork that is going to blow everyone 
              away. Everything from his layouts to his individual panels to his 
              character delineation has come together in a way that&#8217;s very 
              much his own.</p>
            <p><strong><span class="head2">3.</span> What should we look forward 
              to in the third instalment?</strong></p>
            <p> Well, just off the top of my head&#8230; Ultra Magnus, Turbomasters, 
              Sharkticons, Allicons, the return of Megatron, the death of a major 
              character (kind of), Blaster, Perceptor, Seeker clones by the bucketload, 
              the search for Optimus Prime, Nightbeat, Siren, Raindance &amp; 
              Grandslam (plus Slamdance!), Starscream, the High Council eradicated, 
              the Autobots enslaved, Quintessa revealed&#8230; need I go on?</p>
            <p><strong><span class="head2">4.</span> At this stage do you know 
              what involvement you will have in Transformers: Cybertron? Has the 
              announcement of the next TF series had any bearing on your plans 
              for Energon?</strong></p>
            <p> At this year&#8217;s OTFCC I had a chance to sit down and chat 
              with Aaron Archer, who filled me in on the broad story strokes of 
              Cybertron, and how the (animated) Energon series wraps up. So I 
              know roughly how I need to end Dreamwave&#8217;s Energon. Of course, 
              our getting to that point (in the comic) will bear scant resemblance 
              to the TV series. Hasbro has been very accommodating, pretty much 
              letting us tell the story we want to tell. All I have to ensure 
              is that the larger end-moves are similar enough that we lead into 
              Cybertron with the same basic set-up intact, the same imperatives 
              in the story to come. Funnily enough, what I had planned fitted 
              so neatly into the main conceit of Cybertron that I barely have 
              to tinker with my final story arc at all. As for Cybertron, at this 
              point I kind of assume (from brief chats with Dreamwave, again at 
              OTFCC) that I&#8217;m going to be writing Cybertron. But that&#8217;s 
              not confirmed. Hopefully I&#8217;ll be able to say one way or the 
              other soon.</p>
            <p><strong><span class="head2">5.</span> With regard to your work 
              at Dreamwave so far, do you have a particular favourite issue?</strong></p>
            <p> It&#8217;s hard to pick a single issue of either Armada/Energon 
              or War Within, as all of them are very much part of an evolving 
              whole. With Armada, I can pretty much see and chart my level of 
              involvement/excitement with the whole series. Kind of a muted beginning 
              (well, I only thought I was doing a fill-in), the beginnings of 
              a proper story direction (with the Mini-Con Moonbase arc), the sheer 
              &#8216;look at me&#8217; buzz of World&#8217;s Collide and its G1 
              credentials, the rather rushed wrap up to Armada (sorry) and then 
              into Energon, the scale and scope of which I&#8217;m upping with 
              every story arc (and getting more and more excited about myself). 
              I&#8217;m also taking more time leaps, allowing stuff to have happened 
              off camera (so to speak) and dropping the reader into a suddenly 
              much bigger picture. For pure enjoyment (and a change of pace), 
              I really enjoyed the Energon Summer Special story. It was a (rare) 
              chance these days to do something offbeat, something more like the 
              5-page UK b/w stories. So rather than whole issues, I tend to think 
              along the lines of great imagery&#8230; Rad hardwired into an orbital 
              mine (a crowd-pleaser if ever there was), Galvatron revealed in 
              &#8216;Worlds Collide,&#8217; Unicron&#8217;s Four Horsemen riding 
              out towards us at the end of Energon #19, Optimus Prime&#8217;s 
              debut appearance in War Within v1 #1, Devestator and Defensor mixing 
              it up in War Within v2 #2 &amp; 3, the &#8216;death&#8217; scenes 
              in War Within v3 #1, the scenes of massed Terrorcon clones blitzing 
              LA, Tokyo, Toronto and Moscow in Energon #26.</p>
            <p><strong><span class="head2">6.</span> How do you feel Dreamwave's 
              output is going compared to Marvel's?</strong></p>
            <p> It has a more cohesive feel now. Before, with the Marvel run, 
              there was a certain amount of randomness and contradiction and even 
              weariness that crept in, but now there&#8217;s a real gameplan, 
              be it in War Within or G1 or Micromasters or even the Ultimate Guide. 
              Everything that happens in War Within will pay off in G1, and vice 
              versa. Same with Micromasters. Brad (James), Adam and I are all 
              working to ensure that it&#8217;s a complete, rounded picture&#8230; 
              that we&#8217;re not just making stuff up as we go along. The great 
              thing is, all the Dreamwave writers (and artists) KNOW Transformers, 
              inside and out, drawing on all previous variations and incarnations 
              while still creating something brand new. And with the most definitive 
              Who&#8217;s Who ever compiled (in the More Than Meets The Eye series), 
              it&#8217;s difficult to go far wrong.</p>
            <p><strong><span class="head2">7.</span> The original UK comic ran 
              for an incredible 332 issues, however sadly the recent Armada UK 
              run was unable to come close to replicating this success. Can you 
              tell us why you think this was and maybe give us some insight into 
              the different environments that these comics were produced in?</strong></p>
            <p> The Armada UK comic began life with a huge misconception as to 
              the audience it should have been aimed at. It was originally pitched 
              at an age group slightly above pre-school, so the stories were, 
              by necessity, simpler, the art more open and the colours bordering 
              on primary. The original Marvel UK comic was pitched squarely at 
              the boy&#8217;s action/adventure market (which is the real niche 
              Transformers as a whole occupies&#8230; apologies to the many female 
              fans/readers), and if anything veered even older than that. I like 
              to think that a lot of the original UK comic&#8217;s success was 
              down to the fact that the stories had real depth, that they addressed 
              serious issues (albeit in a fantastic setting). Kids, in my experience, 
              hate being talked (or written) down to. They&#8217;re smart&#8230; 
              smarter (and more savvy) now it seems than even 20 years ago. So 
              the tack for the Panini comic was just all wrong. It didn&#8217;t 
              excite new readers and it put off the older fans. But even, in today&#8217;s 
              market, if Panini had put out a more boy&#8217;s action/adventure 
              orientated title, it may still have failed. Comics in the UK (and 
              maybe more generally) need so much more to survive these days, in 
              a fairly limited (but intensely competitive) market. They battle 
              for shelf space in busy High Street outlets, covers dominated by 
              free gifts and cover mounts. It&#8217;s hard to figure what would 
              work&#8230; but I sure hope someone tries.</p>
            <p><strong><span class="head2">8.</span> How much attention do you 
              pay to the rest of the comics industry? If so, which titles hold 
              your interest the most? Is it difficult to read comics when you're 
              also a writer, or is it easy to hop onto the reader's side of the 
              fence?</strong></p>
            <p> I do keep an eye on what&#8217;s happening in the larger comics 
              industry (of which I still consider myself a part), but I&#8217;ve 
              skinnied my actual reading down to just a few titles. Planetary, 
              Straczynski&#8217;s Amazing Spider-Man and Supreme Power, Ultimates 
              (largely because of Bryan&#8217;s breathtaking art), Waid&#8217;s 
              Fantastic Four and other stuff like Lucifer and The Losers. If nothing 
              else, I keep an eye on things because I still do some editorial 
              consultancy for Titan and I need to know what&#8217;s what. But 
              to be brutally honest (and this is a trap I swore I&#8217;d never 
              get into) I don&#8217;t buy comics anymore, I wait for the trades. 
              The main problem, as I see it, with a lot of comics these days is 
              that the storytelling&#8217;s become so decompressed you get whole 
              issues where nothing much happens, and I hate that. I was weaned 
              on Stan Lee and Jack Kirby, who together fitted more story, imagination, 
              style and content into six pages than most people, myself included, 
              manage in six issues.</p>
            <p><strong><span class="head2">9.</span> Moving away from comics for 
              a second - What kind of approach would you ideally want the makers 
              of the new Transformers movie to take?</strong></p>
            <p> I guess I just want it to remain true to the underpinning ideals 
              of Transformers as a concept while not feeling tied by the weight 
              of continuity and fan expectation that comes with the project. There&#8217;s 
              some key notes I feel the movie has to hit, but it has to do that 
              in a new and contemporary way that will excite a whole different 
              audience. It&#8217;s got to look at itself as the first chapter 
              in a franchise that has legs of its own, and isn&#8217;t just a 
              retro 80s homage to be enjoyed by 20-30 somethings.</p>
            <p><strong><span class="head2">10.</span> Do you ever browse Transformers 
              message boards? If so what are your thoughts?</strong></p>
            <p> I take a peek from time to time. It&#8217;s good to know what&#8217;s 
              going on in the world of Transformers and see what people are thinking/wanting. 
              But I put on a full bio-suit when I look at threads that deal with 
              me or my work, because the old adage &#8216;you can&#8217;t please 
              all the people all the time&#8217; applies wholeheartedly to Transformers 
              fandom. The posts veer so dramatically, almost schizophrenically, 
              from adoration to pure bile that you can find yourself left feeling 
              quite down. So&#8230; I try to distance myself from a lot of opinions 
              expressed, try to step back to the point of a distanced bystander. 
              I do pay heed, but I try not let it impact on my own self-belief. 
              The anonymity of the web has a way of bringing out the best and 
              worst in people (not just in TF forums by any means), and some days 
              you need your creative immune system functioning at full capacity 
              just to stay in the game.</p>
            <p><strong><span class="head2">11.</span> Finally, can you tell us 
              what reactions you get from people when you tell them what your 
              job is?</strong></p>
            <p> It&#8217;s either&#8230; &#8216;wow, that&#8217;s the best job 
              in the world!&#8217; or &#8216;Huh?&#8217; (and there&#8217;s a 
              kind of embarrassed silence). It tends to be largely those polar 
              extremes. </p>
            <p><br>
              <strong> Mr Furman, thank you very much for your time. </strong></p>
            <p><strong>Don't forget to check out part 1 of our interview with 
              Simon <a href="interviews_furman.php">here!</a></strong><br>
            </p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor"><img src="images/simonextrab1.jpg" width="197" height="264"> 
                  <p align="center">Transformers the Ultimate Guide<br>
                    out now</p></td>
                <td width="26%" class="subcolor"><img src="images/simonextrab2.jpg" width="197" height="264"> 
                  <p align="center">The War Within Volume 3 Issue 1<br>
                    out this month</p></td>
                <td width="48%" class="subcolor"><img src="images/simonextrab3.jpg" width="197" height="264"> 
                  <p align="center">Energon 27<br>
                    out this week</p></td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar"><p><a href="http://www.titanmagazines.com/transformers.html" target="_blank">Titan 
                    Books</a>: You can see a full list of Titan's Transformers 
                    TPB's here.<br>
                    <a href="http://www.wildfur.net" target="_blank">Wildfur.net</a>: 
                    Andrew Wildman and Simon Furman's production company. The 
                    site includes updates on their current activities as well 
                    as information on services and properties. <a href="http://www.whorunstheengine.net/" target="_blank"><br>
                    The Engine: Industrial Strength</a>: Andrew and Simon's online 
                    graphical story telling project. Check it out. <a href="http://www.dreawwaveprod.com" target="_blank"><br>
                    Dreamwaveprod.com</a>: check out DW's site here. <a href="http://www.dk.com" target="_blank"><br>
                    DK.com</a>: Dorling Kindersley's website, with specific infromation 
                    about the top notch Transformers: The Ultimate Guide <a href="http://uk.dk.com/nf/Book/BookDisplay/0,,11_1405304618,00.html" target="_blank">here</a>.<br>
                    <a href="http://www.amazon.com" target="_blank">Amazon.com</a> 
                    and <a href="http://www.amazon.co.uk" target="_blank">Amazon.co.uk</a>: 
                    find Simon's Dreamwave TPB's here </p>
                  </td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.whorunstheengine.net/" target="_blank"><img src="images/engine.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <br> </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
