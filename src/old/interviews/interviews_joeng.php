<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Joe NG</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Joe Ng<br>
              </span><span class="blackstrong">interviewed in:</span> September 
              2004</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/joeng.jpg" width="90" height="125" class="blueborder"></td>
                <td width="86%" valign="top">Joe Ng started work at Dreamwave 
                  drawing characters for the <a href="http://www.TransFans.co.uk/comics_guide.php?series=20">MTMTE</a> 
                  series. From there Joe progressed to being the regular penciler 
                  on Transformers <a href="http://www.TransFans.co.uk/comics_guide.php?series=16">Energon</a>. 
                  Most recently Joe has become the third artist to work on the 
                  fan favourite series <a href="http://www.TransFans.co.uk/comics_guide.php?series=15">The 
                  War Within</a>. </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <p><strong>Many thanks for taking the time to talk to us. If it's 
              okay with you we will get straight down to the questions.</strong></p>
            <p><strong><span class="head2">1.</span> You were a Transformers fan 
              before you started at Dreamwave. What was it that originally got 
              you into Transformers?</strong></p>
            <p> I was a huge Transformers fan when I was a kid. The original movie 
              was one of my favourites growing up, and to have the original G1 
              Optimus Prime toy was one of my Holy Grail's when I was young. Beast 
              Wars was actually what got me back into Transformers a few years 
              ago, and I've become hooked back onto the license ever since.</p>
            <p><strong><span class="head2">2.</span> As someone who has jumped 
              from &#8216;fan&#8217; to &#8216;professional&#8217;, do you view 
              the fan community any differently now?</strong></p>
            <p><br>
              A little. It's very easy for a fan to criticize something that they 
              don't find satisfactory to their standards. Whether it be with the 
              toys, or the cartoon, or the comics. But I think now that I'm a 
              part of the production process for these products, I can understand 
              some of the limitations that the making of these products would 
              have. But don't get me wrong, I still consider myself a 'fan'. I've 
              made so many friends within the Transformers community, it's hard 
              not to view myself as one.</p>
            <p><strong><span class="head2">3.</span> Can you tell us a bit about 
              how you get the job at Dreamwave? How long did it take from your 
              initial contact to getting a regular gig?</strong></p>
            <p> A lot of people in the community know that I was a Transformers 
              fan artist before I started working with Dreamwave, and basically, 
              that's how I was noticed. Dreamwave's writers, editors, even the 
              other artists, always look at the fan boards for new talent. After 
              a long interview process with James McDonough, I started freelancing 
              the MTMTE series for them. I think after about 4 or 5 months is 
              when I got my first regular assignment.</p>
            <p><strong><span class="head2">4.</span> Is drawing comics a dream 
              come true for you? If so how does the reality compare to the fantasy?</strong></p>
            <p> Oh absolutely. I've been wanting to be a comic illustrator since 
              I can remember! I'm still pinching myself in disbelief! It's hard 
              to say that there really is a reality or fantasy to compare. I did 
              a lot of research about being a comic artist back in college and 
              I wasn't really surprised when I had to literally draw 2 pages a 
              day at times or pull all nighters 3 nights straight! Mentally, I 
              think I was prepared for the harsh realities of being a comic penciler, 
              but physically, that's a different story.</p>
            <p><strong><span class="head2">5.</span> What other artists (both 
              within Transformers and on other titles) do you draw on for inspiration?</strong></p>
            <p> Jim Lee, Bryan Hitch, Travis Charest, Yoshiyuki Tomino, Pat Lee, 
              Don Figueroa, are all artistic influences of mine. But I think my 
              biggest influence would be my brother Danny. He's always been there 
              to support me no matter what I do.</p>
            <p><strong><span class="head2">6.</span> Once you have finished your 
              pencils do you have any say in how your art is treated? For example 
              do you get to choose where special effects such as motion blur are 
              applied?</strong></p>
            <p> Yeah, I add little notes here and there for where to put special 
              text, or symbols, or effects, but it really is a team effort. If 
              the inker or colourist has an idea in mind to do something with 
              my lines, then I'm certainly open to their ideas.</p>
            <p><strong><span class="head2">7.</span> With regard to Energon, how 
              much of your own interpretation were you allowed to apply to the 
              characters? Did you find you had to make many changes to the toy 
              designs to make them look plausible on the page?</strong></p>
            <p> A lot of the Energon toys were already very posable so it wasn't 
              very hard to illustrate them moving and performing certain actions 
              without performing too much 'surgery' to them. In terms of my own 
              interpretations, things like joints or how I do the faces or hands, 
              were pretty consistent to my style, much like how Don's hands are 
              drawn a certain way, or Pat draws his joints a certain way.</p>
            <p><strong><span class="head2">8.</span> In general, which are your 
              favourite and least favourite characters to draw? Is there anyone 
              you will miss drawing now you have left Energon behind?</strong></p>
            <p>I'm really beginning to enjoy drawing Shockwave. His design is 
              so simple yet recognizable. He's quickly becoming one of my favourite 
              Decepticons next to Soundwave. As for the least favourite... whichever 
              one takes more time! Thankfully I don't really have a least favourite 
              Transformer to draw... yet.</p>
            <p>One Energon character that I was really starting to enjoy drawing 
              was Skyblast. Probably because he looks very similar to Skyfire, 
              who's design in the G1 cartoon I absolutely loved.</p>
            <p><strong><span class="head2">9.</span> What's the main difference 
              between working on War Within and Energon?</strong></p>
            <p> Probably the biggest difference so far between the two titles 
              is that one is mostly based on Earth, and the other is mostly based 
              on Cybertron. I find it so much easier to draw alien backgrounds 
              than Earth backgrounds because with Cybertron, you can pretty much 
              make stuff up! With Earth settings, they have to be believable, 
              and look as if it would actually exist in real life. It saves a 
              little time when drawing a Cybertronian background because I don't 
              have to go rummaging through the internet for references of New 
              York, or the Australian desert.</p>
            <p><strong><span class="head2">10.</span> What are you most excited 
              about in the upcoming War Within series?</strong></p>
            <p> Being able to draw some really cool G1 characters before they 
              landed on Earth! I love drawing War Within Starscream, and Ultra 
              Magnus. These are characters that I grew up wishing that I had the 
              toys of, and now I get to depict them in a very important stage 
              in their development as characters! That, to me, is beyond my wildest 
              dreams.</p>
            <p><strong><span class="head2">11.</span> How do you take criticism 
              to your work (whether positive or negative)?</strong></p>
            <p> If it's very constructive criticism then I'll definitely listen 
              to what people have to say. I always visit the message boards to 
              see what they like or dislike about my art, and if it's a truly 
              intellectual argument against how I do my job, then I'm definitely 
              going to read through it and think about it. But if it's a comment 
              like: &quot;It sucks&quot; or &quot;My 5 year old son can draw better&quot; 
              then I just kind of laugh it off. It's no skin off my back.</p>
            <p> <strong><span class="head2">12.</span> Do you ever contribute 
              to the plots? Do you have any aspirations to write as well as draw?</strong></p>
            <p> I'm a terrible writer, haha. I'll leave the scripting and stories 
              to those that know how to do it. But if I think there's a visual 
              that could be a little different than what's written in the script 
              then Simon is more than welcome to hear my thoughts and he's always 
              very positive about the ideas that I put in, no matter how miniscule 
              they are.<br>
              <br>
              <strong><span class="head2">13.</span> Finally, what are your long-term 
              goals as a comic artist? Any particular titles you would like to 
              take a crack at?</strong></p>
            <p> I would love to draw G1, even if it's for one issue. That is the 
              ultimate goal for a Transformers artist I think. But other than 
              that, I really want to try doing a superhero book of some sort. 
              Whether it be my own creation or an already existing character. 
              I think drawing a Marvel or DC book is on any comic artists' wish 
              list, but right now, I'm more than happy drawing for a great franchise 
              like Transformers!</p>
            <p><strong>Mr Ng thank you very much for your time.</strong></p>
            <p> It was my pleasure!</p>
            <p><strong>TransFans.co.uk would like to stress that all opinions are 
              Joe&#8217;s own and do not necessarily reflect the opinions of Dreamwave 
              Productions.</strong></p>
            <p><br>
            </p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor"><img src="images/joeng1.jpg" width="197" height="264"> 
                  <p align="center">Optimus Prime art<br>
                    from Joe's site</p></td>
                <td width="26%" class="subcolor"><img src="images/joeng3.jpg" width="197" height="264"> 
                  <p align="center">The War Within: Age of Wrath - issue 1 internal 
                    art.</p></td>
                <td width="48%" class="subcolor"><img src="images/joeng2.jpg" width="197" height="264"> 
                  <p align="center">War Within: The Age of Wrath - Issue 4 </p></td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar"><p><a href="http://candystirfry.com/" target="_blank">http://candystirfry.com/</a> 
                    - Joe's site, featuring lots of original work.<br>
                    <a href="http://www.dreamwaveprod.ca" target="_blank">http://www.dreamwaveprod.ca</a> 
                    - Dreamwave's official site. </p>
                  </td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <br> </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
