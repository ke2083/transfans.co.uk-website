<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Eric Holmes</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Eric Holmes<br>
              </span><span class="blackstrong">interviewed in:</span> February 2007</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/holmes.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
		Eric Holmes describes himself as "a furious game designer, designing &#8212; furiously &#8212; at Radical Entertainment." Various nextgen console projects are in the works, but on the Transformers front there's the matter of a four issue miniseries for IDW, showcasing the rise of ol' bucket-head himself: Megatron!<br><br> Eric's blog can be found at <a href="http://www.videogamedesign.net/" target="_blank">www.videogamedesign.net</a>. Megatron: Origin #1 is solicited for a May release.
                </td>
              </tr>
            </table> 
       
	     <p><br><strong>Hello Eric. Thanks again for agreeing to do this interview... </strong></p>

<p>Thanks for asking!</p>
	    
            <p><strong><span class="head2">1.</span></strong><strong> So, for those fans not familiar with your work before coming onboard IDW, how about telling us about some of your background? </strong></p>
	    
            <p>Sure. I'm primarily known as a game designer. I work at Radical Entertainment in Vancouver, BC, Canada where I get to spend my days surrounded by some of the most talented people I've ever met; artists, programmers, designers, sound guys; Radical has a crack team and I continue to learn from both the people and the medium on a daily basis.</p>

<p>The last two games my team created were �Incredible Hulk: Ultimate Destruction� in 2005 and back with the Hulk movie in 2003 we made the movie game. To date, our 2005 Hulk:UD game is the highest rated comic book game ever made and that's something we're very proud of. 
 </p>
 
            <p><strong><span class="head2">2.</span> What are your feelings on the big screen outing for The Hulk? </strong></p>
	    
            <p>The movie seemed very divisive &ndash; people loved it or hate it. I'm far from impartial given that we were running along with some of the guys on the movie project as we made our movie game. Given how we went to the movie as a team after working our guts out on the game I had a great time at the cinema, but I can also understand the people who expected something more popcorn-friendly.</p>

<p>I think those people will get what they're looking for next time. If you liked the game, Marvel did too. I wouldn't be surprised if some elements of Hulk: UD pop up in the new movie. 
 </p>
	      
            <p><strong><span class="head2">3.</span> Not many people would know exactly what it takes to make a game. Can you give us an insight? 
 </strong></p>
	      
            <p>Wow, that's a broad question. Working in videogames is a very demanding job, because it's so very, very new and constantly changing &ndash; the terrain literally shifts every year and sometimes even multiple times within a year. Given that most games take 2 years to make and while you're working the whole medium is evolving this makes for a very challenging, demanding, exciting job! No two months are the same, never mind games.</p>

<p>Now, I'm just one part of the team so for my job it's mostly about the concept and direction of the experience, then drilling down as to just how to make that happen. Each game team &ndash; no matter whether you might be making Madden or Manhunt &ndash; will be composed of many types of talented specialists who know just how to make something as good as it can be; whether it's the challenge of turning an everyday room into some scary hell-hole just through lighting or creating a rendering system that can draw a thousand bullets sixty times a second there's usually someone on the team who has a forte at just that sort of quirky problem. If there isn't, then we hire one!</p>

<p>That kind of specialisation and dynamic problem solving is probably the only constant in game development. The rest of the time, it's a lot like hanging out with your friends, posturing �Wouldn't it be cool if�?� - and then figuring out how to do it! Did I mention the pop is free?

 </p>

            <p><strong><span class="head2">4.</span> What exposure have you had to the Transformers phenomenon before now? We take it you're a fan... 
 </strong></p>
	    
            <p>My name is Eric, I'm a crazy fan and my first toy was Cliffjumper. Back in '84 I was 9 years old - the perfect age to get hit by the phenomenon of toys, cartoons and lunchboxes. I'm a Scot, so I grew up with the UK comics &ndash; and that means Bob Budiansky's US reprints mixed in with Simon Furman's original work. I think I can honestly say that Target:2006 blew the doors off my mind &ndash; the timing was perfect. I can still remember how my friends and I had no idea how there could be �New Leaders� when Prime and Megatron were still around.</p>

<p>To make that story, Furman MUST have made a deal with the devil. 
 </p>
	      
            <p><strong><span class="head2">5.</span> Talking of Furman, so far he's been the driving force behind the scripting of the Transformers in IDW's new continuity. How then did you get involved in the Megatron Spotlight mini-series?  
 </strong></p>
	    
            <p>There's a lengthy story behind this, and I'm really not sure how much I can or should say. I've had the itch to work in comics for years, from back when my mum used to get me Eagle and 2000AD and I copied some Mike McMahon �How to draw Judge Dredd!� in one of the annuals.</p>

<p>Games are a growing industry and I'm lucky enough with the Hulk stuff to have been able to bring both gaming and comic book worlds together there. Getting onto a Transformers story is a little like coming home.</p>

<p>What I can tell you is that the story has been bouncing around for around three years now, and IDW is not the first publisher to get excited over it. The format has shuffled back and forth, the publisher has changed repeatedly - but one thing has been consistent &ndash; anyone who has read the overview has responded with �Wow, that's a really good story. We should do it! I can't wait!�</p>

<p>And that's pretty much what Chris Ryall said when he got a look at it last year�so here we are today, I get handed Alex Milne on art and a $200M movie right between issues 2 and 3. Is there a better position to be in? 
 </p>

            <p><strong><span class="head2">6.</span> It would seem that with films such as 'Revenge of the Sith' and 'Hannibal Rising' the appetite for seeing the beginnings of evil characters is very much in vogue... 
 </strong></p>
	    
            <p>It wasn't a conscious tribute to a trend at least. I think it just came out of the fact that Megatron has always been my favourite Transformer, and I think the movie really cemented that. I remember when reading the original issues of the comic &ndash; I must have been 8 &ndash; Megatron seemed really scary. Then when you got to know him a bit better he had this startling combination of animal cunning, fury and greed that seemed to make him the catalyst for most Transformers stories. The way that he nails Prime in the movie, gets mortally wounded himself but even then manages to flip that around and come back reformatted, rebuilt and reinvented is a tribute to just how dangerous he is. I love that.</p>

<p>So we have this badass character, but I've never read anything that really nails how he became that. I think Simon did a great job of that for Prime in War Within 1, but Megatron is pretty much fully formed there. Wouldn't it be fun to go back a little further &ndash; to go back into peacetime?</p>

<p>I don't know where I read it or heard it, but I firmly believe in this nugget: �Every villain is the hero of his own story.� Megatron needs to believe he's doing what he's doing for well founded reasons. Otherwise he's just a cardboard cutout of a �bad guy�, getting in the way of the �good guys� all the time. Wouldn't it be cool as all hell if we had a story that made him the character we recognise?

</p>
	    
            <p><strong><span class="head2">7.</span> What input did Furman have in the workings of the script? 
 </strong></p>
	    
            <p>Simon is kind of like IDW's creative editor. He read the draft, and gave me feedback; we collaborated on tweaking it and making sure it's as well integrated into the �IDW-verse� as possible.</p>

<p>The thing I like about this treatment is that it works for any incarnation of Megatron. It was written with the G1 Megatron in mind; Simon's involvement leveraged it to tie into his current IDW work. That doesn't mean it's not consistent with the G1 story &ndash; it is. What it adds is that a tight alignment with everything that Simon is doing to bring the many disparate TF continuities into a tight focus. A tall order! 
 </p>
	      
            <p><strong><span class="head2">8.</span> What else is the secret to Megatron's character? Since he's had so many incarnations in both cartoons and comics, where did you start? 
  </strong></p>
	    
            <p>Like any character that endures from James Bond to Hulk there are many incarnations, but the key is holding onto the core constants. What I mean by that is what makes him cool, why people respond to him. If I asked Joe Blow on the street to describe Hulk, the first few qualities out of their mouth would probably be Big / Green / Strong / Angry. With Batman the same question might get Scary / Skilled / Gadgets / Vehicles. You will of course get different answers from everyone, but there's something in the general trend. With Megatron, I guess it'd be something like: Leader / Cunning / Violent / Power-Hungry.</p>

<p>Visually, the most iconic thing about Megatron are the helmet and his fusion cannon� now wouldn't it be interesting if we did something with those too?   </p>
	    
            <p><strong><span class="head2">9.</span> Are we going to see a more "youthful", less cynical and/or more vulnerable Megatron? Did he have friendships for instance?
 </strong></p>
	    
            <p>You're going to see him become the character we know. That means he has to start somewhere else, and learn a few lessons along the way. I don't want to say too much more about that at this point, but I think readers will get a kick when things come to come together on a number of levels. You'll see an evolution at a personal level, at a political level and &ndash; they're mechanoids &ndash; at a technical level.  </p>

            <p><strong><span class="head2">10.</span> Did you draw on any real life parallels when fleshing out such a back story (The rise of Nazism for instance)? 
 </strong></p>
	    
            <p>I love to research, often to the point of paralyzing myself with a deadweight of ideas. There's a bunch of materials I've dug into, but I think the main one that sticks out is the Fall of Rome. You have this enormous empire that's so large and so ambitious that it begins to collapse under its own weight. The backdrop of Cybertron is undergoing that sort of stress in our story. What does that mean? It means we see that there are reasons why Cybertronians follow Megatron. The system has failed them and they need a way out. In other cases, we'll see why some others choose to be what will become Autobots. They believe in those values.</p>

<p>It's not as simple as, say, World War 1 where your allegiance is based on where you are born. The Cybertronian war is one of values, an ideological difference. These characters choose for reasons that expose who they really are.  </p>
	    
            <p><strong><span class="head2">11.</span> Megatron's origins have never really been explored before now, apart from perhaps the Marvel UK text story 'State Games' and the cartoon 'War Dawn'. Why do you think it has taken so long? 
  </strong></p>
	    
            <p>I'm pretty sure I'm not the first person to have thought of telling a Megatron origin. The Transformers universe is a very rich one, and there are a lot of different stories to tell. I'd guess that other creative teams preferred the idea they had at the time! </p>
	    
            <p><strong><span class="head2">12.</span> Can you give us any other hints on what we can expect from the upcoming series? 
 </strong></p>
	    
            <p>Reasons. Decisions. Consequences. At every level.</p>

<p>So in that vein, this is something that we're still working on, but something I'd like to do is show a technical evolution of the characters and how their ideologies tie into their physical appearances. For example, the seekers have their weapons ON their arms. Megatron has his fusion cannon ON his arm. Shockwave has swapped out a hand for a blaster. Soundwave has his missile pack mounted to his shoulder.</p>

<p>There are exceptions of course (Trailbreaker leaps to mind), but it seems to me that more Autobots carry their weapons in their hands rather than having them 'mounted' through physical modifications. Wouldn't it be neat if there was a reason for this?</p>

<p>Some characters are confused in their backgrounds, and at least one character that completely lacks a background - we're giving him one. There's going to be some real fun in this series with continuity and consequence and I think it will resonate bigtime with long term fans. It should satisfy those fans intellectually, when they read it and think �Ah, now I get why that character is like that.� It should feel like a puzzle piece snapping together in your mind and make you grin every time you read that character from now on.
 </p>
	    
            <p><strong><span class="head2">13.</span> Are there any other Transformers characters you'd like to write for? 
</strong></p>
	    
            <p>There are a lot of the toys out of the toybox in this story, but�</p>

<p>The cassettes are a great mismatched family. Who doesn't like Ravage? Jazz always seemed like an underexposed character. Doesn't the assignment �Special Operations� sound like he has a couple of war stories? A Starscream solo could also be very fun. Whenever you see him, you just know there's going to be trouble.</p>

<p>I'm a huge fan of the Decepticons &ndash; you could do entire stories with them where the Autobots don't even turn up; that faction has everything you need. There's a bunch of extra stuff written for this series that won't get in, but I always thought Sunstreaker and Sideswipe are a great conflict waiting to happen. If you read Sunstreaker's bio, doesn't he read just like a Decepticon? Hmmm�! 
</p>

            <p><strong><span class="head2">14.</span>  Are there any future Transformers projects that you will be working on?

</strong></p>
	    
            <p>I know you probably think there's some master plan with all sorts of clever deals guiding who creates what, but I am pretty sure that in comics it's the fans that who really decide who comes back and who gets kicked to the kerb. You're voting with your attention and with your dollars, and that is the sort of democracy that publishers respond to.</p>

<p>If you folks enjoy the series, head over to the IDW forums (www.idwpublishing.com) and tell Chris Ryall you want more - he's the guy who makes stuff happen and he really listens to the fans. For example, someone suggested just the other day that it'd be cool if there was a Don Perlin cover for an upcoming comic. Within a week, Chris had made it happen! 
</p>
	    
	    
            <p><strong><span class="head2">15.</span> And finally, some word association: </strong></p>


	    <p><strong>Galvatron    </strong></p>

            <p>Destiny</p>
	    
            <p><strong>Alex Milne  </strong></p>

	    <p>Rising Star</p>

	    <p><strong>Wolverine   </strong></p>

	    <p>Overexposed </p>
	    
	    <p><strong>Michael Bay </strong></p>

	    <p>Roll of the dice </p>
	    
	    <p><strong>Penguins  </strong></p>

	    <p>Tekken 5</p>
	    	    
	    <p><strong>Scotland   </strong></p>

	    <p>Overcast!</p>
	    
	    <p><strong>Thank you very much for your time!</strong></p>
	    
	    
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/holmes1.jpg"><img src="images/holmes1sm.jpg" style="border:2px white solid;"></a><br>Marcelo Matere cover art for<br>Megatron: Origin #1</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/holmes2.jpg"><img src="images/holmes2sm.jpg" style="border:2px white solid;"></a><br>Alex Milne cover pencils for<br>Megatron: Origin #1</td>
	      </tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
