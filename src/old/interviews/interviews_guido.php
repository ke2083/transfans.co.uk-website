<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Guido Guidi</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Guido Guidi<br>
              </span><span class="blackstrong">interviewed in:</span> April 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/guido.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">Guido Guidi started with Dreamwave 
                  in 2001, where his first Transformers work was the swanky Predaking 
                  Lithograph. Soon after he began regular strip work with <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=499&page=1">Transformers 
                  Armada issue 8</a>. Guido stayed with the title as it morphed 
                  into Energon up until <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=522&page=2">Issue 
                  23</a>. Guido was working on both the Generation One Title and 
                  the Energon More Than Meets The Eye guidebook when Dreamwave 
                  folded. Some of Guido's pencils from both of these titles can 
                  be seen at the foot of the page. </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <p><strong>Thanks for taking the time for this interview.</strong></p>
            <p><strong>Let&#8217;s get started, shall we?</strong></p>
            <p><strong><span class="head2">1.</span> How did you get the job at 
              Dreamwave? </strong></p>
            <p>In late 2001, after posting some fan art on the old Dreamwave forums, 
              I got an email from Pat Lee asking if I was interested in doing 
              a test-shot for Robotech (I think Dreamwave were pursuing that license 
              too at that time, with no luck). After a while I was asked to do 
              that Predaking litho and then I started doing regular freelance 
              work for them (toy, box-art, mini-comics), and finally the actual 
              Armada comics.</p>
            <p><strong><span class="head2">2.</span> What are the five pieces 
              you&#8217;re most proud of from your time at Dreamwave? </strong></p>
            <p>Predaking litho<br>
              G1 volume 1 alternate cover for #5<br>
              G1 #12 page 1 (unpublished)<br>
              G1 #12 page 3 (unpublished)<br>
              G1 #12 page 7 (unpublished) </p>
            <p><strong><span class="head2">3.</span> Which artists, both from 
              Transformers and other titles, influenced your work? </strong></p>
            <p>Almost exclusively artists from the old G1 Transformers TV series 
              and comics:<br>
              Floro Dery, Magami Ban, the Wildman/Baskerville duo, William Johnson, 
              Hirofumi Ichikawa, Don Perlin with Ian Akin and Brian Garvey, Kazuo 
              Nakamura (he was a great anime character designer from the 70s), 
              but of course also Don Allan Figueroa, James Raiz, Pat Lee and Hideki 
              Fukushima (TF Armada mecha designer) for my more recent stuff.</p>
            <p><strong><span class="head2">4.</span> Which characters did you 
              love/hate to draw? </strong></p>
            <p>Loved: Armada Megatron, Unicron&#8217;s Four Horsemen - I&#8217;ve 
              created their altered designs and their metallic steeds.<br>
              Hated: While I can&#8217;t say I really hate drawing a Transformer, 
              the characters that posed some problems have been Armada Prime and 
              Energon Scorponok (the arms).</p>
            <p><strong><span class="head2">5.</span> Did Armada and Energon offer 
              different challenges? How did you deal with these? </strong></p>
            <p>Well, the designs of those toys were really intricate sometimes. 
              My background was with classic streamlined Transformers like G1 
              or Beast Wars, but Armada/Energon designs were way more meched-out, 
              and sometimes I got distracted by all those details. I found the 
              Energon designs a bit easier to deal with, though.</p>
            <p>Personally I would liked to re-draw them all the way the G1 toys 
              were redesigned in the G1 TV show, with a simpler streamlined design, 
              and more humanoid shapes, but I think this is due to the fact that 
              I always looked at the Transformers more as living characters than 
              just hi-tech war machines... I&#8217;m sure many fans out there 
              are glad I never did that!</p>
            <p><strong><span class="head2">6.</span> Much was made of the &quot;house 
              style&quot; when it came to Dreamwave's art. How much stylistic 
              freedom did you have? Both Don Figueroa and Andrew Wildman mentioned 
              that their artwork had been altered at some point; were your pencils 
              ever altered?</strong></p>
            <p>Not that I&#8217;m aware&#8230; My humans were ok for them but 
              sometimes I&#8217;ve been asked to do some little &#8220;patches&#8221; 
              here and there (but actually most of them weren&#8217;t used at 
              all).</p>
            <p>In Armada I tried to stay close with the anime TV version style 
              (at first I know this was the goal for that title), but at a certain 
              points I&#8217;ve been asked to draw more like Pat or Don (I think 
              in order to boost sales) which posed some difficulties. I used to 
              put lots of blacks and cross hatching in my drawings, and this style 
              would have clashed with the Dreamwave house style of colouring.</p>
            <p>I think you can see my own style only in the exclusive OTFCC comic 
              TF Wreckers #3. Despite the end of 3H, this is perhaps the comic 
              book I enjoyed drawing the most.</p>
            <p><strong><span class="head2">7.</span> How much control from Hasbro 
              on the storylines were you aware of? </strong></p>
            <p> I know they gave Dreamwave a lot of freedom, but sometimes they 
              asked for some changes, like removing RID Smokejumper and Dreadwing 
              from Armada #12 page 3 and replacing them with Skywarp and Sideways&#8230; 
              I think this would be a good Simon Furman question. :)</p>
            <p><strong><span class="head2">8.</span> What was it like working 
              with Simon Furman? </strong></p>
            <p>Working with Simon has been a great experience, first because I 
              really loved his work as a reader back at the Transformers' Marvel 
              age, and I discovered that he&#8217;s a great writer to work with, 
              very open-minded, and a nice guy who really loves his work. </p>
            <p><strong><span class="head2">9.</span> What did you make from the 
              reactions to your work? </strong></p>
            <p>Sometimes I did read some negative criticism about my work&#8230; 
              sometimes I found them a bit exaggerated, but otherwise I actually 
              tried to understand them in order to improve. Of course compliments 
              always give a boost. :)</p>
            <p><strong><span class="head2">10.</span> It seemed your work on Energon 
              ended just as you were in full flow. What were the circumstances 
              of your departure? Did you still have dealings with Dreamwave at 
              the time of the collapse?</strong></p>
            <p>Basically I had been removed from their titles because Dreamwave 
              looked for in-studio artists when possible (I think this was one 
              of the moves in order to try to save costs without dealing with 
              any overseas artists), but I suspect that another reason could have 
              been that I never matched 100% with the intended house style.</p>
            <p>I stopped working for Dreamwave in March 2004, and I didn&#8217;t 
              have any other work dealing (save for regular communications about 
              my delayed payments) until late October 2004 when I was asked to 
              pencil pieces for Energon MTMTE and then to do G1 #12 and #13, of 
              which I only penciled the first 13 pages of #12, right before the 
              Dreamwave demise. I think the 13 unpublished pages from #12 were 
              among my best work&#8230; unfortunately.</p>
            <p><strong><span class="head2">11.</span> What did you learn from 
              your time at Dreamwave? </strong></p>
            <p>Good communication *should* be always an important part of a work 
              relationship. Makes life easier to everyone. If we all (freelancers) 
              were aware of the true Dreamwave situation, perhaps we could have 
              contributed some way in keeping the titles ongoing&#8230; who knows. 
            </p>
            <p><strong><span class="head2">12. </span> What were Roger and Pat 
              Lee like to work for?</strong></p>
            <p>Well, especially at first it was a lot of fun. </p>
            <p><strong><span class="head2">13.</span> If the opportunity arose, 
              would you work for them again? </strong></p>
            <p>I don&#8217;t think this will happen, especially not at the actual 
              state of things.<br>
              But I don&#8217;t forget that it was because of them that I was 
              introduced to the professional field. </p>
            <p><strong><span class="head2">14.</span> You&#8217;re stuck on a 
              tropical island with only three comics or trades. What would you 
              pick? </strong></p>
            <p>Hard to say&#8230; Marvel&#8217;s Transformers:The Movie adaptation 
              (I have a sort of Italian trade), Marvel TF #75, and my own TF Wreckers 
              #3 (why not?).</p>
            <p><strong><span class="head2">15.</span> What is on the horizon for 
              you? </strong></p>
            <p>Most likely some non-TF stuff, but I still hope to get involved 
              with Transformers comics if the opportunity should arise again, 
              though. Anyway, I&#8217;m still doing some Transformers artwork 
              for Hasbro from time to time (boxarts for upcoming products). </p>
            <p><strong><span class="head2">16.</span> Word Association:</strong></p>
            <p><strong>Rodimus Prime</strong><br>
              The Burden Hardest to Bear.</p>
            <p><strong>Dreamwave</strong><br>
              Cars are expensive.</p>
            <p><strong>Brad Mick</strong><br>
              I don&#8217;t know him enough.</p>
            <p><strong>Energon</strong><br>
              I would like to see this comic finished.</p>
            <p><strong>Pat Lee</strong><br>
              A guy that really knows how to pitch himself.</p>
            <p><strong>Thank you for agreeing to this interview and for your spectacular 
              work on The Transformers over the past couple of years. Everyone 
              here at TransFans.co.uk wishes you the best in all of your future 
              endeavors.</strong><br>
              <br>
            </p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr valign="top"> 
                <td class="subcolor"> <p><a href="http://www.TransFans.co.uk/popup.php?id=2345" target="_blank"><img src="images/guido2.jpg" width="197" height="297" border="0"><br>
                    </a>Issue 12 page 1</p>
                  </td>
                <td class="subcolor"><a href="http://www.TransFans.co.uk/popup.php?id=2344" target="_blank"><img src="images/guido1.jpg" width="197" height="297" border="0"><br>
                  </a>Issue 12 page 3</td>
                <td class="subcolor"> <p><a href="http://www.TransFans.co.uk/popup.php?id=2347" target="_blank"><img src="images/guido5.jpg" width="197" height="297" border="0"><br>
                    </a>Issue 12 page 7</p>
                  </td>
              </tr>
              <tr> 
                <td valign="top" class="subcolor"> <p><a href="http://www.TransFans.co.uk/popup.php?id=2346" target="_blank"><img src="images/guido4.jpg" width="197" height="297" border="0"><br>
                    </a>Issue 12 page 8</p>
                  </td>
                <td valign="top" class="subcolor"> <p><a href="http://www.TransFans.co.uk/popup.php?id=2348" target="_blank"><img src="images/guido3.jpg" width="197" height="297" border="0"><br>
                    </a>all three covers to the Energon: More than meets the Eye 
                    series</p>
                  </td>
                <td valign="top" class="subcolor"> <p><a href="http://TransFans.co.uk/popup.php?id=2033" target="_blank"><img src="images/guido6.jpg" width="197" height="297" border="0"><br>
                    </a>Armada - Conclusion Cover</p>
                  </td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar">Link: Guido's Home Page: <a href="http://xoomer.virgilio.it/guidi_guido/index.html" target="_blank">http://xoomer.virgilio.it/guidi_guido/index.html</a> 
                </td>
              </tr>
            </table>
            <p>This interview was brought to you on behalf of Professor Smooth 
              and the Transfans Admin Staff<br>
            </p>
            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
