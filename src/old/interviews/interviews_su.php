<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: EJ Su</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              EJ Su<br>
              </span><span class="blackstrong">interviewed in:</span> June 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><a href="images/su_full.jpg"><img src="images/su.jpg" width="90" height="124" class="blueborder"></a></td>
                <td width="86%" valign="top">EJ Su's early work on Transformers included contributions to the <strong>Transformers Ultimate Guide</strong> and the second <strong>Transformers/Gi-Joe</strong> crossover from Devil's Due. With the announcement that <a href="http://www.idwpublishing.com/" target="_blank">IDW</a> will now be publishing Transformers comics, EJ was revealed as the regular artist for the new G1 title. </td>
              </tr>
            </table> 

<p><br><strong><span class="head2">1.</span> Transformers fans mainly know you from your work for Devil's Due and on The Ultimate Guide. What else can you tell us about your CV/resume?</strong></p>

<p>When I was 18, I worked as pizza delivery boy.... oh, do you mean comic related resume?</p>

<p>I was first published in the early 90's with NOW! comics, but it was just a short sting, then I didn't do anything professionally with comics until me and a group of friends got together and published an anthology called "Inkpunks Quarterly" around the year 2000. Through that I met Robert Kirkman, who was our publisher at the time. Together we pitched "Tech Jacket" to Image's Superheroes lineup. From that point on, I was lucky enough to get enough work since then.</p>

<p>I am also incredibly fortunate to be able to work on a wide variety of themes and styles. I've done some Micronauts, Cloudfall #1, a Mer-Man one shot, Voltron vol.II, G.IJoe vs TF vol.II, Castlevania, Sam&Twitch, Spookshow International and SuperPatriot. Most of these require me to work a little differently from one to another. As an artist, it's great to be able to have the chance to push one's own limit.</p>

<p><strong><span class="head2">2.</span> What attracts you to drawing Transformers? Who are your artistic influences, and what are your thoughts on some of the previous Transformers artists?</strong></p>

<p>I've always been a robot fan, growing up around Japanese comics and animation. Giant robots fascinate me, and mostly because of my love for the Japanese Microman toy series (some of the Transformers toys came from the Microman toy line). There's just something interesting about turning an everyday object into a robot.</p>

<p>I am influenced by a lot of artists from the past, mostly by Japanese artists such as Tezuka Osamu, Katoki Hajime, Akira Toriyama, Shiro Masamune, Inoue Takehiko, Adachi Mitsuru, the list goes on and on. In recent years, I'm always getting huge influence from Adam Hughes, Travis Charest, Josh Middleton and Graig Mullin.</p>

<p>There are quite a few great Transformers artists from the Dreamwave era. Most of what I've seen seemed to be following Pat Lee's style. I really wish they didn't have to be confined to a certain style.</p>

<p><strong><span class="head2">3.</span> What are your favourite aspects of the franchise?</strong></p>

<p>Turning stuff we are familiar with into giant robots, "robots in disguise" in a matter of speaking. I think being able to identify with the robots' alternative mode is my favorite part.</p>

<p><strong><span class="head2">4.</span> You've been drawing Transformers before IDW. How did that come about?</strong></p>

<p>It started out when my friend Val Staples at MV Creations asked me to work on package to pursue the Transformers comic license. Obviously the license eventually went to Dreamwave, but the creative department of Hasbro liked my work enough to get us to work on some of their internal projects as well as referring my work to their other license holders, such as DK Publishing.</p>

<p><strong><span class="head2">5.</span> Can you tell us what the experience was like, drawing the G.I.Joe/Transformers crossover for Devil's Due?</strong></p>

<p>The guys at Devil's Due are wonderful folks. They are some of the nicest people I've ever met in my life, but I gotta admit I wasn't completely comfortable with the project. I wasn't supposed to draw any humans in the series, but I still had to draw backgrounds. So some of the character placements were somewhat guesswork and it wasn't a pleasant feeling to send out a bunch of half blank pages. Tim Seeley is a great guy, a very creative individual and a wonderful G.I.Joe artist. If we worked in the same location I think things would've been completely different.</p>

<p><strong><span class="head2">6.</span> Can you clarify what artwork you supplied for the Transformers Ultimate Guide?</strong></p>

<p>The pieces done specifically for the Ultimate Guide were the cut-away views of Optimus Prime and Megatron, and also both of their transformation sequences that appeared on the same pages. I also recognised some Armada artwork I did over the years for Hasbro that also appeared in the Ultimate Guide. I wasn't aware of those before I saw the printed book.</p>

<p><strong><span class="head2">7.</span> How did you go about envisioning the complex cross-sections of certain
Transformers for the guide?</strong></p>

<p>I've always enjoyed drawing mechanical parts. If I remember it correctly, I got a few instructive notes from Simon Furman on some of the components that had to show up in cross-section - I put those in places and tried to work other mechanical parts into them. I particularly wanted to have the major mechanics to look functional... I left out a lot of minor supportive systems for the sake of clarity.</p>

<p><strong><span class="head2">8.</span> How did you land the job at IDW?</strong></p>

<p>My first working experience with IDW was on the Castlevania mini series. When I finished the artwork on Castlevania, Chris Ryall saw some of my Transformers samples and he gave me a call. The rest is history.</p>

<p><strong><span class="head2">9.</span> IDW have hinted that your upcoming artwork in the new G1 comic is different to the Transformers work you have had published thus far. How has your work developed exactly?</strong></p>

<p>I don't think they are so much hinting that my work will be dramatically different, but rather it will be a much different experience for the readers. I came from an Industrial Design background, my approach to robots is very different from what Dreamwave has been doing in the past. One of my major focuses on this new series was to tell the story the best I can.</p>

<p><strong><span class="head2">10.</span> How has the preparation been? What have you made of the reactions of the fanbase so far, and are you feeling the pressure?</strong></p>

<p>The pressure is definitely there, there's no question about it. I realise over the years everyone has gotten so used to seeing Dreamwave robots that a lot of fans are not prepared to accept a different style. I realise everything I do will be placed under a microscope but I like challenges.</p>

<p><strong><span class="head2">11.</span> The Transformers' alt. modes are being updated for the new series. Are you completely responsible for the redesigns? How are you going about them?</strong></p>

<p>As long as the character appears in the issues that I am working on, I'll be the one to redesign them. However, there are a lot of restrictions when it comes to G1 characters. I won't be getting the total freedom in certain aspects.</p>

<p><strong><span class="head2">12.</span> Can you reveal anything else at this stage about the upcoming G1 project to keep us going till issue 0?</strong></p>

<p>Megatron's alternative mode will be a panda. <img src="phpBB2/images/smiles/wink.gif"></p>

<p><strong><span class="head2">13.</span> And to finish, a little word association...</strong></p>

<p>Sure.</p>

<p><strong>Chris Ryall:</strong></p>

<p>Supportive</p>

<p><strong>Don Murphy:</strong></p>

<p>Don't know enough about the person, but I am holding my breath for Astroboy</p>

<p><strong>Simon Furman:</strong></p>

<p>Legendary Transformers writer</p>

<p><strong>Issue 0:</strong></p>

<p>Sonic boom!!</p>

<p><strong>Springer:</strong></p>

<p>Jerry?</p>

<p><strong>Pandas:</strong></p>

<p>Poor creatures used as political bargaining chips.</p>

<p><strong>Thank you very much for your time, and we look forward to seeing further display of your talents with the arrival of BotCon. <br>For everyone else, the #0 issue of Transformers from IDW will hit stands in October priced at $0.99&#8212;mark it in your calendars!</strong></p>

            <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
                <td class="subcolor" style="padding:5px;" align="center"><img src="images/su1.jpg"><br>Autobot cover for issue #0</td><td class="subcolor" style="padding:5px;padding-left:0;" align="center"><img src="images/su2.jpg"><br>Decepticon cover for issue #0</td></tr></table>

&nbsp;

            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr valign="top"> 
                <td class="subcolor" align="center"><img src="images/su3.jpg"><br>Fear the panda &#8212; Megatron returns to excite and terrify a new generation of fans</td></tr></table>

	<p>EJ Su's Home Page: <a href="http://www.protodepot.com/" target="_blank">http://www.protodepot.com/</a><br>&nbsp;</p>

            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
