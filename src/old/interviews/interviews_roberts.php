<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: James Roberts</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              James Roberts<br>
              </span><span class="blackstrong">interviewed in:</span> February 2007</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/roberts.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
		Many Transformers fans have written fiction; not so many got 550+ page books printed up or so much appreciation from their peers. James Roberts was active with Brit group TransMasters UK long before this newfangled intarweb thing, and is herein interrogated on such subjects as the history of Transformers fandom over this way, his novel <i>Eugenesis</i>, and fellow TMUK bod Nick Roche landing an art and writing gig with IDW Publishing. 
                </td>
              </tr>
            </table> 
       
	    
            <p><br><strong><span class="head2">1.</span></strong><strong> Firstly, for those not in the know, can you tell us about TMUK and how you came to be involved? </strong></p>
	    
            <p>TMUK was an offshoot of TransMasters, an American Transformers fan club that started in the mid-80s. As I understand it (not being there at the beginning), TMUK took off in 1989 or 1990 when a few British fans, having seen the club mentioned on the letters page of the Marvel US comic, got in touch with their American counterparts. I responded to a similar letter in the Marvel UK comic. The letter-writer mentioned a bizarre TransMasters multiverse in which Blaster had nine wives (TransMasters US being fond of anthropomorphic extremes). Thankfully, the concept of Autobot polygamy did not put me off; conscious that the UK comic was coming to an end, I decided to keep the flame burning by getting involved in a wider fan community.</p>

	    <p>Whereas the American club was about the cartoon, the comic and the toys, TMUK was always about the stories. UK fans were interested in continuing the grand Transformers narrative that had come to an (apparent) end in 'Another Time &amp; Place' (from the 1992 annual). It was all very friendly. And in those pre-Internet days, it was incredibly exciting to meet dozens of like-minded Transformers fans. </p>
 
            <p><strong><span class="head2">2.</span> What do you think it is about the Marvel Transformers series that could have inspired such an organised fanfic-making community? </strong></p>
	    
            <p>I think the original TMUK members shared a love of the Marvel UK comic, which enabled us to draw upon a huge pool of collective knowledge. From 'The Transformers' in 1984 to 'A Rage In Heaven' 11 years later, we had a backstory that featured hundreds of characters and ran to thousands of pages &ndash; that's a hell of a framework for fan-fiction. Also, most UK fans knew, even when reading it, that the UK comic printed material from different sources, American and British &ndash; how else could you explain why some stories featured robotic bounty hunters and rifts in space/time, while others &ndash; the ones with spotty colouring and excessive exposition &ndash; focused on Monstercons from Mars and Micromaster wresting? Seeing how Furman worked his stories around the American reprints while creating story arcs of his own - made TMUK writers realise that a decade of complex continuity was not restrictive, but liberating.</p>

	    <p>Ultimately, though, we wrote our own stories because the originals meant so much to us. We loved the characters and couldn't bear to see the saga come to an end; there were simply too many stories left untold. It's a testament to the skill of Furman and Budiansky that a loose collective of post-pubescent SF fans read issue 332 of Transformers UK and thought, 'Okay. What next?'  </p>
	      
            <p><strong><span class="head2">3.</span> Which Transformers stories particularly influenced you, and in your opinion how do they stand up next to the best non-Transformers comics you have read? What other writers informed your work?  </strong></p>
	      
            <p>Shamefully, I came to Transformers late. The first comic strip I read was part two of 'Prey!' in TFUK issue 97. Even though I didn't know the ins and outs of the story, I realised that the final page &ndash; a splash of Optimus Prime's shredded body &ndash; was A Big Deal. The next issue I found was 113, the first part of 'Wanted: Galvatron, Dead or Alive'. A post-Movie story written by Simon Furman, drawn by Geoff Senior and featuring Rodimus Prime, Galvatron and Death's Head? There was no going back.</p>

	    <p>The stories that influenced me the most were ones that made up the Galvatron arc (Target: 2006, Legacy of Unicron, Time Wars et al) and the Unicron War (TFUS 69 to 75). The scope of these stories is incredible. I loved the multitude of characters, the apocalyptic tone, the sense that everything could change within a few panels. I love them now, but as a 10 year old they completely monopolised my imagination.</p>

	    <p>TFUK was my gateway into other comics: Death's Head, Dragon's Claws -- I was a dyed-in-the wool Marvel UK fan. That said, I was also a fan of Zenith, ABC Warriors, Nemesis and other 2000AD stalwarts. On the American side, I had a real soft spot for Giffen/deMatteis/Maguire-era Justice League and Peter David's stint on the Hulk. I was knocked sideways by Watchmen and V for Vendetta, as so many are. (Someone once &ndash; very generously - compared Eugenesis to Watchmen, and I think my feet actually left the ground.)</p>

	    <p>As for how the classic Transformers stories compare to the mainstream comics at the time, it's a question of critical context. As Comics International said at the start of its comic reviews, you wouldn't judge Batman against Love & Rockets. Transformers, being a 'toy title', had to promote certain products at certain times, within strict guidelines laid down by Hasbro. All that said I rate Furman's Transformers work pretty damn highly, irrespective of context.</p>

	    <p>As for which other writers inform my work, I'm a big fan of Martin Amis, Graham Green, Vladimir Nabokov and US heavyweights like John Updike, Don DeLillo, David Foster Wallace and Philip Roth� I'm not for one minute comparing myself with them (their worst sentence is 1000 times better than my best), but anyone who knows their work and has read Eugenesis will see that I'm a fan.  </p>

            <p><strong><span class="head2">4.</span> Had you always wanted to write a Transformers novel? What was the genesis of Eugenesis? How long did it take to write from start to end?  </strong></p>
	    
            <p>Eugenesis is about a quarter of a million words long and took about four years to write. It started life as a multi-part story called Invasion in a fanzine I used to edit. The fanzine folded just as Invasion was taking off. I took a break from TMUK and, on my return in 1997, mapped out the rest of the story. It seemed big enough to work as a novel. And once I'd made that decision, I went back to the beginning and turned everything up to 10.  </p>
	      
            <p><strong><span class="head2">5.</span> For anyone yet to read the novel, give an idea of the themes and the setting of this startling story.  </strong></p>
	    
            <p>It's all about Blaster and his nine wives� no, wait, that was an earlier draft.</p>

	    <p>Eugenesis is set in 2012 and 2013 and takes place on Earth and Cybertron and beyond. It features a cast of thousands, although the action focuses on a core cast of about 12 (including Nightbeat, Prowl, Ultra Magnus, Soundwave and Galvatron). All the big themes are present and correct: death, loneliness, displacement (both physical and temporal), sacrifice and the personal cost of war. It explores the reality of four million years of conflict, and how the Transformers would react if they really were threatened with extinction.  </p>
   
            <p><strong><span class="head2">6.</span> You kill off several characters quite spectacularly during the course of the novel, in a manner which would make the Furminator proud. But were you able to do so without fear of disrupting TMUK continuity? </strong></p>
	    
            <p>Quite a few people have commented on the body-horrors meted out to some of the Bots and Cons in the course of the story. What I tried to do was to emphasise just how difficult it is to actually kill a Transformer, and then find ways around that. (One of the most memorable TF moments ever is when Death's Head crushes Shockwave's brain module in the 'Legacy of Unicron' story; unlike those characters who take a few hits to the chest, or fall off a cliff, or who disappear in an explosion, you KNEW Shockers wasn't coming back.)</p>

	    <p>As for TMUK continuity, I felt I could do what I wanted provided that I acted responsibly and did not contradict anything else in the TMUK canon. It would have been selfish of me to blast dozens of famous characters into oblivion and prevent other writers from ever using them again (post-2013, anyway). A few very famous Transformers do meet their end in Eugenesis, but they are carefully chosen, and their deaths have consequences. </p>
	      
            <p><strong><span class="head2">7.</span> Perhaps more so than ever before, your writing has enabled us to sympathise and feel the pain of a Transformer. What do you think is the trick to giving human qualities and frailties to a machine without losing what it is than makes them different?  </strong></p>
	    
            <p>There is a danger of making a Transformer too human; ultimately, they are alien life-forms, and robotic ones at that. But the TF mythos endures because of the characters, and that wouldn't have happened unless fans could &ndash; at some level &ndash; identify with those characters. From day one we've heard about the 'heroic' Autobots and the 'evil' Decepticons &ndash; the two sides are defined by very human qualities.</p>

	    <p>I also think that any society, even one comprised of shape-shifting mechanoids, is shaped by politics, race relations, economics, ethics, questions of identity and so on. Transformers society is no different, and that was something I tried to get across in Eugenesis.</p>

	    <p>Notwithstanding the above, you have to try to constantly remind readers that these characters aren't human. As a rule of thumb, if you can swap all the Transformers in your story for humans and still tell the same story, it's not working. The non-human aspects of these characters &ndash; their long, potentially endless life-spans, their concept of pain, their attitude towards non-mechanical life, the way they can modify their bodies, their ambiguous origins� it's all endlessly fascinating.</p>
	    
	    <p><strong><span class="head2">8.</span>You also developed quite a few original characters. Was it a challenge to have them make a mark alongside the official characters?  </strong></p>
	    
            <p>Yes, very much so. 95% of the characters in Eugenesis are 'official', but I rounded out the cast with a handful of fresh faces. The original characters that have most to do and say in Eugenesis are Rev-Tone and Quark, two Autobot low-rankers, and a Decepticon surgeon/engineer named Sygnet. I created them because there weren't any existing characters that were the right fit.</p>

	    <p>It does irritate me when some fans refuse to invest time in a story simply because it features fan-created characters. Would Rev-Tone be any more interesting or legitimate if you could buy him as a toy, or if he'd appeared in a panel in an issue of the original comic?  </p> 

            <p><strong><span class="head2">9.</span> Did you ever consider going through official channels to get the novel published or was it always intended to be fanwork? How did it eventually see print?  </strong></p>
	    
            <p>I knew Eugenesis would never be an official publication. It was written long before Transformers became the rather hip brand it is today; back in the late 90s, a 500-page Transformers novel aimed at adult readers had a very limited audience.</p>

	    <p>I always intended to self-publish. At the time, I felt that releasing an actual book, as opposed to a series of fanzines or an on-line document, would give the whole exercise a certain legitimacy (although I tried to poke fun at my own lofty aspirations by making the finished article look like a Penguin Classic). It was always going to be a non-profit-making exercise &ndash; I lost almost �1,000 turning the manuscript into a large-format, perfect-bound paperback &ndash; but I achieved what I set out to do.   </p>

            <p><strong><span class="head2">10.</span> Your fellow TMUKer Nick Roche is now writing and drawing Transformers comics for a living! Do you harbour similar ambitions? Have you ideas for stories you could tell in the IDW Transformersverse? </strong></p>
	    
            <p>Nick is a fantastic artist and &ndash; from what I hear about the upcoming Kup Spotlight &ndash; an offensively talented writer, too. He and Jack Lawrence &ndash; another TF-fan turned professional comics artist &ndash; were always going to make a splash outside of TMUK. I remind myself of this as I cry myself to sleep each night�</p>

	    <p>But no, I'd jump at the chance to write Transformers stories that reached a wider audience. I used to write strips for the Continued Generation 2 magazine, and I enjoyed the discipline involved. In comics there's a unique emphasis on pace and narrative, and a need for a certain economy of language.</p>

	    <p>Being an avid reader of IDW's TF output, I do have some story ideas and plots that I think would work well in the new, ever-expanding TF Universe. They're a bit different, a bit unusual &ndash; they'd hopefully catch veteran TF fans off-guard. Any stories would have to complement Furman's meticulously plotted master narrative, but necessity is the mother of invention.  </p>
	    
            <p><strong><span class="head2">11.</span> So talking of recent Transformers comics, what do you think of the released works of both Dreamwave and IDW?  </strong></p>
	    
            <p>I was never much of a Dreamwave fan (apart from Furman's two War Withins), but IDW has renewed my interest &ndash; and faith &ndash; in Transformers comics. The whole storyline has a sense of purpose to it &ndash; you know the editorial and creative forces have a clear vision of where the whole thing will lead. That's tremendously comforting, and bodes well for the future.</p>

	    <p>It was a masterstroke to have Furman on board from the beginning, and to trust his sense of what works and what doesn't. But I also find it significant that Chris Ryall, the Editor-in-Chief, has co-written the prequel to this year's movie. You know that as well as caring about his readers, the guy loves Transformers &ndash; and that gives me faith in his stewardship. I suppose Pat Lee loved Transformers too, but� moving swiftly on� </p>
	    
            <p><strong><span class="head2">12.</span> How can any Transformers fans who have yet to read Eugenesis get hold of a copy?  </strong></p>
	    
            <p>Well, since you ask, Eugenesis will soon be available as a PDF document that can be downloaded and printed out free of charge. The novel has been extensively re-edited (a few bits added, some taken away) and the second edition even comes with a Beginner's Guide to the Marvel TF/TMUK Universe in which the story takes place. The Eugen micro-site will be up and running in a couple of months, and TransFans.co.uk will be the first to know about it.  </p>
	    
	    <p><strong><span class="head2">13.</span>  And finally, some word association:</strong></p>


	    <p><strong>Rodimus Prime:   </strong></p>

            <p>Inadequacy  </p>
	    
            <p><strong>Cats:  </strong></p>

	    <p>Andrew Lloyd Webber (unfortunately) </p>

	    <p><strong>Dirk Benedict:  </strong></p>

	    <p>Nick Roche </p>
	    
	    <p><strong>Nick Roche:  </strong></p>

	    <p>Dirk Benedict  </p>
	    
	    <p><strong>Primus: </strong></p>

	    <p>P.R.I.M.U.S. </p>
	    
	    <p><strong>Morrissey:  </strong></p>

	    <p>Genius. </p>	    
	    
	    <p><strong>James Roberts, thank you very much for your time!</strong></p>

   
	    
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/roberts1.jpg"><img src="images/roberts1sm.jpg" style="border:2px white solid;"></a><br>Front cover of <i>Eugenesis</i></td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/roberts2.jpg"><img src="images/roberts2sm.jpg" style="border:2px white solid;"></a><br>Back cover of <i>Eugenesis</i></td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/roberts3.gif"><img src="images/roberts3sm.jpg" style="border:2px white solid;"></a><br>Some Nick Roche TMUK art</i></td>
	      </tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
