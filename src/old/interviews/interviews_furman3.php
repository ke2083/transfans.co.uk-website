<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Simon Furman</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Simon Furman<br>
              </span><span class="blackstrong">interviewed in:</span> September 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/simon.jpg" width="90" height="125" class="blueborder"></td>
                <td width="86%" valign="top"><p>What can you say about Simon Furman and Transformers? Well, lots of stuff, but unless you've been living under a Rock Lord� since 1985 then you probably know most of it. After a brief hiatus from writing Transformers thanks to Pat Lee's financial brilliance, Simon is back where he belongs care of <a href="http://www.idwpublishing.com/" target="_blank">IDW Publishing</a> and will be writing both their new G1 series (Transformers: Infiltration) and a Beast Wars series next year, as well as reviving his relationship with fan fave Death's Head in the very near future. Not bad, eh? <p><p>A full biography for Simon can be found at <a href="http://www.wildfur.net/">www.wildfur.net</a>.</p>
                </td>
              </tr>
            </table> 
   <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.wildfur.net/" target="_blank"><img src="images/wildfur2.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
              <p>&nbsp;</p>



<p><strong><span class="head2">1.</span> How do you think you've changed as a writer since your Marvel days? </strong></p>

<p>I've definitely matured, I guess. I look back at the very early work I did for Marvel UK and I see a writer not quite understanding the constraints of a comic book page (or issue). Back then, I didn't really have a definable style or even a standard approach to storytelling. It was desperately random. As my career goes on, you can see the Marvel (US) influence start to creep in (more rounded character story arcs, better grasp of how to make each issue stand alone... even in a continuing story arc, etc), and as it does so, more of the Stan Lee factor becomes apparent. I always loved his overblown dialogue and the sheer drama of the staging (though, of course, that was largely down to the many talented artists he worked with), and I tried to introduce that in my own work. I think it reached its peak during the final stages of the Marvel US G1 run, circa #69-75. Sometime thereafter, I started working in TV animation, and that, I think, tightened up my style even more. There's no room for wastage or hanging around in a 22-minute animation script, and I think that both helped and hindered my subsequent work for Dreamwave. On the plus side, I think the stuff I did really moved at a pace, with direction and purpose and plenty of 'wham,' but on the minus side, I think I neglected some of the more measured, layered storytelling that was apparent in my Marvel run. With the IDW stuff, I've tried to bring it all together, to keep the pace and excitement but also keep the focus on the characters within the story, to keep subplots building and storylines inter-weaving. I think the simple answer (after this long, rambling one) is that my style is always changing, evolving. Apart from anything else, it's what keeps it interesting and exciting for me.</p>

<p><strong><span class="head2">2.</span> So how would you compare the approach to writing new stories for IDW and Dreamwave? Artistically, is there anything you would have done differently during your time at Dreamwave? </strong></p>

<p>Dreamwave really had that whole post-Image (studio) emphasis on the art, and it was perhaps easy to get sucked into that. I always tried to keep the story paramount, but looking back I can see that sometimes I was writing for artistic effect, and some slackness in my story arc construction was creeping in. With IDW, I've gone almost completely in the opposite direction, building story and character at a measured, evolving pace. It was practically a mandate at Dreamwave that your first splash page be some wham-bam scene, and there's just not so much of that in the IDW work. I'm trying to be economical with the wham factor, so when it does come, it really has some wham to speak of. That said, when we get to July and the summer G1 'event,' look for page after page of the wham factor. </p>

<p><strong><span class="head2">3.</span> How do you look back on the manner in which Dreamwave the company fell apart? </strong></p>

<p>I think what annoys me most is that one month or less before it all crashed and burned I was sitting with Pat Lee in a restaurant in Tulsa and he was waxing lyrical and hyper-enthusiastic about the stuff we were doing and all the things he had in mind to do, and I was going back to my hotel room and basically continuing to crank out stuff for them (as the editorial pedal was firmly to the metal at that point, and a business trip to the US meant taking my laptop with me and working every step of the way), and all the time, he must have known it was the end, that it was over. It makes me angry (when I let it) to think just how far he let me (and a whole host of other freelancers) run (in the process running up more and more debt) before finally, grudgingly it seems, admitting what must have been inevitable months before. If Pat had been some faceless bureaucrat it maybe wouldn't now seem such an acute betrayal, but he looked me square in the eye and said everything's hunky-dory. That's what still, even now, burns.</p>

<p><strong><span class="head2">4.</span> Realistically, are we going to see conclusions for War Within: Age of Wrath or the Energon storyline? Is a conclusion along the likes of 'Alignment' a possibility? </strong></p>

<p>I hope these stories can be brought to a satisfactory conclusion (because I hate, as both professional and fan, stories that are just left dangling), and I know IDW are keen to do so (once the Dreamwave bankruptcy mess is resolved). I just hope I can do them justice, should I get the chance. The problem, increasingly, is that (as mentioned above) my style and approach have evolved again, and I'm wholly committed to the new G1 framework we're laying down at IDW. It'll be strange, both in terms of the actual mechanics of writing and in terms of the story itself, to go back to War Within, to pick it up as was. Even though the scripts for Age of Wrath issues #4 and #5 are written, and #6 was at least started, I might need to revisit those and tinker. We'll see... if it happens. Energon would probably be more straightforward, but I imagine that would need wrapping up in a more direct fashion than I had originally planned.</p>

<p><strong><span class="head2">5.</span> G1 continuity is now being rebooted yet again. After all these years is it ever hard to stir up the enthusiasm to make such a retelling as exciting as ever? </strong></p>

<p>Actually, I thought it might be difficult to create that enthusiasm in myself (necessary to properly enthuse the readers), but actually it was just right there, even as I was writing the overview for the first six-issue arc. Starting over with a more or less blank canvas was incredibly liberating and inspiring, and I realised I had the chance (for the first time in over 20 years of writing Transformers) to make it 'my' vision Transformers, and to make it properly contemporary. The plain fact was, I realised, I didn't want to just tell the same story again. Instead, I looked back at what had been done before and I asked key questions like: does this still work? Could this be improved, or at least made more relevant? Key to IDW's plans was to make Transformers user-friendly, to strip away the baggage and make it something vital again (to today's audience). What a challenge! How could I not be excited? And as things have evolved (even in the last few weeks and months) it's just getting more exciting. Suddenly, a big 'event' has woven its way into the framework of the first two (ongoing) story arcs, and that's just opened up a whole world (or rather... worlds) of possibilities. </p>

<p><strong><span class="head2">6.</span> What other titbits can you give us about the upcoming G1 comic for IDW? </strong></p>

<p>Well, I've just finished writing issue #4, and a certain big-bad Decepticon with a fusion cannon has made his first appearance. No Prime yet... but he's coming. So far, issue #2 is the one that's got the most people (who've read it) excited. EJ is just SO pumped. I can't wait to see the art. Er... what else? How about The Machination, a shady group of industrialists and arms dealers who we'll be slowly introduced during the first arc and then start to play a really BIG role in the second. Or... a grand TF conspiracy (one that will only be revealed in tantalizing stages) that goes back to Earth's prehistory and rolls on into a first contact scenario in, er, 1984. That enough for you?</p>

<p><strong><span class="head2">7.</span> Anything else in the pipeline at IDW apart from G1 and Beast Wars? Is a Cybertron comic likely, and if so would it be a continuation of the fleshed out Armada/Energon universe you built up at Dreamwave? </strong></p>

<p>So far, no word on a Cybertron comic, so I've not really given any mental space to how it might pan out if there was one. And there's a good chance, considering how much else I have on right now (for IDW and beyond) that I wouldn't be able to write it anyway. We'll see. Other than that, there's the big summer 'event,' which is pretty much under wraps currently. All I will tell you, is that it introduces a truly kick-ass menace... and it's NOT Unicron. </p>

<p><strong><span class="head2">8.</span> Death's Head recently won (by a landslide) a recent Marvel online poll which asked users which old character they'd most want to see revamped. Would you expect any involvement in future Death's Head projects? </strong></p>

<p>By now you'll know that I am going to be involved, and that I'm writing a new 5-issue story arc that will run in Amazing Fantasy #16-20. It features an all-new Death's Head (3.0!), but, well, I wouldn't be surprised if some of the old Death's Head isn't in there... somewhere! And hey, I'm working with James Raiz again. How cool is that?</p>

<p><strong><span class="head2">9.</span> Will you be involved any further with the new Transformers movie? Are you optimistic about how it will turn out? Looking back, do you remember your impressions upon seeing the original Transformers movie? </strong></p>

<p>I'm cautiously optimistic. Funnily enough, when the movie was first mooted, I thought: how would I handle this if I was the writer (as you do)? The scenario I came up with became (pretty much) the IDW G1 comic, so I was gratified to read a review of issue #0 on Ain't It Cool where the reviewer commented that if the movie started this way, he wouldn't be disappointed. But as to any actual involvement in the movie itself, no. Nothing. I met with Don Murphy and Tom DeSanto (both great guys and we had a real nice chat), but no actual work has materialised. I'd love to be involved, in some capacity, but as time goes on I'm thinking that's less and less likely. As for optimistic about the movie itself? Well, so much depends on the script. We know that the special effects are there and up to the job, in terms of action/spectacle I'd say Michael Bay's are safe hands, but the story. Hoo-boy, if that sucks... it may sink without trace (or, worse, just be relegated to the category of a purely 'kids' flick). They have to get the tone and pitch just right. But can they possibly match the sheer kick-ass spectacle of the original (animated) movie? It's going to be tough... that movie rocked... big time!</p>

<p><strong><span class="head2">10.</span> What films, music or books have recently inspired you? </strong></p>

<p>In terms of comics, though they're almost polar opposite extremes, the work of Stan Lee and Alan Moore has inspired me most. Both of them, master wordsmiths (in their own distinct ways). Films... Wow, too many to mention. My loose top 10, in no particular order, would be American Werewolf in London, Dirty Harry, Die Hard, The Third Man, The Maltese Falcon, The Big Easy, A Fistful of Dollars, Pulp Fiction, (the original) Assault on Precinct 13 and The Thing. They're my comfort movies, the ones I've seen many times, but can I can just sink into time and again. Books-wise, I'm a big fan of crime/detective fiction. Ed McBain, Robert B. Parker, Robert Crais, Michael Connelly, etc. I also like real science thrillers, and in that vein I read pretty much everything Michael Crichton turns out. Music-wise, I'm a bit stuck in the late 70s/early 80s, and I tend to go for stuff today that mirrors some of that energy: Kaiser Chiefs, Franz Ferdinand, The Strokes, etc. </p>

<p><strong><span class="head2">11.</span> In all seriousness, how often do your own political views come into play when writing about a fantastical civil war between giant robots? </strong></p>

<p>It's almost impossible not to be influenced by real-world political events. Age of Wrath is practically swathed in political parallels. But my own political views I try and keep apart from my work. I don't like to preach, not in a medium like Transformers. Some things should just be political-free zones, IMO. </p>

<p><strong><span class="head2">12.</span> What's the strangest experience you have ever had with a Transformers fan? </strong></p>

<p>Possibly being recognized by one outside Marks &amp; Spencer in Banbury, UK (in case there's a Banbury, US), by a lovely guy called Sid who just happened to be working the comic shop I was looking for at the time. Or maybe meeting an overweight (and sweating) fan dressed in a cardboard Jetfire costume, one that was practically falling off him (revealing long johns underneath), and being asked for my signature. BTW, this was at a TF convention... not outside M&amp;S.</p>

<p><strong><span class="head2">13.</span> Describe a typical working day in the life of Simon Furman�</strong> </p>

<p>Roll out of bed... Work... Eat lunch... Work... Eat supper... Work... Sleep. That's it. Ask my wife!</p>

<p><strong><span class="head2">14.</span> And to conclude, some word association: </strong></p>

<p><strong>Chris Ryall </strong></p>

<p>Meal ticket (just kidding, Chris!)</p>

<p><strong>E J Su </strong></p>

<p>Next big thing</p>

<p><strong>Scorponok </strong></p>

<p>Toast</p>

<p><strong>Dream Engine </strong></p>

<p>Pah</p>

<p><strong>Revenge of the Sith</strong></p>

<p>Double-pah</p>

<p><strong>Football  </strong></p>

<p>Come on you Hammers!</p>

<p>&nbsp;</p>

            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor" style="padding-right:0;"><img src="images/simonextrac2.jpg" width="197" height="264"> 
                  <p align="center">Transformers: Infiltration #0<br>
                    out in October!</p></td>
                <td width="26%" class="subcolor" style="padding-right:0;"><img src="images/simonextrac1.jpg" width="197" height="264"> 
                  <p align="center">Beast Wars preview art<br>
                    coming in 2006</p></td>
                <td width="48%" class="subcolor"><img src="images/simonextrac3.jpg" width="197" height="264"> 
                  <p align="center">The new Death's Head<br>
                    Amazing Fantasy #16 out in Dec</p></td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar"><p><a href="http://www.titanbooks.com/transformers.html" target="_blank">Titan 
                    Books</a>: You can see a full list of Titan's Transformers 
                    TPBs here.<br>
                    <a href="http://www.wildfur.net" target="_blank">Wildfur.net</a>: 
                    Andrew Wildman and Simon Furman's production company. The 
                    site includes updates on their current activities as well 
                    as information on services and properties. <a href="http://www.whorunstheengine.net/" target="_blank"><br>
                    The Engine: Industrial Strength</a>: Andrew and Simon's online 
                    graphical story telling project. Check it out. <a href="http://www.idwpublishing.com/" target="_blank"><br>
                    IDW Publishing</a>: check out IDW's site here for current Transformers information. <a href="http://www.dk.com" target="_blank"><br>
                    <a href="http://www.amazon.com" target="_blank">Amazon.com</a> 
                    and <a href="http://www.amazon.co.uk" target="_blank">Amazon.co.uk</a>: 
                    find TPBs of Simon's existing Transformers work here.</p>
                  </td>
              </tr>
            </table>
   <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.whorunstheengine.net/" target="_blank"><img src="images/engine.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <br> </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
