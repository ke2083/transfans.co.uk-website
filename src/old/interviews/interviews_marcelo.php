<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Marcelo Matere</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Marcelo Matere<br>
              </span><span class="blackstrong">interviewed in:</span> December 2007</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/m1.jpg" width="100" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
                Brazilian artist Marcelo Matere has a bright future ahead of him. He has a plethora of Transformers box art and profile books on his resume but he's also notching up some essential work on IDW's ever-popular Spotlight series. You can also check out some of his artwork here from the unreleased War Within: Age of Wrath... 
		</td>
              </tr>
            </table> 
       
	    <p><strong><span class="head2">1.</span></strong><strong>  So how popular is the Transformers franchise in Brazil, and what was your first exposure to them?
 </strong></p>
	
<p>    
The Transformers franchise in Brazil is big, especially after the movie and with all the related products. Almost everyone knows the brand. Besides that it's not as big and organised as it is in the US but we've got a lot of people who love the brand and help a lot to promote it on their websites, organising small conventions and some meetings to share and promote all the news on the TF Universe.
</p>
<p>
My first exposure was with the cartoon show on Sundays afternoons. I went crazy the first time that I saw those robots transforming. It was amazing! I loved the Dinobots! Unfortunately we didn't receive the big toys in Brazil, only the small ones - some Minibots (mostly the cars versions). Then they relaunched them again with new colours and new insignias.
</p>
            <p><strong><span class="head2">2.</span>  You've been involved in various Transformers comics for a while now. Tell us about your first assignments.
 </strong></p>
	    
<p>I'd already done some stuff for the Genesis book and some box art for Hasbro when Dreamwave contacted me. I was first put to work on the G1 More Than Meets The Eye profile books. Then after that 3H and OTFCC invited me to work as a filler on issue 3 of the Universe comic. That was my first attempt on sequential stuff in Transformers comics.</p>

            <p><strong><span class="head2">3.</span>  One of your first stories was the big Megatron/Scorponok fight in Dreamwave's Energon 30. Did you feel any pressure drawing such a pivotal issue?
 </strong></p>
	      
            <p>Yeah, that was my second comic book! I got nervous when I read the script - like it was a dream coming true - and I couldn't believe that I finally was working on something for a big publisher and drawing Transformers at the same time! I felt a pressure mostly because I wasn't sure if Dreamwave would like my work. It's one thing to draw static robots for box art and profiles books, drawing comics is totally different. I had to explore more perspective, and camera stuff. And we already had Don and Guido doing amazing work for those robots.</p>

            <p><strong><span class="head2">4.</span> You also worked on the unreleased issue 4 of War Within: Age of Wrath. What did we miss? </strong></p>
	    
<p>Take a look at the pictures! It's the first time I've shown this stuff.</p>
                 <table width="100" border="0" cellpadding="0" cellspacing="0">
		<tr valign="top">
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive.jpg"><img src="images/warwithinissue4_exclusive_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 1</td>

	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive2.jpg"><img src="images/warwithinissue4_exclusive2_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 2</td>

		  <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive3.jpg"><img src="images/warwithinissue4_exclusive3_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 3</td>
		  
		</tr>
		<tr valign="top">

	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive4.jpg"><img src="images/warwithinissue4_exclusive4_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 4</td>

	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive5.jpg"><img src="images/warwithinissue4_exclusive5_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 5</td>

		  <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive6.jpg"><img src="images/warwithinissue4_exclusive6_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 6</td>
		  
		</tr>

		<tr valign="top">

	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/warwithinissue4_exclusive7.jpg"><img src="images/warwithinissue4_exclusive7_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>War Within Issue 4 (Exclusive) Pg 7</td>
		
		  <td class="subcolor" style="padding:6px;padding-left:0;" align="center">&nbsp;</td>
		  
		  <td class="subcolor" style="padding:6px;padding-left:0;" align="center">&nbsp;</td>
		
		</table>
            <p><strong><span class="head2">5.</span> How does it compare working for different companies, whether Dreamwave, IDW or the convention exclusives?
 </strong></p>
	    
<p>It's not so different working for those companies. I think Dreamwave and 3H were a little bit different because it was my first time doing professional comic book work so they asked me to send some sketched pages before the final pencils. Most of them give you the freedom to put your style forward and create something new on the pages, especially IDW. I think once Dreamwave asked me to change some of my faces. They asked me to draw them more like Pat Lee, Don Figueroa and Joe Ng and avoid use of the G1 style (that's something I really like to do, but I think only Guido and Makoto can do them really well).</p> 

            <p><strong><span class="head2">6.</span> Spotlight Soundwave was your first big showcase for IDW. Talk us through it.</strong></p>
	    
<p>Yeah, I loved working on that book because I love the character. It was also another opportunity to work with Simon again and start to work with IDW so I thought, "man I have to do my best on this pages." Like thats my chance to show that I could do pages better than that Energon issue that I did for Dreamwave. So I tried to do the entire issue really well. I even took some pictures from the toys to draw the details better. Another thing I tried was to do some of the action scenes in a really dramatic and intense way.</p>

            <p><strong><span class="head2">7.</span> What characters would you like to work on in future Spotlights if you could choose? 
 </strong></p>
	    
            <p>I think the Dinobots and Constructicons. I love those guys. Especially the Eric Holmes Constructicons from the Megatron series. It was pretty cool what they did with those poor Empty guys, ha ha. But I like Prowl too.</p>
	      
            <p><strong><span class="head2">8.</span> Megatron: Origin met with a mixed reaction, though your work on it was essentially the only aspect to escape without criticism. How did you become involved in the project and how did you feel about your contribution?</strong></p>
	    
            <p>Well... Chris Ryall invited me to work on this book when I was working on the Soundwave Spotlight. However, when we realised that it would be complicated to get me on the series as I was delayed with the Soundwave book he asked me if I wanted to work only with the alternative covers. I said okay, let's do it that way.</p>
<p>
After that Chris emailed me again asking to help Alex on the second issue which was really delayed so he sent me the second half of the book. Well it was awesome to draw Sentinel Prime, Megatron and Soundwave but I really had a hard time getting my style close to Alex's, and at the same time draw almost 10 panels per page (joking Eric!). Because of that my stuff was delayed and Alex had to finish the last 3 pages of the book. Overall I liked the final pages, especially my inks but I felt that I could do better.
</p>
            <p><strong><span class="head2">9.</span>  What is it like reading a script for the first time? How do you decide how to translate it? How soon do you get started? </strong></p>
	    
            <p>It usually takes me awhile to start working on pages. I like to read the entire script at least 3 times to get the mood of the issue and to figure out what references I will need, how I can do the panels or if I have to take some pictures to use as references for backgrounds or even for the characters. I'm not so fast when working on pages. I'm not so used to it so sometimes I think they're not good enough. Then I do a lot of sketches of the same page and it takes more time, ha ha. Sometimes more time than I actually have...</p>

            <p><strong><span class="head2">10.</span>  What's your verdict on this year's Transformers movie, and what would you like to see in the next one?
 </strong></p>
	    
            <p>I loved it! I liked the new characters such as Bonecrusher and Barricade and have watched this movie hundreds of times. I listen to the soundtrack score almost every day while I'm working. I really liked this new universe and the new look, especially after watching it on the big screen. The first time I saw the designs and concepts (early 2006) I thought... man... what the hell is that? (like most every fan around the world!), but then when I saw the trailer and finally the movie everything... transformed!</p>

<p>
I really want to see the Dinobots in the next one, and more fights between those giant ****ing robots!</p>
	    
            <p><strong><span class="head2">11.</span> What does the future hold for Marcelo Matere?  </strong></p>
	    
            <p>A lot of cool and different stuff. I've worked on more childrens books, doing some pages for the Bee Movie Storybook, more package art for a new Transformers line and doing more concepts for the Robot Heroes line. For IDW... well I think you might already know what I'm gonna be working on... it's really awesome! [a Grimlock Spotlight with Simon Furman - ed]</p>
	    
            <p><strong><span class="head2">12.</span> Cover Association</strong></p>
	   <p>What memories or thoughts do these covers bring back to you?</p>
 
           <p><strong>Megatron Vs Scorponok</strong></p><p><a href="images/energon30.jpg"><img src="images/energon30_thumb.jpg" height=304 width=200></a></p>

            <p>My first professional comic book. A dream coming true!</p>
	    
	    <p><strong>Soundwave</strong></p><p><a href="images/006_1174754426.jpg"><img src="images/006_1174754426_thumb.jpg" height=304 width=200></a></p>

	    <p>My first cover for a big Transformers publisher! And the first time that I was drawing G1 characters.</p>

	    <p><strong> Megatron: Origin </strong></p><p><a href="images/holmes1.jpg"><img src="images/holmes1_thumb.jpg" height=304 width=200></a></p>

	    <p>One of my best covers. I liked working on this series as it helped me improve my skills.</p>
	    
            <p><strong><span class="head2">13.</span> Word Association</strong></p>
	   <p>What's the first thing that comes to mind when you think of...</p>
 

	    	    <p><strong> Rodimus Prime </strong></p>
	    <p>One of the ugliest G1 toys that I've ever seen!</p>
	    
	    	    <p><strong> The Hobbit</strong></p>
	    <p>Can't wait for this movie. Especially the dragon Smaug!</p>
	    
	    	    <p><strong> Beast Machines</strong></p>
	    <p>I have to watch it again...</p>
	    
	    	    <p><strong> Wheelie</strong></p>
	    <p>The guy we love to hate.</p>
	    
	    	    <p><strong> Midi-Chlorians </strong></p>
	    <p>I need some of them sometimes.well most of the time.</p>
	    
	    	    <p><strong> Pele </strong></p>
	    <p>The King. How I wanted to see this guy playing...</p>
	      
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/m2.jpg"><img src="images/m2_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>Transformers (2007) Decepticons</td>

	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/optimus_prime_low.jpg"><img src="images/optimus_prime_low_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>Alternators Optimus Prime</td>
		
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/TF_book_pg44_pencil.jpg"><img src="images/TF_book_pg44_pencil_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>Transformers (2007) Book</td>
		</tr>
		<tr valign="top">
	      <td colspan="2" class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/Optimus-Primal-Gorilla.jpg"><img src="images/Optimus-Primal-Gorilla_thumb.jpg" height=300 width=600 style="border:2px white solid;"></a><br>Beast Wars Optimus Primal</td>

	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/soundwavespotlight.jpg"><img src="images/soundwavespotlight_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>Spotlight: Soundwave (1)</td>     
		
		</tr>
		
		<tr valign="top">
		
		<td colspan="3" class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/soundwavespotlight2.jpg"><img src="images/soundwavespotlight2_thumb.jpg" height=300 width=200 style="border:2px white solid;"></a><br>Spotlight: Soundwave (2)</td>
		
		</tr.

	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
