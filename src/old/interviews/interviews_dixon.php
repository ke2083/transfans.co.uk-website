<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Chuck Dixon</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Chuck Dixon<br>
              </span><span class="blackstrong">interviewed in:</span> June 2006</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/dixon.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">He's written for some of the most iconic characters in comics, including Batman, Conan and The Punisher, but now Chuck Dixon comes to the world of the Transformers with <i>Hearts of Steel</i>, the first tale in IDW's <i>Evolutions</i> series.<br><br><a href="http://www.dixonverse.net" target="_blank">www.dixonverse.net</a><br><br><a href="http://www.idwpublishing.com/titles/transformers/hos.shtml" target="_blank">IDW information for <i>Hearts of Steel</i></a> </td>
              </tr>
            </table> 

<p><br><strong>Thanks very much for agreeing to take part in our interview... <br><br><span class="head2">1.</span> What experience did you have of the Transformers brand before becoming actively involved in it for IDW? What were your first impressions? What kind of research did you do to get to grips with the concept and lore behind the 20+ year franchise? </strong></p>

<p>I was, of course, aware of the Transformers before going into this. I'd seen the toys and some of the cartoons. Transformers was being done as a comic by Marvel when I was working there and edited by some of the editors I worked with so I got an inside view of how the characters were being adapted to comics form. When I got the assignment I picked up the DK guide along with a bunch of comics and devoured them. </p>

<p><strong><span class="head2">2.</span>  Transformers seems quite a departure for you. What's the attraction of working on what some would refer to as a 'toy comic'? </strong></p>

<p>Transformers is more than a toy. It's a terrific high-concept science fiction adventure and that's how I approached it. Though, my second love (after comics) is toys so the fact that this property is tied to a toy line didn't bother me at all. </p>

<p><strong><span class="head2">3.</span> 'Hearts of Steel' exists in a universe of its own. Did you prefer the opportunity to not write within established continuity?  </strong></p>

<p>Well, this COULD be in continuity. I really didn't feel right dropping into such a long-established franchise and start monkeying with it or showing the creators long associated with it �how it's done.� I'm very aware that the Transformers have a loyal fan following and wanted to do a story that appealed to them. There's a lot of deniability in the story that would allow fans to shoehorn this one into current continuity as a �forgotten tale� or, more accurately, a �deleted file.�  </p>

<p><strong><span class="head2">4.</span> What challenges are there in writing for alien robots instead of, say, superheroes? Where do you think the storytelling has greater potential, and where is it more constrained? </strong></p>

<p>Bending formula and finding something fresh are the real challenges when writing genre stuff. The challenges are doubled when you jump into a continuity-rich deal like the Transformers. The real trick for me is how do these enormous machines remain hidden among us? Coming up with ways for them to hide themselves is the fun part. Especially, as in this limited series, when the story takes place in the 1880s.  </p>

<p><strong><span class="head2">5.</span>  Did you choose the Industrial Revolution setting? </strong></p>

<p>This concept was established before I was asked to participate. But it sure made me agree to do it in a hurry. I love the idea of giant, steam-driven robots interacting with folks in the early industrial age.  </p>

<p><strong><span class="head2">6.</span> It's wonderful to see fan-favourite Guido Guidi on the art of the book, but why did original artist Ted McKeever leave the project? Did the change then inspire any alterations in the script? </strong></p>

<p>I don't have the inside info on why Ted left. I can only assume it was to take on the Princess of Mars project at IDW. I was looking forward to working with him. But Guido's stuff is astounding and that made the shift easier. I really didn't have to shift gears too much for the art change even though their styles vary wildly.  </p>

<p><strong><span class="head2">7.</span> You've worked for nearly every comic publisher going. How does the experience with IDW compare?  </strong></p>

<p>Ted Adams, the Big Man at IDW, and I have been friends since we were both at Eclipse Comics. Ted's a good person and a smart operator. One of the VERY few guys in this business I'd trust on a handshake. </p>

<p><strong><span class="head2">8.</span> Aside from changes in plot direction, what do you think are the pros and cons of writers crossing over between companies? Do fans of one property seem more inclined to try another under a particular author, in your experience? Have you ever found fans of a property hostile not to current writing but to you as a writer &#8212; eg, personal politics, something you once wrote for a character? </strong></p>

<p>Hostile fans? Do you know something I don't?</p>

<p>Comic fans are an opinionated bunch. And they tend to set their opinions in stone. I have a fan base that will buy just about anything that I write. There are others who will never forgive me for some perceived slight or omission in a past script. Nothing I can do about that. I prefer to think of the more casual, mainstream reader over the devoted fan. This mini will reach Transformers fans outside of the area of comics and that's the audience I concentrate on.  </p>

<p><strong><span class="head2">9.</span> Do the dinosaur Transformers seen in the Free Comic Book Day issue play more than a flashback part in 'Hearts of Steel'?  </strong></p>

<p>The dinos are only in the flashback. </p>

<p><strong><span class="head2">10.</span>  How about some more hints on what we can expect in the upcoming mini-series?  </strong></p>

<p>Mark Twain! Jules Verne! John Henry! (that steel drivin' man) Robot vs robot action and the most amazing train crash ever portrayed in a comic book.  </p>

<p><strong><span class="head2">11.</span>  What angle do you think the makers of the new Transformers movie should take in order to make it a success? </strong></p>

<p>Deal a LOT with the concept that the Transformers hide among us. That any machine they see could be a Decepticon or an Autobot. </p>

<p><strong><span class="head2">12.</span>  And to finish, some word association...</strong></p>

<p><strong>Bumblebee </strong></p>

<p>Tuna </p>

<p><strong>Confederacy  </strong></p>

<p>Lost cause </p>

<p><strong>Batgirl </strong></p>

<p>Year One!</p>

<p><strong>Steamboats </strong></p>

<p>Gambling </p>

<p><strong>Muzak </strong></p>

<p>Ick</p>

<p><strong>Adventure  </strong></p>

<p>Let's go!</p>

<p><strong>Thank you very much for your time, and all the best for future projects!</strong></p>

            <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><img src="images/dixonextra1.jpg" style="border:2px white solid;"><br>A moonlit Shockwave &#8212; cover and<br> all art herein by Guido Guidi</td><td class="subcolor" style="padding:6px;padding-left:0;" align="center"><img src="images/dixonextra2.jpg" style="border:2px white solid;"><br>Human meets giant robot in inked art for <i>Hearts of Steel</i> #1, page 21</td><td class="subcolor" style="padding:6px;padding-left:0;" align="center"><img src="images/dixonextra3.jpg" style="border:2px white solid;"><br>Advertised art with Bumblebee and John Henry from the series</td></tr></table>

&nbsp;

            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
