<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Tim Seeley</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Tim Seeley<br>
              </span><span class="blackstrong">interviewed in:</span> February 2006</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/seeley.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">Tim Seeley is the writer and creator of horror comic Hack/Slash, featuring characters such as undead teenager Evil Ernie and his sentient smiley face button. He became regular artist on the G.I. Joe title of <a href="http://www.devilsdue.net/">Devil's Due Publishing</a>, before coming to the attention of Transformers fans as artist on the second volume of <a href="comics_guide.php?series=19">G.I. Joe vs The Transformers</a>. With the third volume Tim takes over as writer. </td>
              </tr>
            </table> 

<p><br><strong>Hi, thanks again for agreeing to an interview with us.<br><br><span class="head2">1.</span> How did you get into the comics business and who and what were your original influences?</strong></p>

<p>I've been a big comic book fan since I was 5, so basically, I've been working on doing this as a job since about the same time I learned to ride a bike. My first really pro gig was when my old Con buddy, Josh Blaylock invited me to work for Devil's Due.</p>

<p>I think I'm probably influenced by a little of everything, from comics to movies to the crazy woman who lives on my corner and always asks for cigarettes. But, as far as creators I really followed and wanted to learn from, I'd say Alan Moore, Erik Larsen, Rob Schrab (of Scud:Disposable Assassin) and Art Adams. </p>

<p><strong><span class="head2">2.</span> Tell us about some of your past and present projects.</strong></p>

<p>I started out writing and drawing Lovebunny and Mr.Hell, which was a tongue and cheek superhero comic. I went from that and got dropped straight onto G.I.Joe. I did 20 issues of that, meanwhile, developing and writing my horror comic, Hack/Slash. Now I'm working as the artist on the Dark Elf Trilogy starring everyone's favorite drow, Drizzt Do'Urden as well as writing a one-shot for Image called Loaded Bible. </p>

<p><strong><span class="head2">3.</span> When it comes to the Transformers and G.I. Joe, what would you say the strengths and weaknesses of each license were? </strong></p>

<p>I think both really work because they have strong core concepts, but are open enough for some reinterpretation and change. G.I. Joe's real strength, to me, is that it combines a lot of things that just about anyone can love... James Bond spy stuff, colourful villains, and human drama. Transformers' strengths lie in one simple thing: BIG ROBOTS WHO CAN CHANGE INTO STUFF KICKING BUTT. That's about as perfect of an idea as anyone has EVER had.</p>

<p>Weaknesses? Hm... well, to me, the one thing that could potentially hold Transformers back is the relate-ability factor. A writer really has to work to find common threads between the average reader, and a 30 ton war machine. It 's not impossible, but I've seen it done badly. G.I.Joe's only real weakness is that it can be difficult to tell a war-story, complete with terrorists, in the current political climate. Joe's tendency to be a little too close to real-life can be a liability. </p>

<p><strong><span class="head2">4.</span> Were you ever a fan?</strong></p>

<p>Heh, I've admitted this before, but growing up I was mostly a He-Man fan. We didn't have cable in my very rural hood, so I never got to watch the Joe or TF toons. But I did religiously read the Transformers comic, and had some TF figures. I was obsessed with the G.I.Joe movie when it came out on video... but I didn't really get to know G.I.Joe until I started at DDP. Now, I'd say I have an expert level knowledge of Joe... working on it EVERYDAY for three years will do that! I had to freshen up a bit on TFs, but I think my childhood love will seep through. </p>

<p><strong><span class="head2">5.</span>  Why have you made the switch from artist to writer for Volume 3 of G.I. Joe Vs Transformers?</strong></p>

<p>I really wanted to write one of these crossovers... I LOVE crossovers, especially between these two properties. I've been throwing out ideas and bothering Josh for years to let me do this. I think maybe seeing me write all those Hack/Slash stories convinced him I was ready... or maybe it was those incriminating photos... mwa hahahahaha. </p>

<p><strong><span class="head2">6.</span>  It seems a classic Transformers and G.I. Joe crossover has yet to be written. What are the ingredients to getting it right? Do you think the two franchises can work together? </strong></p>

<p>Absolutely! It is a tough job to combine two very different flavours into one satisfying treat... but it's possible, and when it works, I think it can remind you why comics are so great. To me, the main thing to getting it right is keeping the characters in character. For a crossover to work, you need to see your favourite characters being who they are, and how they relate to other characters that they don't normally associate with. It's also important to find a balance... you don't want to slight fans of either property. And, last, but not least, you have to be creative as hell... you have to find shared threads that can be played with... you have to find those moments that make readers excited. </p>

<p><strong><span class="head2">7.</span> How do you feel the Devil's Due take on the past crossovers compared with that of Dreamwave's? </strong></p>

<p>Well, ours have been more consistent in that they've actually come out as scheduled. Our take was also more considered... we knew we'd want to continue these stories, so we didn't make it difficult to continue our story by setting it in World War 2 or whatever. I think people will find that our Joe/TF-verse is a unique �universe� unto itself, and it's very respectful to both properties, while creating new situations and characters impossible to do in each property's individual universe. </p>

<p><strong><span class="head2">8.</span> The last volume seemed a little tongue-in-cheek. Are Devil's Due trying not to take the concept too seriously?</strong></p>

<p>Every story takes on it's own flavour based on the premise... and Volume 2 was just more over-the-top than Volume 1. I think keeping that one more �wink-wink-nudge-nudge� was important in selling the cosmic time-travel concept. Art of War is less tongue-in-cheek, but there's still some humour in it. Ultimately, these are meant to be played as the OMEGA BUDGET movie that you'll never get to see... the summer blockbuster that you'd sell your soul to get a ticket. So, ya gotta have a few larfs. </p>

<p><strong><span class="head2">9.</span> Any idea why Devil's Due didn't get the Transformers license? </strong></p>

<p>None whatsoever. </p>

<p><strong><span class="head2">10.</span>  What was the inspiration for the plot for the new series? Can you give us some hints as to what we can expect? </strong></p>

<p>I wanted this one to be the big, �Return of the Jedi� epic... the across time and space race against time, complete with big fights and little moments. In the second volume, we saw Prime asking the Joes to dismantle any Cybertronian based tech, to assure it wouldn't be exploited. In Art of War, the Joes discover the government has built a TF based super soldier called Serpentor, and that's where the fun begins.... </p>

<p><strong><span class="head2">11.</span>  Can you foresee working on Transformers again in some capacity in the future? </strong></p>

<p>Yeah, I'd love to. I have a few ideas, and a major love for Optimus Prime. He's Bruce Lee, John Wayne and a Mac Truck rolled into one.</p>

<p><strong><span class="head2">12.</span>  Are there any other properties in particular that you would like to work on? </strong></p>

<p>As of now, I'm really enjoying making up my own stuff... though I'd definitely jump at the chance to write more Joe or TF stuff. In general though, I'm finding I'm losing interest in writing the Big Comic properties, and gaining interest in creating new concepts in new genres. </p>

<p><strong><span class="head2">13.</span>  Which was better, the G.I. Joe or Transformers movie?</strong></p>

<p>G.I.JOE. No question. We debate this in the office on a weekly basis. I know Cobra-La is a �jump the shark� addition to the Joe verse, but it's actually a really good movie with some really memorable moments. Sgt. Slaughter whupping on Nemesis Enforcer still makes me cheer. The opening sequence alone is one of my favourite five minutes of film ever made. The TF movie, while i like it, still makes me scratch my head... all of a sudden TFs are dying from laser blasts all over the place, when before, getting shot was the equivalent to bumping your head. </p>

<p><strong><span class="head2">14.</span>  And finally some word association...</strong></p>

<p><strong>Joe Ng</strong></p>

<p>The best damn TF artist in North America. And, he can draw people! </p>

<p><strong>Arcee </strong></p>

<p>Sure, she's taller than me, but I'd ask her out. </p>

<p><strong>Josh Blaylock </strong></p>

<p>Josh and I went to see Less than Jake last night... a ska punk band we both listened to when we were teenagers. Nothing makes you feel older than when a band refers to the first album you owned by them, as their �oldies.� </p>

<p><strong>Lost</strong></p>

<p>Man, I wish I was as cool as Sawyer. </p>

<p><strong>Fish</strong></p>

<p>I used to pay my friend Aaron a dollar to swallow live gold fish.</p>

<p><strong>Tim, thanks very much for your time, and we wish you all the best with your forthcoming comics!</strong></p>

            <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><img src="images/seeleyextra1.jpg" style="border:2px white solid;"><br>G.I. Joe vs The TFs: The Art of War #1 & 2 covers (art by Joe Ng)</td><td class="subcolor" style="padding:6px;padding-left:0;" align="center"><img src="images/seeleyextra2.jpg" style="border:2px white solid;"><br>Art of War #3 cover (art by Joe Ng)</td></tr></table>

&nbsp;

            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
