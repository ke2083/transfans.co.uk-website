<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews (2004-2007)</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>

<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>

<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr valign="top"> 
    <td> 
      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="18">&nbsp;</td>
          <td class="greybar" width="45">&nbsp;</td>
          <td width="740" valign="top" class="txt"> <p>&nbsp;</p>
            <p><span class="bluehead">Interviews (2004-2007)</span></p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_marcelo.php" class="noline">Marcelo Matere</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
			<br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                        <td width="100" valign="middle"><a href="interviews_marcelo.php"><img src="images/m1.jpg" width="55" height="75" class="blueborder"></a></td>
                                                        <td valign="top">
                                                       	Brazilian artist Marcelo Matere has a bright future ahead of him. He has a plethora of Transformers box art and profile books on his resume but he's also notching up some essential work on IDW's ever-popular Spotlight series. You can also check out some of his artwork here from the unreleased War Within: Age of Wrath... | <a href="interviews_marcelo.php">Full interview</a>
                                                                                        </td>
                                                                                                      </tr>
                                                                                                                  </table>
                                                                                                                 <br/>
																												 
																												 <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_geoff.php" class="noline">Geoff Senior</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                                        <td width="100" valign="middle"><a href="interviews_geoff.php"><img src="images/geoff_senior1.png" width="55" height="75" class="blueborder"></a></td>
                                                        <td valign="top">
                                                      Call us old fashioned at TransFans.co.uk but Geoff Senior remains our favourite Transformers artist. He made his debut back in January 1986 on issue 42 of the UK comic and instantly revolutionised Transformers storytelling. He went on to contribute iconic visuals in some of the comic's finest ever epics, including Target 2006, The Legacy of Unicron, Matrix Quest, On the Edge of Extinction and the G2 comic. He also co-created Death's Head, drew all 10 issues of Dragon's Claws and recently returned to the Transformers fold with artwork for the first issue of the new UK comic from Titan. He gives us a rare interview on his life and times with our favourite warring mechanoids... | <a href="interviews_geoff.php">Full interview</a>
                                                                                        </td>
                                                                                                      </tr>
                                                                                                                  </table>
                                                                                                                 <br/>
                                                                                                                 
                        <table width="220" border="0" cellspacing="0" cellpadding="0">
                                      <tr>
                                                      <td><strong><a href="interviews_staz.php" class="noline">Stewart "Staz" Johnson</a></strong></td>
                                                                    </tr>
                                                                                  <tr>
                                                                                                  <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
                                                                                                                </tr>
                                                                                                                            </table>
            <br/> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="100" valign="middle"><a href="interviews_staz.php"><img src="images/staz_ico.jpg" width="55" height="75" class="blueborder"></a></td>
                <td valign="top">
                Stewart "Staz" Johnson was a late addition to the legendary Transformers UK art team. Joining not longer after the controversial switch to black and white strips, his debut was in issue 236's Matrix Quest prequel 'Deathbringer Part 2'. Staz went on to be one of the comic's most prolific interior artists, his work extending to a string of memorable covers that did much to forge the visual identity of Transformers UK in its latter years. Afterwards he had much success with The Big Two, drawing some of the most recognised comic characters in the world, but he's still found time to reminisce with us about his days with the robots in disguise... | <a href="interviews_staz.php">Full interview</a>
                </td>
              </tr>
            </table>
           <br/>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_white.php" class="noline">Steve White</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_white.php"><img src="images/white_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">  Things are coming full circle... the UK once more is getting comics with material original for its market, courtesy of Titan Magazines. Heading up efforts is Steve White, who brings a Marvel UK connection and has been instrumental in getting other creators adult fans will recognise on-board! | <a href="interviews_white.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>	
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_holmes.php" class="noline">Eric Holmes</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_holmes.php"><img src="images/holmes_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">   	Eric Holmes describes himself as "a furious game designer, designing &#8212; furiously &#8212; at Radical Entertainment." Various nextgen console projects are in the works, but on the Transformers front there's the matter of a four issue miniseries for IDW, showcasing the rise of ol' bucket-head himself: Megatron! | <a href="interviews_holmes.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>		    
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_roberts.php" class="noline">James Roberts</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_roberts.php"><img src="images/roberts_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">  Many Transformers fans have written fiction; not so many got 550+ page books printed up or so much appreciation from their peers. James Roberts was active with Brit group TransMasters UK long before this newfangled intarweb thing, and is herein interrogated on such subjects as the history of Transformers fandom over this way, his novel <i>Eugenesis</i>, and fellow TMUK bod Nick Roche landing an art and writing gig with IDW.<br><a href="interviews_roberts.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>	    
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_2006.php" class="noline">2006 Round-up</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_2006.php"><img src="images/2006.png" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Since we had some seasonal fun with this last time, we though we'd do it again. A bunch of people involved with Transformers as professionals or fans fill us in on their picks from 2006. (If you didn't have time to submit or your filters ate the mail, drop by the forum and we'll add you in!) Happy new year! | <a href="interviews_2006.php">Full answers</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_ruffolo.php" class="noline">Rob Ruffolo</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_ruffolo.php"><img src="images/ruffolo_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">  	Rob Ruffolo had highs and lows during the Dreamwave era, colouring the now classic first volume of War Within, but also illustrating the widely rather less well-regarded Micromasters. The former Art Director of the Canadian company has now transferred his talents to IDW, and he makes his debut there on the Sixshot issue of the Spotlight series. | <a href="interviews_ruffolo.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>	    
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_budiansky.php" class="noline">Bob Budiansky</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_budiansky.php"><img src="images/budiansky_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top"> 	It's likely none of us would be here if it hadn't been for Bob Budiansky. Responsible for so many Transformers names and profiles from the outset, he had an influential hand in the Marvel US comics over the huge span of its first 55 issues. His influence lasts to this day, as evident in the back-to-basics approach of the recent Infiltration series, and he's back by public demand, adapting the original Transformers movie in a new comic for IDW.<br><a href="interviews_budiansky.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_roche.php" class="noline">Nick Roche</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_roche.php"><img src="images/roche_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Nick Roche grew up watching the Transformers cartoon on television and reading the comics written by Simon Furman. Over 20 years on and Nick is living the dream, providing artwork for Furman on the new Transformers Spotlight series. In fact, he has been onboard IDW's Transformers bandwagon from the very start, illustrating ChrisCharger in their first issue. | <a href="interviews_roche.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>  	    
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_dixon.php" class="noline">Chuck Dixon</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_dixon.php"><img src="images/dixon_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">He's written for some of the most iconic characters in comics, including Batman, Conan and The Punisher, but now Chuck Dixon comes to the world of the Transformers with <i>Hearts of Steel</i>, the Industrial Revolution first out-of-time tale in IDW's <i>Transformers: Evolutions</i> series. | <a href="interviews_dixon.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>  
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_sullivan.php" class="noline">Lee Sullivan</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_sullivan.php"><img src="images/sullivan_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">When it comes to Transformers art, Lee Sullivan is a legend. He began his stint at Marvel UK on issue 92 with the first of many memorable covers, and soon went on to create iconic work in the likes of Salvage and Time Wars. Over the years he's worked on many other titles, including Thundercats, Doctor Who, Robocop and William Shatner's TekWorld to name but a few, and has a noisy fetish for the saxophone. | <a href="interviews_sullivan.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>  
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_seeley.php" class="noline">Tim Seeley</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_seeley.php"><img src="images/seeley_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Tim Seeley is the writer and creator of horror comic Hack/Slash. He became regular artist on the G.I. Joe title of <strong>Devil's Due Publishing</strong>, before coming to the attention of Transformers fans as artist on their second volume of <a href="comics_guide.php?series=19">G.I.&nbsp;Joe&nbsp;vs&nbsp;The&nbsp;Transformers</a>. With the third volume Tim takes over as writer. | <a href="interviews_seeley.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>    
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_2005.php" class="noline">2005 Round-up</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_2005.php"><img src="images/2005.png" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">With this year having a particular resonance for almost any fan, thanks to Victor Caroli, and involving all sorts of Transformers news... we thought it would be nice to end it on a personal note. Though it was a busy holiday season, a range of creators and fans got back to us with what they felt were some highlights of 2005... so, our thanks to them all, and we wish all fans out there a prosperous 2006! | <a href="interviews_2005.php">Full answers</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_ryall.php" class="noline">Chris Ryall</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_ryall.php"><img src="images/ryall_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Many Transformers fans were surprised earlier this year when <strong>IDW Publishing</strong> seemed to pop up out of nowhere to land the license ahead of its competitors. In fact the company has long experience of working with licensed properties, including CSI, 24, Angel, Shaun of the Dead and many more. Chris Ryall is Editor-In-Chief of the company and is playing a large role in bringing the Transformers up to date in the 21st century. He's also Editor-In-Chief of Kevin Smith's website, transforms into a nifty sports car and has red on him. | <a href="interviews_ryall.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_raiz.php" class="noline">James Raiz</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_raiz.php"><img src="images/raiz_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">James Raiz has been delighting us with his Transformers art ever since Dreamwave bagged the licence in 2002, drawing the first 5 issues of Armada. His Transformers work since has included MTMTE, G1 and Energon, and he's added a list of credits for Marvel, DC and Top Cow to his already impressive CV.<br> James has recently been announced as the artist for the third incarnation of everyone's favourite freelance peacekeeping agent, Death's Head. | <a href="interviews_raiz.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_furman3.php" class="noline">Simon Furman</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_furman3.php"><img src="images/simon_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">What can you say about Simon Furman and Transformers? Well, lots of stuff, but unless you've been living under a Rock Lord� since 1985 then you probably know most of it. After a brief hiatus from writing Transformers thanks to Pat Lee's financial brilliance, Simon is back where he belongs care of <strong>IDW Publishing</strong> and will be writing both their new G1 series (Transformers: Infiltration) and a Beast Wars series next year, as well as reviving his relationship with fan fave Death's Head in the very near future. Not bad, eh? | <a href="interviews_furman3.php">Full interview</a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_su.php" class="noline">EJ Su</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_su.php"><img src="images/su_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">EJ Su's early work on Transformers included contributions to the <strong>Transformers Ultimate Guide</strong> and the second <strong>Transformers/Gi-Joe</strong> crossover from Devil's Due. With the announcement that IDW will now be publishing Transformers comics, EJ was revealed as the regular artist for the new G1 title. | <a href="interviews_su.php">Full interview </a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_guido.php" class="noline">Guido 
                  Guidi</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_guido.php"><img src="images/guido_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Guido Guidi started with Dreamwave 
                  in 2001, where his first Transformers work was the swanky Predaking 
                  Lithograph. Soon after he began regular strip work with <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=499&page=1">Transformers 
                  Armada issue 8</a>. Guido stayed with the title as it morphed 
                  into Energon up until <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=522&page=2">Issue 
                  23</a>. | <a href="interviews_guido.php">Full interview </a></td>
              </tr>
            </table>
            <p>&nbsp;</p><table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_figueroa.php" class="noline">Don Figueroa</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="top"><a href="interviews_figueroa.php"><img src="images/figueroa_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Don Figueroa originally came to the 
                  attention of Transformers fans with his awesome online comic 
                  Macromasters. Unsurprisingly Don was recruited by Dreamwave 
                  when they got the Transformers licence and, after drawing several 
                  posters, had his first official Transformers work published 
                  in <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=327" target="_blank">The 
                  War Within</a>, where he got the opportunity to show us how 
                  the Transformers looked before they left Cybertron. | <a href="interviews_figueroa.php">Full 
                  interview </a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_anderson.php" class="noline">Jeff 
                  Anderson</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="middle"><a href="interviews_anderson.php"><img src="images/anderson_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Jeff Anderson inked and coloured 
                  a number of issues for Marvel's Transformers UK before his pencilling 
                  debut with <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=77">Issue 
                  65</a>. After that Jeff was a mainstay of the comic, contributing 
                  to such classics as <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=90">Target:2006</a> 
                  and pencilling the first telling of the Transformers' origin 
                  in <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=183">The 
                  Legacy of Unicron</a>. Jeff's other comic work includes Judge 
                  Dredd and Captain Britain. | <a href="interviews_anderson.php">Full 
                  interview </a></td>
              </tr>
            </table>
            <p><br>
            </p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_joeng.php" class="noline">Joe 
                  Ng </a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="middle"><a href="interviews_joeng.php"><img src="images/joeng_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Joe Ng started work at Dreamwave 
                  drawing characters for the <a href="http://www.TransFans.co.uk/comics_guide.php?series=20">MTMTE</a> 
                  series. From there Joe progressed to being the regular penciler 
                  on Transformers <a href="http://www.TransFans.co.uk/comics_guide.php?series=16">Energon</a>. 
                  Most recently Joe has become the third artist to work on the 
                  fan favourite series <a href="http://www.TransFans.co.uk/comics_guide.php?series=15">The 
                  War Within</a>. | <a href="interviews_joeng.php">Full interview 
                  </a></td>
              </tr>
            </table>
            <p>&nbsp;</p><table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_furman2.php" class="noline">Simon 
                  Furman - Part 2 'The Present' </a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="middle"><a href="interviews_furman2.php"><img src="images/simon_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Simon Furman began his association 
                  with the Transformers in 1985 when he wrote his first issue 
                  for Marvel Comics. You can read more about Simon's time with 
                  Marvel <a href="http://www.TransFans.co.uk/interviews_furman.php" target="_blank">here</a>. 
                  More recently Simon has worked with Dreamwave comics to produce 
                  the acclaimed <a href="http://www.TransFans.co.uk/comics_guide.php?series=15" target="_blank">War 
                  Within</a>, the third volume of which debuts this month, and 
                  the ongoing <a href="http://www.TransFans.co.uk/comics_guide.php?series=16" target="_blank">Energon</a> 
                  series (previously Armada) for<a href="http://www.dreamwaveprod.ca" target="_blank"> 
                  Dreamwave</a>. | <a href="interviews_furman2.php">Full interview 
                  </a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_furman.php" class="noline">Simon 
                  Furman - Part 1 'The Past' </a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="middle"><a href="interviews_furman.php"><img src="images/simon_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Simon Furman began his association 
                  with the Transformers when he wrote his first issue of the UK 
                  comic, <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=25&page=1" target="_blank">issue 
                  13</a>, in 1985, and he has maintained his connection with the 
                  Robots in Disguise ever since. Simon continued to work for Marvel 
                  until the end of the <a href="http://www.TransFans.co.uk/comics_guide.php?series=21" target="_blank">Generation 
                  2</a> series and thereafter maintained his ties by writing the 
                  occasional <a href="http://www.TransFans.co.uk/cartoons_guide.php?series=27" target="_blank">Beast 
                  Wars</a> episodes and convention comics. | <a href="interviews_furman.php">Full 
                  interview </a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_cannon.php" class="noline">Paul 
                  Cannon </a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="middle"><a href="interviews_cannon.php"><img src="images/paulcannon_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Paul Cannon is the organiser and 
                  owner of the popular UK convention <a href="http://www.transforce.org.uk" target="blank_">Transforce</a>, 
                  which has been running (with a break in 2003) since 1999. Paul 
                  also invented the Space Shuttle and vegetables... | <a href="interviews_cannon.php">Full 
                  interview </a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="220" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><strong><a href="interviews_wildman.php" class="noline">Andrew 
                  Wildman</a></strong></td>
              </tr>
              <tr> 
                <td><img src="graphics/sitegraphics/blue.gif" width="100%" height="2"></td>
              </tr>
            </table>
            <br> <table width="95%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="100" valign="middle"><a href="interviews_wildman.php"><img src="images/wildman_ico.jpg" width="55" height="75" border="0" class="blueborder"></a></td>
                <td valign="top">Andrew Wildman pencilled numerous 
                  covers for the original UK Transformers comic and as of 198 
                  onwards was one of the comics regular artists. He later transferred 
                  to the US comic (whilst still providing pencils for some of 
                  the UK black and white stories) starting with issue 69 and was 
                  the artist on the title until the end of the comics run (issue 
                  80), and one of his notable achievements was adding toilet doors 
                  to Unicron. | <a href="interviews_wildman.php">Full interview 
                  </a></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p></td>
          <td class="media" width="218" valign="top">&nbsp; </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
