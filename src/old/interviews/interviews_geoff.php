<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Geoff Senior</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Geoff Senior<br>
              </span><span class="blackstrong">interviewed in:</span> August 2007</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/geoff_senior1.png" width="100" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
                Call us old fashioned at TransFans.co.uk but Geoff Senior remains our favourite Transformers artist. He made his debut back in January 1986 on issue 42 of the UK comic and instantly revolutionised Transformers storytelling. He went on to contribute iconic visuals in some of the comic's finest ever epics, including Target 2006, The Legacy of Unicron, Matrix Quest, On the Edge of Extinction and the G2 comic. He also co-created Death's Head, drew all 10 issues of Dragon's Claws and recently returned to the Transformers fold with artwork for the first issue of the new UK comic from Titan. He gives us a rare interview on his life and times with our favourite warring mechanoids...
                </td>
              </tr>
            </table> 
       
	    <p><strong><span class="head2">1.</span></strong><strong>What art background did you have before working on Transformers? We remember seeing your illustrations in an early Fighting Fantasy book... 
 </strong></p>
	    
<p>I did some 'Future Shocks' for 2000AD and a few other things, which I'm trying to remember! There was a 'Starblazer' or two for DC Thomson, maybe even a 'Commando' book, and possibly 'Warlord' and 'Battle'. It was a long time ago. I was getting most of my work through Pat Kelleher who ran the 'Temple Art Agency' before Ian Rimmer, who was the editor on 'Transformers', gave me a break. I'm not sure if the agency are still going but they were very nice to work for. Marvel UK didn't really deal with agents and I guess I started to drift away when I started to work for Ian. I haven't seen Ian for ages and would like to buy him a beer or two if he's reading this. </p>
 
            <p><strong><span class="head2">2.</span>  What was your first impressions of the Transformers? Did you have an idea of the phenomenon when you first joined the comic as an artist? </strong></p>
	    
<p>I had no idea. I was just pleased to get the work. I didn't know anything about Transformers other than they were toys that seemed popular at the time. Marvel gave me a video tape of the animated series and it was impressive.</p>

            <p><strong><span class="head2">3.</span>  How much artistic license did the scripts give you? Could you introduce ideas of your own? 
 </strong></p>
	      
            <p>I stuck to the storyline of the scripts and can't remember taking liberties with them. I saw my job as to interpret the story as best as I could. </p>

            <p><strong><span class="head2">4.</span> Who were your favourite characters to draw and were any storylines particularly fun to work on?   
 </strong></p>
	    
<p>Thinking about it I found most of them okay to draw. Megatron was a good design and the Dinobots were good too. In fact in the depths of my memory I think I had most fun drawing the Dinobots. </p><p>

I can't remember ever thinking "Oh I wish I could get to the end of this story," so I suppose that would suggest I was having a good time on most adventures. Almost all involved some sort of battle or punch up and that kept you on your toes. Actually I have just remembered! I got really fed up of drawing the Matrix device that was inside Prime's chest (least I think it was inside Prime's chest some of the time). 
</p>

            <p><strong><span class="head2">5.</span> Were there any particular challenges when drawing Transformers as opposed to non-robot comics?  
 </strong></p>
	    
<p>Keeping the 'life' in the drawings. Robots being generally blocky and angular can have a tendency to look a bit static and lifeless. I would be the first to admit that my versions weren't the most technically correct. But that's mainly because I was trying to keep the fluidity and energy in my work. Some people saw this as being some kind of 'cop out' but I didn't see what was gained by drawing every nut and bolt. </p> 

            <p><strong><span class="head2">6.</span>Death's Head was originally intended to be a one-off character, but thanks to your imaginative and colourful design he quickly took on a life of his own. Could you tell us a little about the process you went through in creating him and how you developed the character?   </strong></p>
	    
<p>The brief was simply to come up with a 'throw away' bounty hunter character. The name 'Death's Head' suggested a skull of some description so that's how he developed. Simple moving eyebrows gave him enough expression - in fact sometimes they almost left his head! Once Simon saw him he had no problem in determining his character. 'Tin Head' was the most fun to draw. </p>

            <p><strong><span class="head2">7.</span>  After the UK and US Generation 1 titles came the US Generation 2 comic, a publication often up against it when it came to deadlines. Do you remember how you came to be drafted onto the project and what the experience was like?   
 </strong></p>
	    
            <p>I was asked to help out when the artist was up against it making the deadlines. There was a double issue I believe which really made the timing tight but it was enjoyable to work on the 'Swarm' story with Simon and editor Rob Tokar.  </p>
	      
            <p><strong><span class="head2">8.</span> The transition from Death's Head to Death's Head II was an unpopular move in many people's eyes. How did you feel about it?</strong></p>
	    
            <p>The design was totally changed. I think the humourous side of Death's Head's character wasn't appreciated. The new design was very good but it should have been a totally new character. I don't really know why he had to be altered. He was fine as he was.   </p>

	    
            <p><strong><span class="head2">9.</span>  It seems surprising that you didn't make as big an impact in American comics outside of Transformers as some of your Marvel UK peers. Can you give us an insight into what it's like to attempt to break into such a big comic market? Did it contribute to your decision to move into advertising? 
 </strong></p>
	    
            <p>I guess I wasn't 'driven' enough. I did get the impression that the American comics were very much style orientated. Or maybe I wasn't into 'comics' enough. I just liked to draw and get paid for it. </p><p>

As an artist I've developed more in advertising. I've always liked to work fast and in advertising you certainly have to work FAST. Though I do believe it's important to have an escape into other areas now and again. To this end I've been busy (when I get chance) writing and illustrating a children's book I'm hoping to get published once finished. It's a million miles from advertising or comics which is why I'm enjoying the change.</p>

            <p><strong><span class="head2">10.</span>Transformers art has evolved since the Eighties and Nineties. What do you make of the new generation of Transformers artists? 
 </strong></p>
	    
            <p>I haven't really kept up with how the comic has changed but there are some good artists working on Transformers. The dedication to detail is amazing. They're clearly fans of Transformers which is where I'm sure they get their inspiration. There was a real expectancy that the comic would fold several years ago. How things have changed. And with the film out it should be stronger than ever.  </p>
	    
            <p><strong><span class="head2">11.</span> Whilst you're undoubtedly aware of the praise fans have awarded your work over the years, might we ask your reasons for mostly keeping the franchise at arm's length until now?   
  </strong></p>
	    
            <p>It was 'fresh' to me again. I would have liked to have coloured the strip [issue 1 of the new Transformers UK comic] but time wasn't on my side. I just really enjoy colouring in Photoshop these days. I am hoping to colour the 10 pages for my own enjoyment. The plan is to make prints and take some to the 'Film & Comic Con' at Earl's Court as I'm due to be there on Saturday 1st September. </p>
	    
            <p><strong><span class="head2">12.</span> Did it take awhile to get back in the swing of things, or was it like you'd never been away? And now that you're back, can we look forward to more titles graced with your artwork; Transformers or otherwise?  </strong></p>
	    
            <p>There was no hesitation in drawing Transformers again and I hope to fit more in from time to time. Now I've seen the film and I'm more familiar with the characters I reckon the next strip will be better... but I've got to finish my kids book first (sorry!). 
</p>
	     <p><strong><span class="head2">13.</span>  Tell us what you thought of the new film then...  </strong></p>
            <p>Great action sequences. They could have slowed a few down a bit though - it was difficult to see what was happening in some parts as these characters are really complicated and it can be difficult to work out what's happening. I think it will do well and keep the fans happy. </p>

            <p><strong><span class="head2">14.</span>Why do you think Transformers has lasted for so long?  </strong></p>
	<p>To keep Simon Furman in work? </p>

	<p><strong><span class="head2">15.</span>And lastly, we wanted to run some of your classic covers by you again to see what you make of them...</strong></p>

	    <p><strong>Megatron</strong></p><p><a href="http://TransFans.co.uk/popup.php?id=501"><img src="images/geoff_thumbs/ukcov051.png" height=266 width=200></a></p>

            <p>One of the best to draw. Coloured in ink. Don't see that much nowadays. </p>
	    
	    <p><strong>Triplechangers</strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1440"><img src="images/geoff_thumbs/ukcov088.png" height=266 width=200></a></p>

	    <p>Nice grouping. </p>

	    <p><strong> Galvatron </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1460"><img src="images/geoff_thumbs/ukcov102.png" height=266 width=200></a></p>

	    <p>Yes, the bad guys are always the most fun! </p>
	    
	    	    <p><strong> Hunters </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1475"><img src="images/geoff_thumbs/ukcov117.png" height=266 width=200></a></p>

	    <p>Interesting linear smoke coming from Galvatron's cannon...  </p>
	    
	    	    <p><strong> Headmasters </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1489"><img src="images/geoff_thumbs/ukcov130.png" height=266 width=200></a></p>

	    <p>Boring cover. Sorry, not my idea. </p>

	    	    <p><strong> Perchance to Dream  </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1628"><img src="images/geoff_thumbs/ukcov260.png" height=266 width=200></a></p>

	    <p>Flat and angular?  </p>

	    	    <p><strong> Rodimus Prime  </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1472"><img src="images/geoff_thumbs/ukcov113.png" height=266 width=200></a></p>

	    <p>Reckon Galvatron would take this bloke. </p>	    	    
	    
	    <p><strong>Geoff, thanks very much for your time! </strong></p>
	    <p>Thanks.  It's been a pleasure.</p>
	    
	      
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/gs01%20copyx.jpg"><img src="images/gs2_small.png" height=300 width=200 style="border:2px white solid;"></a><br>Transformers (2007)</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/gs02%20copyx.jpg"><img src="images/gs1_small.png" height=300 width=200 style="border:2px white solid;"></a><br>Transformers (2007) (2)</td>
	      </tr>
               <tr><td colspan=2 class="subcolor" style="padding:6px;padding-left:0;" align="center">Pencils and colours by Geoff Senior</td></tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
