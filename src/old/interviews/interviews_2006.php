<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Target 2006</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt">
    
	  <p><span class="bluehead"><br>Target 2006: Year in Review<br></span></p>
    
              <p>&nbsp;</p>
<p align="center"><strong><a href="#stevebax">Steve Bax</a> &bull; <a href="#bobbudiansky">Bob Budiansky</a> &bull; <a href="#joshburcham">Josh Burcham</a> &bull; <a href="#stuartdenyer">Stuart Denyer</a> &bull; <a href="#paulduggan">Paul Duggan</a> &bull; <a href="#donfigueroa">Don Figueroa</a> &bull; <a href="#nickroche">Nick Roche</a> &bull; <a href="#chrisryall">Chris Ryall</a> <br> <a href="#klausscherwinzki">Klaus Scherwinzki</a> &bull; <a href="#liamshalloo">Liam Shalloo</a> &bull; <a href="#richardstarkings">Richard Starkings</a> &bull; <a href="#ejsu">E.J. Su</a> &bull; <a href="#leesullivan">Lee Sullivan</a> &bull; <a href="#dantaylor">Dan Taylor</a></strong></p>

<br>

<a name="stevebax"></a><img src="images/2006bax.jpg" align="right">
<span class="head2">Steve Bax</span>
<br>
Webmaster 
<br>
<a href="http://www.oneshallstand.com" target="_blank">www.oneshallstand.com</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
Casino Royale: Action-packed, dark and thoughtful. I didn't think it was possible to make Bond new and original again but I was very pleasantly surprised.
<br>
<br>
Superman Returns: I doubt they'll ever top Superman II but it was a good effort. Really enjoyed Kevin Spacey's Luthor.
<br>
<br>
X-Men III: Not sure this was an 06 release but I saw it on an in-flight movie and enjoyed it.
<br>
<br>
The Lakehouse: Chick-flick with an interesting time-travel premise.
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
Didn't buy any (I must be getting old) but I downloaded a lot of stuff from the web. I found a Russian site with new and old songs for 15 cents each - bargin! Gnarls Barkley 'Crazy' is the song that reminds me of my hugely enjoyable and first trip to Australia in April.
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
Doctor Who second series: Tennant does an even better job than Chris Eccleston and the farewell scene with Rose was beautifully written - brought a tear to my eye *almost*. Also enjoyed Lost and The Apprentice.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
After a disappointing 2005 on the TF comics front this year was always going to be an improvement for me. I loved issues 1+2 of Infiltration with the Battlechargers/Thundercracker chasing down Ratchet, and the whole of Stormbringer. Spotlights have been excellent so far, and Escalation #1 has got off to a good start. I loved the shock ending but kind-of hope its not the last we'll see of Sunstreaker and Hunter.
<br>
<br>
Fave book of the year was The Game by Neil Strauss but also enjoyed The Time Traveller's Wife and Piers Morgan's insider confessions about his time at The Mirror.
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
Going to Australia in April/May and seeing Melbourne, Sydney and Ayers Rock - the field of stars you see in the outback is breathtaking. I bought a new car after my trusty Astra's engine blew-up on the A3 (turned out to not be so trusty in the end) and had several opportunities to take charge at the newspaper where I work, planning what's going on the front pages etc. that's been a lot of fun.
<br>
<br>
Mostly getting my TF comics business back online again after the Dreamwave collapse. I think the franchise is in safe hands with IDW and we should be in for an even better 2007. 
<br>
<br>
<br>
<a name="bobbudiansky"></a><img src="images/2006bud.jpg" align="right">
<span class="head2">Bob Budiansky</span>
<br>
Marvel and IDW Transformers writer
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
Casino Royale (I haven't enjoyed a Bond film this much in 33 years - since Live and Let Die!), Borat, Inside Man and� any other movies I would include here would be by default, because the rest of the movies I've seen this year have been passable at best. Okay, X-Men: The Last Stand was better than passable. But not by a lot.
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
I wish I knew of an entire album from 2006 I could call a "favoUrite." How about favoUrite songs of 2006? "Hamoa Beach" by Gomez, "Not Tonight" by the New Cars, and "The Trouble with Dreams" by the Eels, which is actually from 2005, but I only recently discovered this group, so I'm including it.
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
24, Lost, The Office (American version; haven't seen the British version), Monk. 
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
I don't read too many comics these days. As for books, these may not be 2006 vintage, but at least they're recent, and I read them in 2006: The Journey of Man: A Genetic Odyssey, by Spencer Wells; Collapse: How Societies Choose to Fail or Succeed, by Jared Diamond; Skinny Dip, by Carl Hiaasen.
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
Becoming the Recreation Director of the town I live in!
<br>
<br>
<br>
<a name="joshburcham"></a></a><img src="images/2006josh.jpg" align="right">
<span class="head2">Josh Burcham </span>
<br>
IDW Transformers artist and colourist 
<br>
<a href="http://dcjosh.deviantart.com " target="_blank">dcjosh.deviantart.com </a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
Favourite films... man... I had to Wiki what movies came out this year JUST so i could remember but yeah, upon researching I'd have to say these are my favourite movies ive seen this year:
<br>
<br>
Mission: Impossible III - best Mission Impossible yet. I've got no niggles with Tom Cruise like a lot of people. I thought he did good. The movie was non-stop the whole way through and i loved it!
<br>
<br>
X-Men 3: The Last Stand - not much to say other than its X-men and Magneto is the freakin' shiz. Best scene of the whole movie was when he lifted up a chunk of the Golden Gate, that was so rad.
<br>
<br>
Nacho Libre - I love Jack Black so this was an instant classic to me before I even watched it. It was hilarious.
<br>
<br>
Superman Returns - again, not so many niggles as others. Maybe it was a little long but i was pretty impressed. Personally i came into it with a lot lower expectations, I never cared for the original movies and always hated their interpretations of Lex especially. Smallville Lex FTW! But it was great with fantastic effects especially.
<br>
<br>
Pirates of the Caribbean 2 - this is a no brainer. Jack Sparrow. Nuff said. 
<br>
<br>
007: Casino Royale - I wasn't too thrilled with Daniel Craig being picked as the new 007 but i have to say I was impressed. It was a little jarring seeing such a buff and in shape 007, lol, but I really enjoyed the movie. He had a freakin' rad car too (but then again, when doesn't he ride in style?)
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
Skillet: Comatose 
<br>
Pillar: The Reckoning 
<br>
Disciple: The Scars Remain 
<br>
Falling Up: Exit Lights 
<br>
Emery: The Question 
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
Heroes - talk about a kick in the pants. Best TV show of 06 hands down. I love this show.
<br>
Smallville - This one's a given. Although it hasn't held my interest as much as Heroes (but I blame that on this being Smallville's like 5th season and this being Heroes' grand premiere). It's still a great show. I just wish they'd off Lana already. Boo! 
<br>
Without a Trace - I recently discovered this show and man it's great. I dig these type of shows.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
Transformers Stormbringer and Ultimate Spiderman, because i don't have time to read anything else, hehe. 
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
I'd have to say my biggest highlights have been two fold. One, getting to go to San Diego Comic Con and hanging out with some friends. And two, getting to be involved in as many Transformers Movie related projects as I have. Mwahahahahaa!
<br>
<br>
<br>
<a name="stuartdenyer"></a><img src="images/2006den.jpg" align="right">
<span class="head2">Stuart Denyer</span>
<br>
Site Staff
<br>
<a href="http://www.tfarchive.com" target="_blank">www.tfarchive.com</a>
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
Hmm... oh yeah, V For Vendetta was this year... and it's heresy I know, but I prefer it to the comic. Much more focused, the glitz and the glamour covering for a rather generic dystopian Orwell-derived plot. I think Moore's done much better work elsewhere. 
<br>
<br>
I'm quietly curious to see Happy Feet at some point - the foaming at the mouth from certain American critics about how it's supposedly going to make kids ask awkward questions about religious dogma have seen to that. Went to see The Da Vinci Code, and I suppose I'll watch Superman Returns now that it's out on DVD. No films that I've been waiting breathlessly to see, though. Will take a spin on 30 Days of Night next year, and Hellboy II the year after... and I've completely forgotten Bond... yeah, that'll probably be worth a rental. 
<br>
<br>
Looking at Wikipedia, there's a fifth Highlander film coming. Why can't people just let the first one rest in peace? 
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
Technically The Crimea's debut "label" album didn't come out over here until this year, but it's really 2005, and most of it got a low key release the year before that... under the more genuinely new header, The Hot Puppies got their debut sorted and out the door, which was well worth the wait (spiky Welsh indie pop with convoluted lyrics that are fun to unpick.) Nudging inside the boundary, another act with similarly fun lyrics from over that way who'll bear some watching is Pagan Wanderer Lu, but my copy of that EP hasn't turned up yet. Bloody postal service. 
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
Torchwood has been far more engrossing than Doctor Who for me so far, though I do prefer Tennant's Doctor to Eccleston. Both are classic British sci-fi; even with a reasonable budget it tends to look cheap, a bit overacted and forced, but that's part of the charm. I think the main things I like about Torchwood are the recurring cast and childish revelling in not having to script for a Blue Peter audience. 
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
Peter David seems to have gone off the boil a bit with Star Trek: New Frontier. Missing In Action was still quite fun but the second half of a novel and it felt a lot like fanfic this time. Christopher Brookmyre came back in style with the paperback of All Fun and Games Until Somebody Loses an Eye, another "iconically ordinary Scots dumped into the middle of Hollywood plots" title. He also won points for being a writer using tech in books/television that didn't make me want to beat my head off the desk... 
<br>
<br>
...and Transformers, naturally. IDW have turned out to be the anti-Dreamwave, communicating with fans, trying out new ideas and offering a variety of story types and lengths, with just as much thought given to variety in the art. I'm particularly loving the Spotlights so far, and the news that a TMUK bod is not only drawing but scripting one next year has me stoked. Looks like a great lineup to come on the comics front. 
<br>
<br>
Dishonourable mention goes to Wildstorm's universe reboot. 
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
Not as much to report as I'd like, but I did finally pass a driving test. And learn to wallpaper.
<br>
<br>
<br>
<a name="paulduggan"></a><img src="images/2006duggan.png" align="right">
<span class="head2">Paul Duggan</span>
<br>
Webmaster 
<br>
<a href="http://www.TransFans.co.uk" target="_blank">www.TransFans.co.uk</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
Well, obviously this is a bit of a non question with Big Momma's House 2 so blatantly blowing away the field in terms of pathos, plot and pioneering direction.  The academy's refusal to acknowledge Lawrence for his genius will no doubt be looked back on as one of the great scandals of whatever century we are currently in. If, however, I had to choose some distant second choices I would pick out X-Men 3, for just being stupidly enjoyable (depth is for girls) and Clerks 2 for being funny, affectionate and featuring both copious TF references and more importantly Rosario Dawson in a vest top. Also, it's imperative that we note that Miami Vice was both stupid and boring and thusly a massive disappointment.
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
What? New music is for kids. Thanks to the glory of itunes I was busy relieving my now distant youth and indulging in the glory of the Divine Comedy, Gene and the Tindersticks. Go on, look them up, they are way better than today's, so called, 'music'.
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
I enjoyed season 2 of Doctor Who, but probably not as much as the first season. Lost remains infuriatingly addictive and I am looking forward to watching 24 season 5 on Duh Vuh Duh come xmas morning� However Battlestar Galactica is just better than everything really isn't it? Even if I can't figure out how to download season three from the interweb. It wasn't like this in my day you know. Frak.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
Well, to be honest this list hasn't changed much since last year, Astonishing X-Men still heading up the non TF books for me. There isn't much better in life than combining Whedon and Marvel is there? Apart from combining me with Scandinavian twins, naturally. Also took the time to catch up on Young Avengers and Runaways which are both great (recent crossover aside, sadly) and I continued to wait with baited breath for Ultimate Spidey, New Avengers and the Ultimates each month (or each 6 months in the case of that last title). I think I am also enjoying Civil War, even if deep down I suspect it may be a bit daft.
<br>
<br>
Of course, the real highlight of the year was a bunch of Transformers comics that were published and written by people who actually had some idea about what they are doing.  After the monolithic shambles that was Pat Wee (ha, see what I did there)'s Dreamwave, IDW are a breath of fresh air.  All involved should be entitled to massive helpings of ale and comely young maidens.
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
Getting promoted, moving to London, passing my driving test and attending a genuine US of A Thanksgiving in the genuine US of A, which was much akin to actually having lunch in a Disney movie. Oh, and last weekend, which was bloody great.
<br>
<br>
<br>
<a name="donfigueroa"></a></a><img src="images/2006donfig.jpg" align="right">
<span class="head2">Don Figueroa</span>
<br>
IDW Transformers artist
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
I've been pretty swamped all year, and I was only able to go see Superman Returns and Pirates 2 in the theatres and I enjoyed them pretty much. 
<br>
<br>
I bought a lot of DVDs though including Clerks 2. Not as good as the original but it was still enjoyable. 
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
I'm not really a fan of today's music. There are some good ones sure, but I like listening to old stuff, mostly 70's, 80's and 90's to much older music like Elvis, The Beatles, etc.
<br>
<br>
I also like listening to musical scores from movies while working, like from Star Wars and Lord of the Rings. 
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
There's only a few shows I watch, like the US version of The Office. I was sad to see JLU end. I mostly watch the History Channel and the Discovery Channel, but only when they're not showing choppers all day.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
I'm liking the Transformers: Spotlights so far and it's really nice to see a story that doesn't span multiple issues to enjoy. I'm also enjoying Nick's work, it's a very refreshing take on the characters. I also liked All Star Superman and I'm enjoying Civil War so far.
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
Still being able to contribute to the Transformers franchise after all these years is always a highlight for me. Finally getting invited to Botcon this year was also great, especially being able to meet and hang out with a lot of good people. 
<br>
<br>
<br>
<a name="nickroche"></a></a><img src="images/2006roche.jpg" align="right">
<span class="head2">Nick Roche</span>
<br>
IDW Transformers artist
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
X-Men 3, though not as good as t'other two, is the only film that I went to see twice this year. Really melted my butter. Clerks II was in many ways like me: Sarcastic, yet sensitive, with a love for Transformers, and a donkey fetish. Unlike me, it was effin' hilarious. And The Departed kicks every films arse this year. Or, more accurately, shoots them in the head. Repeatedly. 
<br>
<br>
Oh, and I watched Das Cabinet des Dr Caligari while sleep-deprived, and I think it may have added to my enjoyment of it. I urge you to see it if you haven't, and if you need to track it down, I reckon Tim Burton may own a copy.
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
I've been a very poor consumer this year. I have bought very few of these albums you speak of. But Thom Yorke's 'The Eraser' is my top pick, with The Kooks' Inside In-Inside Out as a runner-up. Also enjoyed The Automatic's 'Not Accepted Anywhere', twas good drawing music. (Which is my main criteria for obtaining music these days. ) Just procured The Best of Depeche Mode and Faith No More's Greatest Hits, so that should tell me how arrested my development is.)
<br>
<br>
Basically, if it sounds like it was made in 1985 (Orson's 'No Tomorrow', Van She's 'Kelly' and...ahem...The Feeling's 'I Love It When You Call', it's one of my songs of the year.
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
<br>
When I got the chance, I watched the same stuff everyone else did; Doctor Who, Lost (I really like the the new direction!) and more The Mighty Boosh. Liked the car crash that is The Jeremy Kyle Show. I shouldn't be voyeuristic and feel socially and emotionally superior to these people, but I do. So don't take that away from me. And on perfectly legal download, I watched Lee and Herring's 'Fist of Fun' and 'This Morning With...', and Vic 'n' Bob's hugely underrated BBC 3 series, 'Catterick'. If my musical taste is in the eighties, my comedy leanings are entrenched in the nineties.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
The Rik Mayall's autobiography 'Bigger Than Hitler, Better Than Christ'  is the funniest book I've ever read, with Robin Cooper's 'Timewaster Letters' a viciously close 2nd. Mr Cooper literally shunted off a load of unsolicited correspondence to various companies and corporations with his ideas for inventions, fetes, children's literature and ball-bearing storage units. The book reprints his original letters followed by the bemused yet ever-so-professional replies from the poor gits who had the misfortune to open the damn things. 
<br>
<br>
Best non-fiction book was Neil Gaiman's 'Anansi Boys'. I'm a big fan of his, with American Gods being one of my favourite ever books, and this sequel didn't disappoint. Very different, but probably had more heart and humanity than any of his other stuff. And not too doomy either. Lovely.
<br>
<br>
Comics-wise, it's the same stuff I was reading last year, I think. Gaiman's Eternals is great, and lovely to get a dose of JR Jr art, with extra Kirby Krackle. Ultimates 2 is shaping up to be a phenomenal body of work for HitLar. I'm all aboard for Ultimates 3, but in my head, as many others will I reckon, I shall treat it as a separate entity. Do like Joe Mad a lot though. Y:The Last Man, Ex Machina... still magnifcent. And I'm a big fan of Marvel Civil War. I don't read any of the tie-ins, just the flagship, and it's nice to do that and still understand what's going on in a crossover. Steve McNiven is doing a great job of drawing believable and realistic-looking characters but in a 'comic-booky' way, something the majority of the realism-fetishists don't pull off in my opinion. 
<br>
<br>
IDW make good comics too! Ben Templesmith's 'Wormwood: Gentleman Corpse' is a treasure, and I'm not just saying this, but Ryall's and Ash Wood's 'Zombies vs Robots' was executed beautifully. I believe Ryall had to rush that series into production, because some up-and-coming writer had an idea involving both zombies and robots too, and he didn't wanna look derivative. S'what I hear anyway. And with the TF comics, we are tres spoiled too. Great time to be a fan. 
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
Quite a few, I'm happy to say.  On  personal, non-TF related vibe, I got to visit Cannes and Edinburgh with my wonderful girlfriend Anne-Marie. Edinburgh's possibly my second favourite city in the world after Sydney, and someplace I'd always wanted to visit. And getting to walk around Roslyn Chapel on a freezing winter's morning was a magical experience. (And for the record, I wanted to go there before Dan Brown inflicted his nonsense on the planet, so there.)
<br>
<br>
As far as personal highlights tied into work, though, the buzz leading up to and surrounding the release of the Shockwave Spotlight was immense. It was my first professional interior gig, and I was petrified of the reaction it would get. But, coming from a small country as I do, the experience was intensified somewhat, with the national media picking up on the fact that one of our own is working in international comics. So there were one or two print pieces, and a few radio interviews, one with Ireland's version of John Peel, Mr Dave Fanning. And then the online reaction to the comic itself was marvellous too. I couldn't have asked for a better start in the world of comics.
<br>
<br>
I got to do a 'How To Create Your Own Transformers' Workshop in Belfast with Simon Furman, which allowed me to spend a couple of days in his and his lovely wife Anna's company. That was a really good time, and nice to get the opportunity to hang out rather than talk shop all the time. On a similar note, being a guest at the Dublin City Comic-Con  (hosted by my comic dealer John Hendrick) along with Paul Cornell, Adi Granov, Liam Sharp and Mark Millar was an amazing experience, only surpassed by the dinner myself and Anne-Marie shared with MM later that night. Again, no shop was talked, it was your regular evening's chat about White Supremacy, Catholic Guilt and psychic children. Priceless. 
<br>
<br>
<br>
<a name="chrisryall"></a></a><img src="images/2006ryall.jpg" align="right">
<span class="head2">Chris Ryall </span>
<br>
IDW Editor-In-Chief 
<br>
<a href="http://www.idwpublishing.com" target="_blank">www.idwpublishing.com</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
You know it's a bad sign when you have to look for year-end �Best of� lists to even know what came out this year. My daughter was born on January 3 this year, so I've certainly rented a lot more movies than I've seen in theatres. I pick and choose carefully now, but I loved things like Little Miss Sunshine, Borat, The Prestige� is it a bad sign that I can't think of one big summer movie that stuck with me?
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
Now, music's a separate story from movies - I'm obsessed with my iPod (I think I bought 4 of them this year, either to upgrade to the 80 GB - not enough space, Apple - or to give them as gifts. I've owned like 7 different ones already�). I spend so many hours and dollars downloading music. Probably picked up somewhere north of 100 CDs (or their digital equivalent) this year. I loved - in no particular order, just as I remember them - the live My Morning Jacket, the new Masatadon, Jenny Lewis's solo disc, the new Chili Peppers was pretty great, I liked bits of Stone Sour's disc, Tom Waits, Golden Smog, Tool, Wolfmother, Artic Monkeys, Beck, Motorhead, Built to Spill, Black Rebel Motorcycle Club, Queensryche's Opeation: Mindcrime sequel was decent, there was a good Chris Cornell acoustic live show released online� so many. I thought it was a pretty great year for music, despite the pabulum that got played at the awards shows and featured on magazine covers.
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
<br>
The Office is probably my favourite show right now, and the  Show/Colbert Report double-shot has gotten me to totally forget about late-night talk shows. I also dig The Wire (and HBO's Entourage and The Sopranos), still like The Simpsons and South Park, love Rescue Me, and make time to catch Prison Break, Heroes, Lost, and Studio 60. With the new baby, I seem to catch a lot of TV now.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
I never know/remember when a book came out, but what I read this year that I loved was Jonathan Strange &amp; Mr. Norrell, Joe Hill's short story collection, Max Brooks' World War Z, a book about Rod Serling's Night Gallery� it's hard to remember, since I always have a good half-dozen books going at once.
<br>
<br>
Comics are even harder to judge now, since working on them and knowing many of the creators has me biased in either direction. But the guy whose books give me the most �fan-like� reaction and remind me why I loved comics in the first place is Brian K. Vaughn. His Pride of Badhdad was really excellent and touching, and I love Ex Machina, Y The Last Man, The Escapist and Runaways. I wouldn't feel right mentioning the titles I published, but thought we did some good stuff that I would've enjoyed even if only a fan. Peter David's had a hot streak going now, with all his titles, that's lasted for years, in my eyes. I'm happy to be working with him, and having a chance to read his stuff at Marvel once again, too. I don't know, I read a lot of comics, and enjoy a lot of them, so I'm loathe to start naming books because I know I'll leave off dozens more I liked. Lost Girls was certainly a highlight, though, as Alan Moore proved for the thousandth time that he's so far above anyone else working in this medium that it's almost not fair.
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
The birth of my daughter. And Zombies Vs. Robots #1� 
<br>
<br>
<br>
<a name="klausscherwinzki"></a></a><img src="images/2006klaus.jpg" align="right">
<span class="head2">Klaus Scherwinzki </span>
<br>
IDW Transformers artist
<br>
<a href="http://www.klausscherwinski.de" target="_blank">www.klausscherwinski.de</a>
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
Went to the movies only three times in '06. Allways felt entertained, never blown away. Waiting for Spider-man 3 and a certain Transformers movie next year. Ask me again then.
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
Oh, I sooo suck at bands and their names. I hear so much different stuff, virtually everything except house/techno and death metal! So I only bought few albums in 2006. For one there's Cruciform Injection that plays to my darker electronic roots, then there was the German crew of <a href="http://www.pornophonique.de" target="_blank">www.pornophonique.de</a>. Yes that's their name, and I gave the link here so that nobody had to google "porn-stuff" on the web! Anyway that's actually nothing more than two guys with a c64, a Gameboy, and an acoustic guitar and they ROCK! Indeed I bought the new album by Germany's top rapper Curse - don't know how famous he is beyond Germany but he's really versatile, smart and shows how good German rap-music can be.
<br>
<br>
Lastly I've gotta admit that I listen to a lot of old stuff from the 80's and 90's, and tons of Transformers remixes that keep me in the mood for my work on the series!
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
<br>
The IT Crowd! Then I watched the subtitled Naruto and discovered a series from 2004 called Wonderfalls - a bit late maybe but hey, I live in Germany! Oh, and I had no expectations for the Blade series so I was surprised at the relative quality and entertainment it offered and then it got cancelled. D'oh!
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
Marvel: Ultimates! Except for it's constant lateness it's freaking awesome.
<br>
DC: Justice. Alex Ross. As an artist I just need the painting input.
<br>
<br>
Darkhorse: Conan. Nuff said. Cary Nord's pencils are just beautiful. Lots to learn about looseness there regarding linework.
<br>
<br>
Image: The Walking Dead. And, yes, I only read the trade paperbacks!
<br>
<br>
Idependent: Doc Frankenstein. Incredible. Artwork and story. Entertaining like Kill Bill!
<br>
<br>
IDW: Hahaa, Transformers Infiltration was good already, I had patience waiting for the 'bots. Now having worked on Escalation and just having read the story for #6 I can only say how blown away I am by Furman's work! I can't say anything about the plot but, yes, Escalation is an absolutely fitting title. And EJ finally gets to show the robot-aficionados how powerful his interiors can be - you will not be disappointed! I'm happy I only do the covers, I could never pull off what he is performing on a regular basis. Okay, I'm getting off topic here. Second IDW title would be The Keep. Check it out - it's creepy to the core, story and storytelling are excellent, great package.
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
Working on Transformers and meeting all the guys behind it. You know stuff like travelling to San Diego comic con and going to that panel, and they ask me to be on the stage with them representing the work we do and then seeing my mug on youtube.com a week later. Crazy. Okay, helping to forge a deal to have the Degenesis pen and paper RPG translated into English was great. Really looking forward to seeing that in 2007 - with all my tech illustrations in it and the work I do for that in the moment, that really rocks. Check it out at <a href="http://www.degenesisrpg.com" target="_blank">www.degenesisrpg.com</a>. This year will be tough to beat. Well, if IDW offered me a one-shot (again) and (this time) I have time in my busy schedule to draw and paint it to perfection... that would work.    
<br>
<br>
<br>
<a name="liamshalloo"></a></a><img src="images/2006limabean.jpg" align="right">
<span class="head2">Liam Shalloo </span>
<br>
IDW Transformers Colourist
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
Hmmm... Borat, Pirates 2, Superman Returns, X-Men 3, Jackass 2
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
Haven't really been that up with music at the moment but Stone Sour: Come What(ever) May track 12 is just amazing.
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
<br>
Hmmm, well I honestly don't have the time to watch much TV anymore. But my favourite is Firefly cause I love that show and its just started showing here. Then it's gotta be TNA Impact, Red Dwarf, House and I'm finally getting to watch Beast Wars.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
Transformers Stormbringer, Infiltration and Beast Wars
<br>
Cable and Deadpool
<br>
Ultimate Spider-man
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
Becoming a professional comic book artist! And on a Transformers comic to boot, thats just the iciing on the cake. So to Chris, Dan and everyone else at IDW I've gotta say thanks for having the faith in me. Apart from that I would say graduating from uni was a big deal! Also I learnt to count to 10!
<br>
<br>
<br>
<a name="richardstarkings"></a></a><img src="images/2006starkings.jpg" align="right">
<span class="head2">Richard Starkings</span>
<br>
Marvel Transformers Letterer and Editor
<br>
<a href="http://www.hipflask.com" target="_blank">www.hipflask.com</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
The Last King of Scotland - a riveting and at times gruesome thriller with an Oscar worthy performance by Forest Whitaker, The Last King of Scotland  throws light on Idi Amin's presence in the British news bulletins of the 70's.
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
Fundamental by The Pet Shop Boys, Rudebox by Robbie Williams, Ta-Da by The Scissor Sisters, Highway Companion by Tom Petty, Friendly Fire by Sean Lennon, All the Roadrunning by Mark Knopfler and Emmylou Harris.
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
<br>
Doctor Who, Torchwood, My Name Is Earl, The Office, Planet Earth. 
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
Jack Staff, Walking Dead, Invincible.
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
Would have to be publishing Elephantmen through Image and Ladr�nn receiving an Eisner for his work on Hip Flask:Mystery City.
<br>
<br>
<br>
<a name="ejsu"></a></a><img src="images/2006ej.jpg" align="right">
<span class="head2">E.J. Su</span>
<br>
IDW Transformers artist
<br>
<a href="http://www.protodepot.com" target="_blank">www.protodepot.com</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
Pirates of the Caribbean: Dead Man's Chest
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
Jay Chou's "Still Fantasy".
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
Lost.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
"20th Century Boys" and "Pluto" both by Naoki Urasawa.
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
Contracted to write an art instruction book. It's a new challenge that I never had before.
<br>
<br>
<br>
<a name="leesullivan"></a></a><img src="images/2006sullivan.jpg" align="right">
<span class="head2">Lee Sullivan</span>
<br>
Marvel and Panini Transformers artist 
<br>
<a href="http://www.leesullivan.co.uk" target="_blank">www.leesullivan.co.uk</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong>
<br>
<br>
As usual, lots of catching-up on dvd, but in the cinema, Casino Royale, of course.
<br>
<br>
<strong>Favourite Albums of 2006:</strong>
<br>
<br>
A bit of a cheat here - my band (see <a href="http://www.roxymagic.co.uk)" target="_blank">www.roxymagic.co.uk)</a> have recorded a live dvd, and I've been listening a lot to the audio tracks from it; it's a thrill to hear my sax noodlings over the extremely fine work the other guys are doing, which I don't get to hear much of when we're actually playing!
<br>
<br>
<strong>Favourite TV of 2006:</strong>
<br>
<br>
Green Wing again; Sopranos again and the astonishing Deadwood.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong>
<br>
<br>
'Doctor Who - Battles in Time' for giving me the chance to work regularly in comics again... I don't read much these days as the band and work don't permit much time and I'm too tired all the rest of the time!
<br>
<br>
<strong>Personal Highlights of the year:</strong>
<br>
<br>
Clocking up 20 years marriage with my long-suffering wife Jo. How she puts up with me is unfathomable. Holidaying with her in Somerset, which I fell in love with.
<br>
<br>
Finally working with my author pal Andy Donkin on a project which required jetting off to the South of France for a couple of days (oh the suffering).
<br>
<br>
My sax hero, Roxy Music's Andy Mackay, asking if I'd mind his using a cartoon I did of him on his forthcoming website, then chatting with him about their next album.
<br>
<br>
Most gigs we play - in particular when the audiences really let go.
<br>
<br>
<br>
<a name="dantaylor"></a></a><img src="images/2006taylor.gif" align="right">
<span class="head2">Dan Taylor</span>
<br>
IDW Editor
<br>
<a href="http://www.idwpublishing.com" target="_blank">www.idwpublishing.com</a> 
<br>
<br>
<strong>Favourite Films of 2006:</strong> 
<br>
<br>
Wow! I just realised that I didn't see too many movies in 2006 - I planned on catching most of them on DVD. I did see Pirates of the Caribbean: Dead Man's Chest, a fun and enjoyable movie, even it seemed like a long two and a half hour "half-movie." 
<br>
<br>
<strong>Favourite Albums of 2006:</strong> 
<br>
<br>
Since getting old and listening to more talk radio and podcasts than music I'm not all that hip anymore (as if I ever was). But in 2006 I did dig the Red Hot Chili Peppers' "Stadium Arcadium," a solid double-album effort. Gnarls Barkley's "St. Elsewhere" surprised me above and beyond the song, "Crazy." Rob Zombie's "Educated Horses" may not have been as good as I hoped for, but it grew on me. And I have to give an honourable mention to Tom Petty's "Highway Companion," I caught him and The Heartbreakers on tour this year.
<br>
<br>
<strong>Favourite TV of 2006:</strong> 
<br>
<br>
Yay! I didn't have to wait until January 2007 for the return of Scrubs, my favorite show by far. Studio 60 on the Sunset Strip is my favorite new show of the 2006 fall season - glad that it got picked up for a full season. And my guilty pleasure remains Degrassi: The Next Generation.
<br>
<br>
<strong>Favourite Comics and/or Books of 2006:</strong> 
<br>
<br>
Pride of Baghdad by Brian K. Vaughan and Niko Henrichon was by far the best graphic novel read of 2006. The 9/11 Report: A Graphic Adaptation was sobering, but informational. I'm also digging Marvel's Civil War, all lateness issues aside.
<br>
<br>
<strong>Personal Highlights of the year:</strong> 
<br>
<br>
Well if Transformers at IDW didn't keep me busy enough, landing the Star Trek license put a smile on my face. Being a long time Star Trek fan for just about all of my life I'm looking forward to working on many Star Trek titles next year in addition to what IDW has planned for Star Trek. Also, the addition of my new best friend - my cat, Quint. I can't help but have the theme song to The Courtship of Eddie's Father enter my head whenever he's around.



   <p>&nbsp;</p>
	</td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
