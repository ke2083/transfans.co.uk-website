<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Andrew Wildman</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Andrew Wildman<br>
              </span><span class="blackstrong">interviewed in:</span> June 2004</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/wildman.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%">Andrew Wildman penciled numerous covers for the 
                  original UK Transformers comic and as of <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=226&page=11" >198</a> 
                  onwards was one of the comic&#8217;s regular artists. He later 
                  transferred to the US comic (whilst still providing pencils 
                  for some of the UK black and white stories) starting with issue 
                  69 (UK Issues <a href="comics_guide_detail.php?id=454" >306</a>, 
                  <a href="comics_guide_detail.php?id=456" >307</a> and <a href="comics_guide_detail.php?id=458" >308</a>), 
                  and was the regular artist on the title until the comics run 
                  case to an end with issue 80 (UK Issues <a href="comics_guide_detail.php?id=488" >331</a> 
                  and <a href="comics_guide_detail.php?id=491" >332</a>) &#8211; 
                  during which time one of his notable achievements was adding 
                  toilet doors to Unicron. More recently Andrew provided pencils 
                  for <a href="http://www.dreamwaveprod.ca/!2004/" target="_blank" >Dreamwave&#8217;s</a> 
                  The War Within: The Dark Ages series with long time partner 
                  in crime Simon Furman. Andrew and Simon also share the production 
                  company <a href="http://www.wildfur.net/" target="_blank" >Wildfur</a>, 
                  which amongst other things produces the online graphical story 
                  <a href="http://www.whorunstheengine.net/" target="_blank" >The 
                  Engine</a>. Andrew&#8217;s full biography can be read <a href="http://www.wildfur.net/wildman.html" target="_blank" >here</a>. 
                </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.wildfur.net/" target="_blank"><img src="images/wildfur2.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p><strong>Andrew<br>
              Thanks again for agreeing to answer some questions for us. Predictably 
              we&#8217;d like to start with a few questions about, shock, Transformers, 
              and then we&#8217;ll move on to your other work&#8230;</strong></p>
            <p><strong><span class="head2">1.</span></strong><strong> Before War 
              Within you stated in an interview for <a href="http://www.transfandom.com/" target="_blank">Transfandom</a> 
              that the 'scariest thing' was going to be the reaction you got to 
              your new work. Now that the project is in the bag what are your 
              impressions of that reaction? </strong></p>
            <p>The reactions that I have seen/heard are generally good. Strangely 
              the book did not look as I had expected. Dreamwave took a very 'dark' 
              approach to the finishing of the art. On the WILDFUR site we are 
              giving people the opportunity to see how the original pencils looked. 
              I always find it interesting to see original pencil artwork as the 
              processes that it goes through after that change add or in some 
              cases detract from the original work.</p>
            <p><strong><span class="head2">2.</span> Can you tell us which characters 
              you were responsible for retro designing? Did Dreamwave lay down 
              any specific guidelines around this? </strong></p>
            <p>There were no guidelines other than to consider how Don Figueroa 
              had done it and go for some consistency. I was responsible for retro 
              designing Sideswipe, Broadside, Whirl, Sandstorm, Mirage, Perceptor 
              and Blaster. Not all were used to any great degree and there may 
              be a couple I have overlooked. </p>
            <p><strong><span class="head2">3.</span> How did working for Dreamwave 
              compare to working for Marvel? What were the differences and the 
              similarities? </strong></p>
            <p>Being a comic publisher there were a lot of similarities in the 
              technical side of things like actually doing the work and shipping 
              it over (to Canada). Being a freelance comic artist is a fairly 
              anonymous existence. All communication was done via email and I 
              have as yet never met or spoken to any of the Dreamwave people, 
              although all being well I should have the pleasure of meeting them 
              at the upcoming Chicago Convention. From a practical point of view 
              the biggest difference was that Simon wrote it as a full script 
              rather than the Marvel style breakdowns. </p>
            <p><strong><span class="head2">4.</span> And how did all this compare 
              to working on the sadly short lived UK Armada comic?</strong></p>
            <p>After a number of slight tweaks and changes of direction Armada 
              was just beginning to hit its stride when the book had to be cancelled. 
              There are a number of reasons why this happened but I think it was 
              mostly due to the fact that Armada didn&#8217;t have quite the same 
              toy presence here as it did in the States. </p>
            <p><strong><span class="head2">5.</span> On a more general note, is 
              there one issue of your Transformers work that you would pick as 
              a personal favourite or that you are most proud of? </strong></p>
            <p>I think War Within number three was the best of that series and 
              the one where I had really become comfortable with it all again. 
              Of course the sad thing was that it was limited to six issues. From 
              the Marvel run it is a difficult choice between 70 and 78. Maybe 
              70. Loved all the stuff on the Ark with Prime and the MegaRatch. 
              That is when I was really into that work. </p>
            <p><strong><span class="head2">6.</span> Finally on the comics front 
              can you tell us which artists you look up to and who&#8217;s work 
              you are currently enjoying?</strong></p>
            <p>I have always cited John Buscema as the biggest influence on my 
              work. I loved it while I was growing up as a reader and I now look 
              back on it as some of the most defining and technically excellent 
              work to ever appear. Kirby was a great master of style and drama 
              but for me it was Buscema who set the Marvel style and set the benchmark 
              for everything that happened up until the more recent Manga style 
              kicked in.<br>
              Don't read a lot of comics. In the world of Transformers I do consider 
              Don Figueroa to be the best thing to happen in a Transformers comic 
              since...<br>
              Generally I enjoyed California Out There by Augustyn and Ramos. 
              Other than that I tend to look at books with a more European style 
              and sensibility. </p>
            <p><strong><span class="head2">7.</span> You have spent some of your 
              career in the video games industry. Are you a video games fan? Have 
              you played/seen the new Transformers Armada game? </strong></p>
            <p>The New Armada game is great. It really shows what can be achieved 
              by a PS2 game. I am a sometime game player. I loved the Myst series 
              but my all time favourite game was Re-Volt. Fantastic fun and an 
              opportunity to be exactly where you wanted to be when you were eight 
              years old. </p>
            <p><strong><span class="head2">8.</span> From a personal perspective 
              how big a part of your career have Transformers been? Is it strange 
              that there are a whole bunch of people out there who still associate 
              you with something that until recently you hadn&#8217;t worked on 
              in a decade? </strong></p>
            <p>It&#8217;s all been very strange. When I finished the original 
              Marvel series I was happy to move on to some of their 'proper' books. 
              In hindsight I think it was the most fun I had in comics. To have 
              had the opportunity to do it again has been cool. It&#8217;s nice 
              to be contemporary rather than just 'that bloke that used to draw 
              Transformers'. Some might feel that it is restraining to be so associated 
              with one product but I find it all a lot of fun. Nice to be recognised 
              for something rather than nothing specific. I recently had an email 
              from someone who had remembered it as though I had done most of 
              the Marvel run. Although they were mistaken it does make you feel 
              as though you did make an impact. </p>
            <p><strong><span class="head2">9.</span> Moving on from Transformers 
              for a bit can we ask you about The Engine (Wildfur&#8217;s online 
              graphic story telling project)? Do you and Simon have a finite plan 
              for it, or is it an open ended endeavour? </strong></p>
            <p>The whole thing does have an overall story arc but recent events 
              have now made us reconsider what we are doing with The Engine. We 
              have recently completely revamped the Wildfur website and put all 
              the News and Shop items on it. This has enabled us to now consider 
              making the Engine site purely Engine. The site will be rebuilt and 
              the content reconsidered very shortly. The existing three episodes 
              will still stand but the intention of where we go after that will 
              change. The long awaited episode four will be done for relaunch 
              and there will be more cool stuff. Announcements for the relaunch 
              will obviously be on the Wildfur site and should go through the 
              usual channels. </p>
            <p><strong><span class="head2">10.</span> Speaking of projects, what 
              else is in the pipeline? Will we see more Transformers related stuff 
              any time soon? </strong></p>
            <p>For me there is no Transformers work on the horizon. That may change. 
              I am currently involved in the development of a couple of animated 
              TV shows. One of these will appear on our screens April 2005. Working 
              in animation is fun. My aim is to do more work in this area. As 
              far as comics are concerned I have a pet project which has been 
              in negotiation for some time but will hopefully get a green light 
              soon. I also would very much like to do an Engine graphic Novel. 
              The revamp of the Engine site is designed to increase the chance 
              of this and I want to get those people who have been good enough 
              to subscribe to the site to get involved in making it happen. More 
              news will appear on the Wildfur site soon. </p>
            <p><strong><span class="head2">11.</span> On a slightly random note 
              - What&#8217;s the weirdest question you have ever been asked by 
              a fan (bearing in mind this interview isn&#8217;t quite over yet...)? 
              </strong></p>
            <p>What's your favourite cheese.</p>
            <p><strong><span class="head2">12. </span> The obvious follow up to 
              that question &#8211; what&#8217;s your favourite cheese?</strong></p>
            <p>There ya go. Um. Lychees? :-) </p>
            <p><strong><span class="head2">13.</span> And finally, do you remember 
              drawing <a href="popup.php?id=763" target="_blank">this</a>?</strong></p>
            <p>Oh good grief! Looks more like David Lloyd than me. Guess I need 
              to do another one.</p>
            <p><strong>Mr Wildman, thank you very much for your time.</strong></p>
            <p>You are more than welcome. :-)<br>
              <br>
            </p>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr> 
                <td width="22%" class="subcolor"><p><img src="images/TWW3_1.gif" width="150" height="169" class="blueborder"></p>
                  <p align="center">Additional Information</p></td>
                <td width="52%" valign="top" class="greybar"> 
                  <p>Fans will be pleased 
                    to hear that Andrew and Simon are currently selling limited 
                    addition Art/Script packs containing quality reproductions 
                    of the script and art (didn&#8217;t see that coming did you?) 
                    from The War Within: The Dark Ages series. You can purchase 
                    the packs <a href="http://www.wildfur.net/shop_specials.html" target="_blank">here</a> 
                    and visit the rest of the Wildfur shop <a href="http://www.wildfur.net/shop.html" target="_blank">here</a>. 
                    Similar packs will be available for their Armada Uk work some 
                    time in the future.</p>
                  <p>US fans will be able to see Andrew at <a href="http://www.otfcc.com" target="_blank">OTFCC</a>, 
                    which runs from July 31&#8211; August 1.</p>
                  <p>Andrew will also be appearing at the UK&#8217;s premier Transformers 
                    convention <a href="http://www.transforce.org.uk" target="_blank">Transforce</a>. 
                    The event is being held at Canon's Leisure Centre in Mitcham, 
                    Surrey, on August 28th. See you there!</p></td>
                <td width="26%" valign="top" class="greybar2">
<p><a href="http://www.wildfur.net/" target="_blank">Wildfur.net</a> &#8211; Andrew 
                    Wildman and Simon Furman&#8217;s production company. The site 
                    includes updates on their current activities as well as information 
                    on services and properties.<br>
                    <a href="http://www.whorunstheengine.net/" target="_blank">The 
                    Engine: Industrial Strength</a> - Andrew and Simon&#8217;s 
                    online graphical story telling project. Check it out.<br>
                    <a href="http://www.otfcc.com/" target="_blank">OTFCC</a> 
                    - Official Transformers Collectors' Convention.<br>
                    <a href="http://www.transforce.org.uk" target="_blank">Transforce</a> 
                    - The UK&#8217;s premier Transformers convention</p>
                  </td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.whorunstheengine.net/" target="_blank"><img src="images/engine.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <br> </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
