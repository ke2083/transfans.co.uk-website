<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: It was the Year 2005</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt">
    
    <p><span class="bluehead"><br>"It was the year 2005..."<br></span></p>
    
              <p>&nbsp;</p>
<p align="center"><strong><a href="#stevebax">Steve Bax</a> &bull; <a href="#chrisboothroy">Chris Boothroy</a> &bull; <a href="#joshburcham">Josh Burcham</a> &bull; <a href="#stuartdenyer">Stuart Denyer</a> &bull; <a href="#paulduggan">Paul Duggan</a> &bull; <a href="#adamfortier">Adam Fortier</a> &bull; <a href="#simonfurman">Simon Furman</a> &bull; <a href="#guidoguidi">Guido Guidi</a> <br /><a href="#danreed">Dan Reed</a> &bull;  <a href="#nickroche">Nick Roche</a> &bull; <a href="#chrisryall">Chris Ryall</a> &bull; <a href="#richardstarkings">Richard Starkings</a> &bull; <a href="#ejsu">EJ Su</a> &bull; <a href="#leesullivan">Lee Sullivan</a> &bull; <a href="#ryanyzquierdo">Ryan Yzquierdo</a></strong></p>
<a name="stevebax"></a><br />
<span class="head2">Steve Bax </span>
<br />
Webmaster 
<br />
<a href="http://www.oneshallstand.com" target="_blank">www.oneshallstand.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
I am quite a fan of the pictures though I really haven't been to the cinema that many times this year. I enjoyed 'Hitch' and 'The 40 Year Old Virgin' and am looking forward to seeing 'Narnia' and 'King Kong' before the year is out. As for a favourite, it has to be Star Was Episode III: Revenge of the Sith - space battles, light saber duels, betrayal, tragedy and Darth Vader! I had been waiting a long time for this event and while it wasn't perfect it didn't disappoint. Ian McDairmid put in a great show as the Emperor, and Hayden Christensen's acting had improved since Episode II. I felt a sense of tragedy and anticipation as Vader's mask was lowered on to Anakin's at the end, and you realised you finally knew how it all turns out. 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Hopes and Fears by Keane... and um, that's it. I spent most of my year downloading songs from e-Donkey. 
<br />
<br />
<strong>Favourite TV of 2005:</strong>
<br />
It's been a great year for TV hasn't it? And I'm not talking about I'm a Celebrity. The new Doctor Who series turned out to be brilliant, and I am loving watching Lost at the moment (even though they seem answer one question and then throw 10 more at you). The new Battlestar Galactica series on Sky was another revelation, and turned out to be a lot better than I expected, and I've been enjoying Enterprise too so I'm pretty miffed that it has been cancelled. Aside from sci-fi shows I also enjoyed The Apprentice (the UK one being miles better than the US version - you gotta love Sir Alan's no nonsense attitude) as well as Big Brother and X Factor. 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
It's all been quiet on the comics front for me with Dreamwave going bust at the end of 2004. I picked up a Thundercats Enemy's Pride graphic novel from Amazon and enjoyed the artwork and premise of that, even if the ending was something of a missed opportunity. 
<br />
<br />
IDW's Transformers issue 0 was a joy to receive, because as short and sweet as it was, it was great to have some fresh TF material after so many months. Thank goodness normal service is resumed for '06. 
<br />
<br />
Bookwise I have been reading and enjoying the Da Vinci Code, Bill Clinton's biography, William Pitt the Younger, a book on body language, and a rather eye opening e-book called 'Double Your Dating'. It's by David D Angelo and anyone who's ever failed to get the girl, or wondered what makes women tick, should do themselves a favour and download this. 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
I bought a new Ford Focus in August and it runs a dream. The only downer was that, days before, I'd spent �800 having my old car repaired only for the engine to blow up on the motorway. I enjoyed a few relaxing days in Bath with a young lady and a week in Ibiza with the lads, grappling with an unending succession of beers! Oh and my slave-driving boss finally handed in her notice in November (thank you God!). Oddly enough I find I have better times in the even numbered years, so with 2006 around the corner and the Transformers comics re-starting for real I am looking forward to it. All the best for Christmas and the New Year to you.
<br />
<br />
<br />
<a name="chrisboothroy"></a><br />
<span class="head2">Chris Boothroy, a.k.a. 'Kickback'</span>
<br />
Site Staff
<br />
<a href="http://www.tfw2005.com" target="_blank">www.tfw2005.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Star Wars Ep 3. I'm a nerd, like the rest of us, and there's something cool about seeing the hero fall from grace, knowing he'll come back to be the hero none the less. 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Sevendust "NEXT" ... because Sevendust is the best band ever. 
<br />
<br />
<strong>Favorite Comics and/or Books of 2005:</strong>
<br />
Considering I literally didn't read a single comic this year, I'll say House of M ... because it looked good on paper, though I'm sure Marvel will continue to **** up their own universe by making huge changes only to change them back for "dramatic storyline effects" a year from now. 
<br />
<br />
<strong>Personal Highlights of the year:</strong>
<br />
Letting certain demons of my past go and finally finding a woman who cares about me because of who I am at heart. And no, it's not my mother.
<br />
<br />
<br />
<a name="joshburcham"></a><br /><a href="images/joshmm.jpg"><img src="images/joshmmthumb.jpg" align="right" style="border:1px black solid;"></a>
<span class="head2">Josh Burcham </span>
<br />
IDW and Dreamwave Transformers colourist 
<br />
<a href="http://dcjosh.deviantart.com" target="_blank">dcjosh.deviantart.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Star Wars: Revenge of the Sith 
<br />
War of the Worlds 
<br />
Batman Begins 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Falling Up - Dawn Escapes 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Smallville 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
Lullaby 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Landing a job with IDW Publishing! Thanks Chris and Don!
<br />
<br />
<br />
<a name="stuartdenyer"></a><br />
<span class="head2">Stuart Denyer</span>
<br />
Site Staff
<br />
<a href="http://www.tfarchive.com" target="_blank">www.tfarchive.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
The Hitchhiker's Guide to the Galaxy, Batman Begins and Sin City. 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
The Crimea (Tragedy Rocks) and a bunch of back-catalogue stuff such as Mary Prankster, Reel Big Fish and The Wildhearts. I'd be a very happy bunny if The Hot Puppies got around to releasing an album. 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Miraculously, there's been some. Doctor Who. 
<br />
<br />
<strong>Favorite Comics and/or Books of 2005:</strong>
<br />
Fables, The Authority: Revolutions (not too shabby, certainly better than most of what's been done with the title under DC) and back-catalogue stuff such as Transmetropolitan and Preacher. Looking forward to 2006 for Transformers and a conclusion to Planetary. 
<br />
<br />
<strong>Personal Highlights of the year:</strong>
<br />
Teaching and getting to tourist again in Poland. And, since our regular webmaster is busy and I'm therefore slipping this entry in just under the deadline, I can mention fan response to the new Beast Wars comics &#8212; the enthusiasm has been infectious. Actually, just the fact we're getting original series Transformers updated, Beast Wars and Death's Head all very soon (and all look interesting) is fantastic. Cheers to everyone who's making those happen!
<br />
<br />
<br />
<a name="paulduggan"></a><br />
<span class="head2">Paul Duggan, a.k.a. 'Best First'</span>
<br />
Webmaster
<br />
<a href="http://www.TransFans.co.uk" target="_blank">www.TransFans.co.uk</a>
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Serenity 
<br />
Batman Begins
<br />
Sideways
<br />
Revenge of the Sith was rubbish though, just to be clear.
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Angel Soundtrack 
<br />
Push The Button - Chemical Brothers 
<br />
Stars of CCTV - Hard Fi
<br />
Demon Days - Gorillaz
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Lost 
<br />
Doctor Who
<br />
Peep Show
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
Y The Last Man 
<br />
Astonishing X-Men 
<br />
Ultimate Spidey 
<br />
New Avengers 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
It would be ungentlemanly of me to share.
<br />
<br />
<br />
<a name="guidoguidi"></a><br /><a href="images/guido2005.jpg"><img src="images/guido2005thumb.jpg" align="right" style="border:1px black solid;margin-left:15px;margin-bottom:15px;"></a>
<span class="head2">Guido Guidi </span>
<br />
Dreamwave / IDW Transformers artist
<br />
<a href="http://xoomer.virgilio.it/guidi_guido/" target="_blank">xoomer.virgilio.it/guidi_guido</a> 
<br />
<br />
<strong>Favourite Film of 2005: </strong>
<br />
War of the Worlds.
<br />
<br />
<strong>Favourite Album of 2005: </strong>
<br />
TF History of Music.
<br />
<br />
<strong>Favourite Comic of 2005: </strong>
<br />
The advanced preview of IDW Infiltration #1.
<br />
<br />
<strong>Personal Highlight of the year: </strong>
<br />
Getting the opportunity to work on Transformers again and on Marvel characters.
<br />
<br />
<br />
<a name="adamfortier"></a><br />
<span class="head2">Adam Fortier </span>
<br />
Dreamwave / Devil's Due / IDW consultant 
<br />
<a href="http://www.speakeasycomics.com" target="_blank">www.speakeasycomics.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Has to be the latest Harry Potter flick, it's a pretty darn good movie! 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
The latest Gorrilaz album , as well as the Killers. 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Mmmm... Lost, and House. 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
You know, mostly I've been reading books that didn't come out this year, so I don't think I can give you anything there! 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
We've formed a partnership with a company based out of LA called Ardustry Entertainment... it's going to mean big things for Speakeasy Comics (a new company that we started early in 2005).
<br />
<br />
<br />
<a name="simonfurman"></a><br />
<span class="head2">Simon Furman </span>
<br />
Writer / editor, especially of Transformers stuff
<br />
<a href="http://www.wildfur.net" target="_blank">www.wildfur.net</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Sin City, The Descent, and War of the Worlds.
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Employment (by the Kaiser Chiefs) and You Could Have it So Much Better (by Franz Ferdinand.)
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Casanova, Bleak House, Doctor Who, Lost (and, on DVD, Sopranos and 24.)
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
Ultimates, Planetary. 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
IDW picking up the TF license, and Marvel readers voting Death's Head the next character for an Amazing Fantasy revamp.
<br />
<br />
<br />
<a name="danreed"></a><br /><a href="images/danreed2005.jpg"><img src="images/danreed2005thumb.jpg" align="right" style="border:1px black solid;"></a>
<span class="head2">Dan Reed </span>
<br />
Marvel Transformers artist 
<br />
<a href="http://www.artblazer.com" target="_blank">www.artblazer.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
The superhero stuff of course... Batman Begins, Sin City, Fantastic Four, and I'm looking forward to the new Harry Potter movie, having read all the books, except the newest. 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
I'm not really up on the new music these days. I listen to a lot of books on tape while I'm drawing or painting. 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Smallville, CSI, some specials. Other than that, mostly history, science and nature stuff. 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
The Jack Kirby Collector, a lot of different SF, horror, fantasy, and mystery books, as well as a ton of manuals on Photoshop and dozens of other programs that I've had to learn in order to produce the new DVD comic that I'm creating. 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Working on a full audio/visual DVD comicbook experience due out next year. You'll be able to insert a DVD into your player and watch my newest comicbook creation on your Television set! 
<br />
<br />
<br />
<a name="nickroche"></a><br /><a href="images/roche2005.jpg"><img src="images/roche2005thumb.jpg" align="right" style="border:1px black solid;margin-left:15px;margin-bottom:15px;"></a>
<span class="head2">Nick Roche </span>
<br />
IDW Transformers artist 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Sin City was a beaut, but didn't grab me the way it did many others. The Corpse Bride was equally as gorgeous, but I wanted to love it like Nightmare Before Christmas, and it didn't quite hit it. Same with Charlie and The Chocolate Factory (The 'Flags of The World' skit is my favourite visual joke ever) which was great, but I wanted to be immense. Loved House of Flying Daggers and Harry Potter and The Goblet of Fire left me a very happy customer, but Batman Begins and Revenge of The Sith nailed it for me completely. Not seen Narnia yet, but it'll never be better than the BBC version! (Well, it might, but that's beside the point.) 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Really spent time this year obtaining older music, like all the Eels albums and far too much eighties fromage, if there is such a thing as too much. Generally like the sound of the British Rock scene at the mo, but find the singles are stronger than than the albums (eg. Kaiser Chiefs). Bloc Party are good, as are Hard-Fi, and the Arcade Fire sound great from what I've heard so far. The Revenge of The Sith OST really melted my butter, but the most enjoyable thing I've heard all year is The Darkness' 'One Way Ticket To Hell And Back. It's like the eighties, only, y'know...now! 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Ahh, I wish I could rattle off a load of esoteric documentaries, but it's Lost, innit? Shame the cast are a bunch of mutts. Andrew Graham Dixon's BBC show 'The Secret of Drawing' was fantastic, and encouraging to see such an in-depth show about something I love on t'telly. John Peel's Record Box on Channel 4 was pitch-perfect and quite moving, and I'm all over The Mighty Boosh like a nun sandwich. It's the funniest thing since Reeves and Mortimer ('Comin' outta me, like a yellow cable!' Sorry). 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
I'm waaaay behind on my recommended reading, and I'm pretty safe with my choices, but Astonishing X-Men and Young Avengers shocked me back into loving superheroes again. Dipped my toe into Invincible too, and I'm going back soon. Also started to get into Y: The Last Man and Ex-Machina. Brian K Vaughn writes good, 'e does. Lions, Tigers and Bears by Mike Bullock and the ridiculously talented Jack Lawrence was a comic myself and my 8-year-old cousin could bond over (She loved Darkham Vale more, bless her) and Transformers are back, I hear. I scoffed at all crossovers. 
<br />
<br />
Bit behind on 'real' books too, but finished 'His Dark Materials' by Philip Pullman, and now I've got a new favourite book. Non-fiction-wise, Jon Ronson's 'The Men Who Stare at Goats' is hilarious, informative and frightening, like all the best lessons should be. And I can't wait to get around to Neil Gaiman's 'Anansie Boys' as 'American Gods' blew me away. Love that guy! 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Got to travel loads, to Rome, Berlin and Milwaukee (Yeah! Milwaukee! I'm Irish, I get excited easily!) and hopefully I'll continue these little junkets next year. But it's not every year a dream you've had since you were 5 comes true, so 2005 will always be special, and not just because of the coming of Unicron. Got to become a Transformers artist professionally, and even managed to have my first work printed in the very first issue from IDW, so I can say I've been here from the start! So, so happy for the opportunities I've been given, and I think these are the right guys to look after Transformers too, so the set-up is pretty much perfect. So I gotta thank Chris Ryall, Beau Smith and Dave Hendrick on the pro front for leading me here, and because things ain't always easy on the home front, my amazing parents, fantastic family and friends (they're the same thing at the end of the day) and my beautiful girlfriend Anne-Marie for helping me get this far. Can I go now?
<br />
<br />
<br />
<a name="chrisryall"></a><br />
<span class="head2">Chris Ryall </span>
<br />
IDW Editor-In-Chief 
<br />
<a href="http://www.idwpublishing.com" target="_blank">www.idwpublishing.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Sin City 
<br />
Batman Begins 
<br />
40-Year-Old Virgin 
<br />
Walk the Line 
<br />
Let's be honest, though, this was a pretty crummy year for movies overall. 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
System of a Down's Mesmerize/Hypnotize 
<br />
Neil Diamond's 12 Songs 
<br />
The Eels' Blinking Lights and Other Revelations 
<br />
Bright Eyes' I'm Wide Awake It's Morning 
<br />
Black Rebel Motorcyle Club's Howl 
<br />
Turbonegro's Party Animals 
<br />
Madness' The Dangermen Sessions, Vol. 1 
<br />
Wilco's Kicking Televisions 
<br />
Foo Fighters' In Your Honor 
<br />
Fiona Apple's Extraordinary Machine 
<br />
The Mars Volta's Frances the Mute 
<br />
Queens of the Stone Age's Lullabies to Paralyze 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Arrested Development was the best show on TV this year, so of course it's been cancelled. And Lost, of course. And Stephen Colbert's Daily Show spin-off, The Colbert Report, kills me. I've grown to really love THE OFFICE, too (yes, the remake), and also Ricky Gervais' EXTRAS. 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
Alan Moore and Gene Ha's Top 10 graphic novel 
<br />
Alex Robinson's Tricked 
<br />
Those were the two best graphic novels I read all year. 
<br />
<br />
As for some great comic reads, I love Marvel's She-Hulk, and Mark Millar's "Defenders" issue of The Ultimates was one of the better reads this year. Since I tend to read everything, I tend to forget everything soon after I read it, so not everything is coming to mind right now. But I also really like Mark Waid's Legion of Super-Heroes, Ellis and Templesmith's Fell, The Walking Dead, and I really enjoyed Peter David's Madrox miniseries. Grant Morrison's All-Star Superman is off to a pretty solid start, too. Oh, and Joe Casey and Tom Scioli's Godland, a great new book with a nicely retro feel. 
<br />
<br />
Book-wise, I liked Nick Hornby's latest book, A Long Way Down, much more than How to Be Good. And I'm currently engrossed in Susanna Clarke's Jonathan Strange and Mr. Norrell. The problem with books is, I buy so many and read them when time allows, so I'm never quite sure what year a book comes out versus when I actually find time to read it. So I'll also name The Great and Secret Show by Clive Barker as one of the better things I read this year, even though that came out in 1989 and I read it now because I'll be adapting it next year. I also dug Humberto Eco's The Mysterious Flame of Queen Loana. 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Well, depending on when it happens, it just might be the birth of my first child, which is a nice ending to the year if the little bugger complies (the due date is actually January 3). If not that, taking over as Publisher at IDW, and also seeing the first comics with me as Writer actually published were nice feelings, as was being involved in the thus-far successful relaunch of the Transformers, too. I got published in my first book this year, too, Superheroes and Philosophy, another accomplishment I was pretty proud of. All in all, a good year, and a lot to build on for next year.
<br />
<br />
<br />
<a name="richardstarkings"></a><br /><img src="images/starkings2005.gif" align="right" style="margin-left:15px;margin-bottom:15px;">
<span class="head2">Richard Starkings</span></a>
<br />
Marvel Transformers editor and letterer 
<br />
<a href="http://www.hipflask.com" target="_blank">www.hipflask.com</a>
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
I&#8217;ll have to say KING KONG, even though I haven&#8217;t seen it yet! 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
AERIAL by Kate Bush. Well worth waiting for! 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
The new series of DOCTOR WHO by Russell T. Davies. Finally a DOCTOR WHO I can share with the wife and kids! 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
That would have to be mine &#8212; HIP FLASK: MYSTERY CITY, with Ladronn. Well worth waiting for! 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Getting back to Britain after seven years and showing my kids where I grew up. (Don&#8217;t disillusion them &#8212; they think I HAVE grown up!)
<br />
<br />
<br />
<a name="ejsu"></a><br /><a href="images/su2005.jpg"><img src="images/su2005thumb.jpg" align="right" style="border:1px black solid;"></a>

<span class="head2">EJ Su</span> 
<br />
IDW Transformers artist 
<br />
<a href="http://www.protodepot.com" target="_blank">www.protodepot.com</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
Wallace and Gromit: Curse of the Wererabbit 
<br />
King Kong 
<br />
Batman Begins 
<br />
Kung Fu Hustle 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Gorillaz: Demon Days 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
JLA 
<br />
Foster Home for Imaginery Friends 
<br />
Star Trek TNG re-runs 
<br />
Smallville 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
Invincible (Image) 
<br />
Yotsuba&amp;! 
<br />
Katsu 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Working on concept art for a new series at Cartoon Network studios. 
<br />
Transformers G1 comics. 
<br />
<br />
<br />
<a name="leesullivan"></a><br /><img src="images/sullivan2005thumb.jpg" align="right" style="border:1px black solid;margin-left:15px;margin-bottom:15px;">
<span class="head2">Lee Sullivan </span>
<br />
Marvel and Panini Transformers artist 
<br />
<a href="http://www.leesullivan.co.uk" target="_blank">www.leesullivan.co.uk</a> 
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
The only film I've managed to see in the cinema this year so far (!) is The Aviator; fortunately I enjoyed it. I have watched a fair amount of stuff on dvd but it's not the same... 
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
Due to iPodding, mostly rediscovery of older stuff; currently Human League's 'Secrets', B52s entire catalogue, Goldfrapp, perennially Roxy Music's entire output (see below). 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
'Brian Sewell's Grand Tour' (retracing the steps that well-to-do young aristos took a couple of hundred years or so ago to further their 'cultural education'), 'No Angels', 'Green Wing', 'Desperate Housewives', 'Sopranos', 'Tales of the Green Valley' (a series about the running of a farm in the style typical at the time of the Restoration), the return of 'Doctor Who' - from a slightly bemused viewpoint. 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
I don't read comics (I know - it shows). Books: Steven Saylor's fictional detective stories set in historical Rome. If only they'd made those as a series... 
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
Accidentally meeting Brian Eno; playing sax/synth sax/theremin in Roxy Magic; the Roxy Music tribute band I'm in - it's a real blast (ho ho) to play to appreciative audiences the stuff that made me learn the instrument in the first place. The contrast of working in a band with immediate creative satisfaction/audience feedback is such a contrast to my primary career. See <a href="http://www.roxymagic.com" target="_blank">www.roxymagic.com</a> .
<br />
<br />
<br />
<a name="ryanyzquierdo"></a><br />
<span class="head2">Ryan Yzquierdo</span>
<br />
Webmaster
<br />
<a href="http://www.seibertron.com" target="_blank">www.seibertron.com</a>
<br />
<br />
<strong>Favourite Films of 2005: </strong>
<br />
40 Year Old Virgin (funniest movie ever!), Batman Begins and (of course) Star Wars Revenge of the Sith.
<br />
<br />
<strong>Favourite Albums of 2005: </strong>
<br />
I&#8217;m part of the iTunes fad and haven&#8217;t bought a CD in 4 years so how about my favorite songs for 2005 instead? System of a Down&#8217;s &#8220;BYOB&#8221; and Weezer&#8217;s &#8220;Beverly Hills&#8221;. 
<br />
<br />
<strong>Favourite TV of 2005: </strong>
<br />
Religiously: Smallville (WB). If it just happens to be on: House (Fox), Prison Break (Fox) 
<br />
<br />
<strong>Favourite Comics and/or Books of 2005: </strong>
<br />
The demise of Dreamwave kind of killed the &#8220;favorite comics&#8221; for 2005 for me, so I&#8217;ll go to books instead: Dean Koontz &#8220;Midnight&#8221;.
<br />
<br />
<strong>Personal Highlights of the year: </strong>
<br />
I survived 2005! Unicron didn&#8217;t devour the Earth and I&#8217;m still employed (though not for long if I keep doing emails like this while at work)!!!

   <p>&nbsp;</p>
	</td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
