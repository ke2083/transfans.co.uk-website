<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Jeff Anderson</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Jeff Anderson<br>
              </span><span class="blackstrong">interviewed in:</span> March 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/anderson.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">Jeff Anderson inked and coloured 
                  a number of issues for Marvel's Transformers UK before his pencilling 
                  debut with <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=77">Issue 
                  65</a>. After that Jeff was a mainstay of the comic, contributing 
                  to such classics as <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=90">Target:2006</a> 
                  and pencilling the first telling of the Transformers' origin 
                  in <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=183">The 
                  Legacy of Unicron</a>. Jeff's other comic work includes Judge 
                  Dredd and Captain Britain. </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <p><strong>Hi Jeff</strong></p>
            <p><strong>Thanks very much for agreeing to do an interview for us. 
              </strong> </p>
            <p><strong><span class="head2">1.</span></strong><strong> Can we start 
              by asking how you got into comics, and who your early influences 
              were? </strong></p>
            <p>I&#8217;ve always been interested ever since I can remember; I 
              think originally it was a cousin who stayed with us buying me some. 
              My first memories are from comics around 1963, early Marvel but 
              mainly DC - I loved Curt Swann and Carmine Infantino, I just wish 
              I still had them.</p>
            <p><strong><span class="head2">2.</span> Moving onto Transformers, 
              how did you land that particular job?</strong></p>
            <p>I&#8217;d been to the Marvel offices with my work and been given 
              a few things to do. It was just as the Transformer material was 
              starting to be commissioned. I think the first thing I did was ink 
              a story pencilled by Mike Collins for one of the Annuals.</p>
            <p><strong><span class="head2">3.</span> Did your art briefs from 
              Simon come in the form of full script or Marvel-style overview? 
              As an artist which is more helpful? </strong></p>
            <p>Always full script. I don&#8217;t mind which way I work, full or 
              Marvel style script. Though if I was pushed I think I would say 
              the Marvel way is more interesting from an artist&#8217;s point 
              of view, as it probably allows you to be more in control of the 
              story telling. </p>
            <p><strong><span class="head2">4.</span> Did you have a preferred 
              inker or colourist to work on your pencils? Did you prefer to ink 
              them yourself where possible? </strong></p>
            <p>In a perfect world I would always rather ink and colour myself 
              - again you have more control and if the finished version looks 
              rubbish you have no one to blame but you. On Transformers I enjoyed 
              working with Steve Baskerville, he was brilliant at making all the 
              robots metallic and shiny! </p>
            <p><strong><span class="head2">5.</span> You also inked and coloured 
              other artists' work at times, as well as painted your share of covers. 
              Do you have a preference for a particular role? </strong></p>
            <p>Painting. I love colour and having to produce the whole thing. 
              If I&#8217;m doing a lot of inking I hanker after doing some pencilling 
              and visa versa.</p>
            <p><strong><span class="head2">6.</span> What aspects of Transformers 
              drawing do you consider your forte? And was there anything which 
              proved a particular nuisance to draw? </strong></p>
            <p>Lots of Transformers in one panel! Not sure if I had a particular 
              forte, you would have to ask someone else&#8230; maybe getting the 
              job in on time. Then again maybe not. </p>
            <p><strong><span class="head2">7.</span> Which were your favourite 
              characters to draw? </strong></p>
            <p>The more &#8216;human&#8217; ones, Galvatron in particular. </p>
            <p><strong><span class="head2">8.</span> You debuted several non toy 
              characters during your stint on Transformers, including Impactor, 
              Flame, and the Firebug. Were you responsible for their designs? 
              How would you go about realising a new character? </strong></p>
            <p>Quite often for incidental characters like these I just read the 
              script, look at the character traits and try to reflect that somehow 
              in the design. More often than not you have very little time to 
              think about it seriously and just do it as you go along! </p>
            <p><strong><span class="head2">9.</span> On a similar note, for the 
              striking entrance of Galvatron, Cyclonus and Scourge in Target: 
              2006 the character models were noticeably based on the toys. Next 
              issue they changed to what looked like the animation models. What 
              was the reason for this? </strong></p>
            <p>While we were doing this story to coincide with the release of 
              the movie, [we were] getting all the character designs from the 
              filmmakers. The designs didn&#8217;t turn up in time for the artwork 
              deadline. The toys had just appeared in the shops so I used the 
              toys as reference. By the time the next episode had to be drawn 
              the designs turned up and away we went. </p>
            <p><strong><span class="head2">10.</span> What are you most proud 
              of from your Transformers run? </strong></p>
            <p>Those chapters just mentioned I think, and I&#8217;m quite fond 
              of the &#8216;Firebug&#8217; story. </p>
            <p><strong><span class="head2">11.</span> As one of the longest running 
              artists on the UK comic, how much do you feel the title changed 
              over the years? </strong></p>
            <p>Well with Simon writing it always had strong stories, but there 
              was less room for experiment as the page number dropped and it went 
              b/w. Towards the end I think the toy manufacturers wanted more control 
              which isn&#8217;t always the best thing for doing the artwork, as 
              they tend to want a generic style, leaving little room for individual 
              style and interpretation. </p>
            <p><strong><span class="head2">12. </span> Was there ever any competition 
              between the artists on Transformers UK? </strong></p>
            <p>Not that I can remember, it was all very gentlemanly, apart from 
              the kidnapping and occasional threats. </p>
            <p><strong><span class="head2">13.</span> Were you aware at the time 
              how big the Transformers phenomenon was? Did you expect it to be 
              so strong so many years later? </strong></p>
            <p>No not really, my kids were too young at the time so it was difficult 
              to gauge what was going on, apart from sales and conventions.</p>
            <p><strong><span class="head2">14.</span> You went to your first Transformers 
              convention (Transforce) last year. How did you find it? </strong></p>
            <p>Enjoyed it immensely! Great meeting up with Simon again and meeting 
              Andy. </p>
            <p><strong><span class="head2">15.</span> Do you foresee ever working 
              on Transformers again? </strong></p>
            <p>Don&#8217;t think so at present but never say never as they say. 
            </p>
            <p><strong><span class="head2">16.</span> Finally can you give us 
              an idea of what you have been working on since you finished up on 
              Transformers, and what you are working on now? </strong></p>
            <p>I spent four years or so illustrating the <a href="http://www.amazon.co.uk/exec/obidos/ASIN/0745945988/qid=1110450454/sr=8-1/ref=sr_8_xs_ap_i1_xgl/202-7368129-9991850" target="_blank">Bible</a>, 
              which came out &#8217;99 and has just been republished recently. 
              Apart from that I just work as a jobbing illustrator, and later 
              this year my first comic work for years is published. Its set in 
              the middle ages and is called &#8216;Riddler&#8217;s Fayre&#8217;, 
              and marks some sort of milestone as the covers are my first pieces 
              of work I&#8217;ve produced digitally. </p>
            <p><strong>Mr Anderson, thank you very much for your time.</strong></p>
            <p>My pleasure.<br>
            </p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" valign="top" class="subcolor"><img src="images/anderson1.jpg" width="197" height="264"> 
                  <p align="center">Cover to Riddler's Fayre Issue 2<br>
                  </p></td>
                <td width="26%" class="subcolor"><img src="images/anderson2.jpg" width="197" height="264"> 
                  <p align="center">Target 2006 Prologue based on original toy 
                    designs</p></td>
              </tr>
              <tr> 
                <td valign="top" class="subcolor"><img src="images/anderson3.jpg" width="197" height="264"> 
                  <p align="center">Firebug</p></td>
                <td valign="top" class="subcolor"><img src="images/anderson4.jpg" width="197" height="264"> 
                  <p align="center">Shadow's Edge</p></td>
              </tr>
            </table>
            <p><br>
            </p>
            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
