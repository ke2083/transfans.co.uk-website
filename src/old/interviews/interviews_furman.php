<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Simon furman - Part 1 'The Past'</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Simon Furman - Part 1 'The Past'<br>
              </span><span class="blackstrong">interviewed in:</span> August 2004</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/simon.jpg" width="90" height="125" class="blueborder"></td>
                <td width="86%"> Simon Furman began his association with the Transformers 
                  when he wrote his first issue of the UK comic, <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=25&page=1" target="_blank">issue 
                  13</a>, in 1985, and he has maintained his connection with the 
                  Robots in Disguise ever since. Simon continued to work for Marvel 
                  until the end of the <a href="http://www.TransFans.co.uk/comics_guide.php?series=21" target="_blank">Generation 
                  2</a> series and thereafter maintained his ties by writing the 
                  occasional <a href="http://www.TransFans.co.uk/cartoons_guide.php?series=27" target="_blank">Beast 
                  Wars</a> episodes and convention comics. This lasted until the 
                  Transformers' recent resurgence, which now sees Simon writing 
                  both the <a href="http://www.TransFans.co.uk/comics_guide.php?series=15" target="_blank">War 
                  Within</a> and <a href="http://www.TransFans.co.uk/comics_guide.php?series=16" target="_blank">Energon</a> 
                  (previously Armada) for <a href="http://www.dreamwaveprod.com" target="_blank">Dreamwave</a>. 
                  Much of Simon's original Marvel work has now been republished 
                  by Titan Books, most recently in the <a href="http://www.amazon.co.uk/exec/obidos/ASIN/1840239352/qid=1093853407/sr=1-7/ref=sr_1_10_7/026-9765825-6833223" target="_blank">Second 
                  Generation</a>TPB and he has also recently written the epic 
                  <a href="http://uk.dk.com/nf/Book/BookDisplay/0,,11_1405304618,00.html" target="_blank">Transformers: 
                  The Ultimate Guide</a> for <a href="http://www.dk.com" target="_blank">Dorling 
                  Kindersley</a>. Last but by no means least (phew!) Simon also 
                  runs <a href="http:www.wildfur.net" target="_blank">Wildfur 
                  Productions</a> with Andrew Wildman. </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.wildfur.net/" target="_blank"><img src="images/wildfur2.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <p><strong>Hi Simon</strong></p>
            <p><strong>Thanks again for agreeing to do this interview for us. 
              As this is going to be a two-part interview we thought we would 
              attempt to split things into 'past and present'. If it's ok with 
              you we'll indulge our (not particularly well hidden) inner children 
              and start with the past, i.e. your time at Marvel...</strong></p>
            <p><strong><span class="head2">1.</span> First up, how confident were 
              you of utilising or killing off characters in the UK strip when 
              Bob Budiansky was still working on the US comic? For instance, removing 
              Skids to Limbo seemed a bold move, as was including the Autobot 
              Triple Changers and Deluxe Autobots as part of The Wreckers' line 
              up. Did you have any contact with Bob over such things?</strong></p>
            <p>To be honest, we took a lot of calculated (or, sometimes, not so 
              calculated) risks with the UK stories, because often we only had 
              the loosest idea of what was coming up in the next batch of US stories. 
              Sometimes we&#8217;d get overviews or scripts from the US office, 
              but generally we just saw finished issues (as actual comics) or 
              advance (Xeroxed) b/w pages, if we were lucky&#8230; as and when 
              they were circulated. To an extent, I&#8217;d try and anticipate 
              where Bob was going, and trust in the law of numbers. As new characters 
              were introduced (in the US series), others were necessarily sidelined, 
              so we tried to pick those out. Sometimes we got it right, other 
              times not. Our contact with Bob himself was almost non-existent 
              in the early days.</p>
            <p><strong><span class="head2">2.</span> Several prominent characters 
              in the UK exclusive stories, such as Swoop and the Predacons were 
              unavailable in this country. Was this intentional, coincidence, 
              or were you unaware of the release schedules for the toys and simply 
              worked in characters as you saw fit?</strong></p>
            <p>We largely took our cue from what characters were being introduced 
              into the US storyline. If there was a release schedule for the toys 
              in the UK, we rarely saw it. There were odd occasions where Hasbro 
              UK would actually get it together and coordinate a story with a 
              toy release. The Special Teams (in UK #63-65) story was one such 
              instance, and in that case (because we were some way off reprinting 
              the corresponding US issues) we had to work them in somehow (chronologically 
              ahead of time). Same with Headmasters. But in the case of Swoop 
              and the Predacons, I don&#8217;t think I was consciously aware (at 
              the time) that we were dealing with toys not generally available 
              in the UK. They were just extant characters, and therefore fair 
              game. The advent of the Wreckers showed a definite change of mindset. 
              There we just went hell for leather for it, regardless. It could 
              have been messy, but it worked out OK. Those were characters Bob 
              had barely, if at all, touched on. They worked a bit like the movie 
              characters after that, where we felt they were pretty much &#8216;ours,&#8217; 
              to do with as we will.</p>
            <p><strong><span class="head2">3.</span> Why do you feel continuity 
              suffered more instead of less, between Marvel UK and US when you 
              took over the US book?</strong></p>
            <p>The problem was, we were badly out of synch with the US material 
              reprints by the time I was also writing the US comic. I was trying 
              my hardest to craft semi-crossover stories (like the Deathbringer 
              two-parter with US #65) and then the UK comic would run a batch 
              of old UK reprint material and completely throw it out. I realised 
              I was making matters worse (and more confusing) and not better, 
              and pretty much stopped trying to directly tie the two together. 
              Looking back, as I try my hardest not to do, it&#8217;s very hard 
              to tie the Earthforce stories into a specific time frame (in terms 
              of the US continuity), because (if I&#8217;m brutally honest) I 
              didn&#8217;t try too hard to make it work in the first place. By 
              that point, I was just trying to tell a bunch of fun UK stories 
              that didn&#8217;t necessarily impact on the larger (US) storyline. 
              How was I to know 15 or so years later people would be trying to 
              reconcile it all?</p>
            <p><strong><span class="head2">4.</span> The Marvel UK editorial originally 
              promised a monthly comic as of issue 333, complete with a new 5 
              page story each issue written by yourself. Do you know why this 
              never came to be, and what, if anything, had you planned story wise? 
              Would we have seen a continuation of Earthforce for example?</strong></p>
            <p> I confess I don&#8217;t know. I didn&#8217;t even, to the best 
              of my somewhat unreliable memory, remember ever being approached 
              with that possibility. They probably just assumed I&#8217;d do it&#8230; 
              and they were probably right. Certainly, I&#8217;d have liked to 
              do more, as I felt it all tailed off a bit. Those last few UK stories 
              have &#8216;limited lifespan, why bother?&#8217; written all over 
              them. I&#8217;m not proud of that. But, like I say, who&#8217;d 
              have thought anyone would even notice?</p>
            <p><strong><span class="head2">5.</span> Moving on to Generation 2, 
              what further plans did you originally have for it beyond the 12 
              issue arc? Would Jhiaxus still have met his end in issue 12?</strong></p>
            <p>Oh yeah. Jhiaxus was only ever meant to be a one-arc character. 
              I continually wanted to shift the focus and scale of the saga, and 
              I didn&#8217;t want to keep running with non-toy characters to the 
              extent where Hasbro might kick up a fuss. The next story arc would 
              have been something like &#8216;Alignment&#8217; turned out to be, 
              though whether I&#8217;d have wrapped the Liege Maximo story in 
              the next 12-issue chunk is debatable. I definitely wanted to keep 
              upping the scale and scope of the threat. I do remember having Galvatron 
              very much in mind to be the wild card in whatever happened next.</p>
            <p><strong><span class="head2">6.</span> Do you think there will ever 
              be an official continuation of the G2 storyline? Do you think one 
              is needed after Alignment?</strong></p>
            <p> I feel now, after Alignment, I&#8217;ve done with G2. I&#8217;ve 
              told that story, brought it to a (to me, at least) satisfactory 
              conclusion. I think the question now, as far as Dreamwave continuity 
              goes, is what exactly is G2? It&#8217;s&#8230; G1 really, just a 
              bit later on. Given there&#8217;s a G1 ongoing book, I assume any 
              G2 characters will appear there in due course. It was fun to use 
              the G2(ish) Turbomasters in War Within 3, which starts to move everything 
              into a cohesive G1 (rather than G1/G2).</p>
            <p><strong><span class="head2">7.</span> Looking back on your Marvel 
              work, do you see any missed opportunities, or anything you would 
              have done differently?</strong></p>
            <p>No, not really. I&#8217;m sure (as the writer I am now) I&#8217;d 
              approach certain storytelling aspects differently, but I&#8217;m 
              a firm believer in once it&#8217;s done, it&#8217;s done (even to 
              the extent where after I send a script off, bar a dialogue polish 
              at artwork stage, that&#8217;s it&#8230; I don&#8217;t start running 
              back over it in my mind wondering if I should have done this or 
              that differently). So no, there&#8217;s nothing I&#8217;d change. 
              I can see now how things might have been done better or to greater 
              effect, but I can happily treat them as an archaeologist treats 
              artefacts recovered from a dig site. Those stories there come from 
              the &#8217;87 era, and show developmental stage &#8216;B&#8217; 
              of writer Simon Furman. They&#8217;re frozen now in time. There&#8217;s 
              a few old stories I&#8217;d rather never saw the light of day in 
              trade paperback collections (some of my very earliest work for TF 
              UK), but even these are just me learning to stand upright. As for 
              missed opportunities, I&#8217;ve written so much Transformers stuff, 
              I&#8217;m pretty sure I covered all or most of the bases somewhere 
              along the line. And anything I didn&#8217;t do then, thanks to Dreamwave 
              and OTFCC and iBooks and Panini UK and so on, I can do now&#8230; 
              only differently.</p>
            <p><strong><span class="head2">8.</span> You brought Andrew Wildman 
              back to Transformers after all these years. Are there any other 
              Marvel UK artists you think its possible to work with again?</strong></p>
            <p>Well, I&#8217;ve already worked again with Lee Sullivan (on the 
              Wildfur produced Transformers mini-comic for the Atari game release), 
              and it&#8217;s not so terribly long ago that I last worked with 
              Geoff (Senior), on the Botcon 2000 comic. Plus, Geoff&#8217;s done 
              covers for the Titan reprints, as has Lee and a bunch of other, 
              former Transformers, artists (Barry Kitson, Bryan Hitch). I&#8217;m 
              in touch with most of the artists from back then, so anything&#8217;s 
              possible.</p>
            <p><strong><span class="head2">9.</span> And now, if we may be so 
              bold, the fanboy questions. Firstly - why didn't the Autobots just 
              set Omega Supreme on Galvatron in Target 2006?</strong></p>
            <p>Er&#8230; pass. He was off on a special mission&#8230; somewhere&#8230; 
              doing something else&#8230; honest.</p>
            <p><strong><span class="head2">10.</span> How come Galvatron had memories 
              in Time Wars, even though that Megatron was just a clone?</strong></p>
            <p>Hang on&#8230; Galvatron is created from Megatron in the future, 
              by which point he&#8217;s no longer a clone. Right? I&#8217;m confused 
              now. Don&#8217;t ask me questions like this!</p>
            <p><strong><span class="head2">11.</span> And last but by no means 
              least, whatever happened to present day Ultra Magnus after Deadly 
              Games?</strong></p>
            <p>He settled down on Cybertron and raised two Scraplets and a Mechannibal. 
            </p>
            <p><br>
              <strong> Don't forget to check out part 2 of our interview with 
              Simon <a href="interviews_furman2.php">here!</a></strong></p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor"><img src="images/simonextra1.jpg" width="197" height="264"> 
                  <p align="center">Next Generation TPB from <br>
                    Titan Books. </p></td>
                <td width="26%" class="subcolor"><img src="images/simonextra2.jpg" width="197" height="264"> 
                  <p align="center">Simon's first issue for <br>
                    Marvel, UK #13 </p></td>
                <td width="48%" valign="top" class="subcolor"><img src="images/simonextra3.jpg" width="175" height="264"> 
                  <p align="center">...and his last G2 #12.</p></td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar"><p><a href="http://www.titanmagazines.com/transformers.html" target="_blank">Titan 
                    Books</a>: You can see a full list of Titan's Transformers 
                    TPB's here.<br>
                    <a href="http://www.wildfur.net" target="_blank">Wildfur.net</a>: 
                    Andrew Wildman and Simon Furman's production company. The 
                    site includes updates on their current activities as well 
                    as information on services and properties. <a href="http://www.whorunstheengine.net/" target="_blank"><br>
                    The Engine: Industrial Strength</a>: Andrew and Simon's online 
                    graphical story telling project. Check it out. <a href="http://www.dreawwaveprod.com" target="_blank"><br>
                    Dreamwaveprod.com</a>: check out DW's site here. <a href="http://www.dk.com" target="_blank"><br>
                    DK.com</a>: Dorling Kindersley's website, with specific infromation 
                    about the top notch Transformers: The Ultimate Guide <a href="http://uk.dk.com/nf/Book/BookDisplay/0,,11_1405304618,00.html" target="_blank">here</a>. 
                  </p>
                  </td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.whorunstheengine.net/" target="_blank"><img src="images/engine.gif" width="468" height="60" border="0"></a></div></td>
              </tr>
            </table>
            <br> </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
