<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Rob Ruffolo</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Rob Ruffolo<br>
              </span><span class="blackstrong">interviewed in:</span> October 2006</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><a href="images/ruffolo_sixshot.jpg"><img src="images/ruffolo.jpg" width="90" height="124" class="blueborder"></a></td>
                <td width="86%" valign="top">
		Rob Ruffolo had highs and lows during the Dreamwave era, colouring the now classic first volume of War Within, but also illustrating the widely rather less well-regarded Micromasters. The former Art Director of the Canadian company has now transferred his talents to IDW, and he makes his debut there on the Sixshot issue of the Spotlight series. 
                </td>
              </tr>
            </table> 
       
	    
            <p><br><strong><span class="head2">1.</span></strong><strong> Were you a fan of the Transformers before joining the team at Dreamwave? </strong></p>
	    
            <p>Oh yeah, that's mainly the reason I wanted to work with the Dreamwave
crew. It brought back a lot of old childhood memories.
 </p>
 
            <p><strong><span class="head2">2.</span> You came to the attention of most fans with colouring stints on
celebrated titles like War Within. What was the experience like in
those early days?  </strong></p>
	    
            <p>It was an exciting time, my first published work. I guess simply put,
it was a fun time.
 </p>
	      
            <p><strong><span class="head2">3.</span> What did your duties as Art Director for the company involve? Can
you give us any insight into the 'house style'?
 </strong></p>
	      
            <p>To be honest, it wasn't much fun, partly because I wasn't doing much
artwork other than correcting other peoples but mostly because of the
state of Dreamwave at that time. The house style colours were
influenced by the anime cell cut style, but I think after awhile it
became overdone. Many of the books were turning out muddy and dark. 
 </p>

            <p><strong><span class="head2">4.</span> What did you think when you first read the script for Micromasters?
 </strong></p>
	    
            <p>Too many characters, too much going on. It just wasn't a good story. I
did however learn a lot about the lives of our favorite little 'bots
and how they enjoyed hanging out at an all guys club armwrestling. Not
that there's anything wrong with that. 
 </p>
	      
            <p><strong><span class="head2">5.</span> It proved to be one of Dreamwave's least popular titles, with both
the script and art coming under heavy criticism, at least from the
vocal online community. Did fan reactions surprise you? How would you
approach the same assignment if you were to draw it today? 
 </strong></p>
	    
            <p>No, the reactions didn't really surprise me, I knew it wasn't my best
work but there were several cicumstances that didn't allow me to have
the quality I would have liked. I knew from the beginning it would be
tough sledding but it was a chance to pencil my own book and I knew it
would be a good experience for me no matter the reaction. I'm not sure
I would appraoch it the same way - you have to approach things given
the situation at that particular time. 
 </p>

            <p><strong><span class="head2">6.</span> What are you proudest of from your time at Dreamwave?
 </strong></p>
	    
            <p>It's not one specific thing, I think it would have to be coming
through the whole Dreamwave experience unscathed and able to continue
on my own. 
</p>
	    
            <p><strong><span class="head2">7.</span> So how were you affected by the demise of the company, and what
projects have you been working on since? 
 </strong></p>
	    
            <p>At the time it was difficult for everyone and in times like those you
really get to see peoples true colours (no pun intended) but the
experience has actually been very good for me and I learned a lot.
I've been able to work on several different projects in and outside
the comic book industry since then. 
 </p>
	      
            <p><strong><span class="head2">8.</span> How did you get involved with IDW and the Spotlight comic? 
  </strong></p>
	    
            <p>I started off colouring a few covers here and there, and after that I
pencilled some of the characters for IDW's Tranformers guide book. Dan
Taylor at IDW asked me if I would be interested in doing the Sixshot
Spotlight. It was a great oppertunity because I would be pencilling
inking and colouring the whole book.  </p>
	    
            <p><strong><span class="head2">9.</span> Sixshot has the distinction of being one of the more popular
characters that never played a large part in Western comics or
cartoons. Did that bring additional challenges to getting his
character across to readers, and did you look through Japanese
material for references or decide on a clean slate? 
 </strong></p>
	    
            <p>I did look at a lot of different material but I wanted to bring my own
style to Sixshot's character. I think it's important to put your own
spin on things. I had a lot of fun with this book because all of the
art was mine, pencils inks and colours.  </p>

            <p><strong><span class="head2">10.</span> Can you give us any hints about the storyline? 
 </strong></p>
	    
            <p>Hmmm, I don't want to give anything away but I will say there will be
a few new characters. </p>
	    
            <p><strong><span class="head2">11.</span> What other future projects can we expect from you at IDW?
  </strong></p>
	    
            <p>I'm not exactly sure. Hopefully I can work on something with IDW in
the near future. </p>
	    
            <p><strong><span class="head2">12.</span> What other characters, Transformers or otherwise, would you like
the chance to draw professionally? 
 </strong></p>
	    
            <p>Ahhh, well, I would love to do a Wheeljack story. He's my favorite
Transformer and the first one I ever got . Maybe a story of how
Wheeljack created the Dinobots, a take on the old G1 cartoon episode.
 </p>
	    
            <p><strong><span class="head2">13.</span> Given that you have an alliterative name, are you in fact a Marvel
superhero?
</strong></p>
	    
            <p>Ha ha, no, I'm not, but lets just say my super powers wouldn't be
having exceptional grammar skills. I had to look that word up. 
</p>
	    
            <p><strong><span class="head2">14.</span> And finally, some Word Association... </strong></p>


	    <p><strong>Monopoly   </strong></p>

            <p>Bald old guy with a tophat. </p>
	    
            <p><strong>Snakes on a Plane </strong></p>

	    <p>What happened to you Samuel L? A far cry from the Pulp Fiction days.</p>

	    <p><strong>Countdown  </strong></p>

	    <p>New Years Eve, ummm, I mean a Micromaster. </p>
	    
	    <p><strong>Brad Mick </strong></p>

	    <p>Oh, you mean James McDonough? </p>
	    
	    <p><strong>James Bond </strong></p>

	    <p>Sean Connery. Do any of the other guys come close?</p>
	    
	    <p><strong>Thank you very much for your time!</strong></p>

   
	    
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/ruffolo1.jpg"><img src="images/ruffolo1sm.jpg" style="border:2px white solid;"></a></td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/ruffolo2.jpg"><img src="images/ruffolo2sm.jpg" style="border:2px white solid;"></a></td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/ruffolo3.jpg"><img src="images/ruffolo3sm.jpg" style="border:2px white solid;"></a></td>
	      </tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
