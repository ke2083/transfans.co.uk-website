<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Paul Cannon</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Paul Cannon<br>
              </span><span class="blackstrong">interviewed in:</span> August 2004</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/paulcannon.jpg" width="91" height="125" class="blueborder"></td>
                <td width="86%" valign="top">Paul Cannon is the organiser and 
                  owner of the popular UK convention <a href="http://www.transforce.org.uk" target="blank_">Transforce</a>, 
                  which has been running (with a break in 2003) since 1999. Paul 
                  also invented the Space Shuttle and vegetables. </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td class="greybar"><div align="center"><a href="http://www.transforce.org.uk" target="_blank"><img src="images/transforcelogo2.jpg" width="476" height="59" border="0"></a></div></td>
              </tr>
            </table>
            <p>&nbsp;</p>
            <p><strong>Hi Paul</strong></p>
            <p><strong>Thanks again for agreeing to take the time for this interview, 
              we know that you are busy getting ready for Transforce. Firstly 
              we would like to ask some general questions, so people can get to 
              know you a bit, and then of course there will be questions about 
              the upcoming Transforce event.</strong></p>
            <p><strong><span class="head2">1.</span></strong><strong> Firstly, 
              and perhaps predictably, can you tell us what was it that got you 
              into Transformers? </strong></p>
            <p>One Saturday in October 1984 I spied a new comic on the shelf of 
              my local newsagents. It was issue 2 of Transformers (UK). I was 
              drawn to the image of Jazz on the cover. Being a very young Star 
              Wars fanatic, I was into robots anyway, but this was something different 
              and far more exciting than good old R2D2 and C3PO. I picked up later 
              issues and every time I enjoyed it. I was never very flushed with 
              money when I was younger (a real Charlie bucket case), so I had 
              to rely on family buying me the odd copy. Eventually I managed to 
              convince my dear old Mum to get it reserved for me every week, and 
              so it began. That Christmas I got Optimus Prime, and for the next 
              six months my collection included Windcharger, Prime and Cosmos. 
              Desperate for more I managed to slowly build up a small collection 
              through, though it was never huge (until recently of course). Lego 
              filled the gap for a while. Have you ever seen a Lego Transformer? 
              You will if you come to this year's show.<br>
              Of course I collected for several years, then by 13 or so, didn't 
              buy the toys anymore. I got the comic until it started the black 
              and white reprints, and that was when I though enough is enough! 
              It wasn't until 1998 when I was walking past the 'Index catalogue' 
              shop when I saw Optimus Primal in the window. I cursed Hasbro for 
              the abomination Optimus 'Primal', I couldn't believe he was a MONKEY! 
              A few weeks later I found myself in Toys 'R' Us and spied the original 
              Dinosaur Megaton. Seemingly in a trance I bought it, took it home 
              and opened it in front of my girlfriend who just sighed. That was 
              it, it had begun again! Soon after I did the same as most others 
              and typed &quot;Transformers&quot; into a search engine and WOW! 
            </p>
            <p><strong><span class="head2">2.</span> Do you have any particular 
              favourite toys, cartoon episodes and/or comic issues? </strong></p>
            <p>Shockwave, Jetfire and G1 Megatron are my favourite toys, though 
              Optimal Optimus comes high up on the list, and that 20th Anniversary 
              Prime... YUM!<br>
              I have to admit to having a sincere dislike of much of the cartoon 
              world. They got the story wrong for a start, I am a Furmanite through 
              and through. I always felt cheated by GMTV and TV in general because 
              they just couldn't show an episode of the cartoon all the way through! 
              If you didn't set the alarm on a Saturday morning, you'd miss Roland 
              Rat's famous introduction, &quot;and now it's time for Transformers... 
              YEAAAAH&quot;. I was so looking forward to seeing the full episodes, 
              and when I did, they weren't that great. Who decided the Quintessons 
              created the Transformers - SHEESH! Beast Wars is the best thing 
              to ever hit our TV screens, nothing has surpassed it, and I don't 
              think anything ever will, though I'd like to be wrong about that. 
              The new movie perhaps?<br>
              Target 2006 is easily my favourite comic story. I liked Wanted: 
              Galvatron Dead or alive as it introduced my favourite character 
              Death's Head. As it happens Death's Head makes an appearance at 
              Transforc.... oh, you can wait and see. </p>
            <p><strong><span class="head2">3.</span> And conversely is there anything 
              in Transformers continuity that you would happily set fire to and 
              drop out a 15th story window? </strong></p>
            <p>I've never liked the G1 season 3 cartoon (puttup), Transformers: 
              Robots in Disguise (puttup), Armada (puttup) or Energon (puttup). 
              I've never liked Japanese animation. We will of course be showing 
              Energon and some Japanese stuff at Transforce. In my opinion the 
              Transformers story is on pause at Beast Machines (even though that 
              had it's moments).</p>
            <p><strong><span class="head2">4.</span> How much of your time does 
              Transforce take up? Do you have much time for other hobbies? Or 
              indeed life in general? </strong></p>
            <p>Yes, I spent around six months of the year on Transforce. I am 
              a primary school teacher in my normal life. But spend an hour or 
              so an evening most days leading up to the show, then from June onwards, 
              every spare moment. At the moment I am doing at least a six hour 
              day on Transforce alone. </p>
            <p><strong><span class="head2">5.</span> What can you tell us about 
              the history of Transforce? At what point exactly did you give in 
              to the insane idea of organising a Transformers convention? </strong></p>
            <p>In 1998 I was meeting lots of new and interesting people through 
              TMUK (Transmaster UK). They were all talking about Botcon UK, and 
              how they didn't think it was going to happen. And we waited, and 
              we waited. In the end I'd had enough and had a go myself. Transforce 
              '99 was held in two shops in Church Street, Croydon. A Place in 
              Space and Otaku World. They are still there. It was &pound;1 to 
              get in. I just about covered my costs and I think a fun day was 
              had by everyone. I am grateful to the people who were behind me 
              and pushed me to make it happen. Of course the moment (even second) 
              I went public on Transforce, Botcon Europe was announced. Of course 
              they though I nicked their idea and was stealing their thunder, 
              but that was certainly never my intention. It's like buses, you 
              wait ages for one, then two come along together (though ours was 
              first).</p>
            <p><strong><span class="head2">6.</span> How do you think Transforce 
              distinguishes itself from other conventions? </strong></p>
            <p>It tries to do it's own thing. I think all of the UK Transformers 
              conventions I've been to were good, other (non-Transformers) conventions 
              can be money making machines. Transforce to date has never made 
              a penny, we aren't out to make money, so it's putting on a good 
              show that matters to us, nothing else.</p>
            <p><strong><span class="head2">7.</span> What should we expect in 
              the future for Transforce? You had a break last year, should we 
              expect that again at some point? </strong></p>
            <p>I had a break because it just wasn't possible to run the show and 
              get married. I asked some friends to take it on for the year, but 
              they just didn't want the responsibility or have the time. I even 
              put it up for sale at one point, but then pulled out at the last 
              minute when I wasn't entirely happy about the 'new owner'. I'd rather 
              Transforce was gone forever than took a nosedive. I can't foresee 
              another break in the near future, but I don't promise to run this 
              show forever. </p>
            <p><strong><span class="head2">8.</span> Obviously much of Transforce's 
              original inspiration comes from what we now know as Generation 1. 
              Do you think lines such as Armada or Energon will inspire someone 
              else in the same way 20 years down the line? </strong></p>
            <p>I didn't think so at first, but then actually I have much contact 
              from the younger generation who have the same look in their eyes 
              I once had when I was their age about G1. They have the same tone 
              in their voice when they are talking about this new generation. 
              I've never yet come across anyone who didn't prefer G2, young or 
              old(er). </p>
            <p><strong><span class="head2">9.</span> What are the best things 
              about organising the convention? </strong></p>
            <p>Getting to meet (and work with) your childhood heroes. Meeting 
              all the fans who share your interest. Seeing the effect it's having 
              on people. When I hear someone say, or read on a forum about how 
              excited people are getting over the show, that does it for me. I 
              suppose I'm nervously excited myself at the moment. </p>
            <p><strong><span class="head2">10.</span> And the worst? Or should 
              we leave that until we get to the questions about Alignment? </strong></p>
            <p>Always worrying about how you are going to pay for everything. 
              I hate cutting corners, but to do things properly you need to spend. 
              Oh yes, also I need to spend more time in the sunshine, I don't 
              see a lot of that at the height of summer. Perhaps next time I should 
              run Transforce on my laptop. </p>
            <p><strong><span class="head2">11.</span> As a convention organiser, 
              you have a good view of the fandom in the UK and Europe. What are 
              your impressions? </strong></p>
            <p>I think it is generally a very nice community. I can't stand all 
              that 'my ego is bigger than your ego' nonsense you get in 'certain' 
              countries. I think fandom is well organised over here. Fiction plots 
              generally tie in to a stable continuity. I never see flame wars. 
              I only ever see nice people enjoying their hobby. </p>
            <p><strong><span class="head2">12. </span> Were you concerned at all 
              about organising the convention this year with the uncertainty around 
              OTFCC Europe? </strong></p>
            <p>Other conventions don't really have any impact on Transforce. We 
              try to co-ordinate dates so they are spread throughout the year. 
              Transforce as a rule is always the summer convention. Is there a 
              OTFCC Europe this year? I haven't heard or read anything. there's 
              room for us all. I have plenty of time for Jason and his crew and 
              Sven and Simon. I think OTFCC would do better to move back to their 
              Decepticon roots, there is just too much pressure on them from the 
              big 'H' (and by that I mean Hasbro). </p>
            <p><strong><span class="head2">13.</span> Over the 2001 and 2002 conventions 
              you made available, as a convention exclusive, a Simon Furman penned 
              story called Alignment, which concluded the Cybertronian Empire 
              story begun in G2 Issue. There has been some comment on the jolly 
              old internet since then from some that they do not consider this 
              story to be official or 'canon'. What is your reaction to that? 
              </strong></p>
            <p>It was a book written by Simon Furman. It was his conclusion of 
              the story he started in the Generation 2 comic. Furman has indeed 
              stated that it was 'professional fanfiction' as it was not backed 
              by the big 'H'. However I'm sure that he, nor I, nor anyone could 
              not consider this a part of the Transformers mythology. I think 
              other stories that are considered 'canon' by those same people are 
              so poor, I'm not going to lose sleep over it anymore. I am happy 
              with it, and that's all that matters. Funnily enough I read mixed 
              reviews of the book, most people really liked it, many people didn't. 
              My favourite 'review' of the book said something like 'I did a poo 
              on a piece of paper and it turned out better than Alignment'. That 
              was class! Not sure I agree, and I wouldn't like Simon to see that 
              either. Remind me never to borrow a book from the man who said that.</p>
            <p><strong><span class="head2">14.</span> Still on the subject of Alignment, whilst some people don't 
              see it as official, you have still had problems with piracy of the 
              material. How do you feel about that, and the fact that many fans 
              seem to think they are entitled to a free copy of Alignment, regardless 
              of the money you put into it? </strong></p>
            <p><br>
              I do actually think people have the right to read the story as I 
              think it is something rather special (whether it is 'canon' or not). 
              I do consider it an important part of the Transformers story and 
              I wouldn't want to keep it from people. I was miffed before as it 
              was assumed I'd put it on the website right after the show, but 
              there are so many new fans who missed out, I think they deserve 
              a chance to see what they missed out on. I have downloaded my fair 
              share of MP3's, but I only download tracks which I CANNOT buy (they 
              are deleted). I disagree with getting something for free when you 
              can buy it in a shop. It was made clear to people for a long time 
              how they could get their own copy of Alignment, but some were either 
              too lazy or ignorant to bother. And then when it didn't land in 
              their laps after the show, they didn't like it. But you can't hold 
              grudges forever. It's just not healthy. </p>
            <p><strong><span class="head2">15.</span> Whilst it has been illegally 
              circulated, there remains a demand for Alignment, any plans to republish 
              in some form? </strong><br>
              <br>
              It kind of makes it a cult collectible doesn't it? Though the form 
              it was published in upset me greatly! Limited copies will be on 
              sale at Transforce 2004. It will most likely be available after 
              that on the website. Perhaps a Christmas present for everyone?</p>
            <p><strong><span class="head2">16.</span> On a similar subject, are 
              there any plans to bring Eugenesis back to the convention? </strong></p>
            <p>Ah, now James Roberts is the man to ask about that. When he self-published 
              it in 2001 he did so at a loss, and even though every copy sold 
              out, he didn't make back his expenditure. I don't think a reprint 
              will be likely unless it is signed up by some clever publisher. 
              Fingers crossed. </p>
            <p><strong><span class="head2">17.</span> And finally, and most importantly, 
              what drink should people buy you on the day? </strong></p>
            <p>I'm a Stella man myself, but as I'm driving, I'll be sticking to 
              the coke. I may dump the car and find the pub after the show, in 
              which case, you're on!</p>
            <p><strong>Mr Cannon, thank you very much for your time.</strong></p>
            <p>You are more than welcome, it's been a pleasure.</p>
            <p>&nbsp;</p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor"><img src="images/transforce1a.jpg" width="197" height="264"> 
                  <p align="center">Transforce Exclusive Target 2006 sketch</p></td>
                <td width="26%" class="subcolor"><img src="images/transforce1c.jpg" width="197" height="264"> 
                  <p align="center">Final Transforce 2002 cover by Lee Sullivan</p></td>
                <td width="48%" valign="top" class="subcolor"><img src="images/transforce2.jpg" width="197" height="264">
                  <p align="center">Transformers Alignment Transforce 2001</p>
                  </td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar"><p><a href="http://www.transforce.org.uk" target="blank_">Transforce</a> 
                    is being held at Canon's Leisure Centre in Mitcham, Surrey, 
                    on August 28th. Pre-registration closes on August 14th. Guests 
                    include writer Simon Furman, artists Andrew Wildman, Jeff 
                    Anderson and comics superstar Bryan Hitch. See you there (or 
                    more probably, in the pub afterwards...).</p>
                  <p><a href="http://www.transforce.org.uk" target="blank_">Transforce 
                    website</a> <a href="http://www.transforce.org.uk/twentyyears.mov" target="blank_"><br>
                    Transforce trailer</a> </p></td>
              </tr>
            </table>
            <p><br>
            </p></td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
