<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Lee Sullivan</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Lee Sullivan<br>
              </span><span class="blackstrong">interviewed in:</span> March 2006</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/sullivan.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">When it comes to Transformers art, Lee Sullivan is a legend. He began his stint at Marvel UK on issue 92 with the first of many memorable covers, and soon went on to create iconic work in the likes of Salvage and Time Wars. Over the years he's worked on many other titles, including Thundercats, Doctor Who, Robocop and William Shatner's TekWorld to name but a few, and has a noisy fetish for the saxophone.</td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <p><strong>Hi Lee, thanks a lot for agreeing to an interview.</strong></p>
            <p><strong><span class="head2">1.</span> How did you get the job drawing for the Marvel UK Transformers comic? </strong></p>
            <p>I met John Higgins in my local art shop, who was looking for someone to help him with lettering on a strip he was producing for a music magazine. John was very kind and took me to see some magazine editors and eventually into Marvel UK. I showed my work to Ian Rimmer who was editing Transformers at that point; he commissioned me to produce my first Transformers cover. </p>
            <p><strong><span class="head2">2.</span> You certainly started out with a series of very striking covers. How did you move from that to interior work?  </strong></p>
            <p>I'd always wanted to draw strips (and had for my own entertainment as a kid) and asked Simon for an old script. I took it home, sat and tried to do some roughs and failed entirely &ndash; I didn't understand the grammar of storytelling. So I spent the next few months studying what everyone else was doing, as well as taking note of some of the written advice which I've included on my website subsequently &ndash; knowing the 'rules' helps a lot! Then I asked to do a strip as a commission from Si, knowing that that would make it 'real', with the proviso that if he didn't like it he didn't have to buy it. Fortunately, he liked it and it appeared in an annual under the title 'Altered Image'. </p>
            <p><strong><span class="head2">3.</span> Your early work was somewhat reminiscent of Geoff Senior's style. Was he an early influence? How do you feel your style developed? </strong></p>
            <p>Yes, he was; at first I had no idea what Transformers were or how to draw them, so I just took the style I thought was closest to what I'd like to be producing and tried to draw like that. Very dynamic. But I'm probably a more nit-picky artist in terms of detail, so I think even the earliest ('Altered Image') still has its own look. Later, as I grew more confident, I found a look that was mine, partly as a result of using a brush to ink with and partly due to speed of execution. The faster you work, the more of the raw style of the artist comes through, I think. </p>
            <p><strong><span class="head2">4.</span> What were the biggest challenges?  </strong></p>
            <p>Just understanding Transformers at first &ndash; the whole concept of huge robots that were also trucks etc. was very foreign to me. Though Thundercats were even more unlikely. I'm glad I didn't do much of that! </p>
            <p><strong><span class="head2">5.</span> And what do you feel were your greatest strengths?  </strong></p>
            <p>Reliability and clear storytelling. </p>
            <p><strong><span class="head2">6.</span> As a long serving artist on Transformers, how do you feel the comic changed over the years? </strong></p>
            <p>Well, when I did a few things relatively recently for reprints and the lost-in-action mini-comic for the PS2 or whatever it was, and had to look at the modern interpretations it seemed like a different world &ndash; with much more surface detail! Of course, any successful franchise has to be reinterpreted for a fresh audience: Who; Trek; Bond; Holmes have all moved on every so often. </p>
            <p><strong><span class="head2">7.</span> Unlike some of your contemporaries you never moved over to the Marvel US Transformers title. At the time were you keen to move away from the franchise?  </strong></p>
            <p>That never arose; I was poached by Marvel US to produce RoboCop and subsequently William Shatner's TekWorld, as well as contributing to the UK's Doctor Who strips in Doctor Who Magazine. I got those jobs directly and indirectly as a result of starting on Transformers. But then again, I was never asked! But I've never been ashamed of having worked on Transformers; I had a good time doing them. </p>
            <p><strong><span class="head2">8.</span> So how would you compare drawing G1 characters for Marvel to drawing Armada characters for Panini?  </strong></p>
            <p>Ah, now that is something that in retrospect I AM ashamed of &ndash; but only because of my art. No excuses, but reasons: around the time I was given those to do I was actually going through a hellish period: my mother and father became ill and died within a year of each other and my mind was a loooong way away from my career. I can't really remember much of it &ndash; for a year I was caring for my father after the death of my mother; when he died I fell into depression as well as grieving. I churned out work on Transformers Armada and Thunderbirds which I'm not pleased with, though it was adequate, I suppose; I never wanted to be 'adequate' though!  </p>
            <p><strong><span class="head2">9.</span> What do you make of the new generation of Transformers artists? </strong></p>
            <p>Don't really have much knowledge of them, though I know their work is highly detailed; comics imagery has had to start competing with CGI and I guess the audience expects a more Manga approach. </p>
            <p><strong><span class="head2">10.</span> What was the experience like illustrating the UK Transformers DVD covers? </strong></p>
            <p>Delightful. The company we worked for was very professional and I think pleased to be working with people who knew the subject. I often find that when working for clients outside the comics industry one's accorded far more respect than is ever shown within it! Actually, that's a bit hard on comics editors &ndash; I've very rarely been treated poorly by anyone I've worked for, but they're very blas� about the talent around &ndash; there's so much of it! Incidentally, Simon F was a great editor to work for &ndash; clear about what he wanted but happy to leave the detail to the artist to come up with and probably just relieved that we turned in the work on time! </p>
            <p><strong><span class="head2">11.</span> Can we expect more Transformers work from you?  </strong></p>
            <p>I simply don't know� I just did a piece for a convention booklet cover &ndash; Auto Assembly 2006. Not sure if that counts. </p>
            <p><strong><span class="head2">12. </span> What was it like working on the Doctor Who webcasts? What do you think of the new series? </strong></p>
            <p>Again (see answer 10), a pleasure. James Goss &ndash; who handled all three I was involved with (both editorially as well as animating more or less the whole thing) could not have been a better person to work for; he was very good at encouraging and giving direction when it was required without seeming to! Very smart and supportive and deserves to go far. He also taught me how to embrace cheating &ndash; results are everything!</p>

	<p>The new series is exactly what it has to be &ndash; aimed at a young audience and their families who live in a post-Neighbours/Buffy/Eastenders 'soapy' TV world which relies heavily on personal relationships for its kicks. My generation were (I think) much more interested in incident (didn't matter who the companion was &ndash; what's the story?), but the language of television drama has changed massively since even the last days of Sylvester's time.</p>

	<p>It could have been brought back in a number of ways that would have appealed more to me personally, but I'm not who it's aimed at; it would perhaps have vanished very quickly if I'd had my choice of direction. Instead, it's a successful series with its target audience and has a good chance of being restored to its rightful place as a permanent TV fixture. And of course, I'd work on it like a shot! I have just been commissioned to work on a possible new publication featuring the Doctor. </p>
            <p><strong><span class="head2">13.</span> Have you a favourite Doctor or story? </strong></p>
            <p>Currently Hartnell; I love the early stuff more and more &ndash; they attempt so much with so little, like the Web Planet. I'm old enough to remember its first broadcast &ndash; it was spooky then and it still is for me. </p>
            <p><strong><span class="head2">14.</span> As some fans will know, you're a keen saxophonist. What if you had to choose between music and art? </strong></p>
            <p>Because of the sheer novelty that music presents for me currently, I'd choose that. But there's very little chance I'd ever earn a living at it &ndash; music's even tougher than comics in that respect!</p>

	<p>Although the two disciplines are 'creative', the type of creativity is completely different. Working in graphic arts &ndash; I'm an artisan rather than an Artist &ndash; is a craft, and although I have some ability, it's never particularly easy. I have to grind out every picture and it's rare that I'd look back at a page and think it was completely what I had in mind, let alone successful. The way I play music is a combination of memorising and extemporisation (riffing). I can't read music and have no knowledge of what any of the notes or keys are called &ndash; and so I'm constantly surprised what comes out. But when it flows, there's nothing like the buzz you get from soloing.</p>
	
	<p>Thinking about it, I think the only strips I ever did which came closest to riffing were the Time Wars ones &ndash; it was done at great speed and with a brush (I usually use pens). Don't know whether that makes them better than subsequent work, but it was fun!</p>
	
	<p>There's a greater immediacy with the audience with music too; you play for and feed off them in real time. Sometimes it's been years before I meet people who've seen my work, often never with more obscure stuff, so essentially you work for your own reaction and that of your editor. </p>
            <p><strong><span class="head2">15.</span> Recommend a Roxy Music album for beginners. </strong></p>
            <p>'For Your Pleasure' &ndash; the second Roxy Music album. It's a great mix of their earlier Art Rock origins and hints at the sophistication to come. The track 'Grey Lagoons' has one of the finest rock sax solos ever committed to posterity. The album tends to get into the top 100 lists to this day.  </p>
            <p><strong><span class="head2">16.</span> Word Association:</strong></p>
            <p><strong>Galvatron</strong><br>
              'Heeeere's Johnny!' </p>
            <p><strong>William Shatner</strong><br>
              A god who left messages on my answering machine. </p>
            <p><strong>Thunderbirds</strong><br>
              Each of the episodes are as good as most films. </p>
            <p><strong>Davros</strong><br>
              A cop-out. I'm a Yarvelling and Zolfian man. </p>
            <p><strong>Time Wars</strong><br>
              Ace-tasting-motivating-loose-ends-tying-up-eyeball-sucking-out-riffing heaven.</p>
            <p><strong>Thank you very much for your time!</strong><br>
              <br>
            </p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr valign="top"> 
                <td class="subcolor" align="center"> <p><img src="images/sullivan1.jpg" width="210" height="300" border="0"><br>
                    Marvel UK #134</p>
                  </td>
                <td class="subcolor" align="center"><p><img src="images/sullivan2.jpg" width="210" height="300" border="0"><br>
                    1989 UK annual</p></td>
                <td class="subcolor" align="center"> <p><img src="images/sullivan3.jpg" width="210" height="300" border="0"><br>
                    Marvel UK #157</p>
                  </td>
              </tr>
              <tr> 
                <td valign="top" class="subcolor" align="center"> <p><img src="images/sullivan4.jpg" width="210" height="300" border="0"><br>
                    Marvel UK #200</p>
                  </td>
                <td valign="top" class="subcolor" align="center"> <p><img src="images/sullivan5.jpg" width="210" height="300" border="0"><br>
                    Armada UK #6</p>
                  </td>
                <td valign="top" class="subcolor" align="center"> <p><img src="images/sullivan6.jpg" width="210" height="300" border="0"><br>
                    Lee in performance</p>
                  </td>
              </tr>
              <tr> 
                <td colspan="3" class="greybar"><br>Home Page: <a href="http://www.leesullivan.co.uk/" target="_blank">http://www.leesullivan.co.uk/</a><br>&nbsp;
                </td>
              </tr>
            </table>
            <p>&nbsp;<br>
            </p>
            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
