<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Bob Budiansky</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Bob Budiansky<br>
              </span><span class="blackstrong">interviewed in:</span> September 2006</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/budiansky.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
		It's likely none of us would be here if it hadn't been for Bob Budiansky. Responsible for so many Transformers names and profiles from the outset, he had an influential hand in the Marvel US comics over the huge span of its first 55 issues. His influence lasts to this day, as evident in the back-to-basics approach of the recent Infiltration series, and he's back by public demand, adapting the original Transformers movie in a new comic for IDW.<br>&nbsp;<div style="font-size:8pt;">Photo by Emma Budiansky</div>
                </td>
              </tr>
            </table> 
       
	    
            <p><br><strong><span class="head2">1.</span></strong><strong> How did you end up as editor for the Marvel Transformers comic and then subsequently the writer? </strong></p>
	    
            <p>In November 1983, Jim Shooter, Marvel's Editor in Chief, was working with Hasbro to develop the Transformers into a property that could be adapted into a comic book series and cartoon show. Jim wrote the original Transformers story treatment, and then he enlisted the help of veteran comic book writer and Marvel editor Denny O'Neil to flesh out the treatment with character names and profiles. For whatever reason, Hasbro asked for a lot of revisions on Denny's work, and Denny declined to do them. So Jim had to find another writer to revise the character profiles. In a hurry. After a few writer/editors turned Jim down, I guess he finally arrived at my name. At the time, I was the editor of several monthly Marvel books, like Fantastic Four and Daredevil. But my reputation back then was more as an artist/editor than writer/editor, so I wasn't an obvious choice to revise the Transformers material. Anyway, I did the revisions over a weekend, and Hasbro loved the new names and profiles I came up with. So when Marvel decided to do a 4-issue Transformers mini-series, I was the obvious choice to be its editor, at least obvious to Jim. I went through three writers in those four issues. Nobody could get a handle on how to write this book. By the end, I was almost dictating the plot to the writer. When Jim decided to make this a regular series, he assigned it to a different editor since my workload was too great at the time to accommodate another monthly book. So when the new editor asked me for my advice about the book, I suggested he fire the writer I was using since I was telling him what to write anyway and that's what I would do if I were to continue editing the book, and if he wanted to he could hire me to replace him. Now that might sound like a naked power grab on my part, but the writer was thankful to be fired and put out of his misery. (And 22 years later, he and I are still friends.) So that's how I became the writer. </p>

            <p><strong><span class="head2">2.</span> What do you think were your strengths when writing for Transformers? Were there any parts of the concept that you felt were a particular challenge to put across in the stories?  </strong></p>
	    
            <p>I'd like to say I could do it all, but I suppose I most liked the fact that I was able to play up the contrasts between this group of giant, alien robots and humans. At least in the earlier part of my run, I was able to bring some interesting characterisations to some of the Transformers. I was able to humanise at least some of the Transformers so the readers could identify with them and care about them. I also expanded the mythology of the Transformers in various ways. And I was able to put together a story with a beginning, middle and end in most of the issues I wrote. I tried to give the reader his money's worth.</p>

	    <p>As far as what challenged me about the concept, well� I felt that the whole set-up of these two warring groups of alien robots crash-landing on Earth, remaining dormant for millions of years, then waking up to continue their battle while simultaneously feeling the need to blend in by adapting various Earth-like forms to be extremely contrived. I tried not to dwell on the origin too much because it would just bring more attention to its weaknesses. But then I had to repeatedly go back to the original concept every time I had to introduce a new group of Transformers who also had to find an excuse to assume Earth-like form. That was a challenge! Read the Headmasters mini-series again to see all the hoops I had to jump through plot-wise to explain that concept! On second thought, maybe you shouldn't read it. The Transformers premise was a trap I could never completely escape. Also, just having to introduce waves of new characters every few issues was a challenge. It made it difficult to develop characters and give the book any momentum.  </p>
	      
            <p><strong><span class="head2">3.</span> How much - if any - involvement did you have with the original animated series, the movie, and Marvel UK? </strong></p>
	      
            <p>Zero, none and zilch. Over a period of about five years, I provided names, character profiles and story treatment additions to Hasbro as new toy lines were introduced. What the animated series, the movie and Marvel UK did with them afterwards was none of my concern.  </p>

            <p><strong><span class="head2">4.</span> The Return to Cybertron saga is largely considered by fans as a major highlight, but you rarely set your stories on the planet afterwards. Was there any reason for this?  </strong></p>
	    
            <p>The thing that attracted me most about the Transformers concept was the contrast between them and humans. These giant robots were truly strangers in a strange land. Most of my stories were concerned with playing that up in different ways. So if I wrote more stories that were Cybertron-based, I wouldn't be able to do what I enjoyed writing about the most in the series. I would have been writing stories about a bunch of alien robots fighting each other on some distant world, and that was never my intent, nor did I want the book to be about that. If the book had gone in that direction after Return to Cybertron, I'm sure I would have quit it a lot sooner.   </p>
	      
            <p><strong><span class="head2">5.</span> Were you ever disappointed with the art that served your scripts? </strong></p>
	    
            <p>As an artist, I was aware of how difficult it was to draw from licensed material. It really limits your freedom. Everything has to look like the style guides. And drawing robots made it worse, since it's really difficult to draw those guys so they look alive. So although there were times when I would have liked the art to be more dynamic and attractive, usually I was happy just to be working with an artist who could effectively tell the story I wanted to tell. And that was often the case; I worked with several artists who truly collaborated with me to give me what they thought I wanted.  </p>

            <p><strong><span class="head2">6.</span> Which characters were your favourites to write for?  </strong></p>
	    
            <p>Blaster. Megatron. Ratchet. I'm sure there were others, but it's been a while.  </p>
	    
            <p><strong><span class="head2">7.</span> After the movie came out, was it ever considered replacing the present-day cast of the comic with their future counterparts?  </strong></p>
	    
            <p>I think the topic briefly came up in a conversation with Hasbro, but after I expressed my interest in continuing the story on present-day Earth, things continued accordingly.  </p>
	      
            <p><strong><span class="head2">8.</span> What do you remember of working on the Headmasters comic? Whose idea was it to write a mini-series about that particular group of characters, and do you know if mini-series were considered for other Transformers sub-lines?  </strong></p>
	    
            <p>Transformers was a best-seller, so when Hasbro presented Marvel with the Headmasters concept, it seemed a natural to launch it as a mini-series to take advantage of the demand for Transformers comics. It's possible I brought the idea to Jim Shooter, but he could have just as easily suggested the idea to me. Once that was green-lighted, I wrote the Headmasters treatment that explained their origin, and then did a 4-issue plot synopsis. I'm pretty sure there were discussions to do a Pretenders mini-series after that (or it might have been some other late 1980's sub-line), but by that time sales were slipping, so that idea was shelved.  </p>
	    
            <p><strong><span class="head2">9.</span> Did you have any creative input into the Transformers and G.I.Joe crossover that killed off Bumblebee and introduced Goldbug? </strong></p>
	    
            <p>I was supposed to write it, but then backed out because I was too busy. I reviewed the plots to make sure everything Transformers-related made sense. I might have contributed an idea or two, or asked for an occasional minor change, but I really don't remember. I don't remember much about the Bumblebee/Goldbug storyline either, but I'm sure I was okay with it at the time. </p>

            <p><strong><span class="head2">10.</span> When the time came to leave the comic, how did you go about choosing your successor?  </strong></p>
	    
            <p>First, understand that it's not the writer's job to choose his successor, it's the editor's. But not in this case. At the end of my run on the book, I had been begging the Transformers editor, Don Daley, for months to let me leave the book, and he in turn had been begging me to stay since he couldn't think of anyone who could replace me. In February 1989, I went to England for a vacation. So one day Simon Furman (who I had previously met on one or two occasions on his visits to the New York Marvel office) and I met for lunch in London, and I told him how I was ready to leave the book. Simon wasted no time in telling me how he would love to replace me. So when I went back to New York, I told Don about the deal Simon and I worked out over sandwiches and beer, and the torch was officially passed. Or something like that. Ask Simon�he might remember the menu differently. </p>
	    
            <p><strong><span class="head2">11.</span> Your work on Transformers has been more celebrated amongst fandom in recent years than it was, say, in the 1990s. Why do you think that is?  </strong></p>
	    
            <p>I didn't even know Transformers fandom existed in the 1990s, let alone cared one way or the other about stories I had written a decade earlier. So you probably have more insight into what fandom does or doesn't think about my work than I do. I'll say this much about it�when I first Googled my name about five or six years ago and found my name on Transformers web-sites, I was amazed to discover that I could arouse so much emotion among adults about stories I had written for 10-year olds fifteen years earlier. </p>
	    
            <p><strong><span class="head2">12.</span> Have you read any recent Transformers comics, and if so what are your thoughts?   </strong></p>
	    
            <p>No. (Okay, IDW sent me a current comic a few months ago, which I did read. Had no idea what was going on.) </p>
	    
            <p><strong><span class="head2">13.</span> You've started your first project at IDW. How would you compare the creative process to your days back at Marvel?</strong></p>
	    
            <p>Actually, I've already finished my first project at IDW. In both cases, IDW and Marvel, I've had great freedom to do what I want. And I've had my freedom severely restricted. Of course, my work with IDW is adapting a movie, so although Chris Ryall has been extremely receptive to everything I've turned in, my freedom is limited by what's already in the movie. With Marvel, since I was writing original stories, Hasbro had to approve everything. Generally, my stories got through that process unscathed. But I was under orders at various times to introduce new lines of characters to coincide with the release of new toys. That proved to be a severe limitation to what I could write and how much I could develop certain characters. </p>
	    
            <p><strong><span class="head2">14.</span> What were your initial impressions when you saw Transformers: The Movie?   </strong></p>
	    
            <p>Well, I saw the movie for the first time only a few months ago, when I started working on the adaptation, so my memories of it are still fairly fresh. Hmm� chaotic story, flat dialogue, some very silly scenes, crude animation. Those were my initial impressions.  </p>
	    
            <p><strong><span class="head2">15.</span> What will the comic adaptation offer to readers that is new?    </strong></p>
	    
            <p>I structured the four issues to play up the dramatic events in the story as powerfully as I could. I did what I could, through captions and a few pieces of added dialog, to knit the scenes together so the story comes across more coherently. I added a brief scene at the end of issue 2, showing Hot Rod, Kup and the Dinobots crash-landing on Quintesson. But these are all relatively minor things; I pretty much stuck to the movie as it exists. The biggest plus of this adaptation is the beautiful art provided by Don Figueroa. Oh, and at Don's suggestion, we added the missing Dinobot. I can't remember which one that is (but Don knows.) </p>
	    
            <p><strong><span class="head2">16.</span> Can you give us any hint about what fans can look forward to from you after the movie adaptation?  </strong></p>
	    
            <p>No, because I don't know myself. Perhaps IDW and I will get together for another Transformers project, maybe something I would create from scratch. I'm certainly open to doing more work for IDW.   </p>	    
	    
            <p><strong><span class="head2">17.</span> What does the Transformers brand mean to you personally?  </strong></p>
	    
            <p>Transformers has been very good to me. That's an understatement. It gave me the opportunity to hone my skills as a writer, to spill my imagination all over the page for 50 issues, to play in a universe that to a large degree was of my own making. It was a wonderful opportunity. Apparently, it has also provided me with a small degree of celebrity that might have otherwise escaped me. I can say that since you're interviewing me about the Transformers, 17 years after I left it. (Okay, technically I'm back since I just did that job for IDW.) It's nice to know that my work has transcended the times for which it was created. Also, I made more than a few dollars thanks to the Transformers. Can't forget that.  </p>

            <p><strong><span class="head2">18. </span> And finally, some word association: <br><br>

	    Shockwave  </strong></p>

            <p>Good name for a bad guy. </p>
	    
            <p><strong>Spider-Man Clone Saga  </strong></p>

	    <p>Ugh!</p>

	    <p><strong>Don Figueroa  </strong></p>

	    <p>Transformers artist without peer. And a real nice guy.  </p>
	    
	    <p><strong>Circuit Breaker  </strong></p>

	    <p>The Comics Code approved way of saying "ballbreaker". </p>
	    
	    <p><strong>2007 movies </strong></p>

	    <p>I'll wait until I read the reviews.</p>
	    
	    <p><strong>Thank you very much for your time!</strong></p>

   
	    
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><img src="images/budiansky1.jpg" style="border:2px white solid;"><br>Marvel US #5 by Mark Bright</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><img src="images/budiansky2.jpg" style="border:2px white solid;"><br>Marvel US #47 by Bob Budiansky</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><img src="images/budiansky3.jpg" style="border:2px white solid;"><br>Animated Movie #1 by Don Figueroa</td>
	      </tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
