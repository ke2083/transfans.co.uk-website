<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: James Raiz</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              James Raiz<br>
              </span><span class="blackstrong">interviewed in:</span> October 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/raiz.jpg" width="90" height="125" class="blueborder"></td>
                <td width="86%" valign="top"><p>James Raiz has been delighting us with his Transformers art ever since Dreamwave bagged the licence in 2002, drawing the first 5 issues of Armada. His Transformers work since has included MTMTE, G1 and Energon, and he's added a list of credits for Marvel, DC and Top Cow to his already impressive CV. James has recently been announced as the artist for the third incarnation of everyone's favourite freelance peacekeeping agent, Death's Head. </p>
                </td>
              </tr>
            </table> 

              <p>&nbsp;</p>

<p><strong>Hi James, many thanks for agreeing to do this interview. </strong></p>

<p><strong><span class="head2">1.</span> Many artists who've worked recently on Transformers were fans of the franchise before coming to professional work involving it. Were you? </strong></p>

<p>Absolutely!  I absolutely loved the show and watched it everyday when I was a kid.  Unfortunately my parents didn&#8217;t really buy me many Transformers, I always got the cheaper GoBot toys.  But hey, if IDW does Ultimate GoBots, sign me up!!</p>

<p>But the few Transformers I actually owned, I drew them again and again.</p>

<p><strong><span class="head2">2.</span> So, drawing Transformers - how did you get the gig? </strong></p>

<p>I&#8217;d been working at Dreamwave for a couple of years as the primary background artist. When Pat got the opportunity to do a Transformers image for Wizard magazine, we all wondered how cool it would be to actually do the project.  Unfortunately, they wouldn&#8217;t give me any full pencil work because my style at the time was a pure American style.  I received offers from other companies to do full pencil work and background work.  </p>

<p>All of a sudden, I was with Pat in the elevator, and he asked me &#8211; &#8220;Do you want to pencil Transformers?&#8221; Now at this time, we didn&#8217;t have the license yet.  We were in full pitch mode.  Gi-Joe had just come out from Devil&#8217;s Due and was a complete smash.  I was talking to my good friend Chris Sarracini, and we talked about how much we would like to help out.  So he wrote a three page script for RID.  I drew it, and it was included as a part of the pitch that got us the license.  That solidified the Armada gig for us.</p>

<p><strong><span class="head2">3.</span> What was the experience like illustrating Armada? Were there any specific content restrictions on you since it ran parallel to a toyline aimed at younger audiences? </strong></p>

<p>Everything was fine until we got to the third issue.  I think there was a little misunderstanding when it came to the &#8220;style&#8221; issue.  When I did issue 3, I drew the children in a very American style.  Hasbro&#8217;s main concern was that they didn&#8217;t look enough like the cartoon.  So I tried drawing it in a quazi-anime style (not a very good one) and sent it to them.  Unfortunately for me &#8211; they loved it&#8230;</p>

<p>What I should have done was kept the American style and tried to alter it so it was similar to the cartoon and still maintains my style.  Completely my fault.  But Hasbro has been nothing but supportive of my efforts.</p>

<p>When it came to the colouring however, the colourists asked me to stop using so much black, because it limited them in choosing light sources. I listened to them... sometimes&#8230;</p>

<p><strong><span class="head2">4.</span> You certainly had a grittier style to many of your Dreamwave colleagues. Who were your influences? What other things would you say have influenced your visual style, be it art, cinema, etc? </strong></p>

<p>I&#8217;m a detail freak!  I love details.  I love to draw every hose and bolt there possibly could be.  I used a lot of influences when drawing Mecha such as Geof Darrow, Travis Charest and Stephen Platt.</p>

<p><strong><span class="head2">5.</span> One of your pieces of art for Dreamwave was the original Transformers Mega Litho. How did you set about the task - did you do it in sections and then put them all together as Don Figueroa did, or draw one giant piece?</strong></p>

<p>Ah the litho! In all honesty, that litho was meant to be a cover.  I came to Dreamwave with the idea to put as many Transformers as possible on a cover to break George Perez&#8217;s record for most characters on the cover.</p>

<p>I succeeded in making an image with over 600 characters &#8211; but it was never made into a cover!!!  Maybe a good thing &#8211; would have been hard to read as a cover anyways.  I believe it was Derek Choo-Wing&#8217;s idea to make them pin-ups that connect to one giant litho.</p>

<p>My approach to it was that I completely laid out all the important characters, and every month I would do two pieces each on top of my Armada work.  It makes me respect George Perez even more, because he finished penciling his cover in three weeks!!</p>

<p><strong><span class="head2">6.</span> Frustratingly for some of your fans, after your initial stint on Armada your work at Dreamwave became less regular. Was there any reason for this? </strong></p>

<p>Blame Optimus Prime.  Let me explain&#8230;</p>

<p>Myself, Adam Fortier, Pat, Roger and Chris Sarracini went to Midtown comics in New York for a signing.  There we talked about getting Armada back on schedule.  We decided it was best to have Pat do a couple of fill-ins and I would jump straight into issue 8.  But Chris and I talked about the possibilities of an Optimus Prime mini-series.  He came up with an amazing idea that would have let me go design crazy!!  So we came to Adam and Pat and told them that we wanted to do this series instead.  So they decided to give Armada to Guido, and Chris and I started preparing for our Prime series.</p>

<p>A week or so later, they talked to us and instead of a Prime series, they wanted us to do more of a G2 mini instead.  Not that we had anything against G2, but Chris and I were really stoked about Prime.  Then they threw us the ultimate curve ball &#8211; they offered us the G1 ongoing series.  We were like &#8211; sure!</p>

<p>But then things didn&#8217;t fall our way.  Pat Lee was supposed to draw Teenage Mutant Ninja Turtles, but for some reason that fell through. So they decided to do a G1 Volume 2 with Pat, and pushed the Ongoing until the next year.  They told me that they would keep me busy with stuff until then.</p>

<p>A few days later, I was then told that I would no longer be drawing the first arc but rather the second &#8211; starting from issue 6.  So at that time, I wouldn&#8217;t be drawing Transformers for another year!</p>

<p>I couldn&#8217;t do that.  I was getting married, and I needed money.  So I emailed my friend Warren Ellis, and he got me a gig at Wildstorm/DC comics doing Tokyo Storm Warning, and I left Dreamwave.</p>

<p>I came back for a little bit in 2004, and they wanted me to do the next War Within with Simon Furman, which would have been a blast.  But at the time, the company&#8217;s financial difficulties were so apparent, I couldn&#8217;t risk committing myself to a project that long.  Turned out to be a great move on my part &#8211; I&#8217;m one of the very few who came out of Dreamwave with all my money.</p>

<p><strong><span class="head2">7.</span> What work are you proudest of during your period with the company? </strong></p>

<p>I&#8217;ll say the Litho, just because of the amount of work we put into it.  It looks great on my wall. <img src="phpBB2/images/smiles/smile.gif" alt="Smile" border="0" /></p>

<p><strong><span class="head2">8.</span> You were recently announced as artist on the new Death's Head project. Have you read any of the previous two takes on Death's Head? What are your impressions of the character (or characters, since Death's Head II is a composite entity)? </strong></p>

<p>In all honesty, I haven&#8217;t read the first Death&#8217;s Head.  I was a huge fan of the second.  When I first started reading comics, one of the first books I got was an issue of Death&#8217;s Head II.  So when the opportunity opened for Death&#8217;s Head, I jumped at the chance. I was completely surprised when my editor told me that Simon was writing it too.  </p>

<p><strong><span class="head2">9.</span> What stylistic approach will you take with the new Death's Head series? </strong></p>

<p>Heavy Detail.  That&#8217;s what I&#8217;m all about.  My style is pretty much more realistic now.  I&#8217;m very comfortable with it.  Hopefully everyone will enjoy it!</p>

<p><strong><span class="head2">10.</span> Tell us about your other work outside Transformers. </strong></p>

<p>I worked with DC for a bit, doing a couple of issues of Wonder Woman. I finished up a run of Hulk:Destruction &#8211; my first work for Marvel.  I did issues 2-4 doing the flashback scenes.  I completed a City of Heroes back-up story for Top Cow &#8211; it should be out in issue #6&#8230; I also just completed a Captain Universe / Invisible Woman one shot that will be released in November.  Then, Death&#8217;s Head in December!!</p>

<p><strong><span class="head2">11.</span> What else is on the horizon for you? Anything else for IDW?</strong></p>

<p>Right now, I&#8217;m doing a bunch of covers for them, and would love to do more in the future.  We&#8217;ll see how the wind blows in 2006. <img src="phpBB2/images/smiles/smile.gif" alt="Smile" border="0" /></p>

<p><strong><span class="head2">12.</span> What are your hobbies and interests outside comics? </strong></p>

<p>I love to sing!  I&#8217;m the master at Karaoke!  I also organised and emceed a bunch of events as well.  I&#8217;ve put together concerts that have had attendances of 2500+.  I love the stress. <img src="phpBB2/images/smiles/smile.gif" alt="Smile" border="0" /></p>

<p>I&#8217;m also a religious guy &#8211; I teach Sunday School to children a few times a month.</p>

<p>Other than that &#8211; I love Wrestling, Pride Fighting and comics of course.</p>

<p>And my 9 month old daughter Emma &#8211; she already loves comics &#8211; God help us all&#8230;</p>

<p><strong><span class="head2">13.</span> And to conclude, some word association: </strong></p>

<p><strong>Chris Sarracini </strong></p>

<p>Great Friend, very underrated writer.</p>

<p><strong>Sunstorm </strong></p>

<p>Him giving Jetfire a DDT and the Stone Cold Stunner in issue 3. Don Figueroa&#8217;s the man!!</p>

<p><strong>Beast Machines </strong></p>

<p>Beasties!!!!  I&#8217;ll draw &#8216;em someday. <img src="phpBB2/images/smiles/smile.gif" alt="Smile" border="0" /></p>

<p><strong>Cheese </strong></p>

<p>Great with bacon.  My heart doesn&#8217;t like it as much though&#8230;</p>

<p><strong>Pat Lee</strong></p>

<p>Great singer, fun to hang out with, bad businessman.</p>

<p><strong>Mr Raiz, thank you very much for your time! And a reminder to our readers that they can see more of your art gracing the pages of Amazing Fantasy #16 in December and on one of the covers for Transformers: Infiltration #0, shipping this month.</strong></p>

<p>&nbsp;</p>

            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor" style="padding-right:0;"><a href="images/raizextra4b.jpg"><img src="images/raizextra4a.jpg" width="197" height="264"></a> 
                  <p align="center">unused cover art<br>
                    colours by Josh Burcham</p></td>
                <td width="26%" class="subcolor" style="padding-right:0;"><a href="images/raizextra3b.jpg"><img src="images/raizextra3a.jpg" width="197" height="264"></a> 
                  <p align="center">TF: Genesis art<br>
                    inks by Pierre-Andre Dery</p></td>
                <td width="48%" class="subcolor"><img src="images/raizextra2.jpg" width="197" height="264"> 
                  <p align="center">The new Death's Head<br>
                    Amazing Fantasy #16 out in Dec</p></td>
              </tr>
            </table>

&nbsp;

            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td width="26%" class="subcolor" style="padding-right:0;"><a href="images/raizextra5b.jpg"><img src="images/raizextra5a.jpg" width="320" height="197"></a> 
                  <p align="center">that litho, for those just joining us</p></td>
                <td width="26%" class="subcolor"><a href="images/raizextra6b.jpg"><img src="images/raizextra6a.jpg" width="257" height="197"></a> 
                  <p align="center">in closer detail</p></td>
              </tr>
            </table>

<p><a href="http://www.idwpublishing.com/" target="_blank">
                    IDW Publishing</a>: check out IDW's site here for current Transformers information. <a href="http://www.dk.com" target="_blank"><br>
                    <a href="http://www.amazon.com" target="_blank">Amazon.com</a> 
                    and <a href="http://www.amazon.co.uk" target="_blank">Amazon.co.uk</a>: 
                    find TPBs featuring James' existing Transformers work here.</p>


   <p>&nbsp;</p>

	</td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
