<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Staz Johnson</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Stewart "Staz" Johnson<br>
              </span><span class="blackstrong">interviewed in:</span> July 2007</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
	      <td width="14%" valign="middle"><img src="images/staz_thumbnails/staz_1.jpg" width="100" height="124" class="blueborder"></td>
                <td width="86%" valign="top">
                Stewart "Staz" Johnson was a late addition to the legendary Transformers UK art team. Joining not longer after the controversial switch to black and white strips, his debut was in issue 236's Matrix Quest prequel 'Deathbringer Part 2'. Staz went on to be one of the comic's most prolific interior artists, his work extending to a string of memorable covers that did much to forge the visual identity of Transformers UK in its latter years. Afterwards he had much success with The Big Two, drawing some of the most recognised comic characters in the world, but he's still found time to reminisce with us about his days with the robots in disguise...
                </td>
              </tr>
            </table> 
       
	     <p><br><strong>Thanks again for agreeing to do this interview. </strong></p>

    
            <p><strong><span class="head2">1.</span></strong><strong> Had you always wanted to draw comics? Who and what were your early inspirations?  </strong></p>
	    
<p>I don't think it's fair to say that I always wanted to draw comics. Sure, I always enjoyed drawing and I did quite like the idea of doing it for a living, but making a living from comics didn't occur to me until quite late. Prior to that, I had harboured ambitions to be the guy who painted the Iron Maiden album sleeves... not literally him, of course, but the idea of doing album cover art definately appealed. That said, a lot of comic artists were definately an influence on me: Neal Adams, Carmine Infantino, Gil Kane, and most especially John Buscema, who was and still is my major influence. </p>
 
            <p><strong><span class="head2">2.</span> What was your first art/illustration assignment?  </strong></p>
	    
<p>My first professional job was on a role playing games magazine called Adventurer. I drew a three page Cthulhu-esque strip which appeared every issue (the first few episodes were written by a young Warren Ellis). At the time I was sure it was something which would go largely unnoticed, and although I'm sure it did, I'm amazed at how many times I've been asked to sign copies of those issues at various conventions. Prior to that, I had done a couple of years working for free in comics fanzines. </p>

            <p><strong><span class="head2">3.</span>  How did you get work at Marvel UK? 
 </strong></p>
	      
            <p>At the time I was working for a couple of other RPG magazines... the names escape me now, but I was determined to get work in 2000AD (which in the 1980's was the mecca for young British comic artists) so I periodically would send copies of my published work along with specially prepared samples to the 2000AD editoral department. However, just when I felt my work was of a sufficiently high enough standard to warrant landing that kind of work, I was told by someone at 2000AD that my style was 'too American looking'. I realised that constantly banging on that door was a hiding to nothing, so I took the self same samples which had just been rejected by 2000AD and sent them to Marvel UK... specifically the magazine Action Force. I had picked up an issue and realised that here was a British comic which was using superhero or 'super-soldier' comic art. Fortunately the editor Steve White saw fit to take me on.  </p>

            <p><strong><span class="head2">4.</span> What were your first impressions of the Transformers?  
 </strong></p>
	    
<p>I knew nothing about Transformers prior to working on the comic. I remember one time I had gone down to the Marvel UK offices to deliver a job and Steve White was asking if I'd thought about doing something for Transformers and he showed me some originals by who he told me was in his opinion the best Transformers artist, Lee Sullivan. I remember being impressed by the quality of the originals, but the idea of drawing robots didn't interest me in the slightest - I wanted to draw superhero comics and Action Force was the next best thing. It was only when Action Force folded that I decided to give it a shot. </p>

            <p><strong><span class="head2">5.</span> Did drawing Transformers pose any particular challenges? What are the fundamental differences between drawing robots and, say, superheroes?  
 </strong></p>
	    
<p>The main problem I remember with doing Transformers (and this was obviously due to the fact that I had never read a Transformers comic and had no cognisance of the basic concepts) was that I didn't realise that they were supposed to be huge! I had assumed that they were the same size as a man, so in the first strip I did, that was how I drew them... hiding in doorways etc. It wasn't until I submitted the pencils that the editor pointed out to me that Optimus Prime was like 30 feet tall and would never be able to hide in a shop doorway, that I started to get a handle on them.</p> 

<p>The other main challenge for me was that I had always been a figure artist... all the mechanical stuff was a struggle for me. But the differences between doing Transformers and figure/superhero stuff are really very superficial. Which is to say, you don't get to draw all the cool muscles and stuff, but below the surface gloss and final drawing the discipline is the same when telling the story and conveying the action. </p>

            <p><strong><span class="head2">6.</span> Were there any characters that you enjoyed working on?  </strong></p>
	    
<p>I liked drawing Galvatron and I had something of a soft spot for Prowl and Jazz. Plus there was one who was like a samurai warrior with a skull face... he was fun to do. </p>

            <p><strong><span class="head2">7.</span>  Do you have any memories of writer Simon Furman or any of the artists of that time?  
 </strong></p>
	    
            <p>I didn't know and never met Simon, or any of my fellow artists. Back then I never attended comic conventions and I was based in the north, only very rarely making trips into London to deliver jobs. But I did finally meet Simon at a Bristol convention a couple of years ago.   </p>
	      
            <p><strong><span class="head2">8.</span> A couple of the UK artists moved on to work on the American version of Transformers. Do you know if you were ever in the running?   </strong></p>
	    
            <p>I have no idea, but I would seriously doubt it. I think my Transformers art was always something of a square peg in a round hole. I don't think I was a natural Transformers artist like Geoff Senior or Andy Wildman.  </p>

	    
            <p><strong><span class="head2">9.</span> What do you remember of the comic's cancellation? 
 </strong></p>
	    
            <p>I'm sure that when I got wind of the comic's imminent demise, my focus would have been on finding work elsewhere, rather than taking an interest in the future of Transformers... </p>

            <p><strong><span class="head2">10.</span> The Nineties saw high profile gigs at Marvel and DC for you. Spider-man, Thor, Avengers, Robin and Catwoman all got the Staz treatment. A dream come true? 
 </strong></p>
	    
            <p>Yes it was, of course. I always dreamed of working on comics like that, but in all honesty, I never considered myself to be good enough. So the fact that I have spent the last 15 years or so doing exactly that is still amazing to me. I'm just waiting for the day when they all realise I'm a bit **** really!  </p>
	    
            <p><strong><span class="head2">11.</span> Will you be checking out the new Transformers movie?  
  </strong></p>
	    
            <p>I will, very much so. I have become much more fond of the idea of Transformers since I stopped working on the comic. I even bought a couple of the toys a few years ago.  </p>
	    
            <p><strong><span class="head2">12.</span> Could you ever be tempted back to draw Transformers again, either for Marvel, IDW, or Panini? 
 
 </strong></p>
	    
            <p>Absolutely, given availability. </p>
	     <p><strong><span class="head2">13.</span> Where can Transformers fans find your work right now?  </strong></p>
            <p>Recently I've been doing various bits and pieces for Marvel and I've been working on the online strips on the NBC website which accompany the 'Heroes' TV show. </p>

            <p><strong><span class="head2">13.</span>And finally, we wanted to take you down memory lane and run some of your classic Transformers covers by you... </strong></p>


	    <p><strong>Your first cover</strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1639"><img src="images/staz_thumbnails/ukcov271.jpg" height=266 width=200></a></p>

            <p>Honestly, I don't remember this at all. But it is a good example of how poor my drawing of all the machinery stuff was... look at that shockingly bad background! </p>
	    
	    <p><strong>Galvatron</strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1666"><img src="images/staz_thumbnails/ukcov298.jpg" height=266 width=200></a></p>

	    <p>This was the second version of this cover. I was pleased with the first version, but the editor felt that Galvatron looked too much like he was just rising slowly from the water and he wanted him to look like he was exploding out of the waves so I did this second version. He was right, this one is much better. </p>

	    <p><strong>Issue 300's anniversary cover </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1668"><img src="images/staz_thumbnails/ukcov300.jpg" height=200 width=298></a></p>

	    <p>Not much to say, shocking background again. There's nothing to suggest that this is taking place on a mechanical world. If anything, it looks like a desert. </p>
	    
	    	    <p><strong>Optimus Prime deep in thought </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1674"><img src="images/staz_thumbnails/ukcov306.jpg" height=266 width=200></a></p>

	    <p>When I had drawn this, the thought occured to me that Prime looked like Bruce Forsyth, doing his little pose at the begining of the Generation Game. So (in pencil) I added a little speech balloon saying 'Nice to see you... to see you ..NICE... Good game, good game'. The editor must have thought it was pretty funny, cause he mentioned it in the comic.  </p>
	    
	    	    <p><strong>Scorponok's death </strong></p><p><a href="http://TransFans.co.uk/popup.php?id=1689"><img src="images/staz_thumbnails/ukcov321.jpg" height=266 width=200></a></p>

	    <p>I enjoyed doing these painted covers, there were three I think. It was around this time that I had started to formulate some ideas about how Transformers should be drawn (kinda late I know), and I was experimenting with trying to make them look like they really could turn into trucks or whatever. Most of the Transformers I drew in the strips were done in a fairly organic style, basically I was drawing people and adding bits of cars to them. I realised that I had been doing it the wrong way around... I should have been starting with the vehicle. So in little ways (like Prime's hands) I was really trying to push the machine side to the fore. </p>
	    	    
	    
	    <p><strong>Staz, thanks you very much for your time!</strong></p>
	    <p>You're very welcome.</p>
	    
	      
	      <br><table width="100" border="0" cellpadding="0" cellspacing="0">
              <tr valign="top"> 
	      <td class="subcolor" style="padding:6px;" align="center"><a href="images/staz_2.jpg"><img src="images/staz_thumbnails/staz_2.jpg" height=200 width=260 style="border:2px white solid;"></a><br>Stewart "Staz" Johnson</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/staz_3.jpg"><img src="images/staz_thumbnails/staz_3.jpg" height=300 width=200 style="border:2px white solid;"></a><br>Staz Johnson</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/Heroes_novel_038-5.jpg"><img src="images/staz_thumbnails/Heroes_novel_038-5.jpg" height=300 width=200 style="border:2px white solid;"></a><br>'Heroes' Graphic Novel Excerpt</td>
	      <td class="subcolor" style="padding:6px;padding-left:0;" align="center"><a href="images/Heroes_novel_038-3.jpg"><img src="images/staz_thumbnails/Heroes_novel_038-3.jpg" height=300 width=200 style="border:2px white solid;"></a><br>'Heroes' Graphic Novel Excerpt (2)</td>
	      </tr>
	      </table>

&nbsp;
	    
          </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
