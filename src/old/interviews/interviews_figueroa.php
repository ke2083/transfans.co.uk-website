<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>TransFans.co.uk - Interviews: Don Figueroa</title>

<link rel="stylesheet" href="css/transfans.css" type="text/css">
</head>
<body><div style="padding:1px 0 1px 20px; background-color:#146AC0;"><img style="float:right;" src="z_symbols.gif"><a href="/"><img src="z_logo.gif"></a></div><div style="padding:4px 0 4px 20px; background-color:#105498; color: white; font: 10pt Arial;">&nbsp;
</div><div style="padding:4px; background-color:#0D4277; font: 10pt Arial; margin-bottom: 15px; ">&nbsp;</div>
<table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
  <tr> 
    <td height="10"></td>
  </tr>
  <tr valign="top"> 
    <td> <table width="100%" border="0" cellpadding="0" cellspacing="0" class="fulltables">
        <tr> 
          <td width="19">&nbsp;</td>
          <td class="greybar" width="48">&nbsp;</td>
          <td width="761" valign="top" class="txt"> <p><span class="bluehead"><br>
              Don Figueroa<br>
              </span><span class="blackstrong">interviewed in:</span> March 2005</p>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td width="14%" valign="middle"><img src="images/figueroa.jpg" width="90" height="124" class="blueborder"></td>
                <td width="86%" valign="top">Don Figueroa originally came to the 
                  attention of Transformers fans with his awesome online comic 
                  Macromasters. Unsurprisingly Don was recruited by Dreamwave 
                  when they got the Transformers licence and, after drawing several 
                  posters, had his first official Transformers work published 
                  in <a href="http://www.TransFans.co.uk/comics_guide_detail.php?id=327" target="_blank">The 
                  War Within</a>, where he got the opportunity to show us how 
                  the Transformers looked before they left Cybertron. Don arguably 
                  went on to become Dreamwave's most popular TF artist and was 
                  working on both the Generation One and the (unpublished) Beast 
                  Wars title when Dreamwave folded. Don now works for Devil's 
                  Due publishing. </td>
              </tr>
            </table> 
            <p>&nbsp;</p>
            <p><strong>Hi Don,</strong></p>
            <p><strong><span class="head2">1.</span></strong><strong> Polls frequently 
              list you as the most popular of the current Transformers artists... 
              by a wide margin at that&#8230; but which other artists, both on 
              Transformers and other titles, do you rate?</strong></p>
            <p>I don't know if I'm the &quot;most&quot; popular, but I do appreciate 
              the accolades of my fellow Transfans. As for the other TF artists 
              out there, I'm a big fan of James Raiz, Joe Ng, and Alex Milne. 
              I just like different styles when it comes to TFs. Outside of TFs, 
              I'm also a big fan of Jim Lee, Whilce Portacio, Alex Ross, Joe Quesada 
              and Frank Miller among others. </p>
            <p><strong><span class="head2">2.</span> How would you compare recent 
              art styles on Transformers to the work done during the Marvel run? 
              </strong></p>
            <p>I don't think there were any comics about 'Giant Robots' here in 
              the US then, though I could be wrong. So most artists then were 
              more used to drawing humans and probably influenced a lot by &quot;Metallic&quot; 
              characters like Iron Man, Sentinels or other 'Humans in armour' 
              as it was translated a lot in their TF drawings.</p>
            <p>Today's Transformers artists (like myself) have more references 
              on how robots and mechs are drawn today and how they would work. 
              We also understand that, besides androids, robots and mechs really 
              dont have to be the same proportions as a human, especially when 
              the robots have car parts as part of their anatomy. </p>
            <p><strong><span class="head2">3.</span> What are the top five pieces 
              you are most proud of from your time at Dreamwave? </strong></p>
            <p>The 20th Anniversary Litho: It's my biggest piece yet, though I 
              never did get to put everybody in there. If I had more time, I would 
              have included the G2 'bots there, Unicron and other cartoon/comic 
              characters like Primus, Scrounge, Deceptitran... etc.</p>
            <p>The 'War Within' designs: I'm just glad that most of the fans liked 
              it , I was worried it would be deemed as sacrilege when I drastically 
              changed their looks.</p>
            <p>Armada Unicron Box-art: I'm very surprised that it was deemed worthy 
              enough to be blown up to a 3 stories high image and be the centrepiece 
              in the Time's Square Toys R Us revolving billboard. Especially when 
              the Black-outs occurred, it got stuck there for hours.</p>
            <p>Beast Wars: It was probably my best work there yet, but unfortunately...</p>
            <p>Armada Starscream Box-art: My first Professional TF work for Dreamwave 
            </p>
            <p><strong><span class="head2">4.</span> Are there any particular 
              characters you love or hate to draw? What would you say is the easiest 
              TF to draw? The most difficult? </strong></p>
            <p>After the 'Sunstorm Saga' I got kinda tired of drawing Seekers 
              for a while. The easiest to draw is Megatron, a simple, clean, yet 
              elegant design. </p>
            <p><strong><span class="head2">5.</span> Much was made of the &quot;house 
              style&quot; when it came to Dreamwave's art. How much stylistic 
              freedom did you have? Was it an issue when you decided not to draw 
              Optimus Prime with the giant robot-boobs that Pat Lee envisioned?</strong></p>
            <p>Actually, there was this one time when Rob Ruffulo became Art director 
              for Dreamwave, he insisted I draw Prime and Starscream (and probably 
              all of them) more like how Pat draws them. He even sent his sketches 
              but I declined. As much as I like Pat's work, I'd rather develop 
              my own style. </p>
            <p><strong><span class="head2">6.</span> With work on G1, War Within, 
              Armada and Beast Wars, you've been the most adaptable of recent 
              artists. How does working on the differing continuities compare?</strong></p>
            <p>G1 and War Within are probably the easiest for me, G1 only because 
              I get to work with 'bots who are easily recognisable and I took 
              some liberties when drawing them, and the War Within because I can 
              go nuts with it, since there's no existing references available.</p>
            <p>Armada and Beast Wars were a bit more challenging because I tend 
              to use the toys as reference and those toys are very highly detailed. 
              Miss anything and it'll look ridiculous. </p>
            <p><strong><span class="head2">7.</span> How did the different writers 
              compare to work with? </strong></p>
            <p> Simon's very meticulous with his scripts, he's got very precise 
              descriptions of what happens in every panel, even on the basic shapes 
              of things from buildings to the way a finger is bent. His dialogue 
              is very deep as well, almost speech-like.</p>
            <p>James and Adam are very precise as well and are very heavy with 
              the panels. Usually having a minimum of 6 per page - that's a lot 
              of work but they give me more freedom when it comes to certain scenes, 
              like fights and such. They will even direct me to certain movies 
              that I can refer to, to make things easier.</p>
            <p>All three have been great teachers for me when it comes to storytelling 
              and it's been an honour to have worked with them. </p>
            <p><strong><span class="head2">8.</span> Did the folding of Dreamwave 
              come as a shock, or was it something you saw coming? How was the 
              news broken to you? How did you take the eventual news?</strong></p>
            <p>I kinda sensed something was wrong when they started getting really 
              behind with the checks, but I love what I do, and I've been doing 
              comics for free way before Dreamwave so I didn't really mind, and 
              a check would sometimes show up once in a while so it never really 
              bothered me much.</p>
            <p>I was also assured everything was cool when I saw Pat in Oklahoma 
              a month prior to the closing. We even talked about really exciting 
              stuff planned for the next year and beyond, I guess this is one 
              of those &quot;too good to be true&quot; moments because less than 
              a month later, poof! everything's gone... and I had to hear the 
              bad news from Espen. Nobody bothered to tell me to stop working, 
              if I had known, I probably wouldn't have sent in the Beast Wars 
              pages, and now some of them are gone, probably looted from the office. 
            </p>
            <p><strong><span class="head2">9.</span> Some fans found the nature 
              of Dreamwave's press releases to be somewhat self-aggrandising. 
              Were you restricted at all in terms of what you could and couldn't 
              say in interviews when working for Dreamwave?</strong></p>
            <p>The interviews I did when I worked for Dreamwave were sent to them 
              first and its probably edited before it was sent to me, so in a 
              way, yeah! There was some restriction to my interviews and of course 
              I couldn't talk about storylines or whatever, but that's a given. 
            </p>
            <p><strong><span class="head2">10.</span> Looking back, from your 
              standpoint, what do you feel Dreamwave did well, and what could 
              they have handled better? </strong></p>
            <p>TF wise, I think they did a pretty good job. I can't really comment 
              on how the non TF titles did or handled. </p>
            <p><strong><span class="head2">11.</span> What terms are you on with 
              the Lee brothers at the moment? </strong></p>
            <p>Non-existent. </p>
            <p><strong><span class="head2">12. </span> Would you work with them 
              again? </strong></p>
            <p>Probably not.</p>
            <p><strong><span class="head2">13.</span> It&#8217;s no secret that 
              Andrew Wildman&#8217;s pencils were altered for War Within: The 
              Dark Ages. Some have gone so far as to call it &#8220;butchery.&#8221; 
              Did anything you drew wind up altered to a similar extent?</strong></p>
            <p>I always tend to draw my humans in a more western style, so when 
              I did that Armada Free comic book, they told me that my humans needed 
              to be more &quot;anime&quot; so they altered the faces to conform 
              with the house style. </p>
            <p><strong><span class="head2">14.</span> How did the Devil's Due 
              job come about? </strong></p>
            <p>I've done a couple of Voltron Covers for DDP before, so when Dreamwave 
              committed seppuku, I contacted Josh [Blaylock] and mentioned my 
              free agent status. I said that if he ever needs me, just flash a 
              'D' in the sky and I'm there :)</p>
            <p><strong><span class="head2">15.</span> How do the two jobs compare 
              and can you tell us about any of the work you've got lined up there? 
              </strong></p>
            <p>I enjoyed working for Dreamwave for the most part - I love drawing 
              comics and they gave me a chance at it. DDP is, as of the moment, 
              still trying to secure the TF licence but they are keeping me fairly 
              busy with other stuff, character designs, poster art, some TF stuff 
              for Hasbro... etc. </p>
            <p><strong><span class="head2">16.</span> Obviously you found a niche 
              drawing Transformers. Do you expect any difficulty adapting to non-Transformers 
              work? </strong></p>
            <p>Nah, not really. As long as I have some good reference I can adapt 
              pretty quickly. </p>
            <p><strong><span class="head2">17.</span> This has been floating around 
              the net for over a year now and you have a chance to put this rumour 
              to bed: Did you help with the design for Masterpiece Convoy/20th 
              Anniversary Optimus Prime? If so, in what capacity?</strong></p>
            <p>I helped a bit. The head was based on my drawings as well as the 
              details all over his body. The engineering's all Hasbro/Takara though. 
            </p>
            <p><strong><span class="head2">18.</span> Word Association:</strong></p>
            <p>Fire away...</p>
            <p><strong>Galvatron:</strong></p>
            <p>Movie Galvatron: Badass.</p>
            <p>Post Movie Galvatron: Screams too much.</p>
            <p><strong>Simon Furman:</strong></p>
            <p>A great mind. One of the nicest people I had the pleasure of meeting.</p>
            <p><strong>Optimus Prime:</strong></p>
            <p>Very good role-model, could have used some killer-instinct though.</p>
            <p><strong>Pat Lee:</strong></p>
            <p>Probably the richest guy I know.</p>
            <p><strong>Dreamwave:</strong></p>
            <p>Took the easy way out.</p>
            <p><strong>Chris Sarracini:</strong></p>
            <p>Never met him.</p>
            <p><strong>Macromasters:</strong></p>
            <p>Probably the work I'm gonna be best known for.</p>
            <p><strong>Thanks again. Not just for agreeing to this interview, 
              but also for all of the hard work you&#8217;ve done for the TF community. 
              Everyone here at TransFans.co.uk wishes you the best at your new career 
              at Devil&#8217;s Due.</strong></p>
            <p>Thanks a bunch, it's been fun.</p>
            <p><br>
            </p>
            <table width="100" border="0" cellpadding="5" cellspacing="0">
              <tr> 
                <td class="subcolor"><img src="images/figueroa1.jpg" width="197" height="297"> 
                  <p align="center">Devils Due TF Piece</p></td>
                <td class="subcolor"><p><img src="images/figueroa2.jpg" width="197" height="297"></p>
                  <p align="center"> Night of the Combaticons</p></td>
              </tr>
              <tr> 
                <td colspan="2" class="greybar"><p>Devils Due Publishing: <a href="http://devilsdue.net/index.html" target="_blank">http://devilsdue.net/index.html 
                    </a></p></td>
              </tr>
            </table>
            <p><br>
            </p>
            </td>
          <td class="media" width="193" valign="top">&nbsp; </td>
        </tr>
      </table></td>
  </tr>
  <tr> 
    <td height="10"></td>
  </tr>
</table>

</body>
</html>
