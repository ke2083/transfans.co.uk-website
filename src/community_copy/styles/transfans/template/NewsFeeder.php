<?php

/**
* NewsFeeder
*
* This class will return a JSON type with the most current news
* from the board.
*
* This includes:
*  * the newest user
*  * the most popular topic, in terms of replies, over the last 7 days
*  * the most popular topic, in terms of views, over the last 7 days
*
**/

class NewsFeeder{

$table_prefix = 'phpbb_';

  $connection = null;

  private function ConnectToDatabase(){
    $connection = new mysqli($dbhost, $dbuser, $dbpasswd, $dbname);
  }

  private function CloseConnection(){
    $connection->close();
    $connection = null;
  }

  /**
  * Returns a JSON object as a string representing the current board news.
  **/
  public function Read(){
    ConnectToDatabase();
    return '{ "NewestUser": "Karl", "MostReplies": "Topic ABC", "MostViews": "Topic XYZ"  }';
    CloseConnection();
  }

}

include('../../../config.php');

header('Content-type: application/json');
$newsFeed = new NewsFeeder();
print $newsFeed->Read();

?>
