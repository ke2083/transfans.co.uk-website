<?php if (!defined('IN_PHPBB')) exit; ?></div>
<footer class="navbar navbar-bottom">
  <div class="navbar-inner">
		<ul class="nav">
			<?php if ($this->_rootref['U_TEAM']) {  ?><li><a href="<?php echo (isset($this->_rootref['U_TEAM'])) ? $this->_rootref['U_TEAM'] : ''; ?>"><i class="icon-user"></i> <?php echo ((isset($this->_rootref['L_THE_TEAM'])) ? $this->_rootref['L_THE_TEAM'] : ((isset($user->lang['THE_TEAM'])) ? $user->lang['THE_TEAM'] : '{ THE_TEAM }')); ?></a></li><?php } if (! $this->_rootref['S_IS_BOT']) {  ?><li><a href="<?php echo (isset($this->_rootref['U_DELETE_COOKIES'])) ? $this->_rootref['U_DELETE_COOKIES'] : ''; ?>"><i class="icon-trash"></i> <?php echo ((isset($this->_rootref['L_DELETE_COOKIES'])) ? $this->_rootref['L_DELETE_COOKIES'] : ((isset($user->lang['DELETE_COOKIES'])) ? $user->lang['DELETE_COOKIES'] : '{ DELETE_COOKIES }')); ?></a></li> <?php } ?><li class="divider-vertical"></li>
            <li><a href="#"><i class="icon-time"></i> <?php echo (isset($this->_rootref['S_TIMEZONE'])) ? $this->_rootref['S_TIMEZONE'] : ''; ?></a></li>
		</ul>
  </div>
</footer>
</div>
	<a id="bottom" accesskey="z"></a>
	<?php if (! $this->_rootref['S_IS_BOT']) {  echo (isset($this->_rootref['RUN_CRON_TASK'])) ? $this->_rootref['RUN_CRON_TASK'] : ''; } ?>

</div>
<script type="text/javascript" src="styles/transfans/theme/js/jquery-1.9.1.min.js"></script>
<script type="text/javascript" src="styles/transfans/theme/js/jquery-ui-1.10.3.custom.min.js"></script>
<script type="text/javascript" src="styles/transfans/theme/js/jquery.cookie.js"></script>
<script type="text/javascript" src="styles/transfans/theme/js/modernizr.js"></script>
<script type="text/javascript" src="styles/transfans/theme/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="styles/transfans/theme/js/site.js"></script>

</body>
</html>