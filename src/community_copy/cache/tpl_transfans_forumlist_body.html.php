<?php if (!defined('IN_PHPBB')) exit; if ($this->_rootref['PAGINATION'] == $this->_rootref['FALSE']) {  ?>

<div class="well well-small">
<fieldset>
<?php $_forumrow_count = (isset($this->_tpldata['forumrow'])) ? sizeof($this->_tpldata['forumrow']) : 0;if ($_forumrow_count) {for ($_forumrow_i = 0; $_forumrow_i < $_forumrow_count; ++$_forumrow_i){$_forumrow_val = &$this->_tpldata['forumrow'][$_forumrow_i]; if ($_forumrow_val['S_IS_CAT']) {  ?>

            </fieldset>
            <fieldset>
			<legend><?php echo $_forumrow_val['FORUM_NAME']; ?></legend>
	<?php } else { ?>

		<article>
        <div class="row-fluid">
            
		        <aside class="span9">
       
                <dt>
                    <a href="<?php echo $_forumrow_val['U_VIEWFORUM']; ?>">
                    <?php if ($_forumrow_val['S_UNREAD_FORUM']) {  ?>

                    <i class="icon-asterisk"></i>
                    <?php } else { ?>

                    <i class="icon-minus"></i>
                    <?php } ?>

                    <?php echo $_forumrow_val['FORUM_NAME']; ?></a></dt>
                <dd>
                <?php echo $_forumrow_val['FORUM_DESC']; ?>

                </dd>
                </dl>
             
                
                
                </aside>
            <div class="span3">
        	        <?php if ($_forumrow_val['LAST_POST_TIME']) {  ?>

			
                    <a href="<?php echo $_forumrow_val['U_LAST_POST']; ?>" class="btn btn-info btn-mini">
                        <i class="icon-circle-arrow-right icon-white"></i> 
                    most recent
                    </a>
		
                <?php } ?>

                <span class="label label-inverse btn-medium"><i class="icon-th-list icon-white"></i> <?php echo $_forumrow_val['TOPICS']; ?> topics</span>
    			        <span class="label"><i class="icon-list icon-white"></i> <?php echo $_forumrow_val['POSTS']; ?> replies</span>
                        <?php if ($_forumrow_val['MODERATORS']) {  ?>

    		    <p><?php echo $_forumrow_val['MODERATORS']; ?></p>
			                <?php } if ($_forumrow_val['SUBFORUMS'] && $_forumrow_val['S_LIST_SUBFORUMS']) {  ?>

			                <p><strong><?php echo $_forumrow_val['L_SUBFORUM_STR']; ?></strong> <?php echo $_forumrow_val['SUBFORUMS']; ?></p>
			                <?php } ?>

            </div>
		</div>
	    </article>
        <hr>
	<?php } }} ?>

</fieldset>
</div>
<?php } ?>