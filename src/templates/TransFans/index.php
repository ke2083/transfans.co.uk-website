<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <jdoc:include type="head" />
        <link rel="stylesheet" href="/community/styles/transfans/theme/bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="/community/styles/transfans/theme/bootstrap/css/bootstrap-responsive.min.css" type="text/css" />
        <link rel="stylesheet" href="/community/styles/transfans/theme/jquery-ui.css" type="text/css" />
        <link rel="stylesheet" href="/community/styles/transfans/theme/stylesheet.css" type="text/css" />
        <link rel="shortcut icon" href="/community/favicon.ico" />
        <meta name="viewport" content="width=device-width">
    </head>
    <body>
      <div class="container-fluid">
        <header>
            <jdoc:include type="modules" name="mastHead" /> 
            <a id="top" accesskey="t"></a>
            <div class="navbar">
              <div class="navbar-inner">
                  <a href="<?php echo $this->baseurl ?>" class="brand">
                            <img src="<?php echo $this->baseurl ?>/community/styles/transfans/theme/images/logo.png" alt="Transfans" />
                        </a>
                <nav>
                  <jdoc:include type="modules" name="topNav" /> 
                </nav>
                <jdoc:include type="modules" name="utilityBar" /> 
              </div>
            </div>
            <div class="row-fluid">
                <div class="span4">&nbsp;</div>
                <div class="span8 text-right">
                    <jdoc:include type="modules" name="breadcrumb" /> 
                </div>
            </div>
        </header>
        <div class="content mainSite">
            <jdoc:include type="modules" name="heroSpace" /> 
            <div class="row-fluid">
                <aside class="span3">
                    <jdoc:include type="modules" name="localNav" /> 
                </aside>
                <div class="span9">
                    <article>
                        <jdoc:include type="component" />
                    </article>
                    <div>
                        <jdoc:include type="modules" name="articleFooter" /> 
                    </div>
                </div>
            </div>
        </div>
    </div>
        <footer>
            <jdoc:include type="modules" name="footer" /> 
        </footer>
        
        <script type="text/javascript" src="/community/styles/transfans/theme/js/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="/community/styles/transfans/theme/js/jquery-ui-1.10.3.custom.min.js"></script>
        <script type="text/javascript" src="/community/styles/transfans/theme/js/jquery.cookie.js"></script>
        <script type="text/javascript" src="/community/styles/transfans/theme/js/modernizr.js"></script>
        <script type="text/javascript" src="/community/styles/transfans/theme/js/site.js"></script>
    </body>
</html>