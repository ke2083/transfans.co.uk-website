04:35:00 Legion &bull; wibble 
06:25:53 Aaron Hong &bull; wobble 
06:26:16 Aaron Hong &bull; or should it be 'wob wob wob'? 
14:07:21 Rebis &bull; now is a good time. 
14:07:36 Rebis &bull; although, I'm not sure how soon it is... 
14:15:44 Predabot &bull; Guess maybe it's not such a good time then. 
15:51:53 &lt;3Starscream &bull; Dang it.  no one here? 
16:05:36 &lt;3Starscream &bull; I bought a Armada Starscream last week on ebay and haven't recieved it yet. 
16:05:36 &lt;3Starscream &bull;  It's making me angry.  Was 46 bucks worth it? 
17:33:26 rusty_herring &bull; I've never seen the Armada Starscream. ..or now that I think of it, any of the
17:33:26 rusty_herring &bull;  Armada toys 
17:34:21 &lt;3Starscream &bull; Wow.. 
17:34:31 rusty_herring &bull; oh hey, someone's in here lol 
17:34:58 &lt;3Starscream &bull; Yeah..I tend to not exit and just go away :\ 
17:35:22 rusty_herring &bull; that works, I've always been hoping I could catch someone in here 
17:35:53 &lt;3Starscream &bull; I just noticed the thing about 2 hours ago 
17:37:05 rusty_herring &bull; I was almost about to go home from work, got about 15 minutes left whoooo 
17:37:25 rusty_herring &bull; It's a long weekend here, so everyone else in my office took off, leaving me to
17:37:25 rusty_herring &bull;  man the phones 
17:38:01 &lt;3Starscream &bull; Oh that sucks.  I have a nice easy job.  I get off early all the time 
17:41:26 rusty_herring &bull; oh nice. What kind of work do you do? 
17:41:44 &lt;3Starscream &bull; I make pottery. 
17:44:36 rusty_herring &bull; I guess that wouldn't exactly by a 'by the clock' kind of job 
17:45:00 &lt;3Starscream &bull; Heh it actually is.  
17:45:15 &lt;3Starscream &bull; 4 hours minimum :D 
17:46:39 rusty_herring &bull; Do you like it? 
17:47:08 &lt;3Starscream &bull; Not like I used to.  It's outside so it's very hot. 
17:47:32 rusty_herring &bull; I guess it would have to be.. might be a little toxic indoors eh? 
17:48:32 &lt;3Starscream &bull; Just a bit.  My boss is a freak about that kinda stuff..so we're left to suffer
17:48:32 &lt;3Starscream &bull;  in the Texas heat. 
17:49:32 rusty_herring &bull; ha ha 
17:56:48 rusty_herring &bull; well, sadly I'm going to go and head home. I'll probably pop on here later this
17:56:48 rusty_herring &bull;  evening 
17:57:19 &lt;3Starscream &bull; Maybe everyone else will too.  See ya 
17:58:37 rusty_herring &bull; yup see ya in a couple hours if you're still here 
19:56:55 rusty_herring &bull; hello? 
19:58:22 &lt;3Starscream &bull; Lookie there, people! I'm being distracted by Cybertron.. 
19:58:37 rusty_herring &bull; the show or the planet :P 
19:59:02 &lt;3Starscream &bull; Not the planet yet. :] And yes the show 
20:00:48 rusty_herring &bull; So you first got hooked on TF in the Armada days? (Just from your board avatar) 
20:01:24 &lt;3Starscream &bull; Yeah, got hooked on Armada, hated Energon, hooked on Cybertron and going
20:01:24 &lt;3Starscream &bull;  backwards from there 
20:04:58 rusty_herring &bull; I regret not seeing a lot of Armada 
20:05:19 &lt;3Starscream &bull; It's my favorite series.  A lot of people didn't like it though 
20:05:49 rusty_herring &bull; I liked the fact it was all 2D animation. To be honest I'm not such a huge fan of
20:05:49 rusty_herring &bull;  the newer 3D stuff, but Cybertron was pretty interesting 
20:06:31 &lt;3Starscream &bull; Yeah, Energon was their first try and I thought it was horrible.  Cybertron was
20:06:31 &lt;3Starscream &bull;  a little better, but the lips..wow 
20:09:07 rusty_herring &bull; yeah you're right it's the lips lol. Energon was just kind of weird... everyone
20:09:07 rusty_herring &bull;  'linking' up left right and centre 
20:09:48 &lt;3Starscream &bull; I only got like a 1/4 way through it before I gave up because the animation was
20:09:48 &lt;3Starscream &bull;  too much for me. 
20:12:36 &lt;3Starscream &bull; aw it's over. :( 
20:13:01 rusty_herring &bull; I love the intro music for Cybertron, it's so funky 
20:13:18 rusty_herring &bull; hey, I see a Hot Shot 
20:13:25 &lt;3Starscream &bull; Me too 
20:13:37 Hot Shot &bull;  Wazzap? 
20:14:09 rusty_herring &bull; haven't seen you posting much lately, granted I've been away 
20:15:19 Hot Shot &bull;  I've been shopping on Ebay lately. I still get a post or two in every day, though. 
20:15:32 rusty_herring &bull; I wanted to ask, is your Hot Shot char taken from Armada or Cybertron? (I think
20:15:32 rusty_herring &bull;  the Energon guy was kind of the same) 
20:15:38 &lt;3Starscream &bull; I'm out of Cybertrons....now I don't know what to do to amuse myself until my
20:15:38 &lt;3Starscream &bull;  parents get home -.- 
20:17:10 Hot Shot &bull; He's out of Energon. He was my first TF ever. Oh, what I'd give to relive an isle full
20:17:10 Hot Shot &bull;  of Energon and Alts... 
20:20:17 &lt;3Starscream &bull; I don't even remember what he looked like in Energon..hmm 
20:21:08 rusty_herring &bull; hmmmm how to describe it 
20:21:16 rusty_herring &bull; I have a couple energon DvDs 
20:21:35 &lt;3Starscream &bull; Oh wait, I found a pic of the toy 
20:21:36 rusty_herring &bull; my first two TF toys ever were Optimus Prime and Skywarp 
20:21:56 &lt;3Starscream &bull; Mine was Cybertron Starscream 
20:22:21 Hot Shot &bull; [url=http://seibertron.com/database/character.php?char_id=2780] Does this help?[/url] 
20:23:00 &lt;3Starscream &bull; Yeah 
20:23:32 rusty_herring &bull; aha that's a perfect shot. I have the giant Starscream from Cybertron too. I took
20:23:32 rusty_herring &bull;  him to work at Christmas and it took me and my co-worker 30 minutes to figure
20:23:32 rusty_herring &bull;  out how to transform him 
20:24:16 &lt;3Starscream &bull; Haha.  I took mine to school last year and my friend brought his Energon
20:24:16 &lt;3Starscream &bull;  Starscream, mine was so much better. 
20:25:38 rusty_herring &bull; ha ha the one thing I remember from the Energon show is every episode for at
20:25:38 rusty_herring &bull;  least 10, they always showed Padlock getting blasted and then Wingsaber's trauma 
20:27:03 Hot Shot &bull;  Guys, Karl's watching us... 
20:27:06 &lt;3Starscream &bull; I think I remember..didn't they not know where Megatron was in that series.  My
20:27:06 &lt;3Starscream &bull;  mind is totally blank right now.. 
20:27:19 &lt;3Starscream &bull; He's been there on and off I've noticed. 
20:28:14 Hot Shot &bull; That sounds like RID to me. 
20:29:18 rusty_herring &bull; Energon was where he comes back to life and takes over Unicron? Or something? 
20:29:32 Hot Shot &bull; Correct. 
20:29:40 &lt;3Starscream &bull; Ah ok. 
20:32:31 &lt;3Starscream &bull; There's a mini marathon of Cybertron coming on CN tomorrow I think... 
20:36:38 rusty_herring &bull; how much is mini 
20:36:58 &lt;3Starscream &bull; 4 episodes 
20:38:03 Hot Shot &bull; They're showing "Cybertron", "Showdown", "End", and "Unfinished" if I'm not mistaken.
20:38:03 Hot Shot &bull;  All of which I've seen. :P 
20:38:37 &lt;3Starscream &bull; I don't remember which one I stopped at..boo 
20:40:26 rusty_herring &bull; I was always annoyed because they made Thundercracker some bumbling hick 
20:40:48 &lt;3Starscream &bull; He cracks me up though.  But I know what you mean 
20:41:12 Hot Shot &bull;  Wee, doggies. Hey boss, wait fer me! 
20:41:28 &lt;3Starscream &bull; Haha 
20:46:07 rusty_herring &bull; damn it lol 
20:46:50 rusty_herring &bull; I wish the series would have more girl robots in them 
20:47:45 &lt;3Starscream &bull; Me too. 
20:49:28 Hot Shot &bull;  Agreed. We need Elita-1 and Moonracer! 
20:49:37 rusty_herring &bull; hell yeah 
20:49:48 rusty_herring &bull; I don't know why they don't put them in...? 
20:49:59 &lt;3Starscream &bull; That would make things interesting. 
20:51:16 rusty_herring &bull; it's too bad they had Thunderblast in Cybertron, I wanted to strangle her 
20:51:42 Hot Shot &bull; According to Takara, Fembots don't sell well. Yet little half-dressed loli girls do.  
20:52:24 &lt;3Starscream &bull; I'd buy a fembot if they made one I liked.. 
20:53:28 &lt;3Starscream &bull; But since they dont I will stick to Starscream 
20:54:22 Hot Shot &bull; Funny story, G1 Starscream was a girl in France. 
20:54:49 &lt;3Starscream &bull; Yeah, I heard about that.  Talk about disappointing to me 
20:57:15 rusty_herring &bull; what? you mean like the voice or they actually made the character female in the
20:57:15 rusty_herring &bull;  France version? 
20:57:30 rusty_herring &bull; that's... creepy actually lol very creepy 
20:58:01 &lt;3Starscream &bull; It is 
21:00:47 &lt;3Starscream &bull; Wow original Starscream toy on ebay going to 560 bucks with 4 hours still..i
21:00:47 &lt;3Starscream &bull;  want 
21:01:00 rusty_herring &bull; 560 holy crap 
21:01:10 rusty_herring &bull; I really need to find my originals, lying somewhere in my basement 
21:01:15 Hot Shot &bull; Damn... 
21:01:27 rusty_herring &bull; except my brother bit the rubber point off my Skywarp when he was a baby 
21:02:15 &lt;3Starscream &bull; It's unopened too 
21:04:05 &lt;3Starscream &bull; And the shipping is 40 bucks.  Gross 
21:05:15 Hot Shot &bull;  I've really been wanting Galaxy Force Starscream lately, but every time I find one it
21:05:15 Hot Shot &bull;  sells for about $50. Why, Hasbro? Why didn't you release him here in proper colors?
21:05:15 Hot Shot &bull;  :`( 
21:05:20 rusty_herring &bull; holy crap it's an unopened original wow 
21:05:52 rusty_herring &bull; I've been looking all over the city for alternators that aren't crap robots I
21:05:52 rusty_herring &bull;  don't care about. I've never ever seen a Wheeljack, and my best friend got a
21:05:52 rusty_herring &bull;  Hound and I'm so jealous 
21:06:29 &lt;3Starscream &bull; I wish my Armada Starscream would arrive... 
21:09:01 rusty_herring &bull; so how come you like Starscream? *curious* I like him a lot too 
21:10:39 &lt;3Starscream &bull; Because..I have a knack for favoring bad guys in the first place and I think
21:10:39 &lt;3Starscream &bull;  because he's always trying to stab Megs in the back it makes him like...the
21:10:39 &lt;3Starscream &bull;  ultimate XD 
21:11:14 &lt;3Starscream &bull; And it seems to be a trend for girls to just like him so I have to conform 
21:11:40 rusty_herring &bull; ha ha ha! 
21:12:26 Hot Shot &bull; lol!  
21:13:46 &lt;3Starscream &bull; Wait..what's the difference between Galaxy Force SS and Cybertron.  It looks
21:13:46 &lt;3Starscream &bull;  the same > 
21:14:05 rusty_herring &bull; Galaxy Force is the japanese name for Cybertron isn't it? 
21:14:19 &lt;3Starscream &bull; That's what I thought. 
21:14:33 rusty_herring &bull; I guess one is the japanese packaged version? 
21:14:42 rusty_herring &bull; *in other words I don't know* 
21:14:53 &lt;3Starscream &bull; Looks like the only difference 
21:15:39 Hot Shot &bull; GF SS is the cartoon accurate pre-supercharged SS. He's slightly smaller than Megatron
21:15:39 Hot Shot &bull;  and has two blades with a handgun. 
21:16:00 &lt;3Starscream &bull; Ah, i see 
21:16:26 &lt;3Starscream &bull; I did favor that one over the other one..but oh well 
21:17:17 Hot Shot &bull; http://seibertron.com/toys/gallery.php?id=593&size=0&start=0 Here's a Seibertron
21:17:17 Hot Shot &bull;  gallery. 
21:18:42 &lt;3Starscream &bull; Cool..I have a feeling I'll be getting that one too. 
21:20:10 rusty_herring &bull; I hope you have the re-released plastic original that came out? I forget what
21:20:10 rusty_herring &bull;  that series was called 
21:23:16 rusty_herring &bull; they had like Prime, Bee, Grimmlock, Astrotrain, Starscream 
21:23:28 &lt;3Starscream &bull; I only have cybertron, fast action movie and mcdonalds armada.  I actually
21:23:28 &lt;3Starscream &bull;  finally started to make good money so I cant get anymore as of now. 
21:24:06 &lt;3Starscream &bull; and I'm getting my armada, but I get to go eat some good food.  See ya'll later 
21:26:10 Hot Shot &bull; To rusty: Classics? 
21:29:46 rusty_herring &bull; yes! 
21:29:49 rusty_herring &bull; thank you Hot Shot 
21:30:11 rusty_herring &bull; and see you later SS 
21:30:32 rusty_herring &bull; I can't believe I couldn't think of the name, duh 
21:31:31 Hot Shot &bull; I think I'll be going too. Later RH! 
21:31:42 rusty_herring &bull; see you guys later 
21:31:49 rusty_herring &bull; it was nice talking to you all 
