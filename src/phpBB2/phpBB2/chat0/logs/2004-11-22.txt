01:06:59 saysadie - Now, "Pocketwatch" is a sensible word. Straightforward. It's a watch that goes into
01:06:59 saysadie -  your pocket.
01:07:47 saysadie - Compound words are fun!
01:08:36 Aaron Hong - Just beware of the ones with animals in them.
01:08:59 saysadie - Nightlight is kind of funny. Shouldn't it be called 'lightnight'?
01:09:30 saysadie - My pockets have no animals. Just a rubber band, a quarter and some lint.
01:11:05 Aaron Hong - I mean compound words with animals in them. Like goatf--k.
01:40:42 Optimus Prime Rib - Blacklight isnt really black
08:59:47 saysadie - I know what you meant. but taking things in context isn't nearly as fun as taking them
08:59:47 saysadie -  out of context...
11:21:35 Optimus Prime Rib - you know what I meant? could you kindly explain it to me then?
12:35:32 Rebis - Pocketwatch - good.
12:35:40 Rebis - Pocket calculator - good.
12:35:58 Rebis - Pocket dictionary - wtf? How big are their pockets in Oxford?
12:47:32 Optimus Prime Rib - they are compensating for thier small brains
12:54:30 Rebis - ah, and that's obviously what makes them so smart - increased neural density attracting
12:54:30 Rebis -  relativistic properties of thought and whatnot, eh?
12:55:02 Rebis - that's what I'd do if I had a vice, a cranium and a deathwish.
13:07:34 Optimus Prime Rib - I have a vice you can borrow
13:10:51 Rebis - what about a cranium?
13:22:18 Optimus Prime Rib - you have your own sir
13:23:49 Rebis - h, but I do not have a deathwish, nor do I think that performing the operation on myself
13:23:49 Rebis -  during the experimental stage is pertinent for obtaining a government (or other
13:23:49 Rebis -  institution) grant.
13:24:07 Rebis - and what happened to the initial 'A'?
13:35:36 Optimus Prime Rib - initial a?
13:36:15 Optimus Prime Rib - we could use this moron next door to me who thinks that firing his shotgun
13:36:15 Optimus Prime Rib -  repeatedly while my kids are trying to nap is a GREAT idea
13:38:31 Rebis - no, initial 'A', the dropping of makes "Ah" appear as "h".
14:50:43 Optimus Prime Rib - ah... anyway.. would you still like to use my neighbor
16:07:01 Rebis - well, he'd certainly fulfill the cranium and deathwish categories, but, for bonus points
16:07:01 Rebis -  (and to save yours) does he have his own vice?
20:32:28 metalhead24 - Hello!
20:45:41 metalhead24 - Hello?
22:03:34 Leatherneck - hello.
22:11:50 Aaron Hong - Buhbye.
