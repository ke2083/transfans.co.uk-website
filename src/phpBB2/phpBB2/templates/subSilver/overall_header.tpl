<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html dir="{S_CONTENT_DIRECTION}">
<head>
<title>{SITENAME} | {PAGE_TITLE}</title>
<link rel="icon" href="http://www.transfans.net/favicon/logo2.gif">
<link rel="stylesheet" href="templates/subSilver/{T_HEAD_STYLESHEET}" type="text/css" >
<link rel="alternate" type="application/rss+xml" title="Transfans.NET News" href="http://www.transfans.net/cgi-bin/newsfeed.cgi">
<meta http-equiv="Content-Type" content="text/html; charset={S_CONTENT_ENCODING}">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta name="description" content="Transfans is a long-established online community for fans of the Transformers franchise toys, cartoons and comic books- providing news, reviews, interviews and discussion with a vibrant community of thousands of registered users!">
<meta name="keywords" content="transformers,generation,g1,beast,wars,beastmachines,machines,armada,energon,cybertron,comic,cartoon,movie,optimus,prime,primal,megatron,starscream,soundwave,news,community,reviews,interviews,unicron,primus,furman,simon,transforce, transfans">
<meta name="copyright" content="TransFans">
{META}
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_reloadPage(init) {  //reloads the window if Nav4 resized
  if (init==true) with (navigator) {if ((appName=="Netscape")&&(parseInt(appVersion)==4)) {
    document.MM_pgW=innerWidth; document.MM_pgH=innerHeight; onresize=MM_reloadPage; }}
  else if (innerWidth!=document.MM_pgW || innerHeight!=document.MM_pgH) location.reload();
}
MM_reloadPage(true);

//-->
</script>
<!-- BEGIN switch_enable_pm_popup -->

  <script language="Javascript" type="text/javascript">
<!--
	if ( {PRIVATE_MESSAGE_NEW_FLAG} )
	{
		window.open('{U_PRIVATEMSGS_POPUP}', '_phpbbprivmsg', 'HEIGHT=225,resizable=yes,WIDTH=400');;
	}
//-->
</script>
  <!-- END switch_enable_pm_popup -->
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr> 
          <td width="190" height="26" bgcolor="#146AC0">&nbsp;</td>
          <td bgcolor="#146AC0"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr> 
                <td><img src="../../../graphics/sitegraphics/logo.gif" width="170" height="24"></td>
                <td><div align="right"><img src="../../../graphics/sitegraphics/symbols.gif" width="184" height="20"></div></td>
              </tr>
            </table></td>
        </tr>
        <tr class="menurow2"> 
          <td height="26">&nbsp;</td>
          <td><a href="../index.php" class="mainmenu2">home</a> | <a href="../comics.php" class="mainmenu2">comics</a> 
            | <a href="../interviews.php" class="mainmenu2">interviews</a> | <a href="../downloads.php" class="mainmenu2">downloads</a> 
            | <a href="index.php" class="mainmenu2">community</a> | <a href="chat/" class="mainmenu2">live chat ({CHATUSERS} online)</a> | <a href="../links.php" class="mainmenu2">links</a></td>
        </tr>
        <tr class="menurow3"> 
          <td height="26" colspan="2"></td>
        </tr>
        <tr> 
          <td height="26" colspan="2" class="greybar"><img src="../../../graphics/randomimages/site_main.gif" width="740" height="78"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr> 
          <td width="20" >&nbsp;</td>
          <td width="50" class="greybar">&nbsp;</td>
          <td class="txt"><p align="right"><span class="mainmenu"><a href="{U_LOGIN_LOGOUT}" class="mainmenu"><br>
              {L_LOGIN_LOGOUT}</a> | <a href="{U_FAQ}" class="mainmenu">{L_FAQ}</a> 
              | <a href="{U_SEARCH}" class="mainmenu"> 
              {L_SEARCH}</a> | <a href="{U_MEMBERLIST}" class="mainmenu">{L_MEMBERLIST}</a> 
              | <a href="{U_PROFILE}" class="mainmenu">{L_PROFILE}</a> | <a href="{U_PRIVATEMSGS}" class="mainmenu">{PRIVATE_MESSAGE_INFO}</a> 
              | <a href="{U_REGISTER}" class="mainmenu">{L_REGISTER}</a></span> 
            </p></td>
        </tr>
      </table></td>
  </tr>
</table>
<div id="Layer1" style="position:absolute; left:0px; top:0px; width:64px; height:49px; z-index:1"><img src="../../../graphics/randomimages/prime.gif"></div>

  
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20">&nbsp;</td>
        <td width="50" class="greybar">&nbsp;</td>
        <td class="txt">
