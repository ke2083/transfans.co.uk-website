<?php

/* @tatiana5_profileSideSwitcher/event/viewtopic_topic_tools_after.html */
class __TwigTemplate_f23c11f80e6ec7c09d72fe46af14e3792412d6f67dac1bf2b276e4fe5aad620b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (((isset($context["S_USER_LOGGED_IN"]) ? $context["S_USER_LOGGED_IN"] : null) &&  !(isset($context["S_IS_BOT"]) ? $context["S_IS_BOT"] : null))) {
            // line 2
            echo "\t";
            if ((isset($context["S_PSS_LEFT"]) ? $context["S_PSS_LEFT"] : null)) {
                // line 3
                echo "\t\t<li class=\"small-icon icon-pss-right\">
\t\t\t<a href=\"";
                // line 4
                echo (isset($context["PSS_URL_RIGHT"]) ? $context["PSS_URL_RIGHT"] : null);
                echo "\" id=\"profile-switcher\" onclick=\"switchProfiles(); return false;\" data-ajax=\"pss\">";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PSS_RIGHT");
                echo "</a>
\t\t</li>
\t";
            } else {
                // line 7
                echo "\t\t<li class=\"small-icon icon-pss-left\">
\t\t\t<a href=\"";
                // line 8
                echo (isset($context["PSS_URL_LEFT"]) ? $context["PSS_URL_LEFT"] : null);
                echo "\" id=\"profile-switcher\" onclick=\"switchProfiles(); return false;\" data-ajax=\"pss\">";
                echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PSS_LEFT");
                echo "</a>
\t\t</li>
\t";
            }
        }
    }

    public function getTemplateName()
    {
        return "@tatiana5_profileSideSwitcher/event/viewtopic_topic_tools_after.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 8,  35 => 7,  27 => 4,  24 => 3,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@tatiana5_profileSideSwitcher/event/viewtopic_topic_tools_after.html", "");
    }
}
