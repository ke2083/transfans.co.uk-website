<?php

/* @tatiana5_profileSideSwitcher/event/overall_header_head_append.html */
class __TwigTemplate_8946d6307349d54b7d71e6920408b25ff7087bedb2f23170259f794a0255d9ad extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<script type=\"text/javascript\">
// <![CDATA[
\tfunction switchProfiles() {
\t\tvar pss_cont = \$('#page-body');
\t\tvar pss_btn = \$('#profile-switcher');
\t\tvar pss_url = pss_btn.attr('href');
\t\tvar pss_left = pss_cont.hasClass('leftsided');

\t\tphpbb.addAjaxCallback('pss', function(res) {
\t\t\tif (res.success) {
\t\t\t\tif(pss_left) {
\t\t\t\t\tpss_cont.removeClass('leftsided');
\t\t\t\t\tpss_btn.parent('li').attr('class', 'small-icon icon-pss-left');
\t\t\t\t\tpss_btn.html('";
        // line 14
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PSS_LEFT");
        echo "');
\t\t\t\t\tpss_url = pss_url.substring(0, pss_url.length - 1) + '1';
\t\t\t\t} else {
\t\t\t\t\tpss_cont.addClass('leftsided');
\t\t\t\t\tpss_btn.parent('li').attr('class', 'small-icon icon-pss-right');
\t\t\t\t\tpss_btn.html('";
        // line 19
        echo $this->env->getExtension('phpbb\template\twig\extension')->lang("PSS_RIGHT");
        echo "');
\t\t\t\t\tpss_url = pss_url.substring(0, pss_url.length - 1) + '0';
\t\t\t\t}
\t\t\t\tpss_btn.attr('href', pss_url);
\t\t\t}
\t\t});
\t}
// ]]>
</script>

";
        // line 29
        $asset_file = "@tatiana5_profileSideSwitcher/profile_side_switcher.css";
        $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
        if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
            $asset_path = $asset->get_path();            $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
            if (!file_exists($local_file)) {
                $local_file = $this->getEnvironment()->findTemplate($asset_path);
                $asset->set_path($local_file, true);
            }
            $asset->add_assets_version('8');
        }
        $this->getEnvironment()->get_assets_bag()->add_stylesheet($asset);        // line 30
        $asset_file = (("@tatiana5_profileSideSwitcher/../../" . (isset($context["T_PSS_STYLESHEET_LANG_LINK"]) ? $context["T_PSS_STYLESHEET_LANG_LINK"] : null)) . "");
        $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
        if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
            $asset_path = $asset->get_path();            $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
            if (!file_exists($local_file)) {
                $local_file = $this->getEnvironment()->findTemplate($asset_path);
                $asset->set_path($local_file, true);
            }
            $asset->add_assets_version('8');
        }
        $this->getEnvironment()->get_assets_bag()->add_stylesheet($asset);    }

    public function getTemplateName()
    {
        return "@tatiana5_profileSideSwitcher/event/overall_header_head_append.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 30,  55 => 29,  42 => 19,  34 => 14,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@tatiana5_profileSideSwitcher/event/overall_header_head_append.html", "");
    }
}
