<?php

/* @tatiana5_profileSideSwitcher/event/overall_header_content_before.html */
class __TwigTemplate_9a2d6a44afdb258ff7670d5426569627ab3c27c9c47f85283df1f29c768a3cd1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((isset($context["S_PSS_LEFT"]) ? $context["S_PSS_LEFT"] : null)) {
            // line 2
            echo "\t<script type=\"text/javascript\">
\t// <![CDATA[
\t\tvar pss_cont = document.getElementById('page-body');
\t\tpss_cont.className = 'leftsided';
\t// ]]>
\t</script>
";
            // line 8
            $asset_file = "@tatiana5_profileSideSwitcher/profile_side_switcher.js";
            $asset = new \phpbb\template\asset($asset_file, $this->getEnvironment()->get_path_helper(), $this->getEnvironment()->get_filesystem());
            if (substr($asset_file, 0, 2) !== './' && $asset->is_relative()) {
                $asset_path = $asset->get_path();                $local_file = $this->getEnvironment()->get_phpbb_root_path() . $asset_path;
                if (!file_exists($local_file)) {
                    $local_file = $this->getEnvironment()->findTemplate($asset_path);
                    $asset->set_path($local_file, true);
                }
                $asset->add_assets_version('8');
            }
            $this->getEnvironment()->get_assets_bag()->add_script($asset);        }
    }

    public function getTemplateName()
    {
        return "@tatiana5_profileSideSwitcher/event/overall_header_content_before.html";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 8,  21 => 2,  19 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "@tatiana5_profileSideSwitcher/event/overall_header_content_before.html", "");
    }
}
