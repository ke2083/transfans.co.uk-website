(function plugin($) {
    function siteBehaviour() {
        var d = this;
        var o = 0;
        var n = 12;
        d.initialise = function() {
            d.hideSections(".more");
            $(".viewMore").click(e);
            i();
            if (q()) {}
        };
        d.changeFontSize = function(u) {
            $("body").attr("style", "font-size: " + u + "px")
        };

        function f() {
            if ($.cookie("contrast") && o !== $.cookie("contrast")) {
                var v = $.cookie("contrast");
                if (v > o) {
                    d.increaseContrast(v)
                } else {
                    d.decreaseContrast(v)
                }
                o = v
            }
            if ($.cookie("font") && n !== $.cookie("font")) {
                var u = $.cookie("font");
                d.changeFontSize(u);
                n = u
            }
        }

        function t(u) {
            $.cookie("contrast", u, {
                expires: 999,
                path: "/"
            })
        }

        function s(u) {
            $.cookie("font", u, {
                expires: 999,
                path: "/"
            })
        }

        function m() {
            $("#FontControl").slider({
                min: 6,
                value: n,
                max: 24,
                step: 2,
                slide: function(w, v) {
                    var u = v.value;
                    d.changeFontSize(u);
                    n = u;
                    s(u)
                }
            })
        }

        function k() {
            $("#ContrastControl").slider({
                min: -40,
                value: o,
                max: 40,
                step: 10,
                slide: function(w, v) {
                    var u = v.value;
                    if (u < o) {
                        d.decreaseContrast(u)
                    } else {
                        d.increaseContrast(u)
                    }
                    o = u;
                    t(u)
                }
            })
        }

        function q() {
            return $(".Prime").is(":visible")
        }
        d.hideSections = function(u, v) {
            if (!u.jquery) {
                u = $(u)
            }
            u.slideUp(0);
            if (typeof v === "function") {
                v()
            }
        };

        function e(v) {
            v.preventDefault();
            var w = $(v.target);
            var u = w.prev();
            if (u && u.is(":visible")) {
                u.slideUp(250);
                w.html('<i class="icon-chevron-down"></i> More')
            } else {
                if (u && u.is(":hidden")) {
                    u.slideDown(150);
                    w.html('<i class="icon-chevron-up"></i> Less')
                } else {
                    return
                }
            }
        }

        function i() {
            $.get(window.location.protocol + "//" + window.location.host + "/version.html", function(w) {
                var u = $("<div />");
                u.attr("id", "version");
                u.html('<i class="icon-wrench"></i> ' + w);
                u.addClass("alert alert-success");
                var x = $("<a />");
                x.attr("href", "#");
                x.html("&times;");
                x.addClass("close");
                x.click(function(v) {
                    v.preventDefault();
                    $("#version").slideUp(150)
                });
                u.append(x);
                $("body").append(u)
            })
        }

        function b(u, v) {
            if ($(u.target).is(":checked")) {
                $.cookie("dock-nav", "true", {
                    expires: 999,
                    path: "/"
                });
                g(true)
            } else {
                $.cookie("dock-nav", "false", {
                    expires: 999,
                    path: "/"
                });
                g(false)
            }
            if (typeof v === "function") {
                v()
            }
        }

        function p(z) {
            var w = $("<span />");
            w.addClass("pull-right");
            var x = $("<input />");
            x.attr("type", "checkbox").attr("id", "hideNav");
            x.click(b);
            var u = $('<label for="hideNav" />');
            u.addClass("docker checkbox");
            var v = $("<i />");
            v.addClass("icon-eject");
            v.attr("title", "Toggle whether this menu scrolls with the screen or docks at the top");
            var y = $('<small class="hide">Dock</small>&nbsp;');
            u.append(v).append(y);
            u.append(x);
            w.append(u);
            $("div.navbar:not(.navbar-bottom) div.navbar-inner").append(w);
            if (typeof z === "function") {
                z()
            }
        }

        function j(w) {
            if ($.cookie("dock-nav") === "true") {
                g(true);
                $("#hideNav").attr("checked", "checked")
            } else {
                var v = ["/community/styles/transfans/theme/images/prime.png", "/community/styles/transfans/theme/images/m.png", "/community/styles/transfans/theme/images/g.png", "/community/styles/transfans/theme/images/r.png"];
                var u = v[Math.floor(Math.random() * 4)];
                $("<img/>", {
                    src: u,
                    "class": "Prime",
                    style: "display: none;"
                }).click(function() {
                    window.location.href = "/"
                }).appendTo($(".masthead"));
                setTimeout(function() {
                    $(".Prime").fadeIn(250)
                }, 250)
            }
            if (typeof w === "function") {
                w()
            }
        }
        d.increaseContrast = function(u) {
            r();
            c(h, u)
        };
        d.decreaseContrast = function(u) {
            r();
            c(a, u)
        };

        function r() {
            $("*").each(function() {
                $(this).removeAttr("style")
            })
        }

        function h(u, w) {
            var v = u + w;
            if (v >= 255) {
                return 255
            }
            return v
        }

        function a(u, w) {
            var v = u - w;
            if (v <= 0) {
                return 0
            }
            return v
        }

        function l(z, u, x) {
            var v = z.split(" ");
            if (v.length > 3 && v[3] === "0") {
                return "transparent"
            }
            for (var w = 0; w < v.length; w++) {
                var y = parseInt(v[w]);
                y = u(y, x);
                v[w] = ("00" + y.toString(16)).slice(-2)
            }
            if (v.length > 3) {
                v.splice(3, 1)
            }
            z = v.join("");
            return "#" + z
        }

        function c(u, v) {
            $("*").each(function() {
                var x = $(this).css("color");
                var w = $(this).css("background-color");
                x = x.replace(/[rgba\(\)\,]/g, "");
                w = w.replace(/[rgba\(\)\,]/g, "");
                $(this).attr("style", "");
                if (x) {
                    x = l(x, u, v);
                    $(this).attr("style", "color: " + x + ";")
                }
                if (w) {
                    w = l(w, u, v);
                    $(this).attr("style", $(this).attr("style") + " background-color: " + w + ";")
                }
            })
        }

        function g(u) {
            if (u) {
                $.cookie("dock-nav", "true");
                $("div.navbar:not(.navbar-bottom)").removeClass("navbar-fixed-top");
                $("img.Prime").addClass("hide")
            } else {
                $.cookie("dock-nav", "false");
                $("div.navbar:not(.navbar-bottom)").addClass("navbar-fixed-top");
                $("img.Prime").removeClass("hide")
            }
        }
    };

    $(document).ready(function() {
        var a = new siteBehaviour();
        a.initialise()
    });
})(jQuery);