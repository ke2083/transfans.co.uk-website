# TransFans.co.uk Website

## Getting started

 1. Install VirtualBox
 2. Install Vagrant
 3. On your command line (depending on your system), install:
   * ```vagrant plugin install vagrant-vbguest```
 4. Run ```vagrant up```
 5. Run ```vagrant ssh``` and then ```npm run start:dev```to start developing
