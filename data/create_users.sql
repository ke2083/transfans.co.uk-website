DROP USER 'transfans'@'localhost';
CREATE USER 'transfans'@'localhost' IDENTIFIED BY 'Q4ZtIwrh';
DROP USER 'transfans_tf'@'localhost';
CREATE USER 'transfans_tf'@'localhost' IDENTIFIED BY 'Q4ZtIwrh';
GRANT ALL ON transfans_cms.* TO 'transfans'@'localhost';
GRANT ALL ON transfans_cms.* TO 'transfans_tf'@'localhost';
GRANT ALL ON transfans_phpbb.* TO 'transfans'@'localhost';
GRANT ALL ON transfans_phpbb.* TO 'transfans_tf'@'localhost';
