echo "⚙     Configuring..."

cd /vagrant
yum install deltarpm -y -q
yum update -y -q 
yum install epel-release -y -q
yum install httpd php php-mysql php-pdo php-gd php-mbstring mariadb-server mariadb -y -q

echo "Starting services..."
systemctl disable firewalld
systemctl stop firewalld
systemctl start httpd
systemctl start mariadb

echo "Installing databases..."
mysql -u root mysql < /vagrant/data/transfans_cms.sql
mysql -u root mysql < /vagrant/data/transfans_phpbb.sql
mysql -u root mysql < /vagrant/data/create_users.sql

mv /etc/httpd/conf.d/welcome.conf /etc/httpd/conf.d/welcome.old
mv /vagrant/build/httpd.conf /etc/httpd/conf
chmod -R 755 /vagrant/src
chown -R apache:apache /vagrant/src
setenforce 0
systemctl restart httpd

# Move the user to the correct start folder
echo "cd /vagrant" >> /home/vagrant/.bashrc
echo "----------"
echo "✅     A4/4 ll ready.  Do 'vagrant ssh' to connect."
